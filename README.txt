#########################################
# Pyramid

To setup Pyramid to run on your machine:
    1. Make sure python 2.7 is installed on your machine.
    2. Make sure easy_install for python is installed.
    3. Run pyramid/batch_scripts/Pyramid_One_Time_Configure_Your_Machine.bat

To Start Pyramid:
    Run pyramid/batch_scripts/Pyramid_Development_Start_Personal.bat

If you change any python scripts for Pyramid, or if you change Javascript files, the server will automatically restart.

To stop Pyramid:
    1. Entering Ctrl-C in the running Pyramid shell window, and type "Y" to terminate the batch also.

This starts a Web Server on http://localhost:8001/
