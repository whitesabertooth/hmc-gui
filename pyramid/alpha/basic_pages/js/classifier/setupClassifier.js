//Written by DWS
function setupClassifier()
{
	//Variable setup
    var lastfileid;
    var CountVectorizerValid = true;
    var SGDClassifierValid = true;
    var BasicFormValid = false;
    var CrossValidationValid = true;
	var files = new BigFileUpload({'filetypes':/(\.|\/)(csv|xlsx)$/i});
    var zipfiles = new BigFileUpload({'filetypes':/(\.|\/)(zip)$/i,'fileupload':'#zipupload','progress':'#zipprogress'});
    var lastfileid;
    var DUMMY_VALUE = "null";
    var statusmasteruid = 0;
    var prevUID = "";
    var trainingResults = {};
    var DefaultCountVectorizerData;
    var DefaultSGDClassifierData;
    var DefaultTfidfTransformerData;
    var DefaultCrossValidationData;
    var prevclassfileid = null;
    var jobs = new GetPredJobs();

    //At the completion of the file upload
    files.completefcn = function(fileid,filelink,filename) 
    {
        lastfileid = fileid;
        var fileselected = filename.name;
        $('#fileselected').html(fileselected);

        if(fileselected.match(/.csv$/))
        {
            getHeaderCSV(filename,setupHeader);
        } else {
    	   getHeaderXLSX(fileid,setupHeader);
        }
    }
    files.failfcn = function()
    {
        OpenModalBox('Upload Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
    };

    //At the completion of the file upload
    zipfiles.completefcn = function(fileid,filelink,filename) 
    {
       getClassResult(fileid);
    }
    zipfiles.failfcn = function()
    {
        OpenModalBox('Upload Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
    };

    //Call setup
	$(document).ready(setup);

	//Functions
	function setup()
	{
    	$('#headerinput').hide();
    	$('#submitTraining').hide();
    	$('#submitTraining').on('click',function() {
    		submitTraining();
    	});
        $('#chgfile').on('click',function() {
            $('#fileinput').show();
            $('#chgfile').hide();
        });
        CountVectorizerFormValidator();
        SGDClassifierFormValidator();
        BasicFormValidator();
        CrossValidationFormValidator();

        //Save the initial default of all fields
        DefaultCountVectorizerData = getCountVectorizerData();
        DefaultSGDClassifierData = getSGDClassifierData();
        DefaultTfidfTransformerData = getTfidfTransformerData();
        DefaultCrossValidationData = getCrossValidationData();

        $('#resetdefault').on('click',function(){
            //Reset the configuration to defaults after a pop-up verification
            var msg = "Are you sure you want to reset all classifier configurations to the default?";
            var output = "<button class='btn btn-primary' id='yesacceptclass'>Yes</button>";
            output += "<button class='btn btn-primary' id='noacceptclass'>No</button>";
            OpenModalBox('Reset All Configuration',msg,output);
            $('#noacceptclass').on('click',function() {
                CloseModalBox();
            });
            $('#yesacceptclass').on('click',function() {
                resetAllConfiguration();
                CloseModalBox();
            });

        });
        $('#resetdefaultCount').on('click',function(){
            //Reset the configuration to defaults after a pop-up verification
            var msg = "Are you sure you want to reset Count Vectorizer configurations to the default?";
            var output = "<button class='btn btn-primary' id='yesacceptclass'>Yes</button>";
            output += "<button class='btn btn-primary' id='noacceptclass'>No</button>";
            OpenModalBox('Reset Count Vectorizer Configuration',msg,output);
            $('#noacceptclass').on('click',function() {
                CloseModalBox();
            });
            $('#yesacceptclass').on('click',function() {
                resetCountVectorizer();
                CloseModalBox();
            });

        });
        $('#resetdefaultTfidf').on('click',function(){
            //Reset the configuration to defaults after a pop-up verification
            var msg = "Are you sure you want to reset Tfidf Transformer configurations to the default?";
            var output = "<button class='btn btn-primary' id='yesacceptclass'>Yes</button>";
            output += "<button class='btn btn-primary' id='noacceptclass'>No</button>";
            OpenModalBox('Reset Tfidf Transformer Configuration',msg,output);
            $('#noacceptclass').on('click',function() {
                CloseModalBox();
            });
            $('#yesacceptclass').on('click',function() {
                resetTfidfTransformer();
                CloseModalBox();
            });

        });
        $('#resetdefaultSGD').on('click',function(){
            //Reset the configuration to defaults after a pop-up verification
            var msg = "Are you sure you want to reset SGD Classifier configurations to the default?";
            var output = "<button class='btn btn-primary' id='yesacceptclass'>Yes</button>";
            output += "<button class='btn btn-primary' id='noacceptclass'>No</button>";
            OpenModalBox('Reset SGD Classifier Configuration',msg,output);
            $('#noacceptclass').on('click',function() {
                CloseModalBox();
            });
            $('#yesacceptclass').on('click',function() {
                resetSGDClassifier();
                CloseModalBox();
            });

        });
        $('#resetdefaultCrossValidation').on('click',function(){
            //Reset the configuration to defaults after a pop-up verification
            var msg = "Are you sure you want to reset Cross Validation configurations to the default?";
            var output = "<button class='btn btn-primary' id='yesacceptclass'>Yes</button>";
            output += "<button class='btn btn-primary' id='noacceptclass'>No</button>";
            OpenModalBox('Reset Cross Validation Configuration',msg,output);
            $('#noacceptclass').on('click',function() {
                CloseModalBox();
            });
            $('#yesacceptclass').on('click',function() {
                resetCrossValidation();
                CloseModalBox();
            });

        });
	}

    function plotMatrix(title,cm,runningUID)
    {
        //This is used to plot Confusion Matrix as a HeatMap
        var data = [
          {
            z: cm.matrix,//[[1, 20, 30, 50, 1], [20, 1, 60, 80, 30], [30, 60, 1, -10, 20]],
            x: cm.labels,//['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
            y: cm.labels,//['Morning', 'Afternoon', 'Evening'],
            type: 'heatmap'
          }
        ];
        var layout = {
          title: title,
          xaxis: {
            title: 'Predicted',
            titlefont: {
              family: 'Courier New, monospace',
              size: 18,
              color: '#7f7f7f'
            }
          },
          yaxis: {
            title: 'Truth',
            titlefont: {
              family: 'Courier New, monospace',
              size: 18,
              color: '#7f7f7f'
            }
          }
        };
        Plotly.newPlot('matrix'+runningUID, data,layout);
    }


    function plotCurve(title,learn,runningUID)
    {
        //This is used to plot Learning Curve as line graph
        //learn: {train_sizes,train_scores,valid_scores}
        var trace1 = {
          x: learn.train_sizes,
          y: learn.train_scores,
          name: 'Training score',
          type: 'scatter'
        };

        var trace2 = {
          x: learn.train_sizes,
          y: learn.valid_scores,
          name: "Cross-Validation score",
          type: 'scatter'
        };


        var trace3 = {
          x: learn.train_sizes,
          y: learn.test_scores,
          name: "Test score",
          type: 'scatter'
        };

        var data = [trace1, trace3, trace2];

        var layout = {
          title: title,
          xaxis: {
            title: 'Percent reserved for Test',
            titlefont: {
              family: 'Courier New, monospace',
              size: 18,
              color: '#7f7f7f'
            }
          },
          yaxis: {
            title: 'Score',
            titlefont: {
              family: 'Courier New, monospace',
              size: 18,
              color: '#7f7f7f'
            }
          },
          showlegend:false,
          legend:{
                x:0.9,
                y:1
            }
        };
        Plotly.newPlot('lcurve'+runningUID, data,layout);
    }

    function resetAllConfiguration()
    {
        resetCountVectorizer();        
        resetTfidfTransformer();        
        resetSGDClassifier();        
        resetCrossValidation();
        $('#moretrainingckbox').val(false);//Default to not using
    }

    function resetCountVectorizer()
    {
        setCountVectorizerData(DefaultCountVectorizerData);
        $('#CountVectorizer').bootstrapValidator('validate');
    }

    function resetTfidfTransformer()
    {
        setTfidfTransformerData(DefaultTfidfTransformerData);
    }

    function resetSGDClassifier()
    {
        setSGDClassifierData(DefaultSGDClassifierData);
        $('#SGDClassifier').bootstrapValidator('validate');
    }

    function resetCrossValidation()
    {
        setCrossValidationData(DefaultCrossValidationData);
        setTimeout(function(){$('#CrossValidation').bootstrapValidator('validate');},100);
    }
    // function submitLongJob()
    // {
    //     //Successfully uploaded the file, so now submit it with the ID to long job
    //     var descript = $('#description').val();
    //     var requestLong = {'job':2,'description':descript,'inputfileid':fileid};

    //     //Callback from success of AJAX call
    //     function processSubmitLong(response) {
    //         //{"success":'true', "totalcount" : totalcount, "input": input};
    //         console.log(response);
    //         if(response['result'] == true)
    //         {
    //             OpenModalBox('Successful Submition', 'Succesfully submitted your job.  Use the <b>Get Jobs Button</b> below to see status and get results.');
    //         } else {
    //             OpenModalBox('Submit Error', 'Error during submition on server.  Try again later or contact HMC Tech Group.');
    //         }
    //     }
    //     //Called when Ajax fails
    //     function processSubmitLongError() {
    //         OpenModalBox('Submit Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
    //     }
    //     //Cycle through steps until result total is 0
    //     function ajaxSubmitLongProcess() {

    //         ajaxQuery = $.ajax({
    //             url: scriptLongLocation,
    //             type: "post",
    //             data: JSON.stringify(requestLong),
    //             dataType: "json",
    //             contentType: 'application/json; charset=utf-8',
    //             success: processSubmitLong,
    //             error: processSubmitLongError,
    //             timeout: 1*60*1000 //1 minutes
    //         });
    //     }

    //     ajaxSubmitLongProcess();

    // }


    function addResultBox(name,uid)
    {
        var boxhtml = '<div class="col-md-5">'+
                        '<div class="box" id="box'+uid+'">'+
                            '<div class="box-header">'+
                                '<div class="box-name">'+
                                    '<span>Training Results: ' + name + '&nbsp;<span id="cvresult'+uid+'"></span></span>'+
                                '</div>'+
                                '<div class="box-icons">'+
                                    '<a class="collapse-link">'+
                                        '<i class="fa fa-chevron-up"></i>'+
                                    '</a>'+
                                    '<a class="close-link">'+
                                        '<i class="fa fa-times"></i>'+
                                    '</a>'+
                                '</div>'+
                            '</div>'+
                            '<div class="box-content">'+
                                '<div id="response'+uid+'">'+
                                '</div>'+
                                '<div class="box" style="display: none;" id="predictor'+uid+'">'+
                                    '<div class="box-content">'+
                                        '<h4>Predict Classification of one input:</h4>'+
                                        '<div class="row">'+
                                            '<div class="col-xs-12">'+
                                                "<textarea style='resize:none; max-height:300px; min-height:50px;' class='form-control' id='classtext"+uid+"' placeholder='e.g.  I love to smoke!'/>"+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="row" style="padding-top: 5px;">'+
                                            '<div class="col-sm-2">'+
                                                "<button class='btn btn-primary' id='btnpredict"+uid+"'>Predict</button>"+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="row" style="padding-top: 8px;">'+
                                            '<div class="col-xs-12">'+
                                                '<table id="predhist'+uid+'" class="table table-hover table-heading table-bordered">'+
                                                    '<thead></thead>'+
                                                    '<tbody></tbody>'+
                                                '</table>'+
                                            '</div>'+
                                        '</div>'+
                                        '<br><h4>Predict Classification of file:</h4>'+
                                        '<div class="row form-group">'+
                                            '<div class="col-sm-3">'+
                                                '<span class="btn btn-success fileinput-button">'+
                                                    '<span>Select file...</span>'+
                                                    '<!-- The file input field used as target for the file upload widget -->'+
                                                    '<input id="predupload'+uid+'" accept=".csv,.xlsx" type="file" name="files[]"/>'+
                                                '</span>'+
                                            '</div>'+
                                            '<div class="col-sm-6">'+
                                                '<!-- The global progress bar -->'+
                                                '<div id="predprogress'+uid+'" class="progress">'+
                                                    '<div class="progress-bar progress-bar-success"></div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="row">'+
                                            '<div class="col-xs-12 form-group">'+
                                                '<label class="control-label">Text Column</label>'+
                                                '<select width="100%" id="textcolumn'+uid+'" name="textcolumn">'+
                                                '</select>'+
                                            '</div>'+
                                            '<div class="col-xs-12 form-group">'+
                                                '<label class="control-label">New Label Column</label>'+
                                                '<input width="100%" id="labelcolumn'+uid+'" name="labelcolumn">'+
                                                '</input>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="row" style="padding-top: 5px;">'+
                                            '<div class="col-sm-3">'+
                                                "<button class='btn btn-primary' id='btnpredictfile"+uid+"'>Predict File</button>"+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div id="pause'+uid+'">'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
        $('#statusmaster').prepend(boxhtml);//Add to beginning
        AddMove("#box"+uid);//Add moving capability to new boxes only
    }

    function showPreviousResult(response)
    {
        var runningUID = getNextUID();
        var name = response.config.info.name + "&nbsp;#&nbsp;"+runningUID;//(new Date()).toLocaleTimeString()
        addResultBox(name,runningUID);

        //Merge the result and config, just like how received from initial classification
        dataresult = response.result;
        dataresult.config = response.config;

        showTrainingResults(dataresult,runningUID);
        $('#prevclassbtn').click();//Hides the load Classifier section
    }

    function getClassResult(fileid)
    {
        //Called after data is uploaded
        //Call again to get config and results of .zip file
        var request = {'fileid':fileid};
        var scriptResultLocation = getUrl() + '/db/getClassResult.json';
        var ajaxQuery;

        //Callback from success of AJAX call
        function processGetResult(response) {
            console.log(response);
            if(response['status'] == 'success')
            {
                //Successfully got header
                showPreviousResult(response['result'])
            } else {
                OpenModalBox('Submit Error', 'Error during processing.  Message: ' + response['msg']);
            }
        }
        //Called when Ajax fails
        function processGetResultError() {
            OpenModalBox('Submit Error', 'Could not connect to server or taking too long.  Try again later or contact HMC Tech Group.');
        }
        //Cycle through steps until result total is 0
        function ajaxGetResultProcess() {

            ajaxQuery = $.ajax({
                url: scriptResultLocation,
                type: "post",
                data: JSON.stringify(request),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: processGetResult,
                error: processGetResultError,
                timeout: 1*60*1000 //1 minutes
            });
        }

        ajaxGetResultProcess();

    }

    function showTrainingResults(data,runningUID)
    {
    	// data = 
    	// 'cv': {
	    //        'avg'
	    //        'std'
	    //        'all'
	    //    }
	    //    'report'
	    //    'outfileid'
        //    'config'
        //save results
        trainingResults[runningUID] = data;

	    var output = "";
        var title = "";
        if(data.config.test.testpercent > 1)
        {
            //Learning Curve
            output = "<div style='width: 100%;' id='lcurve"+runningUID+"'></div>"+
                "<button class='btn btn-default' id='loadconfig"+runningUID+"'>Load Config</button>"+
                "<br><button class='btn btn-default' id='showconfig"+runningUID+"'>Show Config</button><pre id='allconfig"+runningUID+"' style='display: none;'>"+JSON.stringify(data.config,null,2)+"</pre><br><br>"
                ;
            title = "(Learning Curve)";
        } else {
            //Normal training
            var cvavg = (Math.round(data.cv.avg*100*100)/100)+"%";
    	    output = "<h4>Cross-Validation " + data.config.classifier.CrossValidation.scoring+" Results:</h4>"+
    	    	"<h5>Averge = "+cvavg+"<br/>Standard Deviation = "+(Math.round(data.cv.std*100*100)/100)+"%</h5><br>"+
    	    	"<button class='btn btn-default' id='showcv"+runningUID+"'>Show All CV&nbsp;<i class='fa fa-chevron-down'></i></button>&nbsp;<span id='allcv"+runningUID+"' style='display: none;'>"+data.cv.all+"</span><br>"+
    	    	"<h4>Testing Results:</h4><br>"+
    	    	"<pre>"+data.report+"</pre>"+
                "<button class='btn btn-default' id='loadconfig"+runningUID+"'>Load Config</button>"+
                "<button class='btn btn-default' id='showconfig"+runningUID+"'>Show Config&nbsp;<i class='fa fa-chevron-down'></i></button><pre id='allconfig"+runningUID+"' style='display: none;'>"+JSON.stringify(data.config,null,2)+"</pre><br><br>"+
                "<button class='btn btn-default' id='showmatrix"+runningUID+"'>Show Confusion Matrix&nbsp;<i class='fa fa-chevron-down'></i></button><br><div id='matrix"+runningUID+"' style='width: 100%;'></div>"+
                "<a href='/db/getfile?fileid="+data.outfileid+"'>Download Classifier</a>"+
                "<br><button class='btn btn-default' id='showpredict"+runningUID+"'>Show Prediction Box&nbsp;<i class='fa fa-chevron-down'></i></button>"+
                "";
            //button to load config: with are-you-sure popup, using trainingResults[runningUID].config
            title = "("+data.config.classifier.CrossValidation.scoring + "="+cvavg+")";
        }

        //Show the data
        $('#cvresult'+runningUID).html(title);
	    $('#response'+runningUID).html(output);

        //Add event listeners
        if(data.config.test.testpercent > 1)
        {
            //Learning curve
            plotCurve(data.config.info.name + " #"+runningUID+" Learning Curve",data.learn,runningUID);
        } else {
            var predfiles = new BigFileUpload({'filetypes':/(\.|\/)(csv|xlsx)$/i,'fileupload':'#predupload'+runningUID,'progress':'#predprogress'+runningUID});
            var loadedfileid = 0;
            //Normal training
            plotMatrix(data.config.info.name + " #"+runningUID+" Confusion Matrix",data.cm,runningUID);//Add confusion matrix
            setTimeout(function(){$('#matrix'+runningUID).hide();},100);//wait so the ploting can happen before hiding (so 100% width is accounted for)
    	    $('#showcv'+runningUID).on('click',function() {
    	    	//Toggle showing vs hidden cv all
    	    	if( $('#allcv'+runningUID+':visible').length > 0 )
    		    {
    		    	$('#allcv'+runningUID+':visible').slideUp(400);
    		    	$('#showcv'+runningUID).html("Show All CV&nbsp;<i class='fa fa-chevron-down'></i>");
    		    } else {
    		    	$('#allcv'+runningUID+':hidden').slideDown(400);
    		    	$('#showcv'+runningUID).html("Hide All CV&nbsp;<i class='fa fa-chevron-up'></i>");
    		    }
    	    });
            $('#showmatrix'+runningUID).on('click',function() {
                //Toggle showing vs hidden cv all
                if( $('#matrix'+runningUID+':visible').length > 0 )
                {
                    $('#matrix'+runningUID+':visible').slideUp(400);
                    $('#showmatrix'+runningUID).html("Show Confusion Matrix&nbsp;<i class='fa fa-chevron-down'></i>");
                } else {
                    $('#matrix'+runningUID+':hidden').slideDown(400);
                    $('#showmatrix'+runningUID).html("Hide Confusion Matrix&nbsp;<i class='fa fa-chevron-up'></i>");
                }
            });
            //setup classifier prediction:
            var predhist = new SimpleTable({'tableid':'predhist'+runningUID,'columns':['Text','Prediction']});
            $('#showpredict'+runningUID).on('click',function() {
                //Toggle showing vs hidden cv all
                if( $('#predictor'+runningUID+':visible').length > 0 )
                {
                    $('#predictor'+runningUID+':visible').slideUp(400);
                    $('#showpredict'+runningUID).html("Show Prediction Box&nbsp;<i class='fa fa-chevron-down'></i>");
                } else {
                    $('#predictor'+runningUID+':hidden').slideDown(400);
                    $('#showpredict'+runningUID).html("Hide Prediction Box&nbsp;<i class='fa fa-chevron-up'></i>");
                }
            });
            var processingprediction = false;
            $('#btnpredict'+runningUID).on('click',function() {
                //Predict the text
                //Do not submit if already processing one, or empty text
                var classtext = $('#classtext'+runningUID).val();

                if(classtext.length == 0) {return;}

                //Lock processing
                $('#btnpredict'+runningUID).prop("disabled",true);

                submitSinglePredictor(classtext,data.outfileid,runningUID,predhist,function() {
                    $('#btnpredict'+runningUID).prop("disabled",false);                    
                });
            });
            //Setup auto resizing of textarea
            function resizeTextarea(e) {
              $(e).css({'height':'auto'}).height(e.scrollHeight);
            }
            $('#classtext'+runningUID).each(function () {
                resizeTextarea(this);
            }).on('input', function () {
                resizeTextarea(this);
            });

            $('#btnpredictfile'+runningUID).on('click',submitCurrentPredictor);

            function showPredictorSelection(headlist)
            {
                var output = [];
                output.push('<option value="'+ DUMMY_VALUE +'">-SELECT-</option>');
                var bodyidx = DUMMY_VALUE;//default to dummy
                $.each(headlist, function(idx, value)
                {
                    if(value.toLowerCase() == 'bodypost')
                    {
                        bodyidx = value;
                    }
                    output.push('<option value="'+ value +'">'+ value +'</option>');
                });

                $('#textcolumn'+runningUID).html(output.join(''));
                $('#textcolumn'+runningUID).val(bodyidx);
            }


            function submitCurrentPredictor(finishfcn)
            {
                var failed = false;
                var msg = "";
                var textcolumn = $('#textcolumn'+runningUID).val();
                var labelcolumn = $('#labelcolumn'+runningUID).val();
                //Verify data
                if(loadedfileid == 0)
                {
                    failed = true;
                    msg += "Have not loaded a file.<br>";
                }
                if(textcolumn == DUMMY_VALUE)
                {
                    failed = true;
                    msg += "Value of text column needs to be selected.<br>";
                }
                if(labelcolumn.length == 0)
                {
                    failed = true;
                    msg += "Value of label column needs to be set.<br>";
                }
                if(failed)
                {
                    OpenModalBox("Submit Prediction Missing Information",msg);
                    return;
                }
                $('#btnpredict'+runningUID).prop("disabled",true);
                //JENKINS:var submitdata = {'job':4,'db':'twitter','fileid':loadedfileid,'columntext':textcolumn,'columnlabel':labelcolumn,'prevfileid':data.outfileid};
                //JENKINS:submitPredictor(submitdata,runningUID,function() {
                //JENKINS:    $('#btnpredict'+runningUID).prop("disabled",false);                    
                //JENKINS:});
                var submitdata = {'fileid':loadedfileid,'columntext':textcolumn,'columnlabel':labelcolumn,'prevfileid':data.outfileid};
                submitPredictor(submitdata,runningUID,function() {
                    $('#btnpredict'+runningUID).prop("disabled",false);                    
                });
            }


            //At the completion of the file upload
            predfiles.completefcn = function(fileid,filelink,fileobj) 
            {
                loadedfileid = fileid;
                var fileselected = fileobj.name;
                if(fileselected.match(/.csv$/))
                {
                    getHeaderCSV(fileobj,showPredictorSelection);
                } else {
                   getHeaderXLSX(fileid,showPredictorSelection);
                }
            }
            predfiles.failfcn = function()
            {
                OpenModalBox('Upload Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
            };
        }

        //Do for both
        $('#showconfig'+runningUID).on('click',function() {
            //Toggle showing vs hidden cv all
            if( $('#allconfig'+runningUID+':visible').length > 0 )
            {
                $('#allconfig'+runningUID+':visible').slideUp(400);
                $('#showconfig'+runningUID).html("Show Config&nbsp;<i class='fa fa-chevron-down'></i>");
            } else {
                $('#allconfig'+runningUID+':hidden').slideDown(400);
                $('#showconfig'+runningUID).html("Hide Config&nbsp;<i class='fa fa-chevron-up'></i>");
            }
        });
        $('#loadconfig'+runningUID).on('click',function() {
            //pop-up asking to load config
            var msg = "Are you sure you want to load the configuration for #" + runningUID + "?";
            var output = "<button class='btn btn-primary' id='yesacceptconfig"+runningUID+"'>Yes</button>";
            output += "<button class='btn btn-primary' id='noacceptconfig"+runningUID+"'>No</button>";
            OpenModalBox('Load Configuration #' + runningUID,msg,output);
            $('#noacceptconfig'+runningUID).on('click',function() {
                CloseModalBox();
            });
            $('#yesacceptconfig'+runningUID).on('click',function() {
                updateConfig(runningUID);
                CloseModalBox();
            });

        });
    }

    function showSinglePredictor(result,predhist)
    {
        var predtext = result.classtext[0];
        var showtext = "<pre>"+result.config.text[0]+"</pre>";
        predhist.appendOne([showtext,predtext]);
    }


    function submitPredictor(request,runningUID,finishfcn)
    {
        //JENKINS:var scriptTrainingLocation = getUrl() + '/db/submitLongJob.json';
        var scriptTrainingLocation = getUrl() + '/db/quickpredict.json';
        var ajaxQuery;
        ajaxTrainingProcess();
        var loadedfileid = request['fileid'];

        //Callback from success of AJAX call
        function processTraining(response) {
            console.log(response);
            endPause(false);
            //JENKINS:if(response['result'])
            if(response['status']=='success')
            {
                //Successfully
                //JENKINS:displayError("Submit Successful", "Successfully submitted the job.  Please see the Get Jobs button below for status.");
                displayError("Successful Predicted", "Successfully predicted file.  <a href='/db/getfile?fileid="+loadedfileid+"'>Download Prediction</a>.");
                
            } else {
                displayError('Submit Error', 'Could not connect to server or taking too long.  Try again later or contact HMC Tech Group.');
            }
        }
        //Called when Ajax fails
        function processTrainingError() {
            endPause(false);
            displayError('Submit Error', 'Could not connect to server or taking too long.  Try again later or contact HMC Tech Group.');
        }
        //Cycle through steps until result total is 0
        function ajaxTrainingProcess() {
            console.log(request);
            startPause();
            ajaxQuery = $.ajax({
                url: scriptTrainingLocation,
                type: "post",
                data: JSON.stringify(request),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: processTraining,
                error: processTrainingError,
                //JENKINS:timeout: 1*60*1000 //1 minutes
                timeout: 5*60*1000 //5 minutes
            });
        }

        //Freeze the window, put button to cancel
        function startPause()
        {
            $('#pause'+runningUID).html('<span><h4>Processing request</h4> <button type="button" id="endDBquery'+runningUID+'" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
            $('#endDBquery'+runningUID).on('click', function() {endPause(true);});
        }

        //Unfreeze window, remove button
        function endPause(isCancel)
        {
            //Check if want to cancel the AJAX too
            if(isCancel)
            {
                ajaxQuery.abort();
            }
            $('#pause'+runningUID).html('');
            finishfcn();//Call this at the completion always
        }

        function displayError(head,msg)
        {
            var output = "<h4>"+head+"</h4>";
            output+="<br><h5>"+msg+"</h5>";
            $('#pause'+runningUID).html(output)
        }
    }


    function submitSinglePredictor(classtext,fileid,runningUID,predhist,finishfcn)
    {
        var scriptTrainingLocation = getUrl() + '/db/predictor.json';
        var ajaxQuery;
        var request = {'config':{'info':{'type':'text'},'file':{'prevfileid':fileid},'text':[classtext]}};
        ajaxTrainingProcess();

        //Callback from success of AJAX call
        function processTraining(response) {
            console.log(response);
            endPause(false);
            if(response['status'] == 'success')
            {
                //Successfully
                showSinglePredictor(response['result'],predhist);
            } else {
                displayError('Submit Error', 'Error during processing.  Message: ' + response['msg']);
            }
        }
        //Called when Ajax fails
        function processTrainingError() {
            endPause(false);
            displayError('Submit Error', 'Could not connect to server or taking too long.  Try again later or contact HMC Tech Group.');
        }
        //Cycle through steps until result total is 0
        function ajaxTrainingProcess() {
            console.log(request);
            startPause();
            ajaxQuery = $.ajax({
                url: scriptTrainingLocation,
                type: "post",
                data: JSON.stringify(request),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: processTraining,
                error: processTrainingError,
                timeout: 5*60*1000 //5 minutes
            });
        }

        //Freeze the window, put button to cancel
        function startPause()
        {
            $('#pause'+runningUID).html('<span><h4>Processing request</h4> <button type="button" id="endDBquery'+runningUID+'" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
            $('#endDBquery'+runningUID).on('click', function() {endPause(true);});
        }

        //Unfreeze window, remove button
        function endPause(isCancel)
        {
            //Check if want to cancel the AJAX too
            if(isCancel)
            {
                ajaxQuery.abort();
            }
            $('#pause'+runningUID).html('');
            finishfcn();//Call this at the completion always
        }

        function displayError(head,msg)
        {
            var output = "<h4>"+head+"</h4>";
            output+="<br><h5>"+msg+"</h5>";
            $('#pause'+runningUID).html(output)
        }
    }

    function getNextUID()
    {
        statusmasteruid++;
        return ''+statusmasteruid;
    }

    function updateConfig(runningUID)
    {
        //This load a previous run configuration
        prevclassfileid = trainingResults[runningUID].outfileid;//save currently loaded classifier fileid
        var config = trainingResults[runningUID].config;
        prevUID = runningUID;//save previous
        $('#moretraining').show();//Allow user to add more training
        $('#moretrainingckbox').val(false);//Default to not using

        setCountVectorizerData(config.classifier.CountVectorizer);
        setSGDClassifierData(config.classifier.SGDClassifier);
        setTfidfTransformerData(config.classifier.TfidfTransformer);
        setCrossValidationData(config.classifier.CrossValidation);

        //Change the filename, and show selected columns (FUTURE: allow load header columns to select different ones)
        // 'file': {
        //     'fileid':lastfileid,
        //     'columntext':txt,
        //     'columnlabel':lbl
        //     'name':origname (only on result)
        // },
        // 'info':{
        //     'description':des,
        //     'name':classname,
        //     'instructions':''
        // },
        // 'test': {
        //     'type':'specific',
        //     'testpercent':parseFloat(tp),
        //     'stratification':[]
        // }
        //Change filename


        //Update the columns to only the column selected
        setupHeader([config.file.columntext,config.file.columnlabel])
        $('#fileselected').val(config.file.name)
        lastfileid = config.file.fileid;
        $('#textcolumn').val(config.file.columntext);
        $('#labelcolumn').val(config.file.columnlabel);
        $('#classname').val(config.info.name);
        $('#description').val(config.info.description);
        var tp = config.test.testpercent.toString();
        $('#testpercent').val(tp);
        //revalidate forms to remove errors
        validateForms();
    }

    function validateForms()
    {
        //Form validation update:
        $('#CountVectorizer').bootstrapValidator('validate');
        $('#basicform').bootstrapValidator('validate');
        $('#SGDClassifier').bootstrapValidator('validate');
        $('#CrossValidation').bootstrapValidator('validate');
    }

    function submitTraining()
    {
	    function verifyTraining()
	    {
            validateForms();

	    	//Verify columntext and columnlabel are not the same
	    	//return true if good, or false if not good data
	    	var errormsg = "Errors:";
	    	var status = true;
			if(txt != DUMMY_VALUE && lbl == txt)
			{
				errormsg +='<br>Text Column cannot be the same as Label Column.';
				status = false;
			}
            //check options
            if(SGDClassifierValid == false)
            {
                errormsg +='<br>SGD Classifier has invalid options.';
                status = false;
            }
            if(CountVectorizerValid == false)
            {
                errormsg +='<br>Count Vectorizer has invalid options.';
                status = false;
            }
            if(BasicFormValid == false)
            {
                errormsg +='<br>Main form has invalid options.';
                status = false;
            }
            if(CrossValidationValid == false)
            {
                errormsg +='<br>Cross Validation form has invalid options.';
                status = false;
            }
			if(!status)
			{
				OpenModalBox('Form Error(s)', errormsg);	
			}
			return status;
	    }

    	var txt = $('#textcolumn').val();
    	var lbl = $('#labelcolumn').val();
		var classname = $('#classname').val();
		var des = $('#description').val();
		var tp = $('#testpercent').val();
        //Handle checking for previous classifier and if more training is selected
        var prevfileid = prevclassfileid;
        var ckboxprev = $('#moretrainingckbox').prop('checked');
        if(!ckboxprev) {prevfileid = null;}
        
        tp = parseFloat(tp);
		//Restrict classname to only good characters
		classname = classname.replace(/[\/:*?"<>|.]/g, "");

    	if(!verifyTraining() )
    	{
    		return;
    	}
        var CountVectorizerData = getCountVectorizerData();
        var SGDClassifierData = getSGDClassifierData();
        var TfidfTransformerData = getTfidfTransformerData();
        var CrossValidationData = getCrossValidationData();
    	var request = {
    		'config':{
                'file': {
                    'fileid':lastfileid,
                    'columntext':txt,
                    'columnlabel':lbl,
                    'prevfileid':prevfileid
                },
    			'classifier':{
                    'CountVectorizer': CountVectorizerData,                    
                    'SGDClassifier': SGDClassifierData,
                    'TfidfTransformer': TfidfTransformerData,
                    'CrossValidation': CrossValidationData
                },
    			'info':{
    				'description':des,
    				'name':classname,
    				'instructions':''
    			},
    			'test': {
    				'type':'specific',
    				'testpercent':tp,
    				'stratification':[]
    			}
    		}
    	};
        var runningUID = getNextUID();
        //Add prevUID if exists
        if(prevUID.length > 0)
        {
            runningUID = prevUID+"-"+runningUID;
        }
        var name = request.config.info.name + "&nbsp;#&nbsp;"+runningUID;//(new Date()).toLocaleTimeString()
        addResultBox(name,runningUID);

        var scriptTrainingLocation = getUrl() + '/db/classifier.json';
        var ajaxQuery;
        ajaxTrainingProcess();

        //Callback from success of AJAX call
        function processTraining(response) {
            console.log(response);
            endPause(false);
            if(response['status'] == 'success')
            {
            	//Successfully
            	showTrainingResults(response['result'],runningUID);
            } else {
                displayError('Submit Error', 'Error during processing.  Message: ' + response['msg']);
            }
        }
        //Called when Ajax fails
        function processTrainingError() {
            endPause(false);
            displayError('Submit Error', 'Could not connect to server or taking too long.  Try again later or contact HMC Tech Group.');
        }
        //Cycle through steps until result total is 0
        function ajaxTrainingProcess() {
        	console.log(request);
        	startPause();
            ajaxQuery = $.ajax({
                url: scriptTrainingLocation,
                type: "post",
                data: JSON.stringify(request),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: processTraining,
                error: processTrainingError,
                timeout: 5*60*1000 //5 minutes
            });
        }

	    //Freeze the window, put button to cancel
	    function startPause()
	    {
	        $('#pause'+runningUID).html('<span><h4>Processing request</h4> <button type="button" id="endDBquery'+runningUID+'" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
	        $('#endDBquery'+runningUID).on('click', function() {endPause(true);});
	    }

	    //Unfreeze window, remove button
	    function endPause(isCancel)
	    {
	        //Check if want to cancel the AJAX too
	        if(isCancel)
	        {
	            ajaxQuery.abort();
	        }
	        $('#pause'+runningUID).html('');
	    }

        function displayError(head,msg)
        {
            var output = "<h4>"+head+"</h4>";
            output+="<br><h5>"+msg+"</h5>";
            $('#response'+runningUID).html(output)
        }



    }

    function getCountVectorizerData()
    {

        var CountVectorizer = document.getElementById('CountVectorizer');
        var token_pattern = $(CountVectorizer.token_pattern).val();
        var max_features = $(CountVectorizer.max_features).val();
        var ngram_range = $(CountVectorizer.ngram_range).val();
        if(ngram_range == "") {ngram_range = null;} else {ngram_range = spliceNgramRange(ngram_range);}
        if(token_pattern == "") { token_pattern=null;}
        if(max_features == "") {max_features = null;} else {max_features = parseInt(max_features);}


        var CountVectorizerData = {
            'analyzer':$(CountVectorizer.analyzer).val(),
            'ngram_range':ngram_range,
            'stop_words':spliceStopWords($(CountVectorizer.stop_words).val()),
            'lowercase':($(CountVectorizer.lowercase).val() === 'true'),
            'token_pattern':token_pattern,
            'max_df':$(CountVectorizer.max_df).val(),//send as string - will be converted on other side
            'min_df':parseFloat($(CountVectorizer.min_df).val()),
            'max_features':max_features
        };
        return CountVectorizerData;
    }

    function setCountVectorizerData(config)
    {

        var CountVectorizer = document.getElementById('CountVectorizer');
        var token_pattern = config.token_pattern;
        var max_features = config.max_features;
        var ngram_range = config.ngram_range;
        if(ngram_range == null) {ngram_range = "";} else {ngram_range = mergeNgramRange(ngram_range);}
        if(token_pattern == null) { token_pattern="";}
        if(max_features == null) {max_features = "";} else {max_features = max_features.toString();}

        $(CountVectorizer.analyzer).val(config.analyzer);
        $(CountVectorizer.ngram_range).val(ngram_range);
        $(CountVectorizer.stop_words).val(mergeStopWords(config.stop_words));
        $(CountVectorizer.lowercase).val(config.lowercase.toString());
        $(CountVectorizer.token_pattern).val(token_pattern);
        $(CountVectorizer.max_df).val(config.max_df);//Is already a string
        $(CountVectorizer.min_df).val(config.min_df.toString());
        $(CountVectorizer.max_features).val(max_features);
    }

    function mergeStopWords(value)
    {
        if(value == 'english')
        {
            return value;
        }
        if(value == null)
        {
            return "";
        }
        return value.join(",")
    }

    function mergeNgramRange(value)
    {
        return value[0] + "-" + value[1];
    }


    function getTfidfTransformerData()
    {

        var TfidfTransformer = document.getElementById('TfidfTransformer');
        var norm = $(TfidfTransformer.norm).val();
        if(norm == DUMMY_VALUE) { norm=null;}

        var Data = {
            'norm':norm,
            'use_idf':($(TfidfTransformer.use_idf).val() === 'true'),
            'smooth_idf':($(TfidfTransformer.smooth_idf).val() === 'true'),
            'sublinear_tf':($(TfidfTransformer.sublinear_tf).val() === 'true')
        };
        return Data;
    }

    function setTfidfTransformerData(config)
    {

        var TfidfTransformer = document.getElementById('TfidfTransformer');
        var norm = config.norm;
        if(norm == null) { norm=DUMMY_VALUE;}

        $(TfidfTransformer.norm).val(norm);
        $(TfidfTransformer.use_idf).val(config.use_idf.toString());
        $(TfidfTransformer.smooth_idf).val(config.smooth_idf.toString());
        $(TfidfTransformer.sublinear_tf).val(config.sublinear_tf.toString());
    }

    function getSGDClassifierData()
    {

        var SGDClassifier = document.getElementById('SGDClassifier');
        var random_state = $(SGDClassifier.random_state).val();
        var class_weight = $(SGDClassifier.class_weight).val();
        var average = $(SGDClassifier.average).val();
        if(random_state == "") {random_state = null;} else {random_state = parseInt(random_state);}
        if(class_weight == DUMMY_VALUE) {class_weight = null;}
        if(average == "") {
            average = null;
        } else if(average == 'true' || average == 'false') {
            average = (average === 'true');
        } else {
            average = parseInt(average);
        }

        var Data = {
            'loss':$(SGDClassifier.loss).val(),
            'penalty':$(SGDClassifier.penalty).val(),
            'alpha':parseFloat($(SGDClassifier.alpha).val()),
            'l1_ratio':parseFloat($(SGDClassifier.l1_ratio).val()),
            'fit_intercept':($(SGDClassifier.fit_intercept).val() === 'true'),
            //NOT USED RIGHT NOW: 'n_iter':parseInt($(SGDClassifier.n_iter).val()),
            'shuffle':($(SGDClassifier.shuffle).val() === 'true'),
            'random_state':random_state,
            'eta0':parseFloat($(SGDClassifier.eta0).val()),
            'power_t':parseFloat($(SGDClassifier.power_t).val()),
            'class_weight':class_weight,
            'average':average
        };
        return Data;
    }

    function setSGDClassifierData(config)
    {

        var SGDClassifier = document.getElementById('SGDClassifier');
        var random_state = config.random_state;
        var class_weight = config.class_weight;
        var average = config.average;
        if(random_state == null) {random_state = "";} else {random_state = random_state.toString();}
        if(class_weight == null) {class_weight = DUMMY_VALUE;}
        if(average == null) {
            average = "";
        } else {
            average = average.toString();
        }

        $(SGDClassifier.loss).val(config.loss);
        $(SGDClassifier.penalty).val(config.penalty);
        $(SGDClassifier.alpha).val(config.alpha.toString());
        $(SGDClassifier.l1_ratio).val(config.l1_ratio.toString());
        $(SGDClassifier.fit_intercept).val(config.fit_intercept.toString());
        //NOT USED RIGHT NOW: 'n_iter':parseInt($(SGDClassifier.n_iter).val()),
        $(SGDClassifier.shuffle).val(config.shuffle.toString());
        $(SGDClassifier.random_state).val(random_state);
        $(SGDClassifier.eta0).val(config.eta0.toString());
        $(SGDClassifier.power_t).val(config.power_t.toString());
        $(SGDClassifier.class_weight).val(class_weight);
        $(SGDClassifier.average).val(average);
    }

    function getCrossValidationData()
    {

        var CrossValidation = document.getElementById('CrossValidation');

        var Data = {
            'cv':(parseInt($(CrossValidation.cv).val())),
            'scoring':($(CrossValidation.scoring).val()),
            'skiplarge':($(CrossValidation.skiplarge).val()),
        };
        return Data;
    }

    function setCrossValidationData(config)
    {

        var CrossValidation = document.getElementById('CrossValidation');

        $(CrossValidation.cv).val(config.cv.toString());
        $(CrossValidation.scoring).val(config.scoring);
        $(CrossValidation.skiplarge).val(config.skiplarge);
    }

    function getHeaderCSV(filename,resultfcn)
    {
        //Update the file being shown
        var columnheader = Papa.parse(filename,{
            // base config to use for each file
            header: false,
            preview: 1,
            complete: function(results, file) {
                resultfcn(results.data[0]);
            },
            error: function(err, file, inputElem, reason) {
                console.log(err);
            }
        });
    }

    function getHeaderXLSX(fileid,resultfcn)
    {
        //Successfully uploaded the file, so now submit it with the ID to long job
        lastfileid = fileid;
        var request = {'fileid':fileid};
        var scriptHeaderLocation = getUrl() + '/db/getHeader.json';
        var ajaxQuery;

        //Callback from success of AJAX call
        function processGetHeader(response) {
            console.log(response);
            if(response['status'] == 'success')
            {
                //Successfully got header
                resultfcn(response['header'])
            } else {
                OpenModalBox('Submit Error', 'Error during processing.  Message: ' + response['msg']);
            }
        }
        //Called when Ajax fails
        function processGetHeaderError() {
            OpenModalBox('Submit Error', 'Could not connect to server or taking too long.  Try again later or contact HMC Tech Group.');
        }
        //Cycle through steps until result total is 0
        function ajaxGetHeaderProcess() {

            ajaxQuery = $.ajax({
                url: scriptHeaderLocation,
                type: "post",
                data: JSON.stringify(request),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: processGetHeader,
                error: processGetHeaderError,
                timeout: 1*60*1000 //1 minutes
            });
        }

        ajaxGetHeaderProcess();

    }

    function setupHeader(headlist)
    {
    	//Display header, hide file upload.
    	$('#fileinput').hide();
    	$('#headerinput').show();
        $('#chgfile').show();
		$('#submitTraining').show();

        var output = [];
        if(headlist.length > 0)
        {
            output.push('<option value="'+ DUMMY_VALUE +'">-SELECT-</option>');
            var bodyidx = DUMMY_VALUE;//default to dummy
            $.each(headlist, function(idx, value)
            {
                if(value.toLowerCase() == 'bodypost')
                {
                    bodyidx = value;
                }
                output.push('<option value="'+ value +'">'+ value +'</option>');
            });

        	$('#textcolumn').html(output.join(''));
            $('#textcolumn').val(bodyidx);
        	$('#labelcolumn').html(output.join(''));
        }
    }

    function spliceNgramRange(value)
    {
        var vals = value.split(/-/);
        var min_n = parseInt(vals[0]);
        var max_n = parseInt(vals[1]);
        return [min_n,max_n];
    }
    function spliceStopWords(value)
    {
        var vals = value.split(/,/);
        var newvals = [];
        //Remove empty strings
        for(var i=0; i < vals.length; i++)
        {
            if(vals[i].length > 0)
            {
                newvals.push(vals[i]);
            }
        }
        if(newvals.length == 1)
        {
            if(newvals[0] == 'english')
            {
                newvals = newvals[0];
            }
        }
        //Default to english
        if(newvals.length == 0)
        {
            newvals = null;
        }
        return newvals;
    }

    function checkdf(value, validator) {
        // Check data
        if(value.length > 0 && !value.match(/^\d+$/) && !value.match(/^\d+\.\d+$/) )
        {
            return {
                valid: false,
                message: "Must be a float 0.0 to 1.0, or integer 1+"
            };
        }
        if(value.match(/\./))
        {
            var num = parseFloat(value);
            if(num > 1.0)
            {
                return {
                    valid: false,
                    message: "Must be a float 0.0 to 1.0, or integer 1+"
                };
            }
        } else {
            var num = parseInt(value);
            if(num == 0)
            {
                return {
                    valid: false,
                    message: "Must be a float 0.0 to 1.0, or integer 1+"
                };
            }
        }


        return true;
    }
    function checkn(value, validator) {
        // Check data
        if(value.length > 0 && !value.match(/^\d+$/) )
        {
            return {
                valid: false,
                message: "Must be a integer 1+"
            };
        }
        var num = parseInt(value);
        if(num == 0)
        {
            return {
                valid: false,
                message: "Must be a integer 1+"
            };
        }


        return true;
    }

    function checkaverage(value, validator) {
        // Check data
        if(value == 'true' || value == 'false')
        {
            return true;
        }
        if(value.length > 0 && !value.match(/^\d+$/) )
        {
            return {
                valid: false,
                message: "Must be true,false or a integer 1+"
            };
        }
        var num = parseInt(value);
        if(num == 0)
        {
            return {
                valid: false,
                message: "Must be true,false or a integer 1+"
            };
        }


        return true;
    }
    function checkratio(value, validator) {
        // Check data
        if(value.length > 0 && !value.match(/^\d+$/) && !value.match(/^\d+\.\d+$/) )
        {
            return {
                valid: false,
                message: "Must be a float 0.0 to 1.0"
            };
        }
        var num = parseFloat(value);
        if(num > 1.0)
        {
            return {
                valid: false,
                message: "Must be a float 0.0 to 1.0"
            };
        }


        return true;
    }


    function checkfloat(value, validator) {
        // Check data
        if(value.length > 0 && !value.match(/^\d+$/) && !value.match(/^\d+\.\d+$/) )
        {
            return {
                valid: false,
                message: "Must be a positive float"
            };
        }


        return true;
    }

    function checkpercent(value, validator) {
        // Check data
        if(value.length > 0 && !value.match(/^\d+$/) && !value.match(/^\d+\.\d+$/) )
        {
            return {
                valid: false,
                message: "Must be a float between 0-100"
            };
        }
        var num = parseFloat(value);
        if(num == 0.0 || num > 100.0)
        {
            return {
                valid: false,
                message: "Must be a float between 0-100"
            };
        }


        return true;
    }

    function checknull(value, validator) {
        // Check data
        if(value == DUMMY_VALUE)
        {
            return {
                valid: false,
                message: "Must select a column."
            };
        }

        return true;
    }
    
    //For information on how to use Validator:
    //https://github.com/nghuuphuoc/bootstrapvalidator
    function CountVectorizerFormValidator(){
        $('#CountVectorizer').bootstrapValidator({
            message: 'This value is not valid',
            excluded: [],//Do not exclude hidden items
            fields: {
                ngram_range: {
                    validators: {
                        callback: {
                            callback: function(value, validator) {
                                if(value.length == 0) {return true;}
                                // Check ngram data
                                if(!value.match(/\d+\-\d+/))
                                {
                                    return {
                                        valid: false,
                                        message: 'The field must be in the format: nnn-nnn'
                                    };
                                }
                                vals = spliceNgramRange(value);
                                var min_n = vals[0];
                                var max_n = vals[1];
                                if(min_n == 0)
                                {
                                    return {
                                        valid: false,
                                        message: 'The range cannot start with 0.'
                                    };
                                }
                                if(max_n == 0)
                                {
                                    return {
                                        valid: false,
                                        message: 'The range cannot end with 0.'
                                    };
                                }
                                if (min_n > max_n) {
                                    return {
                                        valid: false,
                                        message: 'The stop range has to be greater than or equal to the start.'
                                    }
                                }

                                return true;
                            }
                        }
                    }
                },
                stop_words: {
                    validators: {
                        callback: {
                            callback: function(value, validator) {
                                // Check ngram data
                                if(!value.match(/^[\w,]*$/))
                                {
                                    return {
                                        valid: false,
                                        message: 'The field must be in the format: www,www,www'
                                    };
                                }

                                return true;
                            }
                        }
                    }
                },
                max_df: {
                    validators: {
                        notEmpty: {
                            message: "Must not be empty.  Default is 1.0"
                        },
                        callback: {
                            callback: checkdf
                        }
                    }
                },
                min_df: {
                    validators : {
                        notEmpty: {
                            message: "Must not be empty.  Default is 1"
                        },
                        callback: {
                            callback: checkdf
                        }

                    }
                },
                max_features: {
                    validators : {
                        digits: {
                            message: "Must be a number."
                        }
                    }
                }
            }
        })
        .on('error.form.bv', function(e) {
            CountVectorizerValid = false;
        })
        .on('success.form.bv', function(e) {
            CountVectorizerValid = true;
        });

    }
   function SGDClassifierFormValidator(){
        $('#SGDClassifier').bootstrapValidator({
            message: 'This value is not valid',
            excluded: [],//Do not exclude hidden items
            fields: {
                alpha: {
                    validators: {
                        notEmpty: {
                            message: "Must not be empty.  Default is 0.0001"
                        },
                        callback: {
                            callback: checkfloat
                        }
                    }
                },
                l1_ratio: {
                    validators: {
                        notEmpty: {
                            message: "Must not be empty.  Default is 0.15"
                        },
                        callback: {
                            callback: checkratio
                        }
                    }
                },
                n_iter: {
                    validators: {
                        notEmpty: {
                            message: "Must not be empty.  Default is 5"
                        },
                        callback: {
                            callback: checkn
                        }
                    }
                },
                random_state: {
                    validators: {
                        callback: {
                            callback: checkn
                        }
                    }
                },
                eta0: {
                    validators: {
                        notEmpty: {
                            message: "Must not be empty.  Default is 0.0"
                        },
                        callback: {
                            callback: checkfloat
                        }
                    }
                },
                power_t: {
                    validators: {
                        notEmpty: {
                            message: "Must not be empty.  Default is 0.5"
                        },
                        callback: {
                            callback: checkfloat
                        }
                    }
                },
                average: {
                    validators: {
                        callback: {
                            callback: checkaverage
                        }
                    }
                }
            }
        })
        .on('error.form.bv', function(e) {
            SGDClassifierValid = false;
        })
        .on('success.form.bv', function(e) {
            SGDClassifierValid = true;
        });
    }
   function BasicFormValidator(){
        $('#basicform').bootstrapValidator({
            message: 'This value is not valid',
            excluded: [],//Do not exclude hidden items
            fields: {
                classname: {
                    validators: {
                        notEmpty: {
                            message: "Must not be empty."
                        },
                    }
                },
                testpercent: {
                    validators: {
                        notEmpty: {
                            message: "Must not be empty."
                        },
                        callback: {
                            callback: checkdf
                        }
                    }
                },
                textcolumn: {
                    validators: {
                        callback: {
                            callback: checknull
                        }
                    }
                },
                labelcolumn: {
                    validators: {
                        callback: {
                            callback: checknull
                        }
                    }
                },
            }
        })
        .on('error.form.bv', function(e) {
            BasicFormValid = false;
        })
        .on('success.form.bv', function(e) {
            BasicFormValid = true;
        });
    }

   function CrossValidationFormValidator(){
        $('#CrossValidation').bootstrapValidator({
            message: 'This value is not valid',
            excluded: [],//Do not exclude hidden items
            fields: {
                cv: {
                    validators: {
                        notEmpty: {
                            message: "Must not be empty."
                        },
                        callback: {
                            callback: checkn
                        }
                    }
                }
            }
        })
        .on('error.form.bv', function(e) {
            CrossValidationValid = false;
        })
        .on('success.form.bv', function(e) {
            CrossValidationValid = true;
        });
    }
}