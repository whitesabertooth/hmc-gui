//Written by DWS
function setupVisualize()
{
    
    var curdatalist;
    var alldate = 'alldate';
    var downloadcsv = [];
    var downloadlabelcsv = [];
    var datafilename = "";
    var fileselected = "";
    var headlist;
    var DUMMY_VALUE = "-SELECT-";
    var lastHadLabels = false;
    var lastHadTags = false;
    var MAX_FAILED_DATES = 100;    
    var gottags = false;
    var itags = {};
    var othererrors = "";


    var ruletag = new setupProcessDB(gotDB,{"rulehistory":false});
    FileDragDrop('filedrag','fileupload',{accept:/\.[cC][sS][vV]$/,addedFile:getHeader,errorFile:fileError,selectedclass:'fileselected'});

    //Call setup
    $(document).ready(setup);

    //Functions
    function setup()
    {
        $('.headerinput').hide();
        $('#submitVisualize').on('click',function() {
            calcVisualize();
        });
    }

    function fileError(file)
    {
        OpenModalBox('Error loading file',"Filename needs to end in .csv.");
    }


    function gotDB()
    {
        gottags = true;
        itags = invertdict(ruletag.DB_tags);
    }

    function forceOpen(id,button)
    {
        if($(id + ":visible").size() == 0)
        {
            $(button).click();
        }
    }

    function forceClose(id,button)
    {
        if($(id + ":visible").size() > 0)
        {
            $(button).click();
        }
    }

    function getHeader(filedata)
    {
        //Update the file being shown
        var columnheader = Papa.parse(filedata,
            {
                // base config to use for each file
                header: false,
                preview: 2,
                complete: function(results, file) {
                    fileselected = filedata;
                    showHeader(results.data[0],results.data[1]);
                },
                error: function() {
                    OpenModalBox("Error Reading File","Error reading the header row of file.");
                }
            }
        );
    }

    function calcVisualize()
    {
        lastHadLabels = false;
        lastHadTags = false;
        var status = true
        //var uid = $('#uidcolumn').find(':selected').text();
        var tagids = $( '#tagcolumn').find(":selected").map(function(){ return $(this).text(); }).get();
        var labels = $( '#labelcolumn').find(":selected").map(function(){ return $(this).text(); }).get();
        var tags = [];
        var errormsg = "";
        var analyzeSoftEOF = $('#badcharckbox').prop('checked');
        var analyzeIdpostLength = $('#idlengthckbox').prop('checked');
        var idpostname = $('#idpostcolumn').find(':selected').text();
        var analyzeIdpostLength = false;
        if(idpostname != DUMMY_VALUE)
        {
            analyzeIdpostLength = true;
        }
        var IDLENGTH = 20;
        if(tagids.length > 0)
        {
            //Make sure if one thing selected it is not he -SELECT- Value
            if(tagids.length > 1 || (tagids.length == 1 && tagids[0] != DUMMY_VALUE))
            {
                lastHadTags = true;
                for(var i=0; i<tagids.length; i++)
                {
                    if(tagids[i] == DUMMY_VALUE)
                    {
                        errormsg +='Tag Column cannot be the -SELECT- value.\n';
                        status = false;
                        break;
                    }
                    tags.push(tagids[i]);
                }

            }
        }
        if(labels.length > 0)
        {
            //Make sure if one thing selected it is not he -SELECT- Value
            if(labels.length > 1 || (labels.length == 1 && labels[0] != DUMMY_VALUE))
            {
                lastHadLabels = true;
                for(var i=0; i<labels.length; i++)
                {
                    if(labels[i] == DUMMY_VALUE)
                    {
                        errormsg +='Classification Column cannot be the -SELECT- value.\n';
                        status = false;
                        break;
                    }
                }
            }
        }
        if(!lastHadTags && !lastHadLabels)
        {
            errormsg +='Need to select something in the Classification Column and/or something in the Tag Column.\n';
            status = false;
        }
        var date = $('#datecolumn').find(':selected').text();
        var dateyear = $('#dateyearcolumn').find(':selected').text();
        var datemonth = $('#datemonthcolumn').find(':selected').text();
        var dateday = $('#datedaycolumn').find(':selected').text();
        var datesingle = true;
        if(date==DUMMY_VALUE)
        {
            datesingle = false;
            if(dateyear == DUMMY_VALUE || datemonth == DUMMY_VALUE || dateday == DUMMY_VALUE)
            {
                errormsg +='Either Single Date has to not be -SELECT- or all the Year/Month/Day has to not be -SELECT-.\n';
                status = false;
            }
        }
        if(!status)
        {
            OpenModalBox('Form Error(s)', errormsg);    
        } else {
            $('#submitVisualize').prop("disabled",true);
            $('.count-to-csv').hide();
            $('.heatbox').hide();
            $('.labelcounts').hide();
            $('.statusbox').show();
            forceClose('.selectcolumnbox','.selectcolumnclose');
            forceClose('.fileinputbox','.selectfileclose');

            var outdata = {alldate:{}};
            var labeloutdata = {alldate:{},labels:labels};
            var errorcnt = 0;
            var skipcount = 0;
            var baddates = [];
            var starttime = new Date();
            var COUNT_INCREMENT = 10000;
            var HIGHER_INCREMENT = 50000;
            var stepcount = 0;
            var outputstd = "<u>Started</u>";
            var output = "";
            var abortmsg = "";
            datafilename = fileselected.name;
            var results_errors = [];
            var badrowcol = [];
            var badidlength = [];
            var linesFileAnalyze = 50;//max number to sample for row size average
            var linesAnalyzed = 0;
            var estimatedrowlength = 0;
            var sma = simple_moving_averager(50);            

            var size = fileselected.size;
            // check file size
            var kbsize = Math.floor(size / 1024);
            var estimatednumrow = kbsize;
            //estimate 1 KB a line (works for our typical CSV)
            //Hide estimate if undefined
            output += "<span id='estimateall'>Estimated rows = <span id='estimaterow'>" + SimpleTable.prototype.formatNumber(kbsize) + " (based on file size, not actual)</span><br></span>";
            output += "Started CSV parsing at: " + starttime.toLocaleTimeString();
            $('#responseone').html(output);
            $('#response').html(outputstd);
            $('.filename').html(fileselected.name);
            var dataparse;
            var abortparse = false;
            $('#endquery').on('click',function() {
                //dataparse.abort();
                abortparse = true;
                abortmsg = " Manually aborted.";
            });
            $('#pause').show();
            Papa.parse(fileselected,
                {
                    // base config to use for each file
                    header: true,
                    //preview: 10,
                    worker: false,
                    step: function(results, parser) {
                        if(abortparse)
                        {
                            parser.abort();
                        }
                        dataparse = parser;
                        stepcount++;
                        if(stepcount % COUNT_INCREMENT == 0)
                        {
                            COUNT_INCREMENT = HIGHER_INCREMENT;
                            var newtime = new Date();
                            var delta = newtime - starttime;
                            var output = "<u>Processing</u>";
                            var rate = Math.floor(stepcount * 1000.0 / delta);
                            var ratebytes = Math.floor(results.meta.cursor * 1000.0 / delta);
                            output += "<br>Rows/second = " + rate;
                            output += "<br>MegaBytes/second = " + Math.floor(ratebytes/1024/1024*100)/100;//Preserve 2 decimal places of MB
                            var totalsec = Math.floor(delta / 1000);
                            var mina = Math.floor(totalsec / 60);//mins
                            var seca = totalsec - (mina*60);//left over seconds
                            output += "<br>Current duration mm:ss = " + mina + ":" + pad(seca,2);
                            //Calculate the estimated row length based on how many bytes of the file
                            var estimatedrowlength = results.meta.cursor / stepcount;
                            estimatednumrow = Math.floor(size/estimatedrowlength);

                            //The minimum left will remain at 1 second until done
                            var left = Math.max(size - results.meta.cursor,0);
                            var estimate = Math.max(Math.floor(left / ratebytes),1);//total seconds
                            var min = Math.floor(estimate / 60);//mins
                            var sec = estimate - (min*60);//left over seconds
                            output += "<br>Estimate left (mm:ss) = " + min + ":" + pad(sec,2);

                            //Calculate the percent, but hang at 99% if not done yet
                            var currentpercent = Math.min(99,Math.floor(results.meta.cursor / size * 100));
                            //Update progress bar
                            $('#thebar').css('width', currentpercent+'%').attr('aria-valuenow', currentpercent);
                            $('#textbar').html(currentpercent + "% Complete");
                            $('#estimaterow').html(SimpleTable.prototype.formatNumber(estimatednumrow) + " (based on sampling, not actual)");

                            //console.log(output);
                            $('#response').html(output);
                            $('#updatetime').html("Last updated: " + newtime.toLocaleTimeString());

                        }
                        if(results.errors.length > 0)
                        {
                            //Copy all new errors
                            for(var jj=errorcnt;jj<results.errors.length;jj++)
                            {
                                //Don't show row 0 errors (always seem to get at least one)
                                if(results.errors[errorcnt].row == 0)
                                {
                                    results.errors[jj].row = stepcount;

                                    if(results.data.length == 1 && curheadlist[1] in results.data[0] && results.data[0][curheadlist[1]].length == 0)
                                    {
                                        stepcount--;//Remove this row from the total
                                        continue;//Skip if just an empty line basically
                                    }
                                }
                                results_errors.push(results.errors[jj]);
                                errorcnt++;
                            }
                            console.log("Row errors:", results.errors);
                            console.log("Row meta:", results.meta);
                        } else {
                            //Verify the date
                            var rowdate="Invalid Date";
                            if(datesingle)
                            {
                                rowdate = new Date(results.data[0][date]);
                            } else {
                                if(results.data[0][dateyear].length > 0 &&
                                    results.data[0][datemonth].length > 0 &&
                                    results.data[0][dateday].length > 0)
                                {
                                    var CONST_TIME_ZERO = "T00:00:00";//used because a date like 01 will be one day less, unless time is specified.
                                    tmpstr = results.data[0][dateyear] + "-" + results.data[0][datemonth] + "-" + results.data[0][dateday];
                                    rowdate = new Date(tmpstr+CONST_TIME_ZERO);                             
                                }
                            }

                            if(rowdate.toString() == 'Invalid Date')
                            {
                                baddates.push(stepcount);
                                if(stepcount == MAX_FAILED_DATES && baddates.length == stepcount)
                                {
                                    abortparse = true;
                                    abortmsg = " Auto aborted because all date fields are bad.";
                                    parser.abort();
                                }
                                return;
                            }
                            //create year-month-day string
                            var rowdatestr = rowdate.getFullYear() +"-"+pad(rowdate.getMonth()+1,2)+"-"+pad(rowdate.getDate(),2);

                            //Look for Soft EOF character
                            if(analyzeSoftEOF)
                            {
                                for(var jj in results.data[0])
                                {
                                    var datastr = results.data[0][jj];
                                    for(var ii=0;ii<datastr.length;ii++)
                                    {
                                        //Analyze every column for 0x1A, or 26 which is a Soft EOF character that Python cannot handle
                                        if(datastr.charCodeAt(ii) == 26)
                                        {
                                            badrowcol.push([stepcount,jj,ii]);//save row and col
                                        }
                                    }
                                }
                            }

                            //Check for Idpost Length
                            if(analyzeIdpostLength)
                            {
                                if(idpostname in results.data[0])
                                {
                                    if(results.data[0][idpostname].length != IDLENGTH)
                                    {
                                        badidlength.push(stepcount);
                                    }
                                }
                            }

                            //Count tags
                            if(lastHadTags)
                            {
                                outdata[alldate][rowdatestr] = true;
                                for(var i=0; i<tags.length; i++)
                                {
                                    var tag = tags[i];
                                    var rowtag = results.data[0][tag].trim();//remove whitespace
                                    if(rowtag.length > 0)
                                    {
                                        //If got tag lookup, then tack on number
                                        if(gottags)
                                        {
                                            if(rowtag in itags)
                                            {
                                                rowtag = "("+itags[rowtag]+") "+rowtag;
                                            }
                                        }
                                        if(!(rowtag in outdata))
                                        {
                                            outdata[rowtag] = {};
                                        }
                                        if(!(rowdatestr in outdata[rowtag]))
                                        {
                                            outdata[rowtag][rowdatestr] = 0;    
                                        }
                                        outdata[rowtag][rowdatestr]++;
                                    }
                                }
                            }

                            //Count classifications
                            if(lastHadLabels)
                            {
                                //Count the label columns
                                labeloutdata[alldate][rowdatestr] = true;
                                for(var i=0; i<labels.length; i++)
                                {
                                    var label = labels[i];
                                    var rowlabel = results.data[0][label].trim();//remove whitespace
                                    if(rowlabel.length > 0  && parseInt(rowlabel) == 1)
                                    {
                                        if(!(label in labeloutdata))
                                        {
                                            labeloutdata[label] = {};
                                        }
                                        if(!(rowdatestr in labeloutdata[label]))
                                        {
                                            labeloutdata[label][rowdatestr] = 0;    
                                        }
                                        labeloutdata[label][rowdatestr]++;
                                    }
                                }
                            }

                        }
                    },
                    complete: function(results, file) {
                        if(!abortparse)
                        {
                            var newtime = new Date();
                            var delta = newtime - starttime;//ms
                            var ratebytes = Math.floor(size * 1000.0 / delta);
                            var ratemegabytes = Math.floor(ratebytes/1024/1024*100)/100;//Preserve 2 decimal places of MB
                            var rate = Math.floor(stepcount * 1000.0 / delta);//rows/sec
                            var ratebytes = Math.floor()
                            var totalsec = Math.floor(delta / 1000);
                            var min = Math.floor(totalsec / 60);//mins
                            var sec = totalsec - (min*60);//left over seconds
                            var output = "<u>Complete</u>";
                            output += "<br>Total rows = " + SimpleTable.prototype.formatNumber(stepcount);
                            output += "<br>Rows with errors = "+errorcnt;
                            output += "<br>Rows with blank = " + skipcount;
                            output += "<br>Rows with bad dates = " + baddates.length;
                            if(analyzeSoftEOF)
                            {
                                output += "<br>Rows/columns with bad characters = "+badrowcol.length;
                            }
                            if(analyzeIdpostLength)
                            {
                                output += "<br>Rows with bad Idpost length = "+badidlength.length;
                            }
                            output += "<br>Rows/second = " + rate;
                            output += "<br>MegaBytes/second = " + ratemegabytes;
                            output += "<br>Total duration (mm:ss) = " + min + ":" + pad(sec,2);

                            //Print out errors
                            othererrors = "";
                            if(results_errors.length > 0)
                            {
                                othererrors += "<br><br><u>Errors:</u>";
                            }
                            for(var ii=0;ii<results_errors.length;ii++)
                            {
                                othererrors += "<br>Row " + results_errors[ii].row + ": " + results_errors[ii].message;
                            }
                            //Print out bad date rows
                            if(baddates.length > 0)
                            {
                                othererrors += "<br><br><u>Rows with bad dates:</u>&nbsp;";
                                othererrors += "" + baddates[0];
                            }
                            for(var ii=1;ii<baddates.length;ii++)
                            {
                                othererrors += "," + baddates[ii];
                            }
                            if(analyzeSoftEOF)
                            {
                                //Print out bad date rows
                                if(badrowcol.length > 0)
                                {
                                    othererrors += "<br><br><u>Rows/columns with bad characters (row/column/index):</u>&nbsp;";
                                    othererrors += "" + badrowcol[0][0] + "/"+badrowcol[0][1]+ "/"+badrowcol[0][2];
                                }
                                for(var ii=1;ii<badrowcol.length;ii++)
                                {
                                    othererrors += "," + badrowcol[ii][0] + "/"+badrowcol[ii][1]+ "/"+badrowcol[ii][2];;
                                }
                            }
                            if(analyzeIdpostLength)
                            {
                                //Print out bad date rows
                                if(badidlength.length > 0)
                                {
                                    othererrors += "<br><br><u>Rows with bad Idpost length:</u>&nbsp;";
                                    othererrors += "" + badidlength[0];
                                }
                                for(var ii=1;ii<badidlength.length;ii++)
                                {
                                    othererrors += "," + badidlength[ii];
                                }
                            }
                            output += othererrors;
                            $('#response').html(output);
                            $('#pause').hide();
                            $('#estimateall').hide();
                            showCount(outdata,labeloutdata);
                        } else {
                            var output = "Parsing aborted." + abortmsg;
                            $('#response').html(output);
                            $('#pause').hide();
                        }
                        var currentpercent = 0;
                        $('#thebar').css('width', currentpercent+'%').attr('aria-valuenow', currentpercent);
                        $('#textbar').html(currentpercent + "% Complete");
                        $('#submitVisualize').prop("disabled",false);
                    },
                    error: function(error,file){
                        console.log(error);
                        $('#submitVisualize').prop("disabled",false);
                        OpenModalBox('Error visualizing',"Had and error visualizing this file.  Possibly not a CSV.");
                    }
                }
                
            );
        }
    }
    function showHeader(headlist,datalist)
    {
        $('#myfileselected').html(fileselected.name);
        curdatalist = datalist;
        headlist.unshift(DUMMY_VALUE);
        curheadlist = headlist;
        //Display header, hide file upload.
        $('.headerinput').show();
        forceOpen('.selectcolumnbox','.selectcolumnclose');

        var output = [];
        $.each(headlist, function(idx, value)
        {
            output.push('<option value='+ idx +'>'+ value +'</option>');
        });

        $('.datacolumn').html(output.join(''));

        //Auto select Date, or choose 0
        var finddate = Math.max(headlist.indexOf('postedTime'),0);
        var finddateyear = Math.max(headlist.indexOf('Year'),0);
        var finddatemonth = Math.max(headlist.indexOf('Month'),0);
        var finddateday = Math.max(headlist.indexOf('Day'),0);
        var findid = Math.max(headlist.indexOf('Idpost'),0);
        $('#datecolumn').val(finddate);
        $('#dateyearcolumn').val(finddateyear);
        $('#datemonthcolumn').val(finddatemonth);
        $('#datedaycolumn').val(finddateday);
        $('#idpostcolumn').val(findid);

        function updateMultiShow() {
            //Use singlge date, so hide multi
            if($('#datecolumn').val() == 0)
            {
                $('#multidate').show();       
            } else {
                $('#multidate').hide();
            }
        }

        updateMultiShow();

        $('#datecolumn').on('change',updateMultiShow);

        //Autoselect Tags
        $('#tagcolumn option').filter(function() { 
            return ($(this).text().match(/^matchingrulestag/)); //To select
        }).prop('selected', true);

        //Autoselect Label columns
        $('#labelcolumn option').filter(function() { 
            return ($(this).text().match(/^class_/) || $(this).text().match(/_r$/)); //To select
        }).prop('selected', true);
        
    }

    function showCount(outdata,labeloutdata)
    {
        //input format:
        // {<tag>:{ <yyyy-mm-dd>:<count>, ... }, ... }
        if(lastHadTags)
        {
            var headerrow = Object.keys(outdata).sort();//row name of num
            headerrow.splice(headerrow.indexOf(alldate),1);

            downloadcsv = setupMap("A",outdata,"Tag",headerrow);
            plotTagDaily("A",outdata,"dailytags","Tags",headerrow);
        }
        if(lastHadLabels)
        {
            downloadlabelcsv = setupMap("B",labeloutdata,"Classification",labeloutdata.labels.sort());
            plotTagDaily("B",labeloutdata,"dailylabels","Classifications",labeloutdata.labels.sort());
        }

        //TODO: Autodownload label counts
        $('.count-to-csv').show();
        $('.label-count-to-csv').show();
        if(lastHadTags)
        {
            $('.heatbox').show();
            forceClose('#heatmaptagbox','#heatmaptag');
        } else {
            $('.heatbox').hide();
        }

        if(lastHadLabels)
        {
            $('.labelcounts').show();
            forceClose('#heatmaplabelbox','#heatmaplabel');
        } else {
            $('.labelcounts').hide();
        }
        var autodownload = $('#autosave').prop('checked');
        if(autodownload == true)
        {
            if(lastHadTags)
            {
                saveData(downloadcsv,'tag_counts');
            }
            if(lastHadLabels)
            {
                saveData(downloadlabelcsv,'classification_counts');
            }
        }
    }

    function createDateRange(startDate,endDate)
    {
        //Expand the range to be the first day of the 
        //start month and the last day of the end month
        var CONST_TIME_ZERO = "T00:00:00";//used because a date like 01 will be one day less, unless time is specified.
        var newendDate = new Date(endDate+CONST_TIME_ZERO);
        var newDate = new Date(startDate+CONST_TIME_ZERO);
        newDate.setDate(1); //Set to start of Month
        newendDate.setDate(1); //Set to start of Month otherwise if 31st will roll to next next month
        //Set to last day of month (0 is required to be last day of previous month)
        newendDate.setMonth(newendDate.getMonth()+1);
        newendDate.setDate(0);
        var dateStrings = new Array()

        while (newDate <= newendDate){
          str = newDate.getFullYear() + "-" +
                pad(newDate.getMonth() + 1,2) + "-" +
              pad(newDate.getDate(),2);         
          dateStrings.push(str);
          newDate.setDate(newDate.getDate()+1);//add one day
        }
        //console.log(dateStrings);
        return dateStrings;
    }

        //Setup tag
    function setupMap(addedId,counts,idname,rowLabel)
    {
        //addedId is a unique string added to all heatmap elements
        //counts = [ [<header>,...], [<data>,...] , ...]
        var heatdata = [];
        var downloadcsv = [];
        var colLabel = Object.keys(counts[alldate]).sort();
        var maxdate = colLabel[colLabel.length-1];//colLabel.reduce(function(x,y){return Date(x) > Date(y) ? x:y;});
        var mindate = colLabel[0];//colLabel.reduce(function(x,y){return Date(x) > Date(y) ? y:x;});
        colLabel = createDateRange(mindate,maxdate);//Create a date range from min to max so no days are skipped

        //add headerrow to csv
        var headerrow = [];
        headerrow = rowLabel.slice(); //copy
        headerrow.unshift("Date");
        downloadcsv.push(headerrow);

        var hcrow = [];//row num to name
        for(var ii=0;ii<rowLabel.length;ii++)
        {
            hcrow.push(ii);
        }

        var hccol = [];//col num to name
        //Make numbers 0 to X number of columns in array
        for(var j=0;j<colLabel.length;j++)
        {
            hccol.push(j);
        }
        //white to yellow to red: see http://www.strangeplanet.fr/work/gradient-generator/index.php
        var colors = ["#FFFFFF", "#FFFFD6", "#FFFFCC", "#FFFFC1", "#FFFFB7", "#FFFFAD", "#FFFFA3", "#FFFF99", "#FFFF8E", "#FFFF84", "#FFFF7A", "#FFFF70", "#FFFF66", "#FFFF5B", "#FFFF51", "#FFFF47", "#FFFF3D", "#FFFF32", "#FFFF28", "#FFFF1E", "#FFFF14", "#FFFF0A", "#FFFF00", "#FFF400", "#FFE900", "#FFDF00", "#FFD400", "#FFC900", "#FFBF00", "#FFB400", "#FFAA00", "#FF9F00", "#FF9400", "#FF8A00", "#FF7F00", "#FF7400", "#FF6A00", "#FF5F00", "#FF5500", "#FF4A00", "#FF3F00", "#FF3500", "#FF2A00", "#FF1F00", "#FF1500", "#FF0A00", "#FF0000"];
        var maxvalue = 0;


        //Cycle through all rows, skipping the headerrow
        for(var j=0;j<colLabel.length;j++)
        {
            var col = j;//number
            var csvrow = [];
            csvrow.push(colLabel[j]);//date
            for(var i=0;i<rowLabel.length; i++)
            {
                var row = i;//number
                var value = 0;
                if(rowLabel[i] in counts && colLabel[j] in counts[rowLabel[i]])
                {
                    value = counts[rowLabel[i]][colLabel[j]];//number
                    if(value > maxvalue)//save max value
                    {
                        maxvalue = value;
                    }
                }
                heatdata.push({"row":col,"col":row,"value":value}); 
                csvrow.push(value);
            }
            downloadcsv.push(csvrow);
            
        }
        //Add errors from the parsing to the CSV download
        var errorrow = [othererrors];
        for(var i=0;i<rowLabel.lenth;i++)
        {
            errorrow.push("");
        }
        downloadcsv.push(errorrow);

        //Define the legend
        var domain = [0,maxvalue];//range of numbers
        var legenddomain = [];
        var numLegends = 5;
        //Split the domain into equal steps
        var step = Math.floor(maxvalue/numLegends);
        for(var j=0;j<maxvalue-step;j+=step)
        {
            legenddomain.push(j);
        }
        legenddomain.push(maxvalue);//always end with maxvalue

        var options = {
            shiftlabel : 200
        };//how far to shift the legend to the right

        //var hm = new heatMap(addedId,heatdata,rowLabel,colLabel,colors,hcrow,hccol,domain,legenddomain,{'dummy':'dummy'},'DAY');
        //Swapped the col and row to show days by row and groups by column.
        var hm = new heatMap(addedId,heatdata,colLabel,rowLabel,colors,hccol,hcrow,domain,legenddomain,{'dummy':'dummy'},'COL',options);
        return downloadcsv;
    }

    $('.count-to-csv').off('click.all');
    $('.count-to-csv').on('click.all', function() {
        saveData(downloadcsv,"tag_counts");
    });    
    $('.label-count-to-csv').off('click.all');
    $('.label-count-to-csv').on('click.all', function() {
        if(lastHadLabels)
        {
            saveData(downloadlabelcsv,"classification_counts");
        }
    });    

    function saveData(downloadcsv,nname){
        //remove file extension
        savefilename = datafilename.replace(/\.[^\.]+$/, "");

        var csv = new csvWriter();
        var csvdata = csv.arrayToCSV(downloadcsv);
        var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
        saveAs(blob, savefilename + '_'+nname+'.csv');
    }


    function plotTagDaily(addedId,counts,container,titleName,headerrow)
    {
        //This is used to plot a line graph of Tags over days
        //counts: [headerrow,tagcount,tagcount,...]
        //counts = [counts[0],counts[1]];//DEBUG
        var colLabel = Object.keys(counts[alldate]).sort();
        var maxdate = colLabel[colLabel.length-1];//colLabel.reduce(function(x,y){return Date(x) > Date(y) ? x:y;});
        var mindate = colLabel[0];//colLabel.reduce(function(x,y){return Date(x) > Date(y) ? y:x;});
        colLabel = createDateRange(mindate,maxdate);//Create a date range from min to max so no days are skipped

        var title = titleName+" Daily Counts";
        var data = [];
        var alltrace = [];

        //Cycle through all rows, skipping the headerrow
        for(var i=0;i<headerrow.length; i++)
        {
            var csvrow = [];
            for(var j=0;j<colLabel.length;j++)
            {
                var value = 0;
                if(headerrow[i] in counts && colLabel[j] in counts[headerrow[i]])
                {
                    value = counts[headerrow[i]][colLabel[j]];//number
                }
                csvrow.push(value);
            }


            var trace1 = {
              x: colLabel,
              y: csvrow,
              name: headerrow[i],
              type: 'scatter'
            };
            alltrace.push(trace1);

            data.push(csvrow);//save data for moving average
        }


        var layout = {
          //title: title,
          xaxis: {
            title: 'Date',
            titlefont: {
              family: 'Courier New, monospace',
              size: 18,
              color: '#777777'
            }
          },
          yaxis: {
            title: 'Counts',
            titlefont: {
              family: 'Courier New, monospace',
              size: 18,
              color: '#777777'
            }
          },
          hovermode: 'closest',
          //autosize: false,
          //width: '100%',
          height: 600,
          // margin: {
          //   l: 20,
          //   r: 20,
          //   b: 100,
          //   t: 100,
          //   pad: 4
          // },
          showlegend:true
          // legend:{
          //       x:0.9,
          //       y:1
          //   }
        };
        Plotly.newPlot(container, alltrace,layout);

        function updateData(days)
        {
            var graph = document.getElementById(container);
            for(var ii=0;ii<data.length;ii++)
            {
                //remove _id from row
                graph.data[ii].y= movingWindowAvg(data[ii],days);
            }

            Plotly.redraw(graph);
        }

        var slider_range_min_amount = $(".slider-range-min-amount"+addedId);
        var slider_range_min = $(".slider-range-min"+addedId);
        slider_range_min.slider({
            range: "min",
            value: 0,
            min: 0,
            max: 30,
            slide: function( event, ui ) {
                var newtext = "";
                if(ui.value == 0)
                {
                    newtext = "Disabled";
                } else {
                    newtext = ui.value +" Days";
                }
                slider_range_min_amount.val( newtext);
                updateData(ui.value);
            }
        });
        slider_range_min_amount.val("Disabled");

        function resizePlot()
        {
            var plotDiv = document.getElementById(container);
            Plotly.Plots.resize(plotDiv);
        }

        //$('#dailyexpand').on("click",function() {setTimeout(resizePlot,100);});
        $('.show-sidebar').on("click",function() {setTimeout(resizePlot,200);});
        $(window).on("resize",resizePlot);
    }

}