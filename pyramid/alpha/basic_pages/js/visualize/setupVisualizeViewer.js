//Written by DWS
function setupVisualizeViewer()
{
    var outtable = new SimpleTable({tableid:'viewdata'});
    var fileselected;
    var curheadlist = [];
    FileDragDrop('filedrag','fileupload',{accept:/\.[cC][sS][vV]$/,addedFile:getHeader,errorFile:fileError,selectedclass:'fileselected'});

    //Call setup
    $(document).ready(setup);

    //Functions
    function setup()
    {
        $('#submitVisualize').on('click',function() {
            calcVisualize();
        });
        $('.count-to-csv').on('click',function() {
            saveTable();
        });
    }

    function fileError(file)
    {
        OpenModalBox('Error loading file',"Filename needs to end in .csv.");
    }

    function getHeader(filedata)
    {
        //Update the file being shown
        var columnheader = Papa.parse(filedata,
            {
                // base config to use for each file
                header: false,
                preview: 1,
                complete: function(results, file) {
                    fileselected = filedata;
                    showHeader(results.data[0]);
                },
                error: function() {
                    OpenModalBox('Error','Error reading header row of file');
                }
            }
        );
    }

    function showHeader(headlist)
    {
        //Save for use later
        curheadlist = headlist;

        var output = [];
        $.each(headlist, function(idx, value)
        {
            output.push('<option value='+ idx +'>'+ value +'</option>');
        });

        $('.datacolumn').html(output.join(''));
        var listsize = Math.min(30,Math.floor(headlist.length * 0.5));
        $('.datacolumn').prop('size',listsize+"");
        //Display header
        $('.columns').show();
    }

    function saveTable()
    {
        var tabledata = outtable.toDict();
        var nname = "subset";

        //remove file extension
        var savefilename = fileselected.name.replace(/\.[^\.]+$/, "");

        //var csv = new csvWriter();
        var csvdata = Papa.unparse(tabledata);
        var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
        saveAs(blob, savefilename + '_'+nname+'.csv');
    }


    function calcVisualize()
    {
        var displaytype = $('#displaytype').val().trim();//remove the leading and ending spaces
        var validdata = typeof fileselected !== "undefined";
        validdata = validdata && displaytype.match(/^\d+$/);

        if(!validdata)
        {
            OpenModalBox("Form Error","File not selected, or offset is not a number.")
        } else {
            var offset = parseInt(displaytype);
            $('#submitVisualize').prop("disabled",true);
            //$('.selectfileclose').click();//close
            var columns = $( '#showcolumn').find(":selected").map(function(){ return $(this).text(); }).get();
            var displayevery = parseInt($('#displaynumber').val());
            var displaymax = parseInt($('#displaymax').val());
            var totalexamine = displayevery*displaymax+offset;
            $('#filename').html(fileselected.name);

            $('.statusbox').show();
            var stepcount = 0;

            function getNext(inoff)
            {
                if(typeof inoff === "undefined")
                {
                    inoff = 0;
                }
                //using displayevery and displaymax to determine the next row to show
                if(displayevery>1)
                {
                    //Random
                    var max = displayevery;
                    var min = 1;
                    //Inclusive random number
                    var newnum = Math.floor(Math.random() * (max - min + 1)) + min;
                } else {
                    //Every one
                    var newnum = 1;
                }
                var newarr = [stepcount+newnum+inoff,stepcount+displayevery+inoff];
                console.log(newarr);
                return newarr;
            }

            outtable.clear();//remove previous data
            var outputstd = "<u>Started</u>";
            var output = "";
            var starttime = new Date();
            output += "Started CSV parsing at: " + starttime.toLocaleTimeString();
            $('#responseone').html(output);
            $('#response').html(outputstd);
            var dataparse;
            var abortparse = false;
            var abortmsg = "";
            $('#endquery').on('click',function() {
                //dataparse.abort();
                abortparse = true;
                abortmsg = " Manually aborted.";
            });
            $('#pause').show();
            var columnheader;
            var targetline = getNext(offset);
            var results_errors = [];
            var targetlinedisplayed = false;
            var displayrow = 0;

            Papa.parse(fileselected,
                {
                    // base config to use for each file
                    header: true,
                    preview: totalexamine,
                    worker: true,
                    skipEmptyLines: true,
                    step: function(results, parser) {
                        //Check for user abort, and check for manual abort if already have enough lines
                        if(abortparse || stepcount >= totalexamine)
                        {
                            parser.abort();
                        }
                        stepcount++;

                        //If not at offset yet, then skip line
                        if(stepcount <= offset) { return; }

                        if(results.errors.length > 0)
                        {
                            //Copy all new errors
                            for(var jj=0;jj<results.errors.length;jj++)
                            {
                                //Don't show row 0 errors (always seem to get at least one)
                                if(results.errors[0].row == 0)
                                {
                                    results.errors[jj].row = stepcount;
                                }
                                results_errors.push(results.errors[jj]);
                            }
                            console.log("Row errors:", results.errors);
                            console.log("Row meta:", results.meta);
                        } else {

                            //Good data:
                            if(typeof columnheader === 'undefined')
                            {
                                if(columns.length > 0)
                                {
                                    columnheader = columns;
                                } else {
                                    columnheader = Object.keys(results.data[0]);
                                }
                                var tmpcol = columnheader.slice();//copy
                                tmpcol.unshift("#");//add row number
                                outtable.updateColumns(tmpcol);
                            }

                            if(!targetlinedisplayed && targetline[0] <= stepcount)
                            {
                                var values = columnheader.map(function(v) { return results.data[0][v]; });
                                values.unshift(""+stepcount);//add row number
                                outtable.appendOne(values);
                                targetlinedisplayed = true;
                                displayrow++;
                            }

                            //Wait until next section
                            if(targetlinedisplayed && targetline[1] <= stepcount)
                            {
                                //Setup for next target line
                                targetline = getNext();
                                targetlinedisplayed = false;
                            }
                        }

                    },
                    complete: function(results, file) {
                        if(!abortparse)
                        {
                            var newtime = new Date();
                            var delta = newtime - starttime;//ms
                            var rate = Math.floor(stepcount * 1000.0 / delta);//rows/sec
                            var totalsec = Math.floor(delta / 1000);
                            var min = Math.floor(totalsec / 60);//mins
                            var sec = totalsec - (min*60);//left over seconds
                            var output = "<u>Complete</u>";
                            output += "<br>Number of Displayed rows = " + SimpleTable.prototype.formatNumber(displayrow);
                            output += "<br>Total rows parsed = " + SimpleTable.prototype.formatNumber(stepcount);
                            output += "<br>Rows with errors = "+results_errors.length;
                            output += "<br>Total duration (mm:ss) = " + min + ":" + pad(sec,2);
                            //Print out errors
                            if(results_errors.length > 0)
                            {
                                output += "<br><br><u>Errors:</u>";
                            }
                            for(var ii=0;ii<results_errors.length;ii++)
                            {
                                output += "<br>Row " + results_errors[ii].row + ": " + results_errors[ii].message;
                            }
                            $('#response').html(output);
                            $('#pause').hide();
                        } else {
                            var output = "Parsing aborted." + abortmsg;
                            $('#response').html(output);
                            $('#pause').hide();
                        }
                        $('#submitVisualize').prop("disabled",false);
                    },
                    error: function(error,file){
                        console.log(error);
                        $('#submitVisualize').prop("disabled",false);
                        OpenModalBox('Error visualizing',"Had and error visualizing this file.  Possibly not a CSV.");
                    }

            });
        }
    }
}