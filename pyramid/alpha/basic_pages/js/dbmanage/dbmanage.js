//Written by DWS
function dbManageSetup()
{
	var dbcounts = new setupCountDB(gotDB);

    function gotDB()
    {
        //Got the information from the database

        var dataSet = [];
        var tableid = "dbtable";
        var dbdata = dbcounts.collection_count.slice(); //make copy
        var headerrow = dbdata[0].slice();//make copy of header row
        dbdata.shift();//remove header row
        var countidx = headerrow.indexOf('Count');
        headerrow.unshift("");//Must add after index count
        var totalcount = 0;
        //Reformat the number column to be ,'s'
        //var dummyTable = new SimpleTable({});
        for(var i=0;i<dbdata.length;i++)
        {
            totalcount += dbdata[i][countidx];
            dbdata[i][countidx] = SimpleTable.prototype.formatNumber(dbdata[i][countidx]);
            dbdata[i].unshift(i+1);
        }
        inttable = new SimpleTable({'columns':headerrow, 'tableid' : tableid, 'data':dbdata});
        $('#totalcount').html(SimpleTable.prototype.formatNumber(totalcount));
    } 


}