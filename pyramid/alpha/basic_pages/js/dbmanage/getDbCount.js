//Written by DWS
var setupCountDB = function(completeFcn)
{

    //Database unique variables
    this.collection_count;
    this.DBrunning;
    var that = this;
    var configLocation = "/db/mongo.json";
    $(document).ready(setup);

    //Successful conpletion of AJAX for DB Info
    function processDB(response) {
        // config mappings
        if(response.success == true)
        {
            console.log(response.data);//For debug
            that.collection_count = response.data;
            //Set DB name
            that.DBrunning = $('#queryDBAll').val();
            $('#dbname').html(that.DBrunning);

            endPause(false);
            $('.querybox').show();
            completeFcn();

        } else {
            OpenModalBox('Database Selection Error', 'No data returned from about the database selected.');
            endPause(false);
        }
    }

    //Error when getting DB info
    function processDBError() {
        OpenModalBox('Database Selection Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        endPause(false);
    }

    //Get the Database Information
    function ajaxGetDBInfo(dbname) {
        console.log(dbname);
        ajaxDB = $.ajax({
            url: configLocation,
            type: "post",
            data: JSON.stringify({"db":dbname,'cmd':3}),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processDB,
            error: processDBError,
            timeout: 30*1000 //30 seconds
        });
    };


    //Called to start getting the database information
    function getdbInfo()
    {
        //ajax query to get:
        startPause();
        ajaxGetDBInfo($('#queryDBAll').val());
    }

    //Freeze the window, put button to cancel
    function startPause()
    {
        $('#pauseAll').html('<span><h4>Getting Database Information</h4> <button type="button" id="endDBquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
        $('#endDBquery').on('click', function() {endPause(true);});
        $('#queryDBAll').attr('disabled', 'disabled');
    }

    //Unfreeze window, remove button
    function endPause(isCancel)
    {
        //Check if want to cancel the AJAX too
        if(isCancel)
        {
            ajaxDB.abort();
        }
        $('#pauseAll').html('');
        $('#queryDBAll').removeAttr('disabled');
    }

    function setup()
    {
        $('#queryDBAll').on('change.All', function(e) {
             if($('#queryDBAll').val() != 'Select...')
             {
                 getdbInfo();
             }
        });
        if($('#queryDBAll').val() != 'Select...')
        {
            setTimeout(getdbInfo,100);
        }
        $('.querybox').hide();

    }
}