//Written by DWS
var getDataset = function(completeFcn,errorFcn,interimFcn)
{

    //Database unique variables
    var totalCount = 0;
    var that = this;
    var configLocation = "/db/db.json";
    var DBrunning = "";
    var collectionRanges = [];
    var _query = {};
    var collectionGroup = 0;
    var timeColumn = "postedTime";
    var ajaxDB;
    $(document).ready(setup);

    //Successful conpletion of AJAX for DB Info
    function processDB(response) {
        // config mappings
        console.log(response);
        if(response.issuccess == "success")
        {
            //Successfully got data
            totalCount += response['totalcount'];
            interimFcn(response['totalcount'],totalCount);
            nextQuery();
        } else {
            OpenModalBox('Database Selection Error', 'No data returned from the database selected.');
            endPause(false);
        }
    }

    //Error when getting DB info
    function processDBError() {
        OpenModalBox('Database Selection Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        endPause(false);
    }

    //Get the Database Information
    function ajaxGetDBInfo(data) {
        console.log(data);
        ajaxDB = $.ajax({
            url: configLocation,
            type: "post",
            data: JSON.stringify(data),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processDB,
            error: processDBError,
            timeout: 5*60*1000 //5 minutes
        });
    };

    //Called to start getting the database information
    this.getdbInfo = function(datestart,dateend,query,db)
    {
        //ajax query to get:
        $('.resultbox').hide();
        startPause();
        collectionGroup = 0;
        totalCount = 0;
        collectionRanges = setupQuery(datestart,dateend);
        DBrunning = db;
        _query = query;
        if(collectionRanges.length == 0)
        {
            OpenModalBox('Date Range Selection Error', 'Could not find a month range from the Start and End Date.');
            endPause(false);
            return;
        }
        nextQuery();
    };

    //Handle breaking down the query into different collections
    function nextQuery() {
        var increment = 0;//not used
        var step = 0;//not used
        var createSample = false;//No sampling yet
        if(collectionRanges.length > collectionGroup)
        {
            //merge query and timequery if exists
            var tempquery = $.extend({}, collectionRanges[collectionGroup]['timequery'], _query);

            var request = {"query":tempquery,"collection":collectionRanges[collectionGroup]['collection'],"increment":increment,
                           "step":step,"createSample":createSample,"db":DBrunning};

            ajaxGetDBInfo(request);
            collectionGroup++;
        } else {
            //Finished all collections

            endPause(false);
            $('.resultbox').show();//Show the result box

            completeFcn(totalCount);            
        }
    }


    //Freeze the window, put button to cancel
    function startPause()
    {
        $('#pause').html('<span><h4>Getting Database Information</h4> <button type="button" id="endDBquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
        $('#endDBquery').on('click', function() {endPause(true);});
        $('#queryDB').attr('disabled', 'disabled');
    }

    //Unfreeze window, remove button
    function endPause(isCancel)
    {
        //Check if want to cancel the AJAX too
        if(isCancel)
        {
            ajaxDB.abort();
        }
        $('#pause').html('');
        $('#queryDB').removeAttr('disabled');
        errorFcn();
    }

    function setup()
    {
        $('.resultbox').hide();//Hide it at the beginning
    }

    //Setup all query information
    function setupQuery(datestart,dateend) {

        var collectionRanges = [];
        var inputStartDate = new Date(datestart);
        //Set to beginning of day
        inputStartDate.setHours(0);
        inputStartDate.setMinutes(0);
        inputStartDate.setSeconds(0);
        inputStartDate.setMilliseconds(0);
        var newStartDate = new Date(inputStartDate.getTime());
        //Only set to first of month if doing monthly incrementing
        newStartDate.setDate(1);//make the 1st of the month so +1 on month works correctly
        var newEndDate = new Date(dateend);
        //Push to end of day
        newEndDate.setHours(23);
        newEndDate.setMinutes(59);
        newEndDate.setSeconds(59);
        newEndDate.setMilliseconds(999);
        var currentWeek = 0;
        var currentDay = 0;

        while(dates.compare(newStartDate,newEndDate) <= 0)
        {
            var strmonth = (newStartDate.getMonth() + 1).toString();
            if(strmonth.length < 2) { strmonth = '0' + strmonth;}
            strCollection = newStartDate.getFullYear().toString() + strmonth;

            var sliceText = (newStartDate.getMonth()+1) + "/" + newStartDate.getFullYear();

            //narrow down by day if in the same month
            var weekQuery = {};
            if(inputStartDate.getMonth() == newStartDate.getMonth() || newStartDate.getMonth() == newEndDate.getMonth())
            {
                var tempWeekGT = {};
                var tempWeekLT = {};
                if(inputStartDate.getMonth() == newStartDate.getMonth())
                {
                    tempWeekGT = {'$gte': fixDateOffset(inputStartDate)};  
                }
                if(newStartDate.getMonth() == newEndDate.getMonth())
                {
                    tempWeekLT = {'$lte': fixDateOffset(newEndDate)};  
                }
                var tempquery = $.extend({}, tempWeekGT, tempWeekLT);
                weekQuery[timeColumn] = tempquery;
            }

            collectionRanges.push({"collection":strCollection, "timequery":weekQuery, "slice":sliceText, "complete":false, "tempresult":0, "result":0});
            //Increment one month
            newStartDate.setMonth(newStartDate.getMonth() + 1);
        }

        return collectionRanges;

    }

}