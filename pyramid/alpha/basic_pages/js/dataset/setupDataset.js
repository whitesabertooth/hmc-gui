//Written by DWS
function setupDataset()
{

	var cursubset = 0;
	var addDatasetid = "addDataset";
	var submitDatasetid = "submitQuery";
	var submitRuleid = "submitRuleSample";
	var submitTagid = "submitTagSample";
	var queryStarted = false;
    var datasetLocation = getUrl() + "/db/dataset.json";
    var cursubsetQuery = 0; 
    var DBrunning = "";
    var _querytype = "";
    var localTotal = [];
    var localdata = [];
    var dbget = new getDataset(completeDB,errorDB,interimDB);
    var dbgetsample = new getDatasetSample(completeDBSample,errorDBSample);
    var columns = ['Subset','Query','Start Date','End Date','Total Count'];
    var countIdx = columns.indexOf('Total Count');
	var totalTable = new SimpleTable({'tableid':'datasettotal', 'columns':columns});
    var dbgetsamplejob = new GetDatasetJobs();

	function addSubSet()
	{
		var subsetid = "subsets";
		var subsets = $("#" + subsetid);
		var subsetstr = '<div class="row form-group" id="group' + cursubset + '">'
                                +'<div class="col-sm-1" align="right">'
                                +'    <button type="button" set="' + cursubset + '" id="removeDataset' + cursubset + '" class="btn btn-default btn-label-left">'
                                +'            -'
                                +'    </button>'
                                +'</div>'
                                +'<div class="col-sm-4">'
                                +'    <input name="querystring" class="form-control query" type="search" id="query' + cursubset + '" placeholder="e.g. {\'matchingrulestag\' : {\'$ne\': 7}}" data-toggle="tooltip" data-placement="top" title="JSON query directly to Mongo.  (matchingrulestag=Tags, matchingrulesvalue=Rules, $ne=Not Include, $eq=Includes)"/>'
                                +'</div>'
                                +'<div class="col-sm-3">'
                                +'    <input type="text" class="form-control datestart" id="datestart' + cursubset + '" placeholder="Start Date">'
                                +'</div>'
                                +'<div class="col-sm-3">'
                                +'    <input type="text" class="form-control dateend" id="dateend' + cursubset + '" placeholder="End Date">'
                                +'</div>'
                            +'</div>';

        subsets.append(subsetstr);
        setupDatePeriod(cursubset);
        $('#removeDataset' + cursubset).on('click',function() {
        	var dataset = $(this).attr('set'); 
        	$('#group'+dataset).remove();
        });
        cursubset++;
	}


    function setupDatePeriod(subset)
    {
        $('#datestart' + subset).datepicker({
            defaultDate: "+lw",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            maxDate: '0',//set to +0 days from current date
            minDate: new Date('December 1, 2011 00:00:00'), //set to 1/2013
            onClose: function( selectedDate ) {
                $( "#dateend" + subset ).datepicker( "option", "minDate", selectedDate );
                if($( "#dateend" + subset ).val() == '' || $( "#dateend" + subset ).val() < selectedDate)
                {
                    $( "#dateend" + subset ).val(selectedDate);
                }
            }
        });
        $('#dateend' + subset).datepicker({
            defaultDate: "+lw",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            maxDate: '0',//set to +0 days from current date
            minDate: new Date('December 1, 2011 00:00:00'), //set to 1/2013
            onClose: function( selectedDate ) {
                if($( "#datestart" + subset ).val() == '')
                {
                    $( "#datestart" + subset ).val(selectedDate);
                }
            }
        });
    }

    var allQuery = [];
    function submitDataset(querytype)
    {
	    if(!queryStarted)
        {
        	_querytype = querytype;
        	allQuery = [];
            var query = {};
            var error = false;
            localdata = [];
            localTotal = [];
            var allquerytext = $('.query');
            var alldatestart = $('.datestart');
            var alldateend = $('.dateend');
	        DBrunning = $('#queryDB').val();
            if(allquerytext.length != alldatestart.length || alldatestart.length != alldateend.length)
            {
                OpenModalBox('Form Error', 'Bad number of query, start and end');
                return;            	
            }
            for(var i=0; i<allquerytext.length; i++)
            {
            	var querytext = $(allquerytext[i]).val();
	            var newtext = querytext.replace(/'/g,'"');
	            //Validate querytext if is JSON
	            if(!isJSON(newtext))
	            {
	                OpenModalBox('Form Error', 'Bad query: it is not a JSON for dataset ' + (i+1));
	                return;            	
	            }                            
	            query = JSON.parse(newtext);

	            var datestart = $(alldatestart[i]).val();
	            var dateend = $(alldateend[i]).val();

	            if(datestart == '' || dateend == '')
	            {
	                OpenModalBox('Form Error', 'Bad date start/end: must be filled in for dataset ' + (i+1));
	                return;
	            }

                //Successful query so add
                allQuery.push({'query':query,'datestart':datestart,'dateend':dateend});

                if(querytype == "total")
                {
	                //Save the data locally for the table
			        localdata.push([i,newtext,datestart,dateend,createCount(0,false)]);
			        localTotal.push(0);//Add zero count to the totals
		    	}
	        }
	        //Update the display of totals
            queryStarted = true;
            if(querytype == "total")
	        {
	        	totalTable.update(localdata);
	        }
            cursubsetQuery = -1;//initialize to minus 1 because will be incremented one first call
            nextSubset();
		}
    }

    //Create a color of the count based on if it is complete or not
    function createCount(count,isfinal)
    {
    	var color = 'red';
    	if(isfinal)
    	{
    		color = 'green';
    	}
    	return "<font color='" + color + "'>" + SimpleTable.prototype.formatNumber(count) + "</font>";
    }

    //This is called to start a query of the next subset
    function nextSubset()
    {
    	cursubsetQuery++;
		if(cursubsetQuery < allQuery.length)
		{
			if(_querytype == "total")
			{
	        	dbget.getdbInfo(allQuery[cursubsetQuery].datestart,allQuery[cursubsetQuery].dateend,allQuery[cursubsetQuery].query,DBrunning);
	        } 
	        else
	        {
                dbgetsample.getdbInfo(allQuery,_querytype,DBrunning);
	        }
	    } else {
	    	queryStarted = false;
	    }
    }

    //Run when successfully got the DB Info about the Dataset
    function completeDB(totalCount)
    {
    	updateCount(cursubsetQuery,totalCount,true);
    	nextSubset();
    }

    function errorDB()
    {
    	queryStarted = false;
    }

    function interimDB(currentCount, totalCount)
    {
    	updateCount(cursubsetQuery,totalCount,false);
    }

    function updateCount(subset,count,isfinal)
    {
    	localTotal[subset] = count;
    	localdata[subset][countIdx] = createCount(count,isfinal);
    	totalTable.update(localdata);
    	updateTotal();
    }

    function updateTotal()
    {
    	var total = 0;
    	for(var i=0; i<localTotal.length;i++)
    	{
    		total += localTotal[i];    		
    	}
    	$('#datasetTotal').html(SimpleTable.prototype.formatNumber(total));
    }

    //Run when successfully got the DB Info about the Dataset
    function completeDBSample()
    {
        queryStarted = false;
    }

    function errorDBSample()
    {
        queryStarted = false;
    }


    $(document).ready(function() {
        addSubSet();
        $('#'+addDatasetid).on("click",function() { addSubSet();});
        $('#'+submitDatasetid).on("click",function() { submitDataset("total");});
        $('#'+submitTagid).on("click",function() { submitDataset("tag");});
        $('#'+submitRuleid).on("click",function() { submitDataset("rule");});
        //DEBUG:
        // $('#query0').val('{"mrv":478}');
        // $('#datestart0').val('1/1/2015');
        // $('#dateend0').val('1/8/2015');
        // addSubSet();
        // $('#query1').val('{"mrv":478}');
        // $('#datestart1').val('1/1/2014');
        // $('#dateend1').val('1/8/2014');
	});




	//FUTURE DAILY IMPROVEMENT
	// var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
	//   $("#slider").dateRangeSlider({
	//     bounds: {min: new Date(2012, 11, 1), max: new Date()},
	//     defaultValues: {min: new Date(2012, 1, 10), max: new Date(2012, 4, 22)},
	//     scales: [{
	//       first: function(value){ return value; },
	//       end: function(value) {return value; },
	//       next: function(value){
	//         var next = new Date(value);
	//         return new Date(next.setMonth(value.getMonth() + 3));
	//       },
	//       label: function(value){
	//         return months[value.getMonth()] + (value.getYear()-100);
	//       },
	//       format: function(tickContainer, tickStart, tickEnd){
	//         tickContainer.addClass("myCustomClass");
	//       }
	//     }],
	//     formatter:function(val){
	//         var days = val.getDate(),
	//           month = val.getMonth() + 1,
	//           year = val.getFullYear();
	//           dd = months[month-1] + " " + year;
	//         //setTimeout(1,function() {
	//         //	$('#date_example').datepicker({});
	//         //});
	//         //return '<input type="text" class="form-control" id="date_example" value="' + dd + '">';
	//         return dd;
	//       },
	//       step: {
	//       	months: 1
	//       }
	//   });
	 // $('#date_example').datepicker({});
 }