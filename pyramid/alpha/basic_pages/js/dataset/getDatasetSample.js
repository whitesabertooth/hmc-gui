//Written by DWS
var getDatasetSample = function(completeFcn,errorFcn)
{

    //Database unique variables
    var that = this;
    var configLocation = "/db/dataset.json";
    var scriptLongLocation = getUrl() + '/db/submitLongJob.json';
    var ajaxDB;
    var sampleTable = new SimpleTable({'tableid':'datasetsample'});
    var fulltotal = 0;
    var subqueryStarted = false;
    var _allquery = [];
    var DBrunning = "";
    var titletype = "rule";
    var colors = ["#FFFFFF", "#FAFAC8", "#F9F9B6", "#F7F7A4", "#F6F692", "#F4F480", "#F3F36E", "#F1F15C", "#F0F04A", "#F1D844", "#F3C03F", "#F5A93A", "#F69135", "#F87A30", "#FA622B", "#FB4B26", "#FD3321", "#FF1C1C"];
    var colorScale;
    var minscale;
    var maxblackfont;
    var samplenumCLASS;
    var totalCLASS;
    var autocalcsampleCLASS;
    var samplenumcalcCLASS;
    $(document).ready(setup);

    function calcIndividualSample(total,fulltotal)
    {
        //Return a percentage
        if(fulltotal == 0) { return "0%";}//handle division by 0
        return (Math.round(100.0 * 100.0 * total / fulltotal) / 100.0) + "%";
    }

    function calculateSample(total,samplesize)
    {
        var sampletype = "number";
        if(samplesize.endsWith("%")) 
        {
            sampletype="percent"; 
            samplesize = samplesize.substring(0, samplesize.length - 1);
        } 
        if(!$.isNumeric(samplesize))
        {
            OpenModalBox('Form Error', 'Bad sample size: Needs to be either a number or a number with a % at end.');
            return null;       
        }
        var sample = parseFloat(samplesize);
        if(sampletype == "percent")
        {
            if(sample > 100)
            {
                OpenModalBox('Form Warning', 'Sample percent is greater than 100%.  Calculating based on 100%.');
                sample = 100;
            }                
            sample = Math.round(sample * total / 100);
        } else {
            //Check if greater than total
            if(sample > total)
            {
                OpenModalBox('Form Warning', 'Sample size is greater than data size.  Calculating based on total data size.');
                sample = total;
            }
        }

        return sample;
    }

    //Successful conpletion of AJAX for DB Info
    function processDB(response) {
        // config mappings
        console.log(response);
        if(response.issuccess == "success")
        {
            var responsedata = response.data.slice();//make copy of original
            $('#tagfilter').hide();
            //Successfully got data
            //data should look like:
            // Tag, Rule, Total  or  Tag, Total
            var headerdata = responsedata[0].slice();//copy header line
            var DB_tags = {};
            responsedata.shift();//remove header line
            //Add two more columns at end:
            var totalIdx = headerdata.indexOf("Total");
            //Choose the title index for later submission
            var titleIdx = headerdata.indexOf("Rule");
            titletype = "rule";
            var tagIdx = null;
            var titlewidth = '40%';
            if(titleIdx < 0)
            {
                titletype = "tag";
                titleIdx = headerdata.indexOf("Tag");
                titlewidth = '150px';
            } else {
                tagIdx = headerdata.indexOf("Tag");
                headerdata[tagIdx] = {'name':headerdata[tagIdx],'style':'width: 100px;'};
            }
            headerdata[titleIdx] = {'name':headerdata[titleIdx],'style':'width: '+titlewidth +';'};
            headerdata[totalIdx] = {'name':headerdata[totalIdx],'style':'width: 75px;'};
            headerdata.push("Stratification % of All Samples");
            headerdata.push("+ Over/Under Sampling (# or %) of Total");
            headerdata.push("= Combined Calculated Sample #");
            fulltotal = 0;
            //Calculate the sampletotal
            for(var row=0;row<responsedata.length; row++)
            {
                var rowtotal = responsedata[row][totalIdx];
                fulltotal += rowtotal;
            }
            var initialsampletotal = 5000; //Default to this
            if(fulltotal < 5000)
            {
                initialsampletotal = Math.ceil(0.25 * fulltotal);//calculate a 25% size if less than 5000
            }
            for(var row=0;row<responsedata.length; row++)
            {
                var total = responsedata[row][totalIdx];
                var initsample = calcIndividualSample(total,fulltotal);
                var sample_subtotal = calculateSample(initialsampletotal,initsample);
                responsedata[row][totalIdx] = "<span class='total'>" + SimpleTable.prototype.formatNumber(total) + "</span>";//add jquery indexable
                responsedata[row][titleIdx] = "<span class='sampletitle'>" + responsedata[row][titleIdx] + "</span>";//add jquery indexable
                if(tagIdx != null)
                {
                    //Save Tags for later display
                    DB_tags[responsedata[row][tagIdx]] = true;
                    responsedata[row][tagIdx] = "<span class='tagtitle'>" + responsedata[row][tagIdx] + "</span>";//add jquery indexable
                }
                //Add stratification sampling column, 
                //autocalcsample % of autocalcsampletotal = autocalcsampletotalcalc
                responsedata[row].push("<span rownum='" + row + "' class='autocalcsample'>" + initsample + "</span>&nbsp;of&nbsp;<span class='autocalcsampletotal'>" + SimpleTable.prototype.formatNumber(initialsampletotal) + "</span>&nbsp;=&nbsp;<span class='autocalcsampletotalcalc'>" + SimpleTable.prototype.formatNumber(sample_subtotal) + "</span>");

                //Add sampling column, 
                responsedata[row].push("<input type='text' class='samplenum' rownum='" + row + "' size='10px' placeholder='e.g. 1000 or -25%' value='0' />&nbsp;of&nbsp;" + SimpleTable.prototype.formatNumber(total) + "</span>&nbsp;=&nbsp;<span class='samplenumcalc'>0</span>");

                //and calculated sample column
                var calcstr = "<span class='samplesubtotal'>" + SimpleTable.prototype.formatNumber(sample_subtotal) + "</span>";
                responsedata[row].push(calcstr);
            }
            $('#sampletext').val(initialsampletotal);
            sampleTable.updateColumns(headerdata);
            sampleTable.update(responsedata);
            $('#sampletotal').html(SimpleTable.prototype.formatNumber(fulltotal));
            $('#sampledeltatotal').html(0);//No delta initially
            //Show button for download
            $('#submitsample').show();
            updatecalculateRowSample();//Just need to do the Jquery setup one time - This shaves off 1.5 seconds
            updateSampleTotalCaluclation();
            //Display the tag filter
            if(tagIdx != null)
            {
                $('#tagfilter').show();
                setupTagSelection(DB_tags);
            }

            setTimeout(function() {
                updateAllColors();
                //Handle the change of the stratification total sample number
                $('#sampletext').on('change', function()
                {
                    updateStratificationColumn();
                });
                //Setup changes on sample number will re-calculate the sub-sample and update total
                $('.samplenum').on('change', function()
                {
                    var rownum = parseInt($(this).attr('rownum'));
                    updateSampleNum(rownum,true);
                    //updateTotalModifiedSample();
                    updateSampleTotalCaluclation();
                    updateAllColors();//re-update after recalculated total
                });
                $('.samplebox').show();//Show the sample data
            },1);
            //Call complete Function outside this module
            endPause(false);
            completeFcn();
        } else {
            OpenModalBox('Database Selection Error', 'No data returned from the database selected.');
            endPause(false);
        }
    }

    //setup tag filtering for when showing rules
    function setupTagSelection(tags)
    {
        var secondstr = 'Filter by Tags: <select class="s2_with_tag" multiple>';
        $.each ( tags,function(k,v)
        {
            secondstr += "<option>" + k + "</option>";
        });
        secondstr += "</select>";
        $('#tagfilter').html(secondstr);

        //Turn on Select2 capability
        $('.s2_with_tag').select2({placeholder: "Select Tags"});

        //Look for changes in column filters
        $('.s2_with_tag').on('change', function() {
            //Get array of elements selected
            var vals = $( this).find(":selected").map(function(){ return this.value }).get();

            showRows(vals);
            //screenHold("Processing Row Changes","Redrawing row, please wait...","",function() {showRows(vals); });
        });
    }

    //Called when a Tag filtering is selected to update the hide() or show() of the table rows based on the tags selected
    //If no tags selected, then show them all
    function showRows(tags)
    {
        //Filter the rows down to only the rows with the tags
        $('.tagtitle').each(function() {
            if(tags.length == 0)
            {
                //show everything
                $(this).parent().parent().show();
            } else {
                //Only show if in list
                if( $.inArray($(this).html(),tags) >= 0 )
                {
                    $(this).parent().parent().show();
                } else {
                    $(this).parent().parent().hide();
                }
            }
        });
    }

    //Update the sample row based on the user update
    function updateSampleNum(rownum,ismod)
    {
        //Change the sub-total
        var subtotal = calculateRowSample(rownum);
        if(subtotal == null)
        {
            return;
        }
        var final_new_sample_subtotal = subtotal.final_new_sample_subtotal;//add the baseline stratification sample
        $($('.samplesubtotal')[rownum]).html(SimpleTable.prototype.formatNumber(final_new_sample_subtotal));
        //Update the Total Sample Table Oversample column
        if(ismod)
        {
            changeTotalModifiedSample(subtotal.old_oversample,subtotal.new_sample_subtotal);
        }
    }

    function updatecalculateRowSample()
    {
        samplenumCLASS = $('.samplenum');
        totalCLASS = $('.total');
        autocalcsampleCLASS = $('.autocalcsample');
        samplenumcalcCLASS = $('.samplenumcalc');
    }

    //Calcuate the specific row oversample calculation and final sub-total calculation
    function calculateRowSample(rownum)
    {
        var newsample = $(samplenumCLASS[rownum]).val().replace(new RegExp(",", 'g'),"");
        var total = parseInt($(totalCLASS[rownum]).html().replace(new RegExp(",", 'g'),""));
        var new_sample_subtotal = calculateSample(total,newsample);
        var sampletext = $('#sampletext').val();
        var autocalcsample = $(autocalcsampleCLASS[rownum]).html();
        var old_oversample = $(samplenumcalcCLASS[rownum]).html().split(/ /)[0].replace(new RegExp(",", 'g'),"");
        //Verify that the newsample is formated correctly before updating
        if(new_sample_subtotal == null)
        {
            return null;
        }
        var totalsample = calculateSample(fulltotal,sampletext);
        if(totalsample == null)
        {
            return null;
        }
        var autocalctotal = calculateSample(totalsample,autocalcsample);//get the sampling of the total sample
        if(autocalctotal == null)
        {
            return null;
        }
        var capstr = "";
        var pre_sample_subtotal = new_sample_subtotal;
        //Cap the entered amount at the max of total
        if(new_sample_subtotal > total - autocalctotal)
        {
            new_sample_subtotal = total - autocalctotal;
            var delta_subtotal = pre_sample_subtotal - new_sample_subtotal;
            capstr = " - " + SimpleTable.prototype.formatNumber(delta_subtotal) + " = " + SimpleTable.prototype.formatNumber(new_sample_subtotal) + " (capped)";
        }
        //Cap the bottom to be only so negative
        if(-new_sample_subtotal > autocalctotal)
        {
            new_sample_subtotal = -autocalctotal;
            var delta_subtotal = new_sample_subtotal - pre_sample_subtotal;
            capstr = " + " + SimpleTable.prototype.formatNumber(delta_subtotal) + " = " + SimpleTable.prototype.formatNumber(new_sample_subtotal) + " (capped)";
        }
        $(samplenumcalcCLASS[rownum]).html(SimpleTable.prototype.formatNumber(pre_sample_subtotal) + capstr);
        var final_new_sample_subtotal = new_sample_subtotal + autocalctotal;//add the baseline stratification sample

        return {'new_sample_subtotal':new_sample_subtotal, 'final_new_sample_subtotal':final_new_sample_subtotal,'old_oversample':old_oversample};
    }

    //Update the Total Sample Table Over/Under sample column by looking through every row
    function updateTotalModifiedSample()
    {
        var totalmod = 0;
        $('.samplenum').each(function()
        {
            var rownum = parseInt($(this).attr('rownum'));
            var calcrow = calculateRowSample(rownum);
            if(calcrow != null)
            {
                totalmod += calcrow.new_sample_subtotal;
            }
        });
        $('#sampledeltatotal').html(SimpleTable.prototype.formatNumber(totalmod));
    }

    //Update the Total Sample Table Over/Under sample column by with old and new values
    function changeTotalModifiedSample(oldval,newval)
    {
        var totalmod = parseInt($('#sampledeltatotal').html().replace(new RegExp(",", 'g'),""));
        totalmod -= oldval;
        totalmod += newval;
        $('#sampledeltatotal').html(SimpleTable.prototype.formatNumber(totalmod));
    }

    //Update all the colors, as in determine the style for each row after updating the color totals
    function updateAllColors()
    {
        updateColors();//Do one time when sampletotal is static
        $('.samplesubtotal').each(function()
        {
            var samplecalc = parseInt($(this).html().replace(new RegExp(",", 'g'),""));
            var style=determineStyleFromSampleSize(samplecalc);
            $(this).parent().attr("style",style);
        });
    }

    //Update the Total Sample Table calculations
    function updateSampleTotalCaluclation()
    {
        //[Sample] + [Over/Under Sampling Delta] = [Calculated Sample]
        var sampletotal = parseInt($('#sampletotal').html().replace(new RegExp(",", 'g'),""));
        var sampletext = $('#sampletext').val().replace(new RegExp(",", 'g'),"");
        var sampledeltatotal = parseInt($('#sampledeltatotal').html().replace(new RegExp(",", 'g'),""));
        var autocalctotal = calculateSample(sampletotal,sampletext);
        if(autocalctotal == null)
        {
            return;
        }
        autocalctotal += sampledeltatotal;
        $('#sampletotalcalc').html(SimpleTable.prototype.formatNumber(autocalctotal));
    }

    //Call this only if the total sample requested changes; this will update everything
    function updateStratificationColumn()
    {
        var sampletext = $('#sampletext').val();
        var totalsample = calculateSample(fulltotal,sampletext);
        if(totalsample == null)
        {
            return;//problem with new sample
        }
        //Update the display of all the individual rows' total sample
        $('.autocalcsampletotal').each(function ()
        {
            $(this).html(SimpleTable.prototype.formatNumber(totalsample))
        });
        //autocalcsample % of autocalcsampletotal = autocalcsampletotalcalc
        var autocalcsampletotalcalc = $('.autocalcsampletotalcalc');
        $('.autocalcsample').each(function ()
        {
            var rownum = parseInt($(this).attr('rownum'));
            var autocalcsample = $(this).html();
            var newcalc = calculateSample(totalsample,autocalcsample);
            if(newcalc == null)
            {
                return;
            }
            $(autocalcsampletotalcalc[rownum]).html(SimpleTable.prototype.formatNumber(newcalc));
            updateSampleNum(rownum,false);
        });
        //NOT NEEDED because nothing should have changed
        // $('.samplenum').each(function()
        // {
        //     var rownum = parseInt($(this).attr('rownum'));
        // });
        //updateTotalModifiedSample();
        updateSampleTotalCaluclation();
        updateAllColors();//re-update after recalculated total
    }

    //One time update of all color data, used by determineStyle...() function
    function updateColors()
    {
        var sampletotal = $('#sampletotalcalc').html().replace(new RegExp(",", 'g'),"");
        var domain = [0,sampletotal];//range of numbers
        colorScale = d3.scale.quantile()
            .domain(domain)//[ -10 , 0, 10]
            .range(colors);
        minscale = colorScale.invertExtent(colors[1])[0];

        var fontwhite = 4;//number from end of color array to start doing white font
        maxblackfont = colorScale.invertExtent(colors[colors.length-fontwhite-1])[0];
    }

    //Returns the style that should be added to the Row's Sample Sub-Total based on the size it is
    //updateColors() must be called once before this function is called
    function determineStyleFromSampleSize(samplesize)
    {
        //Calculate the style based on the total sample size taken, and do a gradient color white->yellow->red
        //Swap the font color to white if too red
        var color = colorScale(samplesize);
        var fontcolor = 'black';
        if(samplesize < minscale && samplesize > 0)//Cap the lower bound
        {
            color = colors[1];
        }
        if(samplesize > maxblackfont)
        {
            fontcolor = 'white';
        }
        var style = "background: " + color + " !important;";
        style += "color: " + fontcolor + ";";
        return style;
    }

    //Error when getting DB info
    function processDBError() {
        OpenModalBox('Database Selection Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        endPause(false);
    }

    //Get the Database Information
    function ajaxGetDBInfo(data) {
        console.log(data);
        ajaxDB = $.ajax({
            url: configLocation,
            type: "post",
            data: JSON.stringify(data),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processDB,
            error: processDBError,
            timeout: 60*1000*30 //30 minutes
        });
    };

    //Called to start getting the database information
    this.getdbInfo = function(allquery,sampletype,db)
    {
        //Check that a sub-query is not happenning
        if(!subqueryStarted)
        {
            $('.samplebox').hide();//Hide the current samplebox if already displaying another set of data
            subqueryStarted = true;
            //ajax query to get:
            startPause();
            _allquery = allquery;
            DBrunning = db;
            ajaxGetDBInfo({'allquery':allquery,'db':db,'sampletype':sampletype});
        } else {
            errorFcn();//send release to upper class
        }
    };

    //Freeze the window, put button to cancel
    function startPause()
    {
        $('#pause').html('<span><h4>Getting Database Information</h4> <button type="button" id="endDBquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
        $('#endDBquery').on('click', function() {endPause(true);});
        $('#queryDB').attr('disabled', 'disabled');
    }

    //Unfreeze window, remove button
    function endPause(isCancel)
    {
        subqueryStarted = false;
        //Check if want to cancel the AJAX too
        if(isCancel)
        {
            ajaxDB.abort();
        }
        $('#pause').html('');
        $('#queryDB').removeAttr('disabled');
        errorFcn();
    }

    //SUBMIT the download request for all data
    function submitStocasticSample()
    {
        if(!subqueryStarted)
        {
            subqueryStarted = true;
            //Use long job background execution:

            //Gather the sample subtotals
            var samplenums = $('.samplesubtotal');
            var totalnums = $('.total');
            var samplenames = $('.sampletitle');
            var predsample = 0;
            var predtotal = 0;
            var allsample = {};
            var alltotal = {};
            for(var i=0; i<samplenums.length; i++)
            {
                var sampletitle = $(samplenames[i]).html();
                var samplesize = parseInt($(samplenums[i]).html().replace(new RegExp(",", 'g'),""));
                var totalsize = parseInt($(totalnums[i]).html().replace(new RegExp(",", 'g'),""));
                allsample[sampletitle] = samplesize;
                alltotal[sampletitle] = totalsize;
                predsample += samplesize;
                predtotal += totalsize;
            }
            var rollupallsample = {'type':titletype,'allsample':allsample,'alltotal':alltotal};
            var filename = "sample dataset.zip";//TODO: Improve this to be more descriptive
            var requestLong = {'job':3,'allquery':JSON.stringify(_allquery),'allsample':JSON.stringify(rollupallsample),'db':DBrunning,"filename":filename,'predtotal':predtotal,'predsample':predsample};

            //Callback from success of AJAX call
            function processSubmitLong(response) {
                //{"success":'true', "totalcount" : totalcount, "input": input};
                console.log(response);
                if(response['result'] == true)
                {
                    OpenModalBox('Successful Submition', 'Succesfully submitted your query.  Use the <b>Get Jobs Button</b> below to see status and get results.');
                } else {
                    OpenModalBox('Query Error', 'Error during submition on server.  Try again later or contact HMC Tech Group.');
                }
                subqueryStarted = false;
            }
            //Called when Ajax fails
            function processSubmitLongError() {
                OpenModalBox('Query Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
                subqueryStarted = false;
            }
            //Cycle through steps until result total is 0
            function ajaxSubmitLongProcess() {
                console.log(requestLong);//FOR DEBUG
                ajaxQuery = $.ajax({
                    url: scriptLongLocation,
                    type: "post",
                    data: JSON.stringify(requestLong),
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    success: processSubmitLong,
                    error: processSubmitLongError,
                    timeout: 1*60*1000 //1 minutes
                });
            }

            ajaxSubmitLongProcess();
        }        

    }


    //Run after DOM is created
    function setup()
    {
        //DEBUG:
        // var response = {
        //     issuccess : "success",
        //     data : [
        //     ["Tag", "Total"],
        //     ["Smoking", 12],
        //     ["Smoking", 5],
        //     ["Smoking", 123],
        //     ["ECig",98820],
        //     ["ECig",9882]
        //     ]
        // };
        // processDB(response);
        $('#submitsample').hide();
        $('#submitsample').on('click',submitStocasticSample);
        $('.samplebox').hide();
   }


}