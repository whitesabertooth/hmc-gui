//Written by DWS
//CLASS SubmitJob
//Constructer
//Requires: fileUploadLabel tag, label-to-load-csv class, submitQuery tag
// labelFile tag
var SubmitJob = function(dateClass)
{
    //Private variables:
    var fileUploadLabel;
    var file;
    var labelFile;
    var submitJobLocation = getUrl() + "/db/submitEstimate.json";
    var fileLoaded = false;
    var rawjsonfile ='';
    var jsondata = {};
    var ajaxSubmit;
    var submitting = false;
    var uploadform;
    var filedrag = new FileDragDrop('filedrag','fileUploadLabel',{accept:/\.[jJ][sS][oO][nN]$/,addedFile:FileSelected,errorFile:fileError,enableauto:false});

    //Constructor:
    $(document).ready(setup);

    //Private Functions:
    //Run this after Document Loaded
    function setup()
    {
        labelFile = $('#labelFile');

        //Submit Job function
        $('#submitQuery').on('click', Submit);
    }

    function fileError(file)
    {
        OpenModalBox("Error","Filename needs to be .json");
    }

    //Called when the submit button is pressed
    function Submit(e)
    {
        //Stop from submitting if already in the process of submitting
        if(fileLoaded && !submitting && dateClass.checkDate(true))//Allow it to show errors
        {
            var edate = (new Date(dateClass.getEnd()));
            edate = makeEndOfDay(edate);
            edate.setSeconds(edate.getSeconds()+1);//roll to next day because GNIP will not get the last minute requested
            jsondata.start= fixDateOffset(new Date(dateClass.getStart()));
            jsondata.end  = fixDateOffset(edate);
            jsondata.file = rawjsonfile;
            jsondata.filename = file.name;
            submitting = true;
            ajaxSubmitJob();
        } else {
            OpenModalBox("Error","File is not loaded, or running a submit, or bad date.");
        }
    }

    //Called when a different file has been selected
    function FileSelected(filein)
    {
        file = filein;
        var reader = new FileReader();

        reader.onload = FileLoaded;

        reader.readAsText(file);
    }

    //Called when successfully uploaded file
    function FileLoaded(e)
    {
        //Test if JSON
        if(isJSON(e.target.result))
        {
            fileLoaded = true;
            rawjsonfile = e.target.result;
            labelFile.html('(' + file.name + ' @ '+ formatDate(fixDateOffsetDate(new Date()),true,true) +')');
        } else {
            OpenModalBox('Bad JSON File','<h2>Bad formated JSON file</h2>','');
        }

        //Activate now that upload is complete
        filedrag.setDisable(false);
    }

    //Successful conpletion of AJAX
    function processSubmit(response) {
        endPause(false);
        if(response['status'] == 'success')
        {
            OpenModalBox('Successful Job Submited','<h3>Successfully submitted job</h3>','');
        } else {
            OpenModalBox('Error on Job Submit','<h3>Had an error while submitting job</h3>',response['error']);
        }
        submitting = false;
    }

    //Error conpletion of AJAX
    function processSubmitError(response) {
        endPause(false);
        OpenModalBox('Error on Job Submit','<h3>Had an error while submitting job</h3>','Cannot connect');
        submitting = false;
    }

    //Get the Database Information
    function ajaxSubmitJob() {
        console.log(jsondata);
        startPause();
        ajaxSubmit = $.ajax({
            url: submitJobLocation,
            type: "post",
            data: JSON.stringify(jsondata),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processSubmit,
            error: processSubmitError,
            timeout: 30*1000 //30 seconds
        });
    }

    //Freeze the window, put button to cancel
    function startPause()
    {
        $('#pause').html('<span><h4>Getting Database Information</h4> <button type="button" id="endDBquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
        $('#endDBquery').on('click', function() {endPause(true);});
        $('#submitQuery').attr('disabled', 'disabled');
    }

    //Unfreeze window, remove button
    function endPause(isCancel)
    {
        //Check if want to cancel the AJAX too
        if(isCancel)
        {
            ajaxSubmit.abort();
        }
        $('#pause').html('');
        $('#submitQuery').removeAttr('disabled');
    }

};
