//Written by DWS
//CLASS GetJobs
//Constructor
//Required element is #overlaytext as div at top of sub-window
var GetJobs2 = function()
{
    //Private variables:
    var getJobsLocation = getUrl() + "/db/getEstimates.json";
    var downloadJobsLocation = getUrl() + "/db/downloadEstimate.json";
    var acceptJobsLocation = getUrl() + "/db/submitAccept.json";
    var autoGetTime = 5*60*1000;//wait 5 minutes
    var specialBox;
    var box;
    var autoGetInterval;
    var ajaxGet;
    var jobstatusHead;
    var jobstatusBody;
    var checkJob;
    var runningAuto = false;
    var boxfadding = true;
    var jobidIdx;
    var jobnameIdx;

    $(document).ready(setup);

    //Private functions:
    function setup()
    {
        specialBox = document.getElementById('overlaytext');
        box = $('#overlaytext');
        jobstatusHead = $("#jobstatus thead");
        jobstatusBody = $("#jobstatus tbody");
        checkJob = $('#checkJob');

        //Start auto Get
        //autoGetInterval = setInterval(autoGet,autoGetTime);

        checkJob.on("click",startStopAuto);

        //Call the get 1 time on load
        //autoGet();
    }

    function toggleBox(){
        if(!boxfadding){
            //Slowly make it fade
            box.fadeOut(10000);//will make hidden after opacity gone
            boxfadding = true;
        } else {
            box.stop();//stop fading if performing
            specialBox.style.display = "block";
            specialBox.style.opacity = 1.0;
            boxfadding = false;
        }
    }

    function startStopAuto()
    {
        //If not started start time get
        if(runningAuto)
        {
            clearInterval(autoGetInterval);
            checkJob.html("<span><i class='fa fa-refresh txt-primary'></i></span>Start Auto Check Status");
            runningAuto = false;
        } else {
            autoGetInterval = setInterval(autoGet,autoGetTime);                
            autoGet();
            checkJob.html("<span><i class='fa fa-times-circle-o txt-danger'></i></span>Stop Auto Check Status");
            runningAuto = true;
        }
    }

    function autoGet(){
        box.html('Get Jobs...');
        toggleBox();
        ajaxGetJobs();
    }

    function createJobDownloadLink(job)
    {
        //Make the job a link to download
        return "<a href='" + downloadJobsLocation + "?job=" + job + "'>" + job + "</a>";
    }

    //Successful conpletion of AJAX
    function processJobs(allresponse) {
        console.log(allresponse);
        box.html('Successful.');
        toggleBox();
        var headarr = ["ID","Job", "Status", "From", "To", "Complete", "Size", "Estimate Hours", "Expire"];

        //Only with edit permissions
        if(window.editor == 1)
        {
            headarr.splice(0,0,"Accept");
        }
        //Make sure to get the Idx after adding Accept
        jobidIdx = headarr.indexOf('ID');
        jobnameIdx = headarr.indexOf('Job');
        var headstr = "";
        for(var i=0;i<headarr.length;i++)
        {
          headstr+="<th>"+headarr[i]+"</th>";
        }
        jobstatusHead.html(headstr);
      
        var datastr="";
        var data = allresponse["data"];
        var oneJobNotFinished = false;
        for(var idx=0;idx<data.length;idx++)
        {
            var response = data[idx];
            var dataarr = [];
            //Only with edit permissions
            if(window.editor == 1)
            {
                var straccept = "<button type='button' class='acceptbtn btn btn-primary btn-label-left'>"+
                                        "<i class='fa fa-cloud-download'></i>"+
                                    "</button>";
                if(response["status"] == "quoted")
                {
                    dataarr.push(straccept);
                }
                else
                {
                    dataarr.push("");
                }
            }
            function formatDateStr(dd)
            {
                //convert YYYYMMDDhhmm => YYYY-MM-DD hh:mm
                var year = dd.substring(0,4);
                var month = dd.substring(4,6);
                var day = dd.substring(6,8);
                var hour = dd.substring(8,10);
                var minute = dd.substring(10,12);
                var outstr = year+"-"+month+"-"+day+" "+hour+":"+minute;
                return outstr;
            }
            dataarr.push(response['jobid']);
            dataarr.push(createJobDownloadLink(response["title"]));
            dataarr.push(response["status"]);
            dataarr.push(formatDateStr(response["from"]));
            dataarr.push(formatDateStr(response["to"]));
            if(response["status"] == "quoted" || response["status"] == "delivered")
            {
                dataarr.push("100%");
            } else {
                dataarr.push(response["percentComplete"] + "%");
            }
            if(response["status"] == "quoted")//opened
            {
                dataarr.push(numberWithCommas(response["quote"]["estimatedActivityCount"]));
                dataarr.push(response["quote"]["estimatedDurationHours"]);
                dataarr.push(response["quote"]["expiresAt"]);
            }
            else if(response["status"] == "delivered")//opened
            {
                dataarr.push(numberWithCommas(response["result"]["activityCount"]));
                dataarr.push("");
                dataarr.push(response["result"]["expiresAt"]);
            } else {
                dataarr.push("");
                dataarr.push("");
                dataarr.push("");
                oneJobNotFinished = true;
            }
          
            datastr+="<tr>";
            for(var i=0;i<dataarr.length;i++)
            {
              datastr+="<td>"+dataarr[i]+"</td>";
            }
            datastr+="</tr>";
        }

        if(!oneJobNotFinished)
        {
            //All jobs are finished, so stop the autoGet
            startStopAuto();
        }

        jobstatusBody.html(datastr);

        $('.acceptbtn').on('click',function() {
            //Create AJAX submit
            var title = "Accept GNIP Estimate";
            var tr = $(this).parent().parent(); //Get the Table Row
            var jobname = $(tr).find('td:eq('+jobnameIdx+') a').html();
            var jobid = $(tr).find('td:eq('+jobidIdx+')').html();
            var body = "<label class='control-label'>Job ID =&nbsp;</label>" + jobid + "<br><label class='control-label'>Job Name =&nbsp;</label>" + jobname + "<br><label class='control-label'>Automatically upload GNIP data to Database after GNIP creates it?&nbsp;</label><input id='jobupload' type='checkbox' checked='true'/><br>";
            body += '<div id="uploadinfo">'+
            '<div class="row form-group">'+
              '<div class="col-sm-4">'+
                '<label class="control-label">Month</label>'+
                '<select id="jobmonth">'+
                  '<option value="01" selected="true">01</option>'+
                  '<option value="02">02</option>'+
                  '<option value="03">03</option>'+
                  '<option value="04">04</option>'+
                  '<option value="05">05</option>'+
                  '<option value="06">06</option>'+
                  '<option value="07">07</option>'+
                  '<option value="08">08</option>'+
                  '<option value="09">09</option>'+
                  '<option value="10">10</option>'+
                  '<option value="11">11</option>'+
                  '<option value="12">12</option>'+
                '</select>'+
              '</div>'+
              '<div class="col-sm-4">'+
                '<label class="control-label">Year</label>'+
                '<select id="jobyear">'+
                  '<option value="2010" selected="true">2010</option>'+
                  '<option value="2011">2011</option>'+
                  '<option value="2012">2012</option>'+
                  '<option value="2013">2013</option>'+
                  '<option value="2014">2014</option>'+
                  '<option value="2015">2015</option>'+
                  '<option value="2016">2016</option>'+
                  '<option value="2017">2017</option>'+
                  '<option value="2018">2018</option>'+
                  '<option value="2019">2019</option>'+
                  '<option value="2020">2020</option>'+
                '</select>'+
              '</div>'+
            '</div>'+
            '<div class="row form-group">'+
              '<div class="col-sm-6">'+
                '<label class="control-label">Project Name</label>'+
                '<input id="jobproject" type="text" value="Master" />'+
              '</div>'+
              '<div class="col-sm-8">'+
                'This is the string that is appended to the end of the directory name, eg: 201401_Master'+
              '</div>'+
            '</div>'+
            '<div class="row form-group">'+
              '<div class="col-sm-6">'+
                '<label class="control-label">Update Active Rules with this file:</label>'+
                '<select id="jobupdaterule">'+
                  '<option value="yes" selected="true">yes</option>'+
                  '<option value="no">no</option>'+
                '</select>'+
              '</div>'+
            '</div>'+
            '<div class="row form-group">'+
              '<div class="col-sm-4">'+
                '<label class="control-label">Run Download</label>'+
                '<select id="jobdownload">'+
                  '<option value="yes" selected="true">yes</option>'+
                  '<option value="no">no</option>'+
                '</select>'+
              '</div>'+
              '<div class="col-sm-8">'+
                'If yes, will run the Download after successful completion of upload.'+
                    '<br>If no, will stop after upload.'+
              '</div>'+
            '</div>'+
            '</div>'+
            '<div class="row form-group">'+
                '<div id="pause">'+
                '</div>'+
            '</div>';

            var footer = "<button type='button' id='acceptnowbtn' class='btn btn-primary btn-label-left'>"+
                                        "Accept the Estimate"+
                                    "</button>";

            OpenModalBox(title,body,footer);

            var queryactive = false;
            var ajaxGet;
            //Freeze the window, put button to cancel
            function startPause()
            {
                queryactive = true;
                //Remove OpenModalBox active close
                $('body').off('click', 'a.close-link');
                $('#pause').html('<span><h4>Submitting Information</h4> <button type="button" id="endDBquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
                $('#endDBquery').on('click', function() {endPause(true);});
            }

            //Unfreeze window, remove button
            function endPause(isCancel)
            {
                //Check if want to cancel the AJAX too
                if(isCancel)
                {
                    ajaxGet.abort();
                }
                $('#pause').html('');
                queryactive = false;
                //Add OpenModalBox active close
                $('body').on('click', 'a.close-link', function(e){
                    e.preventDefault();
                    CloseModalBox();
                });
            }

            $('#jobupload').on('change', function() {
                if($(this).prop("checked"))
                {
                    $('#uploadinfo').show();
                } else {
                    $('#uploadinfo').hide();
                }

            });

            $('#acceptnowbtn').on('click', function() {
                //jobproject, jobyear, jobdownload, jobmonth
                //previously set: jobid, jobname
                if(!queryactive)
                {
                    var jobproject = $('#jobproject').val();
                    var jobyear = ""+$('#jobyear').val();
                    var jobmonth = ""+$('#jobmonth').val();
                    var jobdownload = $('#jobdownload').val();
                    var jobupdaterule = $('#jobupdaterule').val();
                    var jobupload = $('#jobupload').prop( "checked");
                    startPause(false);
                    ajaxAcceptJob({'Project Name':jobproject,'GNIP Job ID':jobid,'jobname':jobname,'Year':jobyear,'Month':jobmonth,'run_download':jobdownload,'run_upload':jobupload,'update_active_rules':jobupdaterule})
                }
            });

            //Successful conpletion of AJAX
            function processAcceptJob(response) {
                console.log(response);
                endPause(false);
                if(response['issuccess'] == 'success')
                {
                    $('#pause').html("<h4>Successfully completed submission. Use Get Jobs below to see status.</h4>")
                } else {
                    $('#pause').html("<h4>Submission error: " + response['error'] + "</h4>");
                }
            }

            //Error completion of AJAX
            function processAcceptJobError(response) {
                endPause(false);
                $('#pause').html("<h4>Submission error: Talk to HMC Tech Group if this continues.</h4>");
            }
            //Get the Information
            function ajaxAcceptJob(data) {
                console.log(data);
                ajaxGet = $.ajax({
                    url: acceptJobsLocation,
                    type: "post",
                    dataType: "json",
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    success: processAcceptJob,
                    error: processAcceptJobError,
                    timeout: 2*60*1000 //2 minutes
                });
            }
        });
    }
  
    //Error completion of AJAX
    function processJobsError(response) {
        box.html('Failed.');
        startStopAuto();
        toggleBox();
    }
  
    //Get the Information
    function ajaxGetJobs() {
        ajaxGet = $.ajax({
            url: getJobsLocation,
            type: "get",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processJobs,
            error: processJobsError,
            timeout: 2*60*1000 //2 minutes
        });
    }
};
