//Written by DWS
function setupEstimates()
{
    "use strict";

    var dateclass = new DateStartEnd();
    var sjob = new SubmitJob(dateclass);
    var gjob = new GetJobs2();

    $(document).ready(function() {
        // Add Drag-n-Drop feature
        //WinMove();

        //DEMO START BUTTON
        $('#demo').on('click',function() {
            if(sideBarVisible())
            {
                $('.show-sidebar').click();//Shrink left side
            }
            $('#checkJob').click();//start download of Jobs for later

            //Start the tour now that the data is loaded
            var intro = introJs();
              intro.setOptions({
                steps: [
                  { 
                    intro: "This is a tutorial for the GNIP Twitter Estimates webpage."
                  },
                  {
                    element: "#fileinput",
                    intro: "Drag-n-Drop or click this box to select a GNIP Twitter JSON file (in the format [ {\"value\":\"<rule>\",\"tag\":\"<tag>\"},...]  ).  Note: If the file is not in a JSON format an error will pop-up."
                  },
                  {
                    element: "#daterange",
                    intro: "Select the date range (start and end inclusive) to estimate for."
                  },
                  {
                    element: "#submitQuery",
                    intro: "Submit Estimate to GNIP.  If GNIP has an issue with the format of the JSON an error will pop-up stating the issue."
                  },
	              {
	                element: "#checkJob",
	                intro: "Click this button to start checking the status of GNIP estimates.  Note: Estimates are only kept for a limited time by GNIP."
	              }
                ]
              });
              //intro.setOption('showButtons', false);
              //intro.setOption('showBullets', false);
              intro.setOption('doneLabel', 'See Jobs (will wait for Jobs to show up)').start().oncomplete(waitForJobs);
              console.log("started demo");
              $('#demo').hide();
        });


  });//end READY Function

	function waitForJobs()
	{
		if($("#jobstatus th").length == 0)
		{
			setTimeout(waitForJobs,500);//wait 0.5 seconds
		} else {
			seconddemo();
		}
	}

	function seconddemo()
	{
    var initnode = $("#jobstatus th");
    //Look if there is a first row to highlight instead of the column headings
		if($("#jobstatus tr").length > 0)
		{
			initnode = $("#jobstatus tr").eq(0).find("td");
		}
    var name1 = initnode.eq(1+window.editor)[0];//ADD OFFSET IF ACCEPT USER
    var status = initnode.eq(2+window.editor)[0];//ADD OFFSET IF ACCEPT USER
    var percent = initnode.eq(5+window.editor)[0];//ADD OFFSET IF ACCEPT USER
    var num = initnode.eq(6+window.editor)[0];//ADD OFFSET IF ACCEPT USER

		var intro = introJs();
		  intro.setOptions({
		  	steps: [
              {
                element: name1,
                intro: "This column has the GNIP estimate/download job name of what was submitted.  Clicking on the name will download the JSON submitted."
              },
              {
                element: status,
                intro: "The status of the GNIP estimate/download job. (estimating->quoted->creating->delivered)."
              },
              {
                element: percent,
                intro: "For estimating or creating status, this shows the percent complete (Note: it does not increase linearly, i.e. it can hold at 95%)."
              },
              {
                element: num,
                intro: "Number of Tweets estimated, or if status is delivered, then this is actual number of tweets."
              },
              {
                element: $("#jobstatus th").eq(0)[0],//Either the accept column or ID column
                intro: "If an authorized user, the first column has a button to accept estimates for download."
              }
            ]
          });
          //intro.setOption('showButtons', false);
          //intro.setOption('showBullets', false);
          //intro.start();
          intro.setOption('doneLabel', 'End of Tour').start().oncomplete(function() {$('.show-sidebar').click();});
          console.log("started demo 2");
    }
}