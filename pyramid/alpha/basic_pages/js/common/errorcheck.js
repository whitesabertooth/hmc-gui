//This file is used for handling errors

(function(){
	var submitaddress = "/db/submit_error.json";
	var alllog = [];
	window.onerror = function(error, url, line) {
	    alllog.push('ERR:'+error+' URL:'+url+' L:'+line);
	};

    var oldLog = console.log;
    console.log = function (message) {
        // DO MESSAGE HERE.
        oldLog.apply(console, arguments);//arguments
        alllog.push(JSON.stringify(message));
        //oldLog(message);
    };
    $('.openlog').on('click',function() {
    	OpenModalErrorBox("Submit Error to HMC Tech",
    		"Your email address: <input type='text' id='emailinput' width='50' /><br>Please add a message to describe the error that occurred or feedback.  HMC Tech will get back to you as soon as possible.<div width='100%'><textarea id='emailtext' size='4' style='width: 100%;'></textarea></div><div id='pause'></div>",
    		"<button type='button' class='btn btn-primary btnsubmit'>Submit</button><button type='button' class='btn btn-default btncancel'>Close</button>");
    	$('.btncancel').on('click',function() {
    		CloseModalErrorBox();
    	});
    	$('.btnsubmit').on('click',function() {
    		var pagedata = document.documentElement.innerHTML;
    		var submitdata = {inputtext: $('#emailtext').val(), email: $('#emailinput').val(), log: alllog, page: pagedata};

    		ajaxGetDBInfo(submitdata);
    	});

    });

    function processDB(result) {
    	endPause(false);
    	if(result.status == true)
    	{
	        $('#pause').html('<h4>Successfully Submitted am email to HMC Tech.</h4>')
    	} else {
	        $('#pause').html('<h4>Error submitting email.  Try again later or contact HMC Tech.</h4>')    		
    	}
    }

    //Error when getting DB info
    function processDBError() {
        
        endPause(false);
        $('#pause').html('<h4>Communication Error with Server.  Try again later or contact HMC Tech.</h4>')    		
    }

    //Get the Database Information
    function ajaxGetDBInfo(data) {
    	startPause();
        //console.log(data);
        ajaxDB = $.ajax({
            url: submitaddress,
            type: "post",
            data: JSON.stringify(data),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processDB,
            error: processDBError,
            timeout: 60*1000*30 //30 minutes
        });
    };

    //Freeze the window, put button to cancel
    function startPause()
    {
        $('#pause').html('<span><h4>Submitting Information</h4> <button type="button" id="endDBquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
        $('#endDBquery').on('click', function() {endPause(true);});
        $('.btnsubmit').attr('disabled', 'disabled');
        $('.btncancel').attr('disabled', 'disabled');
    }

    //Unfreeze window, remove button
    function endPause(isCancel)
    {
        subqueryStarted = false;
        //Check if want to cancel the AJAX too
        if(isCancel)
        {
            ajaxDB.abort();
        }
        $('#pause').html('');
        $('.btnsubmit').removeAttr('disabled');
        $('.btncancel').removeAttr('disabled');
    }



})();