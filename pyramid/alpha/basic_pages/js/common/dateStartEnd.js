//Written by DWS
//CLASS DateStartEnd
//Requires datestart and dateend tag being text fields
var DateStartEnd = function(options) {
    options = options || {};
    var localoptions = {
        makesame : options.makesame!==undefined ? options.makesame : true
    };

    //Private Variables:
    var datestart;
    var dateend;

    //Constructor
    $(document).ready(setupDatePeriod);

    //Private function
    function setupDatePeriod()
    {
        datestart = $('#datestart');
        dateend = $('#dateend');
        datestart.datepicker({
            defaultDate: "+lw",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            maxDate: '0',//set to +0 days from current date
            minDate: new Date('January 1, 2010 00:00:00'), //set to 1/2013
            onClose: function( selectedDate ) {
                if(true==localoptions.makesame)
                {
                    dateend.datepicker( "option", "minDate", selectedDate );
                    if(dateend.val() == '' || dateend.val() < selectedDate)
                    {
                        dateend.val(selectedDate);
                    }
                }
            }
        });
        dateend.datepicker({
            defaultDate: "+lw",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            maxDate: '0',//set to +0 days from current date
            minDate: new Date('January 1, 2010 00:00:00'), //set to 1/2013
            onClose: function( selectedDate ) {
                if(true==localoptions.makesame)
                {
                    if(datestart.val() == '')
                    {
                        datestart.val(selectedDate);
                    }
                }
            }
        });
    }

    //Public functions
    this.getStart = function() 
    {
        return datestart.val();
    };
    this.getEnd = function()
    {
        return dateend.val();
    };

    //If autoError == true, then display error in ModalBox
    //Returns True if no errors, otherwise false
    this.checkDate = function(autoError)
    {

        if(this.getStart() == '' || this.getEnd() == '')
        {
            OpenModalBox('Form Error', 'Bad date start/end: must be filled in');
            return false;
        } else {
            return true;
        }
    };
};
