//Written by DWS
function startDownload(csvdata,filename)
{
    var isSafari1 = navigator.vendor.indexOf("Apple")==0;
    var isSafari2 = /\sSafari\//.test(navigator.userAgent);
    var isSafari = isSafari1 && isSafari2;
    var outtype = "text/csv;charset=utf-8";
    outtype = "octet/stream";//DEBUG TEMP
    if(isSafari)
    {
        var uriContent = "data:application/octet-stream," + encodeURIComponent(csvdata);
        var newWindow = window.open(uriContent,"New Doc");
    } else {

        var blob = new Blob([csvdata], {type: outtype});
        saveAs(blob, filename);
    }

}

//Assumre duration is in minute decimal
function formatMinuteDuration(duration)
{
    var hrduration = Math.floor(duration/60);
    var minduration = decimalFloor(duration - hrduration*60,0);//round decimals off
    return (pad(hrduration,2) + ":" + pad(minduration,2));
}

function formatDate(olddate,addTime,removeDate)
{
    var newdate = (new Date(olddate));//reversefixDateOffsetDate
    var datestr = "";
    if(!removeDate)
    {
        datestr = newdate.getFullYear() + '-' + pad((newdate.getMonth()+1).toString(),2) + '-' + pad(newdate.getDate().toString(),2);
    }
    if(addTime)
    {
        if(datestr.length > 0)
        {
            datestr += " ";
        }
        datestr += pad(newdate.getHours().toString(),2) + ":"
            + pad(newdate.getMinutes().toString(),2) + ":"
            + pad(newdate.getSeconds().toString(),2);
    }
    return datestr;
}

function decimalFloor(num,place)
{
    //If zero or less, then just floor the number as int
    if(place <= 0)
    {
        return Math.floor(num);
    }
    var placenum = Math.pow(10,place);
    return Math.floor(num*placenum)/placenum;
}


function getUrl()
{
    //Figure out what the location is for AJAX call
    var pathArray = location.href.split( '/' );
    var protocol = pathArray[0];
    var host = pathArray[2];
    return protocol + '//' + host;
}

//Pad a string with zeros at beginning
function pad(n, width, z) {
  var zz = z || '0';
  var nn = n + '';
  return nn.length >= width ? nn : new Array(width - nn.length + 1).join(zz) + nn;
}

//Returns ISO string with timesize fix
function fixDateOffset(olddate)
{
    var tzoffset = olddate.getTimezoneOffset() * 60000; //offset in milliseconds
    return (new Date(olddate-tzoffset)).toISOString();
}

//Return Date string with timesize fix
function fixDateOffsetDate(olddate)
{
    var tzoffset = olddate.getTimezoneOffset() * 60000; //offset in milliseconds
    return (new Date(olddate-tzoffset));
}

//Returns Date with timesize fix
function reversefixDateOffsetDate(olddate)
{
    var tzoffset = olddate.getTimezoneOffset() * 60000 * 2; //offset in milliseconds
    return (new Date(olddate.getTime()+tzoffset));
}

//Returns Date with timesize fix
function inversefixDateOffsetDate(olddate)
{
    var tzoffset = olddate.getTimezoneOffset() * 60000; //offset in milliseconds
    return (new Date(olddate.getTime()+tzoffset));
}

function makeEndOfDay(olddate)
{
    olddate.setHours(23);
    olddate.setMinutes(59);
    olddate.setSeconds(59);
    olddate.setMilliseconds(999);
    return olddate;
}

function makeStartOfDay(olddate)
{
    olddate.setHours(0);
    olddate.setMinutes(0);
    olddate.setSeconds(0);
    olddate.setMilliseconds(0);
    return olddate;
}

function numberWithCommas(x) {
    if(x == undefined || typeof x == 'string' || x.length == 0)
    {
        return x;
    }
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function simple_moving_averager(period) {
    var nums = [];
    return function(num) {
        nums.push(num);
        if (nums.length > period)
            nums.splice(0,1);  // remove the first element of the array
        var sum = 0;
        for (var i in nums)
            sum += nums[i];
        var n = period;
        if (nums.length < period)
            n = nums.length;
        return(sum/n);
    };
}
 //EXAMPLE OF SMA above
//             var sma3 = simple_moving_averager(3);
//             var sma5 = simple_moving_averager(5);
//             var data = [1,2,3,4,5,5,4,3,2,1];
//             for (var i in data) {
//                 var n = data[i];
//                 // using WSH
//                 WScript.Echo("Next number = " + n + ", SMA_3 = " + sma3(n) + ", SMA_5 = " + sma5(n));
//             }



function isJSON (jsonString){
    try {
        var o = JSON.parse(jsonString);

        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
        // so we must check for that, too.
        if (o && typeof o === "object" && o !== null) {
            return true;
        }
    }
    catch (e) { }

    return false;
}


//Freeze the screen, then add timeout of 1 second to allow time for screen to freeze
function screenHold(title,body,footer,fcn)
{
    OpenModalBox(title,body,footer);
    $('body').off('click', 'a.close-link');
    setTimeout(function(){ fcn(); screenUnHold();},1000);
}

//Unfreeze the screen
function screenUnHold()
{
    CloseModalBox();
    $('body').on('click', 'a.close-link', function(e){
        e.preventDefault();
        CloseModalBox();
    });
}

var invertdict = function (obj) {
  var new_obj = {};

  for (var prop in obj) {
    if(obj.hasOwnProperty(prop)) {
      new_obj[obj[prop]] = prop;
    }
  }

  return new_obj;
};

function convertMonthNumToText(month)
{
    var monthconvert = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    return monthconvert[month-1];
}

function convertMonth(temp)
{
    //Change date to better format
    for(var ii=0;ii<temp.length;ii++)
    {
        if(temp[ii] != "_id")
        {
            var year = temp[ii].slice(0,4);
            var month = temp[ii].slice(4,6);
            temp[ii] =  convertMonthNumToText(parseInt(month)) + "-" + year;
        }
    }

}

function addStyle(tt)
{
    //Creates a new css style, and returns what created.
    var css = tt,
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');

    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);
    return style;
}

var is_same = function(array1,array2) {
    if(array1 === null || array2 === null) {return false;}
    var compare = (array1.length == array2.length) && array1.every(function(element, index) {
        return element === array2[index]; 
    });
    return compare;
};

//Escape regular expressions to be a string that is searchable using regular expression
function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

var parseQueryString = function() {

    var str = window.location.search;
    var objURL = {};

    str.replace(
        new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
        function( $0, $1, $2, $3 ){
            objURL[ $1 ] = $3;
        }
    );
    return objURL;
};

//--------------------------------------------------------
//Used for moving average and d3 checks for Count pages
//--------------------------------------------------------
var d3_numeric = function(x) {
    return !isNaN(x);
};

var d3sum = function(array, f) {
  var s = 0,
      n = array.length,
      a,
      i = -1;
  if (arguments.length === 1) {
     // zero and null are equivalent
    while (++i < n) if (d3_numeric(a = +array[i])) s += a; 
  } else {
    while (++i < n) if (d3_numeric(a = +f.call(array, array[i], i))) s += a;
  }
  return s;
};

var movingWindowAvg = function (arr, step) {  
    return arr.map(function (_, idx) { 
        var wnd = arr.slice(Math.max(0,idx - step), Math.min(idx + step + 1,arr.length)); 
        var result = d3sum(wnd) / wnd.length; if (isNaN(result)) { result = _; }
        return result; 
    });
};
//--------------------------------------------------------
//--------------------------------------------------------
