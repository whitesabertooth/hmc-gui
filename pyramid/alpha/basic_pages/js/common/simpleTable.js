//Written by DWS
//Option for 2 step query: for counts, then query for sample
//Make a history section of count queries with total number and ability to sample (always random) or download the query
var SimpleTable = function(input)
{
    //var that = this;
    var tablehistoryHead = null;
    var tablehistoryBody = null;
    var tableid = "";
    var columns = [];
    var data = [];
    constructor(input);

    function constructor(input)
    {
        tableid = input.tableid || "";
        columns = input.columns || [];
        data = input.data || [];
        $(document).ready(setup);
    }


    function setupID()
    {
        tablehistoryHead = $("#" + tableid + " thead");
        tablehistoryBody = $("#" + tableid + " tbody");
    }

    //Private functions:
    function setup()
    {
        setupID();
        displayhead();
        displaybody();
    }

    this.update = function(newdata)
    {
        data = newdata;
        displaybody();
    }

    this.updateColumns = function(newdata)
    {
        columns = newdata;
        displayhead();
    }

    this.updateOne = function(newdata,row,col)
    {
        $('#' + tableid + ' tr:eq(' + row + ') > td:eq(' + col + ')').html(newdata);
    }

    this.appendOne = function(newdata)
    {
        //Requires newdata to be an array the size of the table
        $('#' + tableid).append(displayRow(newdata));
    }

    this.getOne = function(newdata,row,col)
    {
        return $('#' + tableid + ' tr:eq(' + row + ') > td:eq(' + col + ')').html();
    }

    function getCell(column, row) 
    {
        var column = $('#' + column).index();
        var row = $('#' + row)
        return row.find('td').eq(column);
    }

    this.toMat = function()
    {
        //converts the table to array of array string
        var columns = $('#' + tableid + ' thead th').map(function() {
          // This assumes that your headings are suitable to be used as
          //  JavaScript object keys. If the headings contain characters 
          //  that would be invalid, such as spaces or dashes, you should
          //  use a regex here to strip those characters out.
          return $(this).text();
        }).get();
        var tableObject = [columns];
        $('#' + tableid + ' tbody tr').map(function(i) {
          var row = [];
         
          // Find all of the table cells on this row.
          $(this).find('td').each(function(i) {
            // Determine the cell's column name by comparing its index
            //  within the row with the columns list we built previously.
            //var rowName = columns[i];
         
            // Add a new property to the row object, using this cell's
            //  column name as the key and the cell's text as the value.
            row.push($(this).text());
          });
         
          // Finally, return the row's object representation, to be included
          //  in the array that $.map() ultimately returns.
          tableObject.push(row);
         
        // Don't forget .get() to convert the jQuery set to a regular array.
        });
        return tableObject;//array of arrays
    }

    this.toDict = function()
    {
        //converts the table to array of array string
        var columns = $('#' + tableid + ' thead th').map(function() {
          // This assumes that your headings are suitable to be used as
          //  JavaScript object keys. If the headings contain characters 
          //  that would be invalid, such as spaces or dashes, you should
          //  use a regex here to strip those characters out.
          return $(this).text();
        });
        var tableObject = $('#' + tableid + ' tbody tr').map(function(i) {
          var row = {};
         
          // Find all of the table cells on this row.
          $(this).find('td').each(function(i) {
            // Determine the cell's column name by comparing its index
            //  within the row with the columns list we built previously.
            var rowName = columns[i];
         
            // Add a new property to the row object, using this cell's
            //  column name as the key and the cell's text as the value.
            row[rowName] = $(this).text();
          });
         
          // Finally, return the row's object representation, to be included
          //  in the array that $.map() ultimately returns.
          return row;
        // Don't forget .get() to convert the jQuery set to a regular array.
        }).get();
        return tableObject;//array of arrays
    }

    function displayhead()
    {
        //Columns
        var headarr = columns;
        var headstr = "";
        for(var i=0;i<headarr.length;i++)
        {
            if(typeof headarr[i] === "string")
            {
                headstr+="<th>"+headarr[i]+"</th>";
            } else {
                //Assume map of {'name','style'}
                var style = "";
                var name = headarr[i].name;
                if("style" in headarr[i])
                {
                    style = " style='" + headarr[i].style + "'";
                }
                headstr+="<th" + style + ">" + name + "</th>";
            }
        }
        if(tablehistoryHead == null)
        {
            setupID();
        }
        tablehistoryHead.html(headstr);
    }

    function displayRow(datai)
    {
        var datastr = "";
        datastr+="<tr>";
        for(var j=0;j<datai.length;j++)
        {
          datastr+="<td>"+datai[j]+"</td>";
        }
        datastr+="</tr>";
        return datastr;
    }

    function displaybody()
    {
        var datastr = "";
        for(var i=0;i<data.length;i++)
        {
            datastr+=displayRow(data[i]);
        }
        if(tablehistoryBody == null)
        {
            setupID();
        }
        tablehistoryBody.html(datastr);
    }

    this.clear = function()
    {
        tablehistoryHead.html("");
        tablehistoryBody.html("");
    }

}

SimpleTable.prototype.formatDate = function(olddate)
{
    var newdate = (new Date(olddate));//reversefixDateOffsetDate
    return newdate.getUTCFullYear() + '-' + pad((newdate.getUTCMonth()+1).toString(),2) + '-' + pad(newdate.getUTCDate().toString(),2)
};

SimpleTable.prototype.formatNumber =function(x) {
    return numberWithCommas(x);
};