//Written by DWS
//CLASS MonthStartEnd
//Requires datestart and dateend tag being text fields
var MonthStartEnd = function(options) {
    options = options || {};
    var localoptions = {
        makesame : options.makesame!==undefined ? options.makesame : true
    };

    //Private Variables:
    var datestart;
    var dateend;

    //Constructor
    $(document).ready(setupDatePeriod);

    //Private function
    function setupDatePeriod()
    {
        datestart = $('#datestart');
        dateend = $('#dateend');
        datestart.MonthPicker({
            Button: false,
            MaxMonth: '0',//set to +0 days from current date
            MinMonth: '1/2010', //set to 1/2013
            OnAfterChooseMonth: function( selectedDate ) {
                if(true===localoptions.makesame)
                {
                    dateend.MonthPicker( "option", "MinMonth", selectedDate );
                    if(dateend.val() === '' || dateend.val() < selectedDate)
                    {
                        dateend.val(selectedDate);
                    }
                }
            }
        });
        dateend.MonthPicker({
            Button: false,
            MaxMonth: '0',//set to +0 days from current date
            MinMonth: '1/2010', //set to 1/2013
            OnAfterChooseMonth: function( selectedDate ) {
                if(true===localoptions.makesame)
                {
                    if(datestart.val() === '')
                    {
                        datestart.val(selectedDate);
                    }
                }
            }
        });
    }

    //Public functions
    this.getStart = function() 
    {
        return datestart.val();
    };
    this.getStartDate = function ()
    {
        var tmpmonth = this.getStart();
        if(tmpmonth === '')
        {
            return '';
        }
        var monthargs = tmpmonth.split(/\//);
        if(monthargs.length < 2)
        {
            return '';
        }
        var tmpdate = new Date(monthargs[0] + '/1/' + monthargs[1]);
        return tmpdate;
    };
    this.getEnd = function()
    {
        return dateend.val();
    };
    this.getEndDate = function ()
    {
        var tmpmonth = this.getEnd();
        if(tmpmonth === '')
        {
            return '';
        }
        var monthargs = tmpmonth.split(/\//);
        if(monthargs.length < 2)
        {
            return '';
        }
        var tmpdate = new Date(parseInt(monthargs[1]),parseInt(monthargs[0]),0);
        //tmpdate.setMonth(monthargs[0]);
        //tmpdate.setYear(monthargs[1]);
        //tmpdate.setDate(1);
        //tmpdate.setDate(-1);//minus 1 day to roll to last day of previous month
        return tmpdate;
    };

    //If autoError == true, then display error in ModalBox
    //Returns True if no errors, otherwise false
    this.checkDate = function(autoError)
    {

        if(this.getStart() === '' || this.getEnd() === '')
        {
            OpenModalBox('Form Error', 'Bad date start/end: must be filled in');
            return false;
        } else {
            return true;
        }
    };
};
