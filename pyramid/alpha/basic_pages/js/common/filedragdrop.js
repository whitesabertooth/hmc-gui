var FileDragDrop = function(tagid,fileid,options)
{
    options = options || {};
    options.accept = options.accept || "";
    options.addedFile = options.addedFile || undefined;
    options.errorFile = options.errorFile || undefined; 
    options.selectedclass = options.selectedclass || undefined; 
    options.disableauto = !( (options.disableauto === false) || false ); //default true
    options.enableauto = !( (options.enableauto === false) || false ); //default true 

    var disabled = false;

    //Call setup
    $(document).ready(setup);

    this.setDisable = function(isdis)
    {
        disabled = (isdis===true);
    };

    //Functions
    function setup()
    {
        var filedrag = document.getElementById(tagid);
        filedrag.addEventListener('dragover',FileDragHover,false);
        filedrag.addEventListener('dragleave',FileDragHover,false);
        filedrag.addEventListener('drop',FileSelectHandler,false);
        $('#'+tagid).on('click',function(e) {
            e.preventDefault();
            e.stopPropagation();
            $('#'+fileid).click();
        });
        $('#'+fileid).on('change',function() {
            verifyFile(this.files[0]);
        });
        $('#'+fileid).hide();

    }

    function verifyFile(file)
    {
        //verify filename is *.csv
        if(file.name.match(options.accept))
        {
            if(typeof options.addedFile !== "undefined")
            {
                options.addedFile(file);
            }
            if(typeof options.selectedclass !== "undefined")
            {
                $('.'+options.selectedclass).html("("+file.name+")");
            }
        } else {
            if(typeof options.errorFile !== "undefined")
            {
                options.errorFile(file);
            }
        }
        $('#'+fileid).val('');//reset so they can use the same file again and it will load again
        if(options.disableauto)
        {
            disabled = false;
        }

    }

    // file drag hover
    function FileDragHover(e) {
        e.stopPropagation();
        e.preventDefault();
        var filedrag = document.getElementById(tagid);
        filedrag.className = (e.type == "dragover" ? "hover" : "");
        e.dataTransfer.dropEffect = 'copy';
    }

    // file selection
    function FileSelectHandler(e) {
        //Disable only while processing, so only 1 at a time
        if(!disabled)
        {
            if(options.disableauto)
            {
                disabled = true;
            }
            // cancel event and hover styling
            FileDragHover(e);

            // fetch FileList object
            var files = e.dataTransfer.files; // FileList object.
            verifyFile(files[0]);
        }
    }
};