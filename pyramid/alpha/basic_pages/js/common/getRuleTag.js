//Written by DWS
var setupProcessDB = function(completeFcn,options)
{
    $(document).ready(setup);

    //Database unique variables
    this.DB_tags;
    this.DB_rules;
    this.DB_rules2tags;
    this.DB_rule2time;
    this.DB_minDate;
    this.DB_maxDate;
    this.DBrunning;
    var that = this;
    var configLocation = "/db/config.json";
    var historyLocation = "/db/rulehistory.json";
    this.opts = options || {"rulehistory":true};

    //Successful conpletion of AJAX for DB Info
    function processDB(response) {
        // config mappings
        if(response.success == 'true')
        {
            that.DB_tags = response.data.tags;
            that.DB_rules = response.data.rules;
            that.DB_rules2tags = response.data.rules_tags;
            //Set DB name
            that.DBrunning = $('#queryDBAll').val();
            $('#dbname').html(that.DBrunning);


            //Continue to get rule history
            if(that.opts.rulehistory)
            {
                ajaxGetDBHistory(that.DBrunning);
            } else {
                endPause(false);
                $('.querybox').show();
                completeFcn();
            }

        } else {
            OpenModalBox('Database Selection Error', 'No data returned from about the database selected.');
            endPause(false);
        }
    }

    //Error when getting DB info
    function processDBError() {
        OpenModalBox('Database Selection Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        endPause(false);
    }

    //Get the Database Information
    function ajaxGetDBInfo(dbname) {
        console.log(dbname);
        ajaxDB = $.ajax({
            url: configLocation,
            type: "post",
            data: JSON.stringify({"db":dbname}),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processDB,
            error: processDBError,
            timeout: 30*1000 //30 seconds
        });
    };

    //Successful conpletion of AJAX for DB Info
    function processDBHistory(response) {
        // config mappings
        if(response.success == 'true')
        {
            that.DB_rulehistory = response.data;
            that.DB_minDate = new Date('December 1, 2011 00:00:00');
            that.DB_maxDate = new Date();

        } else {
            OpenModalBox('Database History Retrival Error', 'No data returned from request for rule history about the database selected.');
        }
        endPause(false);
        $('.querybox').show();
        completeFcn();
    }

    //Error when getting DB info
    function processDBErrorHistory() {
        OpenModalBox('Database History Retrival Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        endPause(false);
        $('.querybox').show();
        completeFcn();
    }

    //Get the Database Information
    function ajaxGetDBHistory(dbname) {
        console.log(dbname);
        ajaxDB = $.ajax({
            url: historyLocation,
            type: "post",
            data: JSON.stringify({"db":dbname}),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processDBHistory,
            error: processDBErrorHistory,
            timeout: 30*1000 //30 seconds
        });
    };

    //Called to start getting the database information
    function getdbInfo()
    {
        //ajax query to get:
        startPause();
        ajaxGetDBInfo($('#queryDBAll').val());
    }

    //Freeze the window, put button to cancel
    function startPause()
    {
        $('#pauseAll').html('<span><h4>Getting Database Information</h4> <button type="button" id="endDBquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
        $('#endDBquery').on('click', function() {endPause(true);});
        $('#queryDBAll').attr('disabled', 'disabled');
    }

    //Unfreeze window, remove button
    function endPause(isCancel)
    {
        //Check if want to cancel the AJAX too
        if(isCancel)
        {
            ajaxDB.abort();
        }
        $('#pauseAll').html('');
        $('#queryDBAll').removeAttr('disabled');
    }

    function setup()
    {
        $('#queryDBAll').on('change.All', function(e) {
             if($('#queryDBAll').val() != 'Select...')
             {
                 getdbInfo();
             }
        });
        if($('#queryDBAll').val() != 'Select...')
        {
            setTimeout(getdbInfo,100);
        }
        $('.querybox').hide();

    }
}