//Written by DWS
var heatMap = function(addedId,dbdata,rowLabel,colLabel,colors,hcrow,hccol,domain,legenddomain,tag_to_rule,DAY_MONTH,options) {
  options = options || {};
  var colHeaderSize = 15;
  var rowHeaderSize = options.rowHeadSize || 200;
  var buffersize = 50;
  var maxlength = options.rowHeadMax || 25;
  var shiftlabelright = options.shiftlabel || 0;
  var currowvis = hcrow;
  var legendsize = 150;
  var customIdx = 3; //Index in the Selection of Custom
  var minLongRefresh = 500;//minimum number of rows before a long refresh is needed
  var margin = { top: legendsize, right: 10, bottom: 50, left: rowHeaderSize };
  var cellSize=12;
  var col_number=hccol.length;
  var row_number=hcrow.length;
  var width = cellSize*col_number; // - margin.left - margin.right,
  var height = cellSize*row_number; // - margin.top - margin.bottom,
  var legendElementWidth = cellSize*6.5; 
  var legendFullWidth = legendElementWidth * legenddomain.length + shiftlabelright;
  var colorBuckets = colors.length;
  var currentTags = Object.keys(tag_to_rule);
  //Expand width if smaller than legend size:
  width = Math.max(width,legendFullWidth);
  $('#chart'+addedId).html("");//delete previous

  $('#chart'+addedId).css("height",height + margin.top + margin.bottom + buffersize);
  processData(dbdata);

  function processData(data) {
    var colorScale = d3.scale.quantile()
        .domain(domain)//[ -10 , 0, 10]
        .range(colors);
    var minscale = colorScale.invertExtent(colors[1])[0];
    var svg = d3.select("#chart"+addedId).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        ;
    var rowSortOrder=false;
    var colSortOrder=false;
    var lastsortC="";
    var lastsortR="";
    var rowLabels = svg.append("g")
        .selectAll(".rowLabelg")
        .data(rowLabel)
        .enter()
        .append("text")
        .text(function (d,i) {
          //DEBUG:d = i + ";" + d;
          if(d.length>maxlength)
          {
            return d.substring(0,maxlength)+"...";//crop the text
          } else {
            return d;
          }
          
        })
        .attr("x", 0)
        .attr("y", function (d, i) { return i * cellSize; })//same thing as rowLabel.indexOf(d)
        .style("text-anchor", "end")
        .attr("transform", "translate(-6," + cellSize / 1.5 + ")")
        .attr("class", function (d,i) { return "visible rowLabel rowLabel"+addedId+" mono r"+i+ addedId;} ) 
        .on("mouseover", function(d) {d3.select(this).classed("text-hover",true);})
        .on("mouseout" , function(d) {d3.select(this).classed("text-hover",false);})
        .on("click", function(d,i) {
          if(lastsortR==d){
            rowSortOrder=!rowSortOrder;
          }else{
            rowSortOrder=false;
          }
          lastsortR=d; 
          d3.selectAll(".rowLabel.text-sort").classed("text-sort",false);//remove all sorted highlights
          d3.select(this).classed("text-sort",true);//add new sorted highlight
          d3.select("#order"+addedId).property("selectedIndex", customIdx).node().focus();
          
          if(row_number > minLongRefresh)
          {
            screenHold("Processing Row Changes","Redrawing row, please wait...","",function() {sortbylabel("r",i,rowSortOrder);});
          } else {
            sortbylabel("r",i,rowSortOrder);
          }
        })
        .append("svg:title")//add hover text
        .text(function(d) {return d;})
        ;

    var colLabels = svg.append("g")
        .selectAll(".colLabelg")
        .data(colLabel)
        .enter()
        .append("text")
        .text(function (d) { return d; })
        .attr("x", 0)
        .attr("y", function (d, i) { return hccol.indexOf(i) * cellSize; })
        .style("text-anchor", "left")
        .attr("transform", "translate("+cellSize/2 + ",-" + colHeaderSize + ") rotate (-90)")
        .attr("class",  function (d,i) { return "colLabel colLabel"+addedId + " mono c"+i+ addedId;} )
        .on("mouseover", function(d) {d3.select(this).classed("text-hover",true);})
        .on("mouseout" , function(d) {d3.select(this).classed("text-hover",false);})
        .on("click", function(d,i) {
          if(lastsortC==d)
          {
            colSortOrder=!colSortOrder;
          } else {
            colSortOrder=false;
          }
          lastsortC=d;
          d3.selectAll(".colLabel.text-sort").classed("text-sort",false);//remove all sorted highlights
          d3.select(this).classed("text-sort",true);//add new sorted highlight
          d3.select("#order"+addedId).property("selectedIndex", customIdx).node().focus();
          if(row_number > minLongRefresh)
          {
            screenHold("Processing Row Changes","Redrawing row, please wait...","",function() {sortbylabel("c",i,colSortOrder);});
          } else {
            sortbylabel("c",i,colSortOrder);
          }
        })
        ;

    //WORKING ON ADDING OVERLAY TO DISABLE CLICKING
    //var strdiv = "<div class='overlaybox' style='top: 0px; left: 0px; width: " + width + "px; height: " + height + "px;' id='overlaydiv' transform='translate(" + margin.left + "," + margin.top + ")'></div>";
    //.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    //$('#chart'+addedId).append(strdiv)
    //$('#overlaydiv').on("click",function(e){e.preventDefault();})

    var heatMap = svg
          .append("g")
          .attr("class","g3")
          .selectAll(".cellg")
          .data(data,function(d){return d.row+":"+d.col;})
          .enter()
          .append("rect")
          .attr("x", function(d) { return hccol.indexOf(d.col) * cellSize; })
          .attr("y", function(d) { return hcrow.indexOf(d.row) * cellSize; })
          .attr("class", function(d){return "visible rect cell"+addedId + " cell-border cr"+hcrow.indexOf(d.row)+addedId+" cc"+(d.col)+addedId;})
          .attr("width", cellSize)
          .attr("height", cellSize)
          .style("fill", function(d) { 
            //Handle if not zero, but less than first quartile, so bump up to 1st quartile
            if(d.value < minscale && d.value > 0)
            {
              return colors[1];
            } else {
              return colorScale(d.value); 
            }
          })
          .on("mouseover", function(d){
                 //highlight text
                 d3.select(this).classed("cell-hover",true);
                 d3.selectAll(".rowLabel"+addedId).classed("text-highlight",function(r,ri){ return ri==(hcrow.indexOf(d.row));});
                 d3.selectAll(".colLabel"+addedId).classed("text-highlight",function(c,ci){ return ci==(d.col);});
          
                 //Update the tooltip position and value
                 d3.select("#tooltip"+addedId)
                   .style("left", (d3.event.offsetX+10+20) + "px")
                   .style("top", (d3.event.offsetY-10) + "px")
                   .select("#value"+addedId)
                   //DEBUG information if needed in future:
                   //+"\nrow-col-idx:"+d.col+","+d.row+"\ncell-xy "+this.x.baseVal.value+", "+this.y.baseVal.value  ...(" + d.row + "," + hcrow.indexOf(d.row) +")
                   .html("ROW:"+rowLabel[hcrow.indexOf(d.row)]+"<br/>"+DAY_MONTH+"="+colLabel[d.col]+"<br/>COUNT="+SimpleTable.prototype.formatNumber(d.value));
                 //Show the tooltip
                 d3.select("#tooltip"+addedId).classed("hidden1", false);
          })
          .on("mouseout", function(){
                 d3.select(this).classed("cell-hover",false);
                 d3.selectAll(".rowLabel"+addedId).classed("text-highlight",false);
                 d3.selectAll(".colLabel"+addedId).classed("text-highlight",false);
                 d3.select("#tooltip"+addedId).classed("hidden1", true);
          })
          ;

    var legend = svg.selectAll(".legend"+addedId)
        .data(legenddomain)
        .enter().append("g")
        .attr("class", "legend"+addedId);
   
    legend.append("rect")
      .attr("x", function(d, i) { return legendElementWidth * i+shiftlabelright; })
      .attr("y", -legendsize+(cellSize*2))
      .attr("width", legendElementWidth)
      .attr("height", cellSize)
      .style("fill", function(d, i) { return colorScale(d); });
   
    legend.append("text")
      .attr("class", "mono")
      .text(function(d) { return SimpleTable.prototype.formatNumber(d); })
      .attr("width", legendElementWidth)
      .attr("x", function(d, i) { return legendElementWidth * i+shiftlabelright; })
      .attr("y", -legendsize + (cellSize*4));

    // Change ordering of cells

    function sortbylabel(rORc,i,sortOrder){
         var t = svg.transition().duration(0);
         var log2r=[];
         var log2row = [];
         var log2col = [];
         var sorted; // sorted is zero-based index
         d3.selectAll(".c"+rORc+i+addedId+".visible") 
           .filter(function(ce){
              log2r.push(ce.value);
              log2row.push(ce.row);
              log2col.push(ce.col);
            })
         ;
         if(rORc=="r"){ // sort log2ratio of a gene
           sorted =d3.range(log2col.length).sort(function(a,b){ if(sortOrder){ return log2r[b]-log2r[a];}else{ return log2r[a]-log2r[b];}});//d3.range(col_number)
           t.selectAll(".cell"+addedId+".visible")
             .attr("x", function(d) { return sorted.indexOf(log2col.indexOf(d.col)) * cellSize; })
             ;
           t.selectAll(".colLabel"+addedId)
            .attr("y", function (d, i) { return sorted.indexOf(i) * cellSize; })
           ;
         }else{ // sort log2ratio of a contrast
            //var sorted = currowvis.slice();//copy before sort
           sorted =d3.range(log2row.length).sort(function(a,b){if(sortOrder){ return log2r[b]-log2r[a];}else{ return log2r[a]-log2r[b];}});//d3.range(row_number)
           t.selectAll(".cell"+addedId+".visible")
            .attr("y", function(d) { 
              return sorted.indexOf(log2row.indexOf(d.row)) * cellSize; 
            })
            ;
           t.selectAll(".rowLabel"+addedId+".visible")
            .attr("y", function (d, i) { 
              return sorted.indexOf( log2row.indexOf( hcrow[rowLabel.indexOf(d)] ) ) * cellSize; 
            })
           ;
         }
    }
    d3.select("#order"+addedId).on("change",function(){
      var val = this.value;
      if(row_number > minLongRefresh)
      {
        screenHold("Processing Row Changes","Redrawing row, please wait...","",function() {order(val);});
      } else {
        order(val);
      }
    });

    $('.filtercolumn'+addedId).on('change', function() {
        //Get array of elements selected
        var vals = $( this).find(":selected").map(function(){ return this.value }).get();

        screenHold("Processing Row Changes","Redrawing row, please wait...","",function() {showRows(vals);});
    });

    function showRows(tags)
    {
      //Check for no tags selected, then show all
      if(tags.length == 0)
      {
        d3.selectAll(".cell"+addedId)
          .classed("hidden",false)
          .classed("visible",true)
          ;
        d3.selectAll(".rowLabel"+addedId)
          .classed("hidden",false)
          .classed("visible",true)
          ;
        currowvis = hcrow;
        //Set order back to row col sort
        $("#order"+addedId).val("probecontrast");
        order("probecontrast");
        currentTags = Object.keys(tag_to_rule);
        return;
      }
      currentTags = tags;
      //hide all
      d3.selectAll(".cell"+addedId)
        .classed("hidden",true)
        .classed("visible",false)
        ;
      d3.selectAll(".rowLabel"+addedId)
        .classed("hidden",true)
        .classed("visible",false)
        ;
      currowvis = [];
      //Show only rules identified
      var yidx = 0;
      $.each(tags, function(i,tag)
      {
        if(tag_to_rule[tag] != undefined)//some tags have no rules
        {
          $.each(tag_to_rule[tag], function(i,row)
          {
            //This maps the tag_to_rule.row from hcrow.value => hcrow.index, 
            // which .cell and .rowLabel are identified as hcrow.index
            var row = hcrow.indexOf(row);
            //Make sure the row hasn't been removed
            if(row >= 0)
            {
              currowvis.push(row);

              d3.selectAll(".cell"+addedId + ".cr"+row+addedId)
                .classed("hidden",false)
                .classed("visible",true)
                ;
              d3.selectAll(".rowLabel"+addedId + ".r"+row+addedId)
                .classed("hidden",false)
                .classed("visible",true)
                ;
            }
          });
        }
      });
      //Always order by tags
      $("#order"+addedId).val("tags");
      order('tags');
    }

    function order(value){
     var log2r=[];
     var log2row = [];
     var log2col = [];
     var sorted; // sorted is zero-based index
     d3.selectAll(".cr"+currowvis[0]+addedId+".visible") 
       .filter(function(ce){
          //log2r.push(ce.value);
          //log2row.push(ce.row);
          log2col.push(ce.col);
        })
     ;
     d3.selectAll(".cc1"+addedId+".visible") 
       .filter(function(ce){
          //log2r.push(ce.value);
          log2row.push(ce.row);
          //log2col.push(ce.col);
        })
     ;
     d3.selectAll(".text-sort").classed("text-sort",false);//remove all sorted highlights
     if(value=="hclust"){
      var t = svg.transition().duration(0);
      t.selectAll(".cell"+addedId+".visible")
        .attr("x", function(d) { return log2col.indexOf(d.col) * cellSize; })
        .attr("y", function(d) { return log2row.indexOf(d.row) * cellSize; })
        ;

      t.selectAll(".rowLabel"+addedId+".visible")
        .attr("y", function (d, i) { return i * cellSize; })//hcrow.indexOf(i)
        ;

      t.selectAll(".colLabel"+addedId)
        .attr("y", function (d, i) { return i * cellSize; })
        ;

     }else if (value=="probecontrast"){
      var t = svg.transition().duration(0);
      t.selectAll(".cell"+addedId+".visible")
        .attr("x", function(d) { return log2col.indexOf(d.col) * cellSize; })
        .attr("y", function(d) { return log2row.indexOf(d.row) * cellSize; })
        ;

      t.selectAll(".rowLabel"+addedId+".visible")
        .attr("y", function (d, i) { return log2row.indexOf( hcrow[rowLabel.indexOf(d)] ) * cellSize; })
        ;

      t.selectAll(".colLabel"+addedId)
        .attr("y", function (d, i) { return i * cellSize; })
        ;

     }else if (value=="probe"){
      var t = svg.transition().duration(0);
      t.selectAll(".cell"+addedId+".visible")
        .attr("y", function(d) { return log2row.indexOf(d.row) * cellSize; })
        ;

      t.selectAll(".rowLabel"+addedId+".visible")
        .attr("y", function (d, i) { return log2row.indexOf( hcrow[rowLabel.indexOf(d)] ) * cellSize; })
        ;
     }else if (value=="contrast"){
      var t = svg.transition().duration(0);
      t.selectAll(".cell"+addedId+".visible")
        .attr("x", function(d) { return log2col.indexOf(d.col) * cellSize; })
        ;
      t.selectAll(".colLabel"+addedId)
        .attr("y", function (d, i) { return i * cellSize; })
        ;
     }else if (value=="tags"){
      var t = svg.transition().duration(0);
      //use currowvis that is an array of hcrow.index; 
      //so d.row and rowLabel.d needs to be converted to hcrow.index before looking up
      t.selectAll(".cell"+addedId+".visible")
        .attr("y", function(d) { return currowvis.indexOf(hcrow.indexOf(d.row)) * cellSize; })
        ;

      t.selectAll(".rowLabel"+addedId+".visible")
        .attr("y", function (d, i) { return currowvis.indexOf( rowLabel.indexOf(d) ) * cellSize; })
        ;
     }
    }
    // NOT WORKING YET
    // var sa=d3.select(".g3")
    //     .on("mousedown", function() {
    //         if( !d3.event.altKey) {
    //            d3.selectAll(".cell-selected"+addedId).classed("cell-selected",false).classed("acell-selected",false);
    //            d3.selectAll(".rowLabel"+addedId).classed("text-selected",false);
    //            d3.selectAll(".colLabel"+addedId).classed("text-selected",false);
    //         }
    //        var p = d3.mouse(this);
    //        sa.append("rect"+addedId)
    //        .attr({
    //            rx      : 0,
    //            ry      : 0,
    //            class   : "selection",
    //            x       : p[0],
    //            y       : p[1],
    //            width   : 1,
    //            height  : 1
    //        })
    //     })
    //     .on("mousemove", function() {
    //        var s = sa.select("rect"+addedId+".selection");
        
    //        if(!s.empty()) {
    //            var p = d3.mouse(this),
    //                d = {
    //                    x       : parseInt(s.attr("x"), 10),
    //                    y       : parseInt(s.attr("y"), 10),
    //                    width   : parseInt(s.attr("width"), 10),
    //                    height  : parseInt(s.attr("height"), 10)
    //                },
    //                move = {
    //                    x : p[0] - d.x,
    //                    y : p[1] - d.y
    //                }
    //            ;
        
    //            if(move.x < 1 || (move.x*2<d.width)) {
    //                d.x = p[0];
    //                d.width -= move.x;
    //            } else {
    //                d.width = move.x;       
    //            }
        
    //            if(move.y < 1 || (move.y*2<d.height)) {
    //                d.y = p[1];
    //                d.height -= move.y;
    //            } else {
    //                d.height = move.y;       
    //            }
    //            s.attr(d);
        
    //                // deselect all temporary selected state objects
    //            d3.selectAll('.cell-selection'+addedId+'.cell-selected'+addedId).classed("cell-selected"+addedId, false).classed("acell-selected",false);
    //            d3.selectAll(".text-selection"+addedId+".text-selected").classed("text-selected",false);

    //            d3.selectAll('.cell'+addedId).filter(function(cell_d, i) {
    //                if(
    //                    !d3.select(this).classed("cell-selected"+addedId) && 
    //                        // inner circle inside selection frame
    //                    (this.x.baseVal.value)+cellSize >= d.x && (this.x.baseVal.value)<=d.x+d.width && 
    //                    (this.y.baseVal.value)+cellSize >= d.y && (this.y.baseVal.value)<=d.y+d.height
    //                ) {
        
    //                    d3.select(this)
    //                    .classed("cell-selection"+addedId, true)
    //                    .classed("cell-selected"+addedId, true).classed("acell-selected",true);

    //                    d3.select(".r"+(cell_d.row)+addedId)
    //                    .classed("text-selection"+addedId,true)
    //                    .classed("text-selected",true);

    //                    d3.select(".c"+(cell_d.col)+addedId)
    //                    .classed("text-selection"+addedId,true)
    //                    .classed("text-selected",true);
    //                }
    //            });
    //        }
    //     })
    //     .on("mouseup", function() {
    //           // remove selection frame
    //        sa.selectAll("rect"+addedId+".selection").remove();
        
    //            // remove temporary selection marker class
    //        d3.selectAll('.cell-selection'+addedId).classed("cell-selection"+addedId, false);
    //        d3.selectAll(".text-selection"+addedId).classed("text-selection"+addedId,false);
    //     })
    //     .on("mouseout", function() {
    //        if("tagName" in d3.event.relatedTarget && d3.event.relatedTarget.tagName=='html') {
    //                // remove selection frame
    //            sa.selectAll("rect"+addedId+".selection").remove();
    //                // remove temporary selection marker class
    //            d3.selectAll('.cell-selection'+addedId).classed("cell-selection"+addedId, false);
    //            d3.selectAll(".rowLabel"+addedId).classed("text-selected",false);
    //            d3.selectAll(".colLabel"+addedId).classed("text-selected",false);
    //        }
    //     })
    //     ;
  }//end data load
}