//Written by DWS
function setupReHydrate()
{

    var dbgetsamplejob = new GetReHydrateJobs();
    var scriptLongLocation = getUrl() + '/db/submitLongJob.json';
    var jobnum = 5;
    $(document).ready(setup);        

    function setup()
    {
        $("#submitReHydrate").on("click",checkSubmit);
    }


    function checkSubmit()
    {
        var status = true;
        var errormsg = "";

        var inputfile = $("#fileselectinput").val();
        var outputfile = $("#fileselectoutput").val();
        var testexp = /^[H|h|J|j]:[\\|\/].*\.[c|C][s|S][v|V]$/;

        if(!inputfile.match(testexp))
        {
            errormsg += "Input needs to be H: or J: drive path of a .csv file.\n";
            status = false;
        }

        if(!outputfile.match(testexp))
        {
            errormsg += "Output needs to be H: or J: drive path of a .csv file.\n";
            status = false;
        }

        if(!status)
        {
            OpenModalBox("Form Error",errormsg);
            return;
        }

        submitJob(inputfile,outputfile);
    }

    function submitJob(inputfile,outputfile)
    {
        var requestLong = {'job':jobnum,'inputpath':inputfile,'outputpath':outputfile};

        //Callback from success of AJAX call
        function processSubmitLong(response) {
            console.log(response);
            if(response['result'] == true)
            {
                OpenModalBox('Successful Submition', 'Succesfully submitted your query.  Use the <b>Get Jobs Button</b> below to see status and get results.');
            } else {
                OpenModalBox('Error', 'Error during submition on server.  Try again later or contact HMC Tech Group.');
            }
            subqueryStarted = false;
        }
        //Called when Ajax fails
        function processSubmitLongError() {
            OpenModalBox('Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
            subqueryStarted = false;
        }
        //Cycle through steps until result total is 0
        function ajaxSubmitLongProcess() {
            console.log(requestLong);//FOR DEBUG
            ajaxQuery = $.ajax({
                url: scriptLongLocation,
                type: "post",
                data: JSON.stringify(requestLong),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: processSubmitLong,
                error: processSubmitLongError,
                timeout: 1*60*1000 //1 minutes
            });
        }

        ajaxSubmitLongProcess();
    }
 }