//Written by DWS
function ruleSetup()
{
    var ruletag = new setupProcessDB(gotDB);
    var scriptLocation = "/db/addruletag.json";
    var subqueryStarted = false;

    $(document).ready(setup);

    function setup()
    {
        //Add rule adding button if has permission
        if(window.editor == 1)
        {
            $('.add-new-rule').show();
            $('.add-new-rule').on('click', function() {
                OpenModalBox('Add New Rule','Enter a Rule and Tag and click submit.<br>Rule:<textarea id="ruleid" style="width: 100%; border: 1px; border-style: solid; resize: vertical;" /><br>Tag:&nbsp;<select id="tagid"/>','<button type="button" id="addrule" class="btn btn-primary"><i class="fa fa-upload"></i>&nbsp;Add New Rule</button><div id="infosub"></div>')

                var secondstr = "";
                //Add a dummy value
                var DUMMY_VALUE = '-SELECT-';
                secondstr += "<option value='"+DUMMY_VALUE+"' selected>-SELECT-</option>";
                for (var atag in ruletag.DB_tags)
                {
                    secondstr += "<option value='"+ruletag.DB_tags[atag]+"'>("+atag+") " + ruletag.DB_tags[atag] + "</option>";
                }
                $("#tagid").html(secondstr);

                $('#addrule').on('click', function() {
                    if(!subqueryStarted)
                    {
                        var rule = $("#ruleid").val();
                        var tag = $("#tagid").val();
                        if(rule.length == 0 || tag == DUMMY_VALUE)
                        {
                            displayError("Input Error","Need a rule and tag to submit.");
                            return;
                        }
                        subqueryStarted = true;

                        submitRule(rule,tag);
                    }
                });
            });
        }
        //DEMO START BUTTON
        $('#demo').on('click',firstdemo);
    }

    //DEMO START BUTTON
    function firstdemo() 
    {
        if(sideBarVisible())
        {
            $('.show-sidebar').click();//Shrink left side
        }
        $('#checkJob').click();//start download of Jobs for later

        //Make transparent, but save to remove later
        tempstyle = addStyle(".introjs-helperLayer {background-color: transparent !important;}");

        //Start the tour now that the data is loaded
        var intro = introJs();
          intro.setOptions({
            steps: [
              { 
                intro: "This is a tutorial for the Rule Management webpage."
              },
              {
                element: "#dbselect",
                intro: "Select the Database to query (currently only twitter has data)."
              },
              {
                element: "#dbselect",
                intro: "After the page loads it will automatically download the latest Tags and Rules from the selected database (you will see a red spinning button while loading)."
              }
            ]
          });
          //intro.setOption('showButtons', false);
          //intro.setOption('showBullets', false);
          intro.setOption('doneLabel', 'Show Rules').start().oncomplete(seconddemo);
          console.log("started demo");
          $('#demo').hide();
    }

    //used for introjs transparent switch
    var tempstyle;

    function seconddemo()
    {
        //Remove transparent introjs
        var head = document.head || document.getElementsByTagName('head')[0];
        head.removeChild(tempstyle);

        var addrule = '.count-to-csv';
        //handle if add new rule is shown then highlight it
        if(window.editor == 1)
        {
            addrule = '.add-new-rule';
        }

        //Start the tour now that the data is loaded
        setTimeout(function() {
            var intro = introJs();
              intro.setOptions({
                steps: [
                  {
                    element: '.count-to-csv',
                    intro: "Click to download a CSV of the current Rules and Tags."
                  },
                  {
                    element: '.beauty-table-to-csv-all',
                    intro: "Click to download a JSON of the current Active Rules in the GNIP format."
                  },
                  {
                    element: addrule,
                    intro: "If an authorized user, a button will be shown to manually add a Rule to the database."
                  },
                  { 
                    element: '#ruletable_filter',
                    intro: "You can search all the columns in the table for a string."
                  },
                  { 
                    element: '#ruletable_info',
                    intro: "This describes how many rows are in the table.  If filtered by a search it will say how many filtered."
                  },
                  { 
                    element: '#ruletable_paginate',
                    intro: "Rules are sectioned into different pages.  Click to see more."
                  },
                  { 
                    element: '#ruletable_length',
                    intro: "Change the number of rules to show per-page."
                  },
                  {
                    element: $(".DTFC_Cloned thead tr th").eq(0)[0],
                    intro: "This column has highlighted rows where the Rule is currently Active (i.e. it was in the request for the most recent month that data was pulled from GNIP)."
                  },
                  {
                    element: '.filtercolumn',
                    intro: "Users can filter the table based on selecting this drop down as highlighted (X) or empty.  If multiple filters are set, then rules have to have all set correctly to be shown."
                  },
                  {
                    element: '.s2_with_tag',
                    intro: "Filter the rules by selecting the a certain set of tags."
                  },
                  {
                    element: '.search_rule',
                    intro: "Can also filter by matching a string to the rule text."
                  },
                  {
                    element: $(".dataTables_scrollHeadInner thead tr th").eq(3)[0],
                    intro: "All the YYYY-MM columns have highlights in them if there is data for that rule in the database for that month."
                  }
                ]
              });
              //intro.setOption('showButtons', false);
              //intro.setOption('showBullets', false);
              //intro.start();
              intro.setOption('doneLabel', 'End of Tour').start().oncomplete(function() {$('.show-sidebar').click();});
              console.log("started demo a");
        },100);

    }




    function displayError(head,msg)
    {
        var output = "<h4>"+head+"</h4>";
        output+="<h5>"+msg+"</h5>";
        $('#infosub').html(output)
    }

    function submitRule(rule,tag)
    {
        var requestLong = {'db':ruletag.DBrunning,'rule':rule,'tag':tag};

        //Callback from success of AJAX call
        function processSubmitLong(response) {
            console.log(response);
            if(response['status'] == 'success')
            {
                displayError('Successful Added', 'Succesfully submitted the following rule with the new ID:<br>Rule='+rule+'<br>Tag='+tag+'<br>ID='+response['newid']);
            } else {
                displayError('Error', 'Error during submition on server.  Try again later or contact HMC Tech Group.');
            }
            subqueryStarted = false;
        }
        //Called when Ajax fails
        function processSubmitLongError() {
            displayError('Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
            subqueryStarted = false;
        }
        //Cycle through steps until result total is 0
        function ajaxSubmitLongProcess() {
            console.log(requestLong);//FOR DEBUG
            var ajaxQuery = $.ajax({
                url: scriptLocation,
                type: "post",
                data: JSON.stringify(requestLong),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: processSubmitLong,
                error: processSubmitLongError,
                timeout: 30*1000 //30 seconds
            });
        }

        ajaxSubmitLongProcess();
    }

    function gotDB()
    {
        //Got the information from the database
        var dataSet = [];
        var ruledata = ruletag.DB_rulehistory.slice(); //make copy
        var headerrow = ruledata[0].slice();//make copy
        ruledata.shift();//remove header row
        var ruleidx = headerrow.indexOf("_id");
        var activeidx = headerrow.indexOf("Active");
        headerrow.splice(ruleidx,1);//Remove rule
        headerrow.splice(activeidx,1);//Remove rule

        //Before adding anything back in, change all dates from YYYYMM => YYYY-MM
        for(var row=0; row<headerrow.length; row++)
        {
            var year = headerrow[row].substring(0,4);
            var month = headerrow[row].substring(4,6);
            headerrow[row] = year + "-" + month;
        }

        headerrow.unshift("Active","Tag","Rule");//Add to front
        dataSet.push(headerrow);
        for(var row=0;row<ruledata.length; row++)
        {
            var active = ruledata[row][activeidx];
            var value = ruledata[row][ruleidx];
            ruledata[row].splice(ruleidx,1);//remove rule - at end, so do first (no order change)
            ruledata[row].splice(activeidx,1);//remove rule - at beginning so do last (otherwise it changes the order)
            var key = ruletag.DB_rules[value];
            var newline = [];
            newline.push(active ? "X" : "&nbsp;");//Add Active
            var tagnum = ruletag.DB_rules2tags[value];
            var tagname = ruletag.DB_tags[tagnum];
            if(tagname != undefined)
            {
                newline.push("("+tagnum+") "+tagname);
                newline.push("("+value+") "+key);
                for(var col=0;col<ruledata[row].length; col++)
                {
                    newline.push(ruledata[row][col] ? "X" : "&nbsp;");//Append all dates to newline
                }
                dataSet.push(newline);
            } else {
                console.log("Error with the following rule value and key:");
                console.log(value);
                console.log(key);
            }
        }
        setupRuleTable(dataSet); 

        $('.count-to-csv').on('click',function() {
            downloadRules();
        })
        $('#demo').show();//allow demo to happen
    } 

    /*var dataSet = [
        ['Active','Tag','Rule','9/15','10/15','11/15','12/15','1/16'],
        ['X','smoking', 'smoke','X','X','X',' ','X'],
        ['&nbsp;','smoking', 'cigar','&nbsp;','&nbsp;','X','X','X'],
        ['X','ecig', 'cigar ','X','&nbsp;','&nbsp;','X','X']
    ];*/


    function setupRuleTable(dataSet) 
    {


        var emptyFilter = "Filter...";
        var dt = undefined;
        var tagidx;
        var ruleidx;
        var activeidx;
        var rulewidth = "300px";

        setupData(dataSet);

        function createJsonFromTable()
        {
            function unescapeHTML(encoded)
            {
                //Converts &amp; and &nbsp; back to original.
                var elem = document.createElement('textarea');
                elem.innerHTML = encoded;
                var decoded = elem.value;
                return decoded;
            }
            function stripnum(value)
            {
                //Remove the "(x) " from the front
                var endidx = value.indexOf("\)");
                endidx+=2;//add for space and skip )
                var newvalue = value.substring(endidx);

                newvalue = unescapeHTML(newvalue);
                return JSON.stringify(newvalue);
            }
            //Make sure that the data table is created
            if(dt != undefined)
            {
                var jsonSet = "[\n";
                dt.rows().every(function(rowidx,tableloop,rowloop) {
                    var row = this.data();
                    if(row[0] == "X")
                    {
                        var jsonData = '{"value":'+stripnum(row[2])+',"tag":'+stripnum(row[1])+'},\n';
                        jsonSet += jsonData;
                    }
                });
                //remove last ,\n => \n
                jsonSet = jsonSet.substring(0,jsonSet.length-2) + "\n";

                jsonSet += "]\n";
                startDownload(jsonSet,"master_" + ruletag.DBrunning + "_rules.json");
            }
        }

        function createHeaderTR(headarr,tags)
        {
            var headstr = "<tr>";
            var secondHeadstr = "<tr class='secondhr'>";
            for(var i=0;i<headarr.length;i++)
            {
                var classname = "";
                var secondstr = "";
                if(i == ruleidx)
                {
                    classname = " class='ruleclass'";
                    //This is a rule - allow search
                    secondstr = '<input style="width:' + rulewidth + '" class="search_rule" type="text" placeholder="Search '+headarr[i]+'" />';
                }
                else if(i == tagidx)
                {
                    classname = " class='tagclass'";
                    //Style height is used to make the header second row small
                    secondstr = '<select style="height: 30px" class="s2_with_tag filtercolumn" multiple>';
                    var atag;
                    for (var atagidx = 0; atagidx < tags.length; atagidx++)
                    {
                        atag = tags[atagidx];
                        secondstr += "<option>" + atag + "</option>";
                    }
                    secondstr += "</select>";
                }
                else
                {
                    //This is a date or Active
                    secondstr = '<select class="filtercolumn">';
                    secondstr += "<option selected>" + emptyFilter + "</option>";
                    secondstr += "<option>&nbsp;</option>";
                    secondstr += "<option>X</option>";
                    secondstr += "</select>";
                }
                secondHeadstr+="<th" + classname + ">"+secondstr+"</th>";
                headstr+="<th" + classname + ">"+headarr[i]+"</th>";
            }
            headstr+="</tr>";
            secondHeadstr+="</tr>";
            return headstr + secondHeadstr;
        }

        //Create a new array of only the unique values in an array
        function findUnique(arr)
        {
            function onlyUnique(value, index, self) { 
                return self.indexOf(value) === index;
            }

            return arr.filter( onlyUnique ); // returns ['a', 1, 2, '1']
        }

        //Input: array of arrays, returns the column index of the sub-array as a new array
        function getColumn(arr,colidx)
        {
            var newarr = [];
            for(var i=0; i<arr.length; i++)
            {
                newarr.push(arr[i][colidx]);
            }
            return newarr;
        }


        function createBody(dataSet)
        {
            var bodystr = "";
            for(var row=0;row<dataSet.length;row++)
            {
                bodystr += "<tr>";
                for(var colidx=0;colidx<dataSet[row].length;colidx++)
                {
                    var className = "";
                    var data = dataSet[row][colidx];
                    if(data == "X")
                    {
                        //this.data("");
                        if(colidx == activeidx)
                        {
                            className = " class='activeColumn'";
                        } else {
                            className = " class='selectedColumn'";
                        }
                    }
                    bodystr += "<td" + className + ">" + data + "</td>";
                }
                bodystr += "</tr>";
            }
            return bodystr;

        }

        function setupData(dataSet)
        {
            var headerrow = dataSet.slice(0,1)[0];//copy the first row
            tagidx = headerrow.indexOf('Tag');
            ruleidx = headerrow.indexOf('Rule');
            activeidx = headerrow.indexOf('Active');
            dataSet.shift();//remove first row

            var tagarr = getColumn(dataSet,tagidx);
            var tags = findUnique(tagarr);

            //Setup header for table
            var inittable = "<table class='table table-border' id='ruletable'><thead></thead><tbody></tbody></table>";
            $("#initruletable").html(inittable);
            $('#ruletable > thead').html(createHeaderTR(headerrow,tags));
            $('#ruletable > tbody').html(createBody(dataSet));
            var allColumnsExceptTagRule = [];
            for(var i=0; i<headerrow.length;i++) {
                //remove all visible columns
                if(i!=tagidx && i!=ruleidx)
                {
                    allColumnsExceptTagRule.push(i);//Make everything
                }
            }

            //Create DataTable
            dt = $('#ruletable').DataTable({
                "dom": "<'row'<'col-sm-6'f><'col-sm-6 text-right'ipl><'clearfix'>>rt",//resiZe,Reorder,Scroller
                //"columnDefs": [
                     //{"className" : "tagclass", "targets" : tagidx},
                     //{"className" : "ruleclass", "targets" : ruleidx},
                     //{"width" : "125px", "targets" : "_all"},
                //],
                "pagingType": "bootstrap",
                "autoWidth":false,
                //"data": dataSet,
                "ordering": false,
                "paging": true,
                "language": {
                    "search": "Global Search&nbsp;&nbsp;"
                },
                "scrollY" : "800px",//"500px", 
                "scrollX" : true, 
                "scrollCollapse": true,
                "fixedColumns":   {
                    "leftColumns": 3
                },
                "destroy":true
            });

            //Turn on Select2 capability
            $('.s2_with_tag').select2({placeholder: "Select Tags"});

            // //Add class if X in cell
            // dt.cells().every(function() {
            //     var colidx = this.index().column;
            //     if(colidx == tagidx || colidx == ruleidx)
            //     {
            //         return;
            //     }
            //     var data = this.data();
            //     if(data == "X")
            //     {
            //         //this.data("");
            //         var className = "";
            //         if(colidx == activeidx)
            //         {
            //             className = "activeColumn";
            //         } else {
            //             className = "selectedColumn";
            //         }
            //         $(this.node()).addClass(className);
            //     }
            // });

            // Apply the search
            $( '.search_rule' ).on( 'keyup change', function () {
                var that = dt.column(ruleidx);
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );


            //Look for changes in column filters
            $('.filtercolumn').on('change', function() {
                //debugger;
                //Get array of elements selected
                var vals = $( this).find(":selected").map(function(){ return this.value }).get();
                if(!(vals instanceof Array))
                {
                    vals = [vals];
                }
                //Find column in datatable
                var col = $(this).closest("th");
                var that = dt.column(col.index());
                if(vals.length == 0 || (vals.length == 1 && vals[0] == emptyFilter))
                {
                    //Selected empty, so remove filter
                    that.search("").draw();
                    return;
                }
                //Append all values together by |
                var valstr = vals.join("|");
                valstr = valstr.replace(/\(/,"\\(")
                valstr = valstr.replace(/\)/,"\\)")
                //Perform a regular expression search
                that.search(valstr,true,false,false).draw();
            });

            $('.beauty-table-to-csv-all').on('click.DT', function(e){
                e.preventDefault();
                createJsonFromTable(); 
            });
        }
    }

    function downloadRules()
    {
        var data = [];
        var header = ["Rule","Rule Number","Tag","Tag Number"];
        data.push(header);
        for(var ii in ruletag.DB_rules)
        {
            var newline = [];
            var ruleName = ruletag.DB_rules[ii];
            var ruleNum = ii;
            var tagNum = ruletag.DB_rules2tags[ruleNum];
            var tagName = ruletag.DB_tags[tagNum];
            newline.push(ruleName+"");
            newline.push(ruleNum+"");
            newline.push(tagName+"");
            newline.push(tagNum+"");

            data.push(newline);
        }
        var newdate = new Date();//get now
        var datestr = newdate.getUTCFullYear() + '-' + pad((newdate.getUTCMonth()+1).toString(),2) + '-' + pad(newdate.getUTCDate().toString(),2);

        var savefilename = "rule_tag_"+datestr+".csv";
        var csv = new csvWriter();
        var csvdata = csv.arrayToCSV(data);
        var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
        saveAs(blob, savefilename);
    }

}