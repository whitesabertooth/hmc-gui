function setupTopicModelView()
{

	var params = parseQueryString();
	var filepath = "input/lda.json";
	if(params['fileid'] != undefined)
	{
		filepath = getUrl() + '/db/getfile?fileid=' + params['fileid'];
	}
	var vis = new LDAvis("#lda", filepath);
}