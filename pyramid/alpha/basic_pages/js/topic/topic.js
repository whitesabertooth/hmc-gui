//Written by DWS
function setupTopicModel()
{


    //Constants:
    var scriptLongLocation = getUrl() + '/db/submitLongJob.json';
    var fileselected = null;
    var currentfileid = null;
    var curheadlist = null;
    var DUMMY_VALUE = "-SELECT-";

    //CLASS GetJobs
    //Constructor
    //Required element is #overlaytext as div at top of sub-window
    $(document).ready(setup);

    var GetJobs = function()
    {
        //Private variables:
        var getJobsLocation = getUrl() + "/db/getLongJob.json";
        var autoGetTime = 5*60*1000;//wait 5 minutes
        var specialBox;
        var box;
        var autoGetInterval;
        var ajaxGet;
        var jobstatusHead;
        var jobstatusBody;
        var checkJob;
        var runningAuto = false;
        var boxfadding = true;

        $(document).ready(setup);
  
        //Private functions:
        function setup()
        {
            specialBox = document.getElementById('overlaytext');
            box = $('#overlaytext');
            jobstatusHead = $("#jobstatus thead");
            jobstatusBody = $("#jobstatus tbody");
            checkJob = $('#checkJob');

            //Start auto Get when clicked
            checkJob.on("click",startStopAuto);
        }

        function toggleBox(){
            if(!boxfadding){
                //Slowly make it fade
                box.fadeOut(10000);//will make hidden after opacity gone
                boxfadding = true;
            } else {
                box.stop();//stop fading if performing
                specialBox.style.display = "block";
                specialBox.style.opacity = 1.0;
                boxfadding = false;
            }
        }

        function startStopAuto()
        {
            //If not started start time get
            if(runningAuto)
            {
                clearInterval(autoGetInterval);
                checkJob.html("<span><i class='fa fa-refresh txt-primary'></i></span>Start Auto Check Status");
                runningAuto = false;
            } else {
                autoGetInterval = setInterval(autoGet,autoGetTime);                
                autoGet();
                checkJob.html("<span><i class='fa fa-times-circle-o txt-danger'></i></span>Stop Auto Check Status");
                runningAuto = true;
            }
        }

        function autoGet(){
            box.html('Get Jobs...');
            toggleBox();
            ajaxGetJobs();
        }

        //Successful conpletion of AJAX
        function processJobs(allresponse) {
            box.html('Successful.');
            toggleBox();
            
            //Returns {
                //'builds': [
                //   {
                    //  'number', 
                    //  'data' {
                    //       'parameters': {
                        //     'query', 'downloadfileid','samplefileid','startdate','enddate'
                        //    } , 
                        //  'duration', 'estimatedDuration', 'timestamp', 'result', 'building' 
                        //} 
                    //} 
                //]}
            // Retrieve actions->[parameters->[{name,value}]]  (array of name/value pair), convert to dictionary 'parameters'        
            // Retrieve duration (number)
            // Retrieve estimatedDuration (number)
            // Retrieve timestamp (Number)
            // Retrieve result (SUCCESS,FAIL,UNSTABLE)
            // Retrieve building (true/false)

            function formatDate(olddate)
            {
                var newdate = (new Date(olddate));//reversefixDateOffsetDate
                return newdate.getUTCFullYear() + '-' + pad((newdate.getUTCMonth()+1).toString(),2) + '-' + pad(newdate.getUTCDate().toString(),2)
            }

            function formatLink(oldlink,number)
            {
                if(oldlink === '')
                {
                    return '';
                }
                return "<a href=" + urllink + oldlink + ">Link</a>"+ number;
            }
            function formatTopic(oldlink)
            {
                if(oldlink === '')
                {
                    return '';
                }
                return "<a href=" + viewurllink + oldlink + ">View</a>";
            }

            console.log(allresponse);
            var headarr = ["","#","Status", "Date Executed", "Description", "Execution Time (s)", "Input File", "Model File", "Topic File", "View Topic"];
            var headstr = "";
            for(var i=0;i<headarr.length;i++)
            {
              headstr+="<th>"+headarr[i]+"</th>";
            }
            jobstatusHead.html(headstr);
          
            var urllink = getUrl() + "/db/getfile?fileid=";
            var viewurllink = getUrl() + "/gui/#ajax/topic_model_view.html?fileid=";
            var datastr="";
            var data = allresponse.result["builds"];
            var oneJobNotFinished = false;
            var completeimg = "img/green.png";
            var inprocessimg = "img/yellow.png";
            var errorimg = "img/red.png";
            for(var idx=0;idx<data.length;idx++)
            {
                var response = data[idx];
                var dataarr = [];
                var modelfileid = "";
                var jsonfileid = "";
                var inputfileid = "";
                var jsonfileid_view = "";
                var duration = "";
                var statusstr = "";
                var colorimg = "";
                // var totalcount = "";
                // var samplecount = "";
                // if('env' in response && 'totalcount' in response['env'])
                // {
                //     totalcount = "&nbsp;(" + numberWithCommas(response['env']['totalcount']) + ")";
                // }
                // if('env' in response && 'samplecount' in response['env'])
                // {
                //     samplecount = "&nbsp;(" + numberWithCommas(response['env']['samplecount']) + ")";
                // }
                if(response["data"]["building"] == false)
                {
                    if(response["data"]["result"] == "FAILURE")
                    {
                        colorimg = errorimg;
                        statusstr = "Error";
                    } else {
                        colorimg = completeimg;
                        statusstr = "Complete";
                        modelfileid = formatLink(response["data"]["parameters"]["modelfileid"],'');
                        jsonfileid = formatLink(response["data"]["parameters"]["jsonfileid"]);
                        inputfileid = formatLink(response["data"]["parameters"]["inputfileid"],'');
                        jsonfileid_view = formatTopic(response["data"]["parameters"]["jsonfileid"]);
                    }
                    duration = response["data"]["duration"]/1000;
                } else {
                    colorimg = inprocessimg;
                    statusstr = "In Process";
                    oneJobNotFinished = true;
                    //Compute the duration based on right now vs the timestamp given by Jenkins
                    duration = "" + ( ((new Date()).getTime() - (new Date(response["data"]["timestamp"])).getTime()) / 1000);
                }
                dataarr.push("<img width='16' height='16' src='" + colorimg + "'>")
                dataarr.push(response["number"]);
                dataarr.push(statusstr);
                dataarr.push(formatDate(response["data"]["timestamp"]));
                
                dataarr.push(response["data"]["parameters"]["description"])
                dataarr.push(duration);
                dataarr.push(inputfileid);
                dataarr.push(modelfileid);
                dataarr.push(jsonfileid);
                dataarr.push(jsonfileid_view);
              
                datastr+="<tr>";
                for(var i=0;i<dataarr.length;i++)
                {
                  datastr+="<td>"+dataarr[i]+"</td>";
                }
                datastr+="</tr>";
            }

            if(!oneJobNotFinished)
            {
                //All jobs are finished, so stop the autoGet
                startStopAuto();
            }

            jobstatusBody.html(datastr);
        }
      
        //Error conpletion of AJAX
        function processJobsError(response) {
            box.html('Failed.');
            startStopAuto();
            toggleBox();
        }
      
        //Get the Information
        function ajaxGetJobs() {
            var requestGetLong = {'job':2}
            ajaxGet = $.ajax({
                url: getJobsLocation,
                type: "post",
                dataType: "json",
                data: JSON.stringify(requestGetLong),
                contentType: 'application/json; charset=utf-8',
                success: processJobs,
                error: processJobsError,
                timeout: 2*60*1000 //2 minutes
            });
        }
    };

    function getUrl()
    {
        //Figure out what the location is for AJAX call
        var pathArray = location.href.split( '/' );
        var protocol = pathArray[0];
        var host = pathArray[2];
        return protocol + '//' + host;
    }

    var files = new BigFileUpload({'filetypes':/(\.|\/)(csv)$/i});
    files.completefcn = function(fileid,filelink,filename)
    {
        currentfileid = fileid;
        var fileselected = filename.name;
        $('#fileselected').html(fileselected);
        //var filedata = document.getElementById("fileupload");

        getHeaderFileID(fileid,showHeader);
        //getHeader(files.getFiles());
    };
    files.failfcn = function()
    {
        OpenModalBox('Upload Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
    };

    //At the completion of the file upload
    function getHeader(filedata)
    {
        //Update the file being shown
        var columnheader = Papa.parse(filedata,
            {
                // base config to use for each file
                header: false,
                preview: 2,
                complete: function(results, file) {
                    fileselected = filedata;
                    showHeader(results.data[0],results.data[1]);
                },
                error: function() {
                    OpenModalBox("Error Reading File","Error reading the header row of file.");
                }
            }
        );
    }


    function getHeaderFileID(fileid,resultfcn)
    {
        //Successfully uploaded the file, so now submit it with the ID to long job
        var request = {'fileid':fileid};
        var scriptHeaderLocation = getUrl() + '/db/getHeader.json';
        var ajaxQuery;

        //Callback from success of AJAX call
        function processGetHeader(response) {
            console.log(response);
            if(response['status'] == 'success')
            {
                //Successfully got header
                resultfcn(response['header'])
            } else {
                OpenModalBox('Submit Error', 'Error during processing.  Message: ' + response['msg']);
            }
        }
        //Called when Ajax fails
        function processGetHeaderError() {
            OpenModalBox('Submit Error', 'Could not connect to server or taking too long.  Try again later or contact HMC Tech Group.');
        }
        //Cycle through steps until result total is 0
        function ajaxGetHeaderProcess() {

            ajaxQuery = $.ajax({
                url: scriptHeaderLocation,
                type: "post",
                data: JSON.stringify(request),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: processGetHeader,
                error: processGetHeaderError,
                timeout: 1*60*1000 //1 minutes
            });
        }

        ajaxGetHeaderProcess();

    }


    function quicksubmit()
    {
        var textname = $('#textcolumn').find(':selected').text();
        if(textname == DUMMY_VALUE)
        {
            OpenModalBox('Error', 'The text column must be selected.');
            return;
        }
        var ajaxQuery;
        //Upload file and get ID and then run Topic Model
        var url = '/db/topicmodel.json';
        //Use fileupload.fileupload() with fileselected as fileobj
        var submitdata = {'textid':currentfileid,'textcolumn':textname};

        //Callback from success of AJAX call
        function processSubmit(response) {
            //{"success":'true', "totalcount" : totalcount, "input": input};
            console.log(response);
            endPause(false);
            if(response['result'] == true)
            {   
                //var tmpurl = getUrl() + '/#ajax/topic_model_view.html';
                var tmpurl = 'ajax/topic_model_view.html';
                //window.location.href = tmpurl;
                //window.open(tmpurl);
                window.location.hash = tmpurl;
                window.location.search = '?fileid=' + response['saveid'];
                LoadAjaxContent(tmpurl);
                //LoadAjaxContent(tmpurl);
            } else {
                displayError('Submit Error', 'Error during submition on server.  Try again later or contact HMC Tech Group.');
            }
        }
        //Called when Ajax fails
        function processSubmitError() {
            endPause(false);
            displayError('Submit Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        }
        //Cycle through steps until result total is 0
        function ajaxSubmitProcess() {
            console.debug(submitdata);
            startPause();
            ajaxQuery = $.ajax({
                url: url,
                type: "post",
                data: JSON.stringify(submitdata),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: processSubmit,
                error: processSubmitError,
                timeout: 30*60*1000 //30 minutes
            });
        }

        //Freeze the window, put button to cancel
        function startPause()
        {
            $('#pause').html('<span><h4>Processing request</h4> <button type="button" id="endDBquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
            $('#endDBquery').on('click', function() {endPause(true);});
        }

        //Unfreeze window, remove button
        function endPause(isCancel)
        {
            //Check if want to cancel the AJAX too
            if(isCancel)
            {
                ajaxQuery.abort();
            }
            $('#pause').html('');
        }

        function displayError(head,msg)
        {
            var output = "<h4>"+head+"</h4>";
            output+="<br><h5>"+msg+"</h5>";
            $('#pause').html(output)
        }


        ajaxSubmitProcess();
    }
    
    function jenkinssubmit(fileid)
    {
        //Successfully uploaded the file, so now submit it with the ID to long job
        var descript = $('#description').val();
        var requestLong = {'job':2,'description':descript,'inputfileid':fileid};

        //Callback from success of AJAX call
        function processSubmitLong(response) {
            //{"success":'true', "totalcount" : totalcount, "input": input};
            console.log(response);
            if(response['result'] == true)
            {
                OpenModalBox('Successful Submition', 'Succesfully submitted your job.  Use the <b>Get Jobs Button</b> below to see status and get results.');
            } else {
                OpenModalBox('Submit Error', 'Error during submition on server.  Try again later or contact HMC Tech Group.');
            }
        }
        //Called when Ajax fails
        function processSubmitLongError() {
            OpenModalBox('Submit Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        }
        //Cycle through steps until result total is 0
        function ajaxSubmitLongProcess() {

            ajaxQuery = $.ajax({
                url: scriptLongLocation,
                type: "post",
                data: JSON.stringify(requestLong),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: processSubmitLong,
                error: processSubmitLongError,
                timeout: 1*60*1000 //1 minutes
            });
        }

        ajaxSubmitLongProcess();
    }

    function showHeader(headlist)
    {
        headlist.unshift(DUMMY_VALUE);
        curheadlist = headlist;
        //Display header, hide file upload.
        $('.headerinput').show();

        var output = [];
        $.each(headlist, function(idx, value)
        {
            output.push('<option value='+ idx +'>'+ value +'</option>');
        });

        $('#textcolumn').html(output.join(''));

        $('#headerinput').show();
    }

    var gjob = new GetJobs();
    function setup() {
        // Add Drag-n-Drop feature
        //WinMove(); //REMOVE FOR NOW because I hide and show boxes, which doesn't work


        //FileDragDrop('filedrag','fileupload',{accept:/\.[cC][sS][vV]$/,addedFile:getHeader,errorFile:failfcn,selectedclass:'fileselected'});

        $('#headerinput').hide();
        $('#submitTopicModel').on("click",function() {
            quicksubmit();
        });
    }//end READY FUNCTION

}
