//Written by DWS
var QueryHistory = function(input)
{
    //var that = this;
    var inttable;
    var intdata = [];
    var MAX_SAMPLE_SIZE = 10000;
    var qhist = this;
    var DBrunning = "";

    constructor(input);

    function constructor(input)
    {
        var columns = ["Query","Start Date", "End Date","Total Count","Perform Data Download","Sample Size","Perform Random Sample"];
        var tableid = "queryhistory";
        if(input)
        {
            intdata = input.intdata || [];
            DBrunning = input.DBrunning || "";
        } else {
            intdata = [];
        }
        inttable = new SimpleTable({'columns':columns, 'tableid' : tableid});
        $(document).ready(setup);
    }

    function setup()
    {
        queryupdate();
    }

    //data = {'query':"", 'sdate':Date, 'edate': Date, 'total': num}
    this.addrow = function(data)
    {
        intdata.push(data);
        queryupdate();
    }

    function queryupdate()
    {
        var newdata = [];
        for(var i=0;i<intdata.length;i++)
        {
            var newline = [];
            var downloadlink = "<a class='downloadlink'>Submit Job</a>";
            var samplelink = "<a class='samplelink'>Submit Job</a>";
            var sampleform = '<input type="text" width="150px" class="form-control" id="samplesize' + i + '" placeholder="e.g. 1000 or 25%">';
            newline.push(JSON.stringify(intdata[i].query));
            newline.push(inttable.formatDate(intdata[i].sdate));
            newline.push(inttable.formatDate(intdata[i].edate));
            newline.push(inttable.formatNumber(intdata[i].total));
            newline.push(downloadlink);
            newline.push(sampleform);
            newline.push(samplelink);

            newdata.push(newline);
        }
        inttable.update(newdata);

        //Add monitors for rows
        $('.downloadlink').off('click.DL');
        $('.samplelink').off('click.DL');
        $('.downloadlink').on('click.DL', download);
        $('.samplelink').on('click.DL', sample);
    }

    function findData(target)
    {
        //figure out what row number
        var tr = $(target).closest("tr");
        var rowidx = tr.index();
        var selectedData = intdata[rowidx];
        return {'data':selectedData,'sample':$('#samplesize' + rowidx).val()};
    }

    var download = function(e)
    {
        var selectedData = findData(e.target);
        //Submit download

        var queryString = createQueryString(JSON.stringify(selectedData.data.query),selectedData.data.sdate,selectedData.data.edate);
        countData(selectedData.data.query,selectedData.data.sdate,selectedData.data.edate,'All',false,true,0,'','',qhist,queryString,DBrunning);
    }

    var sample = function(e)
    {
        var selectedData = findData(e.target);
        var samplesize = selectedData.sample;
        var sampletype = 'number';
        //Submit sample
        if(samplesize.endsWith("%")) 
        {
            sampletype="percent"; 
            samplesize = samplesize.substring(0, samplesize.length - 1);
        } 
        if(!$.isNumeric(samplesize))
        {
            OpenModalBox('Form Error', 'Bad sample size: Needs to be either a number or a number with a % at end.');
            return;       
        }
        samplesizenum = parseInt(samplesize,10);
        if(samplesizenum > MAX_SAMPLE_SIZE || samplesizenum <= 0 || (sampletype == "percent" && (samplesizenum > 100) ) )
        {
            OpenModalBox('Form Error', 'Bad sample size: Too large or 0 or negative.');
            return;       
        }

        var queryString = createQueryString(JSON.stringify(selectedData.data.query),selectedData.data.sdate,selectedData.data.edate);
        countData(selectedData.data.query,selectedData.data.sdate,selectedData.data.edate,'All',true,false,samplesizenum,sampletype,'random',qhist,queryString,DBrunning);
    }




}
