//Written by DWS
function setupQueryStuff()
{
    //Constants:
    //var searchtag = "matchingrulestag";
    var searchtag = "matchingrulestag";//abreviated name
    //var searchrule = "matchingrulesvalue"; //by number
    var searchrule = "matchingrulesvalue"; //abreviated name
    //var dataColumns = ["Idpost","bodypost","retweetCount","generatordname","filterlevel","kloutscore","matchingrulestag1","matchingrulestag2","matchingrulestag3","matchingrulestag4","matchingrulestag5","matchingrulestag6","matchingrulesvalues","matchingrulesvalue1","matchingrulesvalue2","matchingrulesvalue3","matchingrulesvalue4","matchingrulesvalue5","matchingrulesvalue6","languagevalue","favoritesCount","objpostedTime","objsummary","objlink","objid","objobjType","bodyoriginal","actorpreferredusername","actorname","actorlinkshref","actorTimeZone","actorimage","actorverified","actorstatusuesCount","actorsummary","actorlanguages","actorutcOffset","actorlink","actorfollowersCount","actorfavoritesCount","actorfriendsCount","actorlistedCount","actorPostedTime","actorid","actorObjType","entitiesusrmentionsidstr1","entitiesusrmentionssname1","entitiesusrmentionsname1","entitiesusrmentionsidstr2","entitiesusrmentionssname2","entitiesusrmentionsname2","entitiesusrmentionsidstr3","entitiesusrmentionssname3","entitiesusrmentionsname3","entitiesusrmentionsidstr4","entitiesusrmentionssname4","entitiesusrmentionsname4","entitiesusrmentionsidstr5","entitiesusrmentionssname5","entitiesusrmentionsname5","entitieshtagstext1","entitieshtagstext2","entitieshtagstext3","entitieshtagstext4","entitieshtagstext5","entitiesurlexpandedurl","entitiesmdaexpandedurl","lang","postedTime","Year","Month","Day","Time","objgeotype","objlocdname","geotype","geocoordinates","verb","link","locdname","locname","loctwcountrycode","loccountrycode","locgeocoordinates","gniplocdname","gniploccountry","gniplocregion","gniploccounty","gniploclocality","gniplocgeocoordinates","gnipurl","gnipexpandedurl"];
    //var timeColumn = "postedTime";//full name
    var MAX_SAMPLE_SIZE = 10000;
    //Database unique variables
    var DB_tags;
    var DB_rules;
    var DB_rules2tags;
    var DB_rule2time;
    var DB_minDate;
    var DB_maxDate;
    var DBrunning;

    var ajaxDB;

    var andStr = "&&";
    var orStr = "||";
    var qhist = undefined;


    var configLocation = getUrl() + "/db/config.json";
    
    var ruletag = new setupProcessDB(gotDB);


    function addRulesTags() {
        var output = [];
        $.each(ruletag.DB_rules, function(value, key)
        {
          output.push('<option value='+ value +'>('+value+') '+ key +'</option>');
        });
        $('.s2_with_rule').html(output.join(''));

        output = [];
        $.each(ruletag.DB_tags, function(value, key)
        {
          output.push('<option value='+ value +'>('+value+') '+ key +'</option>');
        });
        $('.s2_with_tag').html(output.join(''));

        MakeSelect2();
    }


    var didMakeSelect2 = false;
    function MakeSelect2()
    {  
        //If already created then destroy
        if(didMakeSelect2)
        {
            $('.s2_with_tag').select2("destroy");
            $('.s2_with_rule').select2("destroy");
        }
        didMakeSelect2 = true;
        $('.s2_with_tag').select2({placeholder: "Select Tags"});
        $('.s2_with_rule').select2({placeholder: "Select Rules"});
    }
    

    function setupDatePeriod()
    {
        $('#datestart').datepicker({
            defaultDate: "+lw",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            maxDate: '0',//set to +0 days from current date
            minDate: new Date('December 1, 2011 00:00:00'), //set to 1/2013
            onClose: function( selectedDate ) {
                $( "#dateend" ).datepicker( "option", "minDate", selectedDate );
                if($( "#dateend" ).val() == '' || $( "#dateend" ).val() < selectedDate)
                {
                    $( "#dateend" ).val(selectedDate);
                }
            }
        });
        $('#dateend').datepicker({
            defaultDate: "+lw",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            maxDate: '0',//set to +0 days from current date
            minDate: new Date('December 1, 2011 00:00:00'), //set to 1/2013
            onClose: function( selectedDate ) {
                if($( "#datestart" ).val() == '')
                {
                    $( "#datestart" ).val(selectedDate);
                }
            }
        });
    }
    
    function gotDB()
    {
        DBrunning = ruletag.DBrunning;
        addRulesTags();
        MakeSelect2();
        updateMinMaxDate();
        qhist = new QueryHistory({"DBrunning":DBrunning});
    }

    function updateMinMaxDate()
    {
        $( "#dateend" ).datepicker( "option", "minDate", ruletag.DB_minDate );
        $( "#datestart" ).datepicker( "option", "maxDate", ruletag.DB_maxDate );
    }

    function checkConvertQuery()
    { 
        var convertval = $('#pquery').val();

        //replace ands
        var re = new RegExp("\\s+and\\s+","g");
        convertval = convertval.replace(re,andStr);

        //replace ors
        re = new RegExp("\\s+or\\s+","g");
        convertval = convertval.replace(re,orStr);

        try {
            var newval = MongoQueryBuilder(convertval);

            var strval = JSON.stringify(newval);
            $('#query').val(strval);
        } catch (e) {
            OpenModalBox('Query Error', e.message);
            return false;
        }  
        return true;
    }
    function setupUpdate() {
        $('#updateQuery').on('click', function(e) {
            checkConvertQuery();
        });
        $("#addRule").on("click", function() {
            $("#pquery").val($("#pquery").val() + searchrule + " == " + $("#ruleInc").val());
            updateNameQuery();
        });

        $("#addTag").on("click", function() {
            $("#pquery").val($("#pquery").val() + searchtag + " == " + $("#tagInc").val());
            updateNameQuery();
        });
        $("#addRuleNot").on("click", function() {
            $("#pquery").val($("#pquery").val() + searchrule + " != " + $("#ruleInc").val());
            updateNameQuery();
        });

        $("#addTagNot").on("click", function() {
            $("#pquery").val($("#pquery").val() + searchtag + " != " + $("#tagInc").val());
            updateNameQuery();
        });

        $("#oplp").on("click", function() {
            $("#pquery").val($("#pquery").val() + "(");
            updateNameQuery();
        });
        $("#oprp").on("click", function() {
            $("#pquery").val($("#pquery").val() + ")");
            updateNameQuery();
        });
        $("#opand").on("click", function() {
            $("#pquery").val($("#pquery").val() + " and ");
            updateNameQuery();
        });
        $("#opor").on("click", function() {
            $("#pquery").val($("#pquery").val() + " or ");
            updateNameQuery();
        });

        $("#pquery").on("change", updateNameQuery);
    }

    function updateNameQuery()
    {
        var curquery = $("#pquery").val();

        //cycle through finding ( or ) or \n

        var curindent = 0;
        var indent_multiply = 4;
        function multi(len,character)
        {
            return new Array(len + 1).join( character );
        }

        function getIndent()
        {
            return multi(curindent * indent_multiply,"&nbsp;");
        }
        var prevmin = 0;
        var amin = -1;
        var newquery = "";

        while(true)
        {
            var aopen = curquery.indexOf("(",amin+1);
            var aclose = curquery.indexOf(")",amin+1);
            var aand = curquery.indexOf("and",amin+1);
            var aor = curquery.indexOf("or",amin+1);
            var CONST_MAX = 99999;
            if(aopen < 0) {
                aopen = CONST_MAX;
            }
            if(aclose < 0) {
                aclose = CONST_MAX;
            }
            if(aand < 0) {
                aand = CONST_MAX;
            }
            if(aor < 0) {
                aor = CONST_MAX;
            }
            prevmin = amin;
            amin = Math.min(aopen,aclose,aand,aor);


            if(prevmin+1 < amin)
            {
                newquery += curquery.substring(prevmin+1,amin);
            }

            //End search if no more data
            if(amin == CONST_MAX)
            {
                break;
            }

            if(curquery.charAt(amin) == '(')
            {
                //add carriage return before and after
                newquery+="<br>" + getIndent() + "(<br>";
                curindent++;
                newquery+=getIndent();
            }
            else if(curquery.charAt(amin) == ')')
            {
                curindent--;
                newquery+="<br>" + getIndent() + ")";
                newquery+=getIndent();
            }
            else if(curquery.charAt(amin) == 'a')
            {
                //add indent
                amin+=2;
                newquery += "<br>" + getIndent() + "and";
            }
            else if(curquery.charAt(amin) == 'o')
            {
                //add indent
                amin+=1;
                newquery += "<br>" + getIndent() + "or";
            }
        }

        curquery = newquery;
        //Pretty print to look like:
        /*
        (
            <span class='searchrule'>mrv</span> <span class='searcheq'>==</span> "blah"
            and mrv == "you"
            or (
                   mrt == "no"
               )
        )
        */
        $.each(ruletag.DB_rules, function(value, key)
        {
            var re = new RegExp(searchrule + "\\s*([=!])=\\s*" + value + "(?=\\D|$|\\<)","g");
            curquery = curquery.replace(re,"<i class='searchrule'>" + searchrule + "</i> $1= <i class='searchruleval'>\"" + key + "\"</i>");
        });

        $.each(ruletag.DB_tags, function(value, key)
        {
            var re = new RegExp(searchtag + "\\s*([=!])=\\s*" + value + "(?=\\D|$|\\<)","g");
            curquery = curquery.replace(re,"<i class='searchtag'>" + searchtag + " $1= <i class='searchtagval'>\"" + key + "\"</i>");
        });

        re = new RegExp("\\sand\\s","g");
        curquery = curquery.replace(re," <i class='searchand'>and</i> ");

        re = new RegExp("\\sor\\s","g");
        curquery = curquery.replace(re," <i class='searchor'>or</i> ");

        re = new RegExp("\\s==\\s","g");
        curquery = curquery.replace(re," <i class='searcheq'>==</i> ");

        re = new RegExp("\\s!=\\s","g");
        curquery = curquery.replace(re," <i class='searchneq'>!=</i> ");


        $("#namequery").html(curquery);
    }
  

    var gjob = new GetJobs();
    $(document).ready(function() {
        setupUpdate();
        setupDatePeriod();
        // Add Drag-n-Drop feature
        //WinMove(); //REMOVE FOR NOW because I hide and show boxes, which doesn't work
        
        //Hide query box because it has not been filled until a Database is selected
        $('.querybox').hide();
        $('.queryresultbox').hide();
        $('.querytablebox').hide();
        $('.samplenumselection').hide();
        $('.samplemethodselection').hide();


        window.queryStarted = false;//Allow only one query to be going on at a time
        var queryString = "";
        $('#querymethod').on('change', function(e) {
            var newval = $('#querymethod').val();

            $('.samplenumselection').hide();
            $('.samplemethodselection').hide();

            if(newval == "sample")
            {
                $('.samplenumselection').show();
                $('.samplemethodselection').show();
            }
            if(newval == "simple")
            {
                $('.samplenumselection').show();
            }
        });
        $('#submitQuery').on('click', function(e) {
            if(!queryStarted)
            {
                var ismongo = $("#querycheck").prop("checked");
                var isquery = false;
                if(ismongo)
                {
                    isquery = true;
                } else {
                    isquery = checkConvertQuery();//translate query
                }
                var query = {};
                var error = false;
                queryString = "";
                if(isquery)
                {
                    var querytext = $('#query').val();
                    queryString = querytext;
                    var newtext = querytext.replace(/'/g,'"');
                    //Validate querytext if is JSON
                    if(!isJSON(newtext))
                    {
                        OpenModalBox('Form Error', 'Bad query: it is not a JSON');
                        error = true;
                    }                            
                    query = JSON.parse(newtext);
                } else {
                    error = true;
                }

                if(error == false)
                {      
                    var datestart = $('#datestart').val();
                    var dateend = $('#dateend').val();

                    if(datestart == '' || dateend == '')
                    {
                        OpenModalBox('Form Error', 'Bad date start/end: must be filled in');
                    } else {
                        //convert all single ' to "
                        var timeslice = $('#timeSlice').val();
                        var minslice = "";
                        if(timeslice == "All"){minslice = "a";}
                        else if(timeslice == "Monthly"){minslice = "m";}
                        else if(timeslice == "Daily"){minslice = "d";}
                        else if(timeslice == "Yearly"){minslice = "y";}
                        else if(timeslice == "Weekly"){minslice = "w";}
                        queryString = createQueryString(queryString,datestart,dateend);

                        var querymethod = $('#querymethod').val();
                        var makeSample = false;
                        var samplesize = "";
                        var samplesizenum = 0;
                        var sampletype = "number";
                        var samplemethod = $('#samplemethod').val();
                        var makeDownload = false;
                        if(querymethod == "count")
                        {
                            //accept defaults
                             queryString += "_" + minslice;//add slice to filename
                        }
                        else if(querymethod == "simple")
                        {
                            makeSample = true;
                            sampletype = "number";
                            samplemethod = "first";
                            samplesize = $("#samplesize").val();
                            if(samplesize.endsWith("%")) 
                            {
                               OpenModalBox('Form Error', 'Bad sample size: Cannot be a percentage if using the simple sample method.');
                               return;       
                            }
                            if(!$.isNumeric(samplesize))
                            {
                                OpenModalBox('Form Error', 'Bad sample size: Needs to be a number.');
                                return;       
                            }

                        }
                        else if(querymethod == "sample")
                        {
                            makeSample = true;
                            makeDownload = true;
                            samplesize = $("#samplesize").val();
                            if(samplesize.endsWith("%")) 
                            {
                                sampletype="percent"; 
                                samplesize = samplesize.substring(0, samplesize.length - 1);
                            } 
                            if(!$.isNumeric(samplesize))
                            {
                                OpenModalBox('Form Error', 'Bad sample size: Needs to be either a number or a number with a % at end.');
                                return;       
                            }
                        }
                        else if(querymethod == "download")
                        {
                            makeDownload = true;
                        }

                        //Check if sample value is good
                        if(makeSample)
                        {
                           samplesizenum = parseInt(samplesize,10);
                           if(samplesizenum > MAX_SAMPLE_SIZE || samplesizenum <= 0 || (sampletype == "percent" && (samplesizenum > 100) ) )
                           {
                               OpenModalBox('Form Error', 'Bad sample size: Too large or 0 or negative.');
                               return;       
                           }
                        }

                        window.queryStarted = true;
                        countData(query,datestart,dateend,timeslice,makeSample,makeDownload,samplesizenum,sampletype,samplemethod,qhist,queryString,DBrunning);
                    }
                }
            }
        });


    
        //Initially hide the options
        $('.label-to-csv').hide();
        $('.beauty-table-to-save').hide();
        $('.beauty-table-to-csv').hide();
        $('.beauty-table-to-csv-all').hide();
        $('.beauty-table-to-crop').hide();


        //DEMO START BUTTON
        $('#demo').on('click',function() {
            if(sideBarVisible())
            {
                $('.show-sidebar').click();//Shrink left side
            }
            $('#checkJob').click();//start download of Jobs for later

            //Make transparent, but save to remove later
            tempstyle = addStyle(".introjs-helperLayer {background-color: transparent !important;}");

            //Start the tour now that the data is loaded
            var intro = introJs();
              intro.setOptions({
                steps: [
                  { 
                    intro: "This is a tutorial for the Query webpage."
                  },
                  {
                    element: "#dbselect",
                    intro: "Select the Database to query (currently only twitter has data)."
                  },
                  {
                    element: "#dbselect",
                    intro: "After the page loads it will automatically download the latest Tags and Rules from the selected database (you will see a red spinning button while loading)."
                  }
                ]
              });
              //intro.setOption('showButtons', false);
              //intro.setOption('showBullets', false);
              intro.setOption('doneLabel', 'See Query').start().oncomplete(firstdemo);
              console.log("started demo");
              $('#demo').hide();
        });

    });//end READY FUNCTION
    //used for introjs transparent switch
    var tempstyle;

    function firstdemo()
    {
        //Remove transparent introjs
        var head = document.head || document.getElementsByTagName('head')[0]
        head.removeChild(tempstyle);

        //Start the tour now that the data is loaded
        setTimeout(function() {
            var intro = introJs();
              intro.setOptions({
                steps: [
                  {
                    element: '#mainquery',
                    intro: "Use this section to build the query.  (If you know how to use MongoDB style query, hang in there, we'll get to that.)"
                  },
                  {
                    element: '#mainquery',
                    intro: "A query is written in the form of a sentence with multiple comparisions, like <b>\"Tag equals 1 and Rule not equals 35\"</b>.  This sentence would be written in a query language like <b>\"matchingrulestag == 1 and matchingrulesvalue != 35\"</b>."
                  },
                  {
                    element: '#mainquery',
                    intro: "A query is built one comparison at a time."
                  },
                  {
                    element: '#tagsection',
                    intro: "Use this section to add a tag comparison to the query."
                  },
                  {
                    element: '#tagIncOut',
                    intro: "Select a Tag from the drop down (or search for it)."
                  },
                  {
                    element: '#addTag',
                    intro: "Then click this button to add 'Tag equals 1' to the query."
                  }
                ]
              });
              //intro.setOption('showButtons', false);
              //intro.setOption('showBullets', false);
              intro.setOption('doneLabel', 'Add Tag equals 1').start().oncomplete(seconddemo);
              console.log("started demo a");
        },100);

    }

    function seconddemo()
    {
        $('#addTag').click();//add the Tag == 1

        //Start the tour now that the data is loaded
        setTimeout(function() {
            var intro = introJs();
              intro.setOptions({
                steps: [
                  {
                    element: "#pqueryOut",
                    intro: "See the current query here."
                  },
                  {
                    element: "#namequery",
                    intro: "This shows the current query translation back to a sentence (without numbers)."
                  },
                  {
                    element: '#separators',
                    intro: "These are the comparision separators that can be used.  The parenthesis define the order of how MongoDB processes the query."
                  }
                ]
              });
              //intro.setOption('showButtons', false);
              //intro.setOption('showBullets', false);
              intro.setOption('doneLabel', 'Add \"And\"').start().oncomplete(demoThree);
              console.log("started demo 2");
      },200);
    }

    function demoThree()
    {
        $('#opand').click();//add the And

        //Start the tour now that the data is loaded
        setTimeout(function() {
            var intro = introJs();
              intro.setOptions({
                steps: [
                  {
                    element: "#rulesection",
                    intro: "Use this section to add a rule comparison to the query."
                  },
                  {
                    element: '#ruleIncOut',
                    intro: "Select a Rule from the drop down (or search for it)."
                  },
                  {
                    element: '#addRuleNot',
                    intro: "Then click this button to add 'Rule not equal 1' to the query."
                  }
                ]
              });
              //intro.setOption('showButtons', false);
              //intro.setOption('showBullets', false);
              intro.setOption('doneLabel', 'Add \"Rule not equal 1\"').start().oncomplete(demoFour);
              console.log("started demo 3");
          },200);
    }

    function demoFour()
    {
        $('#addRuleNot').click();//add Rule != 1

        //Start the tour now that the data is loaded
        setTimeout(function() {
            var intro = introJs();
              intro.setOptions({
                steps: [
                  {
                    element: "#pqueryOut",
                    intro: "See the current query here."
                  },
                  {
                    element: "#namequery",
                    intro: "This shows the current query translation back to a sentence (without numbers)."
                  },
                  {
                    element: "#showmongo",
                    intro: "Click this + to create a MongoDB query (or see what the current query is when translated to MongoDB style)."
                  }
                ]
              });
              //intro.setOption('showButtons', false);
              //intro.setOption('showBullets', false);
              intro.setOption('doneLabel', 'See MongoDB query style').start().oncomplete(demoFive);
              console.log("started demo 4");
          },200);
    }
    
    function demoFive()
    {
        $('#showmongo').click();//show mongo

        //Start the tour now that the data is loaded
        setTimeout(function() {
            var intro = introJs();
              intro.setOptions({
                steps: [
                  {
                    element: "#queryOut",
                    intro: "This is where you can enter MongoDB style query (or see translation)."
                  },
                  {
                    element: "#querycheckOut",
                    intro: "Check this box to use whatever is in the box to the right as the custom MongoDB query to run (and not use the query above)."
                  },
                  {
                    element: "#updateQuery",
                    intro: "Click this button to see the current query translated to MongoDB style."
                  },
                  {
                    element: "#daterange",
                    intro: "Select the Date Range to query (start and end inclusive)."
                  },
                  {
                    element: "#showtimeslice",
                    intro: "Time slice is only used when performing Counts.  It determines how the counts will be performed (e.g. by month)."
                  },
                  {
                    element: "#querymethod",
                    intro: "You can perform 4 types of queries. <br>1) <b>Counts Only</b>: Creates a graph with an option to download the numbers as a CSV."
                  },
                  {
                    element: "#querymethod",
                    intro: "You can perform 4 types of queries. <br>2) <b>In browser: Sample observations</b>: User selects the # of samples desired, and the data is returns to the browser to view."
                  },
                  {
                    element: "#querymethod",
                    intro: "You can perform 4 types of queries. <br>3) <b>Submit: Sample Only</b>: User selects # or % of samples using either a random or first samples.  This will be submitted to the Server to process and create a .CSV of all the sample data.  Users can monitor the job on this page down below.  Once complete a download link will be provided."
                  },
                  {
                    element: "#querymethod",
                    intro: "You can perform 4 types of queries. <br>4) <b>Submit: Download data (no sampling)</b>: All query data will be downloaded.  This will be submitted to the Server to process and create a .CSV of all the sample data.  Users can monitor the job on this page down below.  Once complete a download link will be provided."
                  },
                  {
                    element: "#submitQuery",
                    intro: "After all options are select, click this button to run the query.  If something isn't filled out correctly it will prompt the user."
                  },
                  {
                    element: "#response",
                    intro: "During query running a progress bar will be shown.  Sometimes a query can take a long time (depending on the date range and the complexity of the query)."
                  },
                  {
                    element: "#queryhistory",
                    intro: "After a Count query is complete, the total number and MongoDB style query will be added to the bottom in this section.  Once the page is refreshed this will all vanish."
                  },
                  {
                    element: "#queryhistory",
                    intro: "Also from this history you can submit a query to download all the data or just a sample of it."
                  },
                  {
                    element: "#checkJob",
                    intro: "Click this button to start the automated status update of all query jobs submitted to the Server."
                  },
                  {
                    element: $("#jobstatus tr").eq(0).find("td").eq(2)[0],
                    intro: "This is the MongoDB style query that was performed.  If it is too long then if the mouse hovers over it you can see the full query as a pop-up.  Or to be able to select it, click the cell to expand the query."
                  },
                  {
                    element: $("#jobstatus tr").eq(0).find("td").eq(6)[0],
                    intro: "If a query is running, then a red spinning box will be shown.  Clicking on the box will cancel the query."
                  },
                  {
                    element: $("#jobstatus tr").eq(0).find("td").eq(6)[0],
                    intro: "If a query had an error, Please try the query again.  If it still has an error, contact HMC Tech and tell them the row number of this table to look into why."
                  },
                  {
                    element: $("#jobstatus tr").eq(0).find("td").eq(10)[0],
                    intro: "This cell will give a link to the download of all data (if that was the option selected).  It will also show the total count of the query in parenthesis."
                  },
                  {
                    element: $("#jobstatus tr").eq(0).find("td").eq(11)[0],
                    intro: "This cell will give a link to the sample (if that was the option selected).  It will also show the sample size of the query in parenthesis."
                  }
                ]
              });
              //intro.setOption('showButtons', false);
              //intro.setOption('showBullets', false);
              //intro.start();
              intro.setOption('doneLabel', 'End of Tour').start().oncomplete(function() {$('.show-sidebar').click();});
              console.log("started demo 5");
          },200);
    }

    // function demoSeven()
    // {

    //     //Start the tour now that the data is loaded
    //     setTimeout(function() {
    //         var intro = introJs();
    //           intro.setOptions({
    //             steps: [
    //             ]
    //           });
    //           //intro.setOption('showButtons', false);
    //           //intro.setOption('showBullets', false);
    //           intro.start();
    //           //intro.setOption('doneLabel', 'See Query Status').start().oncomplete(demoSeven);
    //           console.log("started demo 6");
    //     },10000);
    // }

}
function createQueryString(querytext,datestart,dateend)
{

    function reformatDate(ds)
    {
        //Takes a MM/DD/YYYY and outputs YYYYMMDD
        var dates = ds.split('/');//break out month/day/year
        var startformat = dates[2] + pad(dates[0],2) + pad(dates[1],2);
        return startformat;
    }

    queryString = querytext.replace(/[\[\]\\\.\(\) $@!`~\-_+=\{\}\|;'"\:\?\/,<>\*&\^\%]/g,"");
    var queryString = "Query_" + queryString;
    queryString += "_" + reformatDate(datestart) + "_" + reformatDate(dateend);
    return queryString;

}
