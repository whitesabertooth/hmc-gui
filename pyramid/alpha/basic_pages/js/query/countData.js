//Written by DWS
//Start a pull of count data
function countData(query,datestart,dateend,timeslice,createSample,createDownload,sampleSize,sampleType,sampleMethod,qhist,queryString,DBrunning) /*sTag,sRule,sTimeStart,sTimeEnd,sSplice*/{                  

    var scriptLongLocation = getUrl() + '/db/submitLongJob.json';
    var scriptLocation = getUrl() + "/db/db.json";
    var timeColumn = "postedTime";//abreviated name
    var totalSample = 1000;
    var dddData = {};
    var chart = null;
    var isTwitter = false;//temp  
    var currentCount = 0;
    var totalCount = 0;
    var ajaxQuery;
    var collectionRanges = [];
    var collectionGroup = 0;
    var collectionInc = -1;//Will be incremented on first call
    var increment = 100000;
    var step = -1;//Will be incremented on first call
    var request = {};

    var startquerydate = new Date();//gets current time
    var lastdate = new Date();
    var sma = simple_moving_averager(5);
    window.fileid = "0";

    var tableCreated = false;
    var setupLink = false;
    var getSample = false;
    //samplemethod = 'first' or 'random'
    var delaySample = (( (sampleMethod != 'first' || (sampleMethod == "first" && sampleType == "percent"))));
    var saveQuery = (delaySample || createDownload);
    var needSampleSize = sampleSize;
    var currentpercent = 0;
    var estimatestr = "";//allow old times to be there

    runCount();

    function runCount()
    {
        //cap the filename
        if(queryString.length > 90)
        {
            queryString = queryString.substring(0,90);
        }
        //Hardcode sample to monthly - quickest method
        if(createSample)
        {
            timeslice = 'Monthly';
        }


        if(!saveQuery)
        {
            //Delete previuos table and toggle fields and hide buttons
            $('#response').html('');
            $('#createtable').html("");
            $('#toggleMe').html("");
            //Only hide if producing something new for this webpage
            $('.label-to-csv').hide();
            $('.beauty-table-to-save').hide();
            $('.beauty-table-to-csv').hide();
            $('.beauty-table-to-csv-all').hide();
            $('.beauty-table-to-crop').hide();
            $('.querytablebox').hide();
            $('.queryresultbox').hide();
            //Performing a direct response execution
            //Setup query
            if(!setupQuery(datestart,dateend,timeslice))
            {
                OpenModalBox('Form Error', 'Resulted in no query data');
                return null;
            }

            //Only show the results expected
            if(createSample)
            {
                //Creating a sample
                $('.querytablebox').show();
            } else {
                //Creating a plot of counts
                $('.queryresultbox').show();
                $('.label-to-csv').show();
                $('.label-to-csv').off('click.all');
                $('.label-to-csv').on('click.all', savePlot);
                plotData();
            }

            //Create plot after setup done
            $('#response').html('<h4>Started query</h4> <button type="button" id="endquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button><br><h6 id="updatetime"></h4><div class="progress progress-striped active"><div class="progress-bar progress-bar-danger" id="thebar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span id="textbar">0% Complete</span></div></div>');
            //Start the cycle
            nextQuery();

        } else {
            //Use long job background execution:
            var getSample = createSample ? 'true' : 'false';
            var newdateStart = fixDateOffset(new Date(datestart));
            var newdateEnd = fixDateOffset(new Date(dateend));
            var requestLong = {'job':1,'samplemethod':sampleMethod,'sampletype':sampleType,'sample':sampleSize,'getSample':getSample,'filename':queryString + '.zip','startdate':newdateStart,'enddate':newdateEnd,'query':JSON.stringify(query),'db':DBrunning};

            //Callback from success of AJAX call
            function processSubmitLong(response) {
                //{"success":'true', "totalcount" : totalcount, "input": input};
                console.log(response);
                if(response['result'] == true)
                {
                    OpenModalBox('Successful Submition', 'Succesfully submitted your query.  Use the <b>Get Jobs Button</b> below to see status and get results.');
                } else {
                    OpenModalBox('Query Error', 'Error during submition on server.  Try again later or contact HMC Tech Group.');
                }
                window.queryStarted = false;
            }
            //Called when Ajax fails
            function processSubmitLongError() {
                OpenModalBox('Query Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
                stopQuery();
                window.queryStarted = false;
            }
            //Cycle through steps until result total is 0
            function ajaxSubmitLongProcess() {
                console.log(requestLong);//FOR DEBUG
                ajaxQuery = $.ajax({
                    url: scriptLongLocation,
                    type: "post",
                    data: JSON.stringify(requestLong),
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    success: processSubmitLong,
                    error: processSubmitLongError,
                    timeout: 1*60*1000 //1 minutes
                });
            }

            ajaxSubmitLongProcess();

        }
    }


    function stopQuery()
    {
        $('#response').html("<h4>QUERY CANCELLED</h4>");    
        window.queryStarted = false;
    }

    $('#endquery').on('click',function ()
    {
        ajaxQuery.abort();
        stopQuery();
    });

    function completeUpdate()
    {
        var newdate = new Date();//gets current time
        var diffdate = newdate.getTime() - startquerydate.getTime();
        diffdate /= 1000;
        $('#response').html("<h4>QUERY COMPLETE</h4><br>Total Elapsed Seconds: " + diffdate);  
        window.queryStarted = false;
                       
        if(!createSample)
        {
            //Count Only Query - Add result to history
            var newline = {'query':query,'sdate':datestart, 'edate':dateend,'total':totalCount};
            qhist.addrow(newline);
        }
    }

    function savePlot(){
        var data = [];
        //Create header
        var temp = ['Date','Count'];
        data.push(temp);
        for(var i=0;i<collectionRanges.length;i++)
        {
            temp = [];
            //Only save final results
            if(collectionRanges[i]['complete'])
            {
                temp.push(collectionRanges[i]['slice']);
                temp.push(collectionRanges[i]['result'].toString());
            } else {
                temp.push(collectionRanges[i]['slice'] + " (incomplete result)");
                temp.push(collectionRanges[i]['tempresult'].toString());
            }
            data.push(temp);        
        }            

        //Add Header row
        var csvdata = $.csv.fromArrays(data,{'experimental':true});
        var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
        saveAs(blob, queryString + '.csv');
    }

    //Plots the data
    function plotData()
    {
        $('#morris-chart-1').empty();//destroy previous
        var colors = ['#A30000','#038C2C'];
        //Do not allow a re-plot setup
        if(chart == null)
        {  
            chart = Morris.Bar({
                element: 'morris-chart-1',
                data: collectionRanges,
                xkey: 'slice',
                ykeys: ['tempresult', 'result'],
                labels: ['Updating Count', 'Complete Count'],
                hideHover: 'auto',
                barColors: colors,
                stacked: true,
                resize: true,
                hoverCallback: function(index, options, content) {
                    var data = options.data[index];
                    var stringName = 'Count: ';
                    var outstr =  '<div style="color:';
                    if(collectionRanges[index]['complete'])
                    {
                        outstr += colors[1] + '">' + collectionRanges[index]['slice'] + '<br>'+ stringName;
                        outstr += data.result.toLocaleString();
                    } else {
                        outstr += colors[0] + '">' + collectionRanges[index]['slice'] + '<br>'+ stringName;
                        outstr += data.tempresult.toLocaleString();
                    }
                    outstr += '</div>';
                    return outstr;
                },
                xLabelAngle: 20
            });
        }
    };

    function updatePlot()
    {
        chart.setData(collectionRanges);
    };

    //Update display of count
    function updateCount(group,finalUpdate)
    {
        var newdate = new Date();//gets current time
        if(!createSample)
        {
            if(finalUpdate)
            {
                collectionRanges[group]['result'] = currentCount;
                collectionRanges[group]['tempresult'] = 0;
                collectionRanges[group]['complete'] = true;
                updatePlot();

                //Calculate estimate by simple moving average (last 5)
                // var diffdateprev = newdate.getTime() - lastdate.getTime();
                // diffdateprev /= 1000;
                // var avg = sma(diffdateprev);
                // var numleft = collectionRanges.length - group + 1;//group starts at 0
                // var estimate = avg*numleft;
                // estimate = Math.round(estimate*100)/100;//2 decimal places
                // currentpercent = Math.floor(100*(group + 1)/collectionRanges.length);//assume every step is equal
                // estimatestr = "<br>Seconds Estimate Left: " + estimate; //NOT USED AT THIS TIME because it is incorrect
                //console.log("Finished group");
            } else {
                collectionRanges[group]['tempresult'] = currentCount;
                currentpercent = Math.floor(100*(collectionInc+1)/collectionRanges.length/collectionRanges[group]['range'].length + 100*(group)/collectionRanges.length);//assume all are equal

                updatePlot();
                //console.log("Finished segment");
            }
        } else {
            //Calculate percentage based on number of samples received vs how many desired
            currentpercent = Math.floor(100*totalCount/sampleSize);
        }
        //Update progress bar
        $('#thebar').css('width', currentpercent+'%').attr('aria-valuenow', currentpercent);
        $('#textbar').html(currentpercent + "% Complete");

        var diffdate = newdate.getTime() - startquerydate.getTime();
        diffdate /= 1000;
        $('#updatetime').html("Last updated: " + newdate.toUTCString() + "<br>Total Elapsed Seconds: " + diffdate);
    };

    //Setup all query information
    function setupQuery(datestart,dateend,timeslice) {

        var collectionRange = [];
        var inputStartDate = new Date(datestart);
        //Set to beginning of day
        inputStartDate.setHours(0);
        inputStartDate.setMinutes(0);
        inputStartDate.setSeconds(0);
        inputStartDate.setMilliseconds(0);
        var newStartDate = new Date(inputStartDate.getTime());
        //Only set to first of month if doing monthly incrementing
        if(timeslice == 'All' || timeslice == 'Yearly' || timeslice == 'Monthly')
        {
            newStartDate.setDate(1);//make the 1st of the month so +1 on month works correctly
        }
        var newEndDate = new Date(dateend);
        //Push to end of day
        newEndDate.setHours(23);
        newEndDate.setMinutes(59);
        newEndDate.setSeconds(59);
        newEndDate.setMilliseconds(999);
        var currentWeek = 0;
        var currentDay = 0;

        while(dates.compare(newStartDate,newEndDate) <= 0)
        {
            var strmonth = (newStartDate.getMonth() + 1).toString();
            if(strmonth.length < 2) { strmonth = '0' + strmonth;}
            strCollection = newStartDate.getFullYear().toString() + strmonth;

            collectionRange.push(strCollection);

            //Perform timeslice split
            if(timeslice == 'All')
            {
                //Look at next month now
                newStartDate.setMonth(newStartDate.getMonth() + 1);      
                if(dates.compare(newStartDate,newEndDate) > 0)
                {
                    //Always narrow down by date
                    //TODO: Could be better if broke out by month query
                    var weekQuery = {};
                    weekQuery[timeColumn] = {'$gte': fixDateOffset(inputStartDate) , '$lte': fixDateOffset(newEndDate) }; 

                    //all done:
                    collectionRanges.push({"range":collectionRange, "timequery":weekQuery, "slice":"All", "complete":false, "tempresult":0, "result":0});
                    collectionRange = [];
                }                      
            } else if(timeslice == 'Yearly')
            {
                var prevStartDate = new Date(newStartDate.getTime());
                //Increment one month
                newStartDate.setMonth(newStartDate.getMonth() + 1);   
                //Check for year roll-over, or end of time period
                if(newStartDate.getMonth() == 0 || dates.compare(newStartDate,newEndDate) > 0)
                {
                    prevStartDate.setMonth(newStartDate.getMonth() + 1);
                    prevStartDate.setDate( prevStartDate.getDate() - 1);//Backup to end of month
                    //Set end of day on last day
                    prevStartDate.setHours(23);
                    prevStartDate.setMinutes(59);
                    prevStartDate.setSeconds(59);
                    prevStartDate.setMilliseconds(999);
                    //narrow down by day if in the same year
                    //TODO: Could be better if broke out by query
                    var weekQuery = {};
                    if(inputStartDate.getFullYear() == prevStartDate.getFullYear() || prevStartDate.getFullYear() == newEndDate.getFullYear())
                    {
                        var tempWeekGT = {};
                        var tempWeekLT = {};
                        if(inputStartDate.getFullYear() == prevStartDate.getFullYear())
                        {
                            tempWeekGT = {'$gte': fixDateOffset(inputStartDate)};  
                        }
                        if(prevStartDate.getFullYear() == newEndDate.getFullYear())
                        {
                            tempWeekLT = {'$lte': fixDateOffset(newEndDate)};  
                        }
                        var tempquery = $.extend({}, tempWeekGT, tempWeekLT);
                        weekQuery[timeColumn] = tempquery;
                    }

                    //at end of year:
                    var sliceText = prevStartDate.getFullYear().toString();
                    collectionRanges.push({"range":collectionRange, "timequery":weekQuery, "slice":sliceText, "complete":false, "tempresult":0, "result":0});
                    collectionRange = [];
                }                                    

            } else if(timeslice == 'Monthly')
            {
                var sliceText = (newStartDate.getMonth()+1) + "/" + newStartDate.getFullYear();

                //narrow down by day if in the same month
                var weekQuery = {};
                if(inputStartDate.getMonth() == newStartDate.getMonth() || newStartDate.getMonth() == newEndDate.getMonth())
                {
                    var tempWeekGT = {};
                    var tempWeekLT = {};
                    if(inputStartDate.getMonth() == newStartDate.getMonth())
                    {
                        tempWeekGT = {'$gte': fixDateOffset(inputStartDate)};  
                    }
                    if(newStartDate.getMonth() == newEndDate.getMonth())
                    {
                        tempWeekLT = {'$lte': fixDateOffset(newEndDate)};  
                    }
                    var tempquery = $.extend({}, tempWeekGT, tempWeekLT);
                    weekQuery[timeColumn] = tempquery;
                }

                collectionRanges.push({"range":collectionRange, "timequery":weekQuery, "slice":sliceText, "complete":false, "tempresult":0, "result":0});
                collectionRange = [];
                //Increment one month
                newStartDate.setMonth(newStartDate.getMonth() + 1);                  
            } else if(timeslice == 'Weekly')
            {
                var prevStartDate = new Date(newStartDate.getTime());
                var checkDate = new Date(newStartDate.getTime());
                var countShift = 0;
                //Cycle for 7 days, or until the end, or until the month changes
                while(dates.compare(checkDate,newEndDate) <= 0 && countShift < 8 && checkDate.getMonth() == newStartDate.getMonth())
                {
                    countShift++;
                    checkDate.setDate(checkDate.getDate() + 1);
                }
                if(countShift == 8)
                {
                    //Successfully counted 7 days to shift will not change month or go past end of compare time
                    newStartDate.setDate(newStartDate.getDate() + 7);
                } else {
                    //back up one day from countShift because that day failed a test
                    newStartDate.setDate(newStartDate.getDate() + countShift-1);                                    
                }

                var nextStartDate = new Date(newStartDate.getTime());
                //Push to end of day
                nextStartDate.setHours(23);
                nextStartDate.setMinutes(59);
                nextStartDate.setSeconds(59);
                nextStartDate.setMilliseconds(999);       

                var sliceText = prevStartDate.toLocaleDateString() + " to " + newStartDate.toLocaleDateString();
                var weekQuery = {};
                weekQuery[timeColumn] = {'$gte': fixDateOffset(prevStartDate), '$lte': fixDateOffset(nextStartDate)};  
                collectionRanges.push({"range":collectionRange, "timequery":weekQuery, "slice":sliceText, "complete":false, "tempresult":0, "result":0});
                collectionRange = [];

                //Add one more day
                newStartDate.setDate(newStartDate.getDate() + 1);   
            } else if(timeslice == 'Daily')
            {

                var nextStartDate = new Date(newStartDate.getTime());
                //Push to end of day
                nextStartDate.setHours(23);
                nextStartDate.setMinutes(59);
                nextStartDate.setSeconds(59);
                nextStartDate.setMilliseconds(999);

                var sliceText = newStartDate.toLocaleDateString();
                var weekQuery = {};
                weekQuery[timeColumn] = {'$gte': fixDateOffset(newStartDate), '$lte': fixDateOffset(nextStartDate)};
                collectionRanges.push({"range":collectionRange, "timequery":weekQuery, "slice":sliceText, "complete":false, "tempresult":0, "result":0});
                collectionRange = [];

                //Add one more day
                newStartDate.setDate(newStartDate.getDate() + 1);  
            }
        }

        //console.log(collectionRanges);
        if(collectionRanges.length == 0)
        {
            return false;
        } else {
            return true;
        }

    };

    function updateRequest()
    {
        //merge query and timequery if exists
        var tempquery = $.extend({}, collectionRanges[collectionGroup]['timequery'], query);
        //console.log(tempquery);
        request = {"query":tempquery,"collection":collectionRanges[collectionGroup]['range'][collectionInc],"increment":increment,"step":step,"sample":needSampleSize,"sampletype":sampleType,"samplemethod":sampleMethod,"createSample":createSample,"db":DBrunning};
    }

    //Handle breaking down the query into different collections
    function nextQuery() {
        if(collectionRanges.length > collectionGroup)
        {
            collectionInc++;
            if(collectionRanges[collectionGroup]['range'].length > collectionInc)
            {
                step = -1;//reset Step - will be incremented later

                updateRequest();

                ajaxProcess();
            } else {
                //Complete
                updateCount(collectionGroup,true);

                collectionGroup++;

                //Check if more groups
                if(collectionGroup < collectionRanges.length)
                {
                    //Reset group counters
                    step = -1;//reset Step - will be incremented later
                    collectionInc = 0;
                    currentCount = 0;

                    updateRequest();

                    ajaxProcess();
                } else {
                    //fully complete
                    completeUpdate();
                }
            }
        }
    }

    //Callback from success of AJAX call
    function process(response) {
        //{"success":'true', "totalcount" : totalcount, "input": input};
        console.log(response);
        if(response['issuccess'] == 'success')
        {

            totalCount += response['totalcount'];
            currentCount = currentCount + response['totalcount'];
            updateCount(collectionGroup,false);

            if(createSample)
            {
                //Handle the data
                //Add data to table
                if(!tableCreated)
                {
                    tableCreated = true;
                    //add empty columns of data for every field missing for first row, on initial table creation:
                    //Change data to be column format, array of array, not json/dictionary
                    var newdataset = [];

                    //Delete table and recreate
                    $('#createtable').html('<table class="table table-bordered table-striped table-hover table-heading table-datatable beauty-table-mod" id="datatable-3">' +  
                                        '<thead>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                        '<tfoot>' +
                                        '</tfoot>' +
                                    '</table>');


                    currentTableSize = response['data'].length;

                    oTable = setupDataTable(response['data'],queryString + '.csv',[]);
                    $(window).off('beforeunload'); //Remove on leaving message

                    console.log('Got first response, so created table');
                    $('.beauty-table-to-crop').show();//Add CROP

                } else {
                    //Add rows to oTable
                    if(response['data'].length > 1)
                    {
                        //Remove header row
                        response['data'].shift()
                        currentTableSize = response['data'].length;
                        oTable.rows.add(response['data']);
                        oTable.draw();
                        console.log('Got another response - appended');
                    } else {
                        currentTableSize = 0;
                    }
                }
                needSampleSize -= currentTableSize;
                //If we received what we asked for then Stop
                if(needSampleSize <= 0)
                {
                    completeUpdate();
                    return;
                }

            }

        }
        // NOT NOW - Only one query and one response, no incrementing
        if(response['totalcount'] == increment)
        {
            //Call again
            ajaxProcess();
        } else {
            nextQuery();
        }
    };

    //Called when Ajax fails
    function processError() {
        OpenModalBox('Query Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        stopQuery();
    }
    //Cycle through steps until result total is 0
    function ajaxProcess() {
        request['step'] = request['step'] + 1;
        console.log(request);
        ajaxQuery = $.ajax({
            url: scriptLocation,
            type: "post",
            data: JSON.stringify(request),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: process,
            error: processError,
            timeout: 30*60*1000 //30 minutes
        });
    };
}

