//Written by DWS

//CLASS GetJobs
//Constructor
//Required element is #overlaytext as div at top of sub-window
var GetJobs = function()
{
    //Private variables:
    var getJobsLocation = getUrl() + "/db/getLongJob.json";
    var cancelJobLocation = getUrl() + "/db/cancelLongJob.json";
    var autoGetTime = 1*60*1000;//wait 1 minutes
    var specialBox;
    var box;
    var autoGetInterval;
    var ajaxGet;
    var jobstatusHead;
    var jobstatusBody;
    var checkJob;
    var runningAuto = false;
    var boxfadding = true;
    var canGetJobs = true;
    var numidx = 0;//Set after getting a job result

    $(document).ready(setup);

    //Private functions:
    function setup()
    {
        specialBox = document.getElementById('overlaytext');
        box = $('#overlaytext');
        jobstatusHead = $("#jobstatus thead");
        jobstatusBody = $("#jobstatus tbody");
        checkJob = $('#checkJob');

        //Start auto Get when clicked
        checkJob.on("click",startStopAuto);
    }

    function toggleBox(){
        if(!boxfadding){
            //Slowly make it fade
            box.fadeOut(10000);//will make hidden after opacity gone
            boxfadding = true;
        } else {
            box.stop();//stop fading if performing
            specialBox.style.display = "block";
            specialBox.style.opacity = 1.0;
            boxfadding = false;
        }
    }

    function startStopAuto()
    {
        if(canGetJobs)
        {
            //If not started start time get
            if(runningAuto)
            {
                clearInterval(autoGetInterval);
                checkJob.html("<span><i class='fa fa-refresh txt-primary'></i></span>Start Auto Check Status");
                runningAuto = false;
            } else {
                autoGetInterval = setInterval(autoGet,autoGetTime);                
                autoGet();
                checkJob.html("<span><i class='fa fa-times-circle-o txt-danger'></i></span>Stop Auto Check Status");
                runningAuto = true;
            }
        }
    }

    function autoGet(){
        box.html('Get Jobs...');
        toggleBox();
        ajaxGetJobs();
    }

    //Successful conpletion of AJAX
    function processJobs(allresponse) {
        box.html('Successful.');
        toggleBox();
        var qshortsize = 20;
        var queryColumnSize = "150px";
        var numColumnSize = "40px";
        
        //Returns {
            //'builds': [
            //   {
                //  'number', 
                //  'data' {
                //       'parameters': {
                    //     'query', 'downloadfileid','samplefileid','startdate','enddate'
                    //    } , 
                    //  'duration', 'estimatedDuration', 'timestamp', 'result', 'building' 
                    //} 
                //} 
            //]}
        // Retrieve actions->[parameters->[{name,value}]]  (array of name/value pair), convert to dictionary 'parameters'        
        // Retrieve duration (number)
        // Retrieve estimatedDuration (number)
        // Retrieve timestamp (Number)
        // Retrieve result (SUCCESS,FAIL,UNSTABLE)
        // Retrieve building (true/false)


        function formatLink(oldlink,number)
        {
            if(oldlink === '')
            {
                return '';
            }
            return "<a href=" + urllink + oldlink + ">Link</a>"+ number;
        }

        function formatQuery(query)
        {
            if(query.length > qshortsize)
            {
                var shortquery = query.substring(0,qshortsize) + "...";
                var querylink = "<span class='shortquery' title='" + query + "'>" + shortquery + "</span>";
                return querylink;
            }
            else
            {
                return query;
            }
        }

        console.log(allresponse);
        var headarr = ["","#","Query","Start Date","End Date","Database", "Status", "Date Executed", "Sample Description", "Execution Time (hh:mm)", "Download File", "Sample File"];
        var queryidx = headarr.indexOf("Query");
        numidx = headarr.indexOf("#");
        var headstr = "";
        for(var i=0;i<headarr.length;i++)
        {
          headstr+="<th>"+headarr[i]+"</th>";
        }
        jobstatusHead.html(headstr);
      
        var urllink = getUrl() + "/db/getfile?fileid=";
        var datastr="";
        var data = allresponse.result["builds"];
        var oneJobNotFinished = false;
        var completeimg = "img/green.png";
        var inprocessimg = "img/yellow.png";
        var errorimg = "img/red.png";
        var abortedimg = "img/aborted.png";
        for(var idx=0;idx<data.length;idx++)
        {
            var response = data[idx];
            var dataarr = [];
            var downloadfileid = "";
            var samplefileid = "";
            var duration = "";
            var statusstr = "";
            var colorimg = "";
            var totalcount = "";
            var samplecount = "";
            var errorstr = "Error";
            if('env' in response && 'totalcount' in response['env'])
            {
                totalcount = "&nbsp;(" + numberWithCommas(parseInt(response['env']['totalcount'])) + ")";
            }
            if('env' in response && 'samplecount' in response['env'])
            {
                samplecount = "&nbsp;(" + numberWithCommas(parseInt(response['env']['samplecount'])) + ")";
            }
            if('env' in response && 'error' in response['env'])
            {
                //Add error string as a hover text over Error status
                errorstr = "<span title='" + response['env']['error'] + "'>Error</span>";
            }
            if(response["data"]["building"] == false)
            {
                if(response["data"]["result"] == "FAILURE")
                {
                    colorimg = errorimg;
                    statusstr = errorstr;
                } else if(response["data"]["result"] == "SUCCESS") {
                    colorimg = completeimg;
                    statusstr = "Complete";
                    if(response["data"]["parameters"]["getSample"] == "true")
                    {
                        samplefileid = formatLink(response["data"]["parameters"]["samplefileid"],samplecount);
                        downloadfileid = "" + totalcount; //No download data for Sample any more
                    } else {
                        downloadfileid = formatLink(response["data"]["parameters"]["downloadfileid"],totalcount);
                        //No sample data if only download
                    }
                } else if(response["data"]["result"] == "ABORTED") {
                    colorimg = abortedimg;
                    statusstr = "Aborted";
                } else {
                    colorimg = errorimg;
                    statusstr = "Unknown " + errorstr;
                }
                duration = response["data"]["duration"]/1000/60;
            } else {
                colorimg = inprocessimg;
                statusstr = "In Process";
                //If detailed status exists, then add it as hove text
                if("status" in response)
                {
                    statusstr = "<span class='shortquery' title='" + statusstr + "..." + response["status"] + "'>" + statusstr + "...</span>";
                }
                var cancelbtn = "<button class='btn btn-danger cancelbtn'>Cancel</button>";
                statusstr = cancelbtn + "&nbsp;" + statusstr;
                oneJobNotFinished = true;
                downloadfileid = "";
                samplefileid = "";
                duration = (((new Date()).getTime() - (new Date(response["data"]["timestamp"])).getTime()) / 1000) / 60;
            }
            dataarr.push("<img width='16' height='16' src='" + colorimg + "'>")
            dataarr.push(response["number"]);
            dataarr.push(formatQuery(response["data"]["parameters"]["query"]));
            dataarr.push(formatDate(response["data"]["parameters"]["startdate"],false));
            dataarr.push(formatDate(response["data"]["parameters"]["enddate"],false));
            //Have default to Twitter database
            var db = "twitter";
            if("db" in response["data"]["parameters"] && response["data"]["parameters"].length > 0) 
            {
                db = response["data"]["parameters"]["db"];
            }
            dataarr.push(db);
            dataarr.push(statusstr);
            dataarr.push(formatDate(response["data"]["timestamp"],true));
            var sampledescript = "None";
            if(response["data"]["parameters"]["getSample"] == "true")
            {
                sampledescript = response["data"]["parameters"]["samplemethod"] + " " + response["data"]["parameters"]["sample"];
                if(response["data"]["parameters"]["sampletype"] == "percent")
                {
                    sampledescript += "%";
                }
            }
            dataarr.push(sampledescript)
            dataarr.push(formatMinuteDuration(duration));
            dataarr.push(downloadfileid);
            dataarr.push(samplefileid);
          
            datastr+="<tr>";
            for(var i=0;i<dataarr.length;i++)
            {
                var className = "";
                if(i == queryidx)
                {
                    className = " style='width: " + queryColumnSize + "'";
                }
                else if(i == numidx)
                {
                    className = " style='width: " + numColumnSize + "'";
                }
                datastr+="<td" + className + ">"+dataarr[i]+"</td>";
            }
            datastr+="</tr>";
        }

        if(!oneJobNotFinished)
        {
            //All jobs are finished, so stop the autoGet
            startStopAuto();
        }

        jobstatusBody.html(datastr);

        //Allow shrink/expand of Query and In Progress Status
        $('.shortquery').on('click', function(e) {
            var longquery = 'longquery';
            var shortquery = 'short';
            if($(this).hasClass(longquery))
            {
                //Is expanded, so shrink
                var longtext = $(this).html();
                var shorttext = $(this).attr(shortquery);
                $(this).attr('title',longtext);
                $(this).html(shorttext);
                $(this).removeClass(longquery);
            } else {
                //Is shrunk, so expand
                var longtext = $(this).attr('title');
                var shorttext = $(this).html();
                $(this).attr(shortquery,shorttext);
                $(this).removeAttr('title');
                $(this).html(longtext);
                $(this).addClass(longquery);
            }
        });
        $('.cancelbtn').on('click', function(e) {
            if(!$(this).hasClass(cancelbtnStarted))
            {
                //Start the cancel
                startCancel(this);
            } else {
                //stop the Cancel
                stopCancel();
            }
        });
    }

    var cancelbtnStarted = 'cancelbtnStarted';
    var runningCancel = false;
    function startCancel(clickedbtn)
    {
        if(! runningCancel)
        {
            var jobid = parseInt($(clickedbtn).closest("tr").find("td:eq(" + numidx + ")").html());
            //Ask user if they are sure they want to cancel the job
            OpenModalBox("Cancel Job " + jobid,"Are you sure you want to cancel job " + jobid + "?","<button class='btn btn-primary confirmbtn'>Confirm</button><button class='btn btn-danger notconfirmbtn'>Cancel</button>");
            $('.confirmbtn').on('click', function(e) {
                CloseModalBox();
                ajaxGet.abort();//kill Ajax if running
                //Stop the auto
                if(runningAuto)
                {
                    startStopAuto();
                }
                canGetJobs = false;//stop auto running
                box.html('Cancel ' + jobid + '...');
                toggleBox();
                $(clickedbtn).addClass(cancelbtnStarted);
                $(clickedbtn).html("Stop Cancel");
                ajaxCancelJob(jobid);
                runningCancel = true;
            });
            $('.notconfirmbtn').on('click', function(e) {
                CloseModalBox();
            });
        }
    }

    function resetCancelButton()
    {
        var clickedbtn = '.' + cancelbtnStarted;
        $(clickedbtn).html("Cancel");//Need to change the html before removing class (because we are referencing by class)
        $(clickedbtn).removeClass(cancelbtnStarted);
    }

    function stopCancel()
    {
        resetCancelButton();
        ajaxGet.abort();
        box.html('Failed.');
        toggleBox();
        canGetJobs = true;//start auto running
        runningCancel = false;
    }
  
    //Error conpletion of AJAX
    function processJobsError(response) {
        box.html('Failed.');
        startStopAuto();
        toggleBox();
    }
  
    //Get the Information
    function ajaxGetJobs() {
        var requestGetLong = {'job':1}
        ajaxGet = $.ajax({
            url: getJobsLocation,
            type: "post",
            dataType: "json",
            data: JSON.stringify(requestGetLong),
            contentType: 'application/json; charset=utf-8',
            success: processJobs,
            error: processJobsError,
            timeout: 2*60*1000 //2 minutes
        });
    }

  
    //Get the Information
    function ajaxCancelJob(jobid) {
        var requestGetLong = {'job':1,'jobid':jobid}
        ajaxGet = $.ajax({
            url: cancelJobLocation,
            type: "post",
            dataType: "json",
            data: JSON.stringify(requestGetLong),
            contentType: 'application/json; charset=utf-8',
            success: processCancelJob,
            error: stopCancel,
            timeout: 2*60*1000 //2 minutes
        });
    }

    function processCancelJob(response) {
        //successful cancel
        //update process list
        if(response['result'])
        {
            box.html('Successful.');
            toggleBox();
            canGetJobs = true;//start auto running
            autoGet();
            runningCancel = false;
        } else {
            stopCancel();
        }
    }
};
