//Written by DWS
function setupCountData()
{
	var dbcounts = new setupRTCountDB(gotDB);

    function gotDB()
    {
        //Got the information from the database
        //Reformat the data into JSON {row,col,value} for the heat map
        //Make 2 heatmapings, tags and rules
        //Allow filtering by tags for Rules (just like RuleHistory)
        //Have rule display shortage to 10 characters, but tooltip of everything
        // 
        screenHold("Processing Counts","Generating the Heat Map of the Counts.  Please wait...","",
         function() {
            setupTagSelection();
            setupMap("A",dbcounts.DB_tagcount,dbcounts.DB_tags,false);
            setupMap("B",dbcounts.DB_rulecount,dbcounts.DB_rules,true);
            plotTagDaily(dbcounts.DB_tagdailycount.data,dbcounts.DB_tags);
        });

        $('.label-to-csv').show();
        $('.rule-count-to-csv').off('click.all');
        $('.rule-count-to-csv').on('click.all', function() {
            saveData(dbcounts.DB_rulecount,dbcounts.DB_rules,"Rule",true);
        });
        
        $('.tag-count-to-csv').off('click.all');
        $('.tag-count-to-csv').on('click.all', function() {
            saveData(dbcounts.DB_tagcount,dbcounts.DB_tags,"Tag",false);
        });

        $('.tag-daily-count-to-csv').off('click.all');
        $('.tag-daily-count-to-csv').on('click.all', function() {
            saveDailyData(dbcounts.DB_tagdailycount.download,dbcounts.DB_tags);
        });
    } 

    //setup tag selection
    function setupTagSelection()
    {
        var secondstr = '<select class="s2_with_tag filtercolumnB" multiple>';
        $.each ( dbcounts.DB_tags,function(k,v)
        {
            secondstr += "<option>("+k+") " + v + "</option>";
        });
        secondstr += "</select>";
        $('#tagfilter').html(secondstr);

        //Turn on Select2 capability
        $('.s2_with_tag').select2({placeholder: "Select Tags"});

        //Look for changes in column filters
    }

    //Setup tag
    function setupMap(addedId,counts,maps,isrule)
    {
        var heatdata = [];
        var headerrow = counts[0];
        var ididx = headerrow.indexOf('_id');
        var rowLabel = [];//row name of num
        var hcrow = [];//row num to name
        var hcrow_looup = {};
        //var hcrow_revlooup = {};
        var inc = 0;
        var tag_to_rule = {};

        var copyhcrow = {};//make, quick lookup and removal
        $.each(maps, function(k, v) {
            //the key and value pair
            //add tag to front of label
            var label = v;
            //For rules add the tag to the beginning of the Label
            if(isrule)
            {
                if(!(parseInt(k) in dbcounts.DB_rules2tags))
                {
                    console.log("Skipping " + parseInt(k) + " of " + v + ", because no tag");
                    return;//skip this rule because it does not have a tag
                }
                var tagid = dbcounts.DB_rules2tags[parseInt(k)];
                var tagname = "("+tagid+") "+dbcounts.DB_tags[tagid];
                label =  "{" + tagname + "} " + v;
                if(!(tagname in tag_to_rule))
                {
                    tag_to_rule[tagname] = [];
                }
                tag_to_rule[tagname].push(inc);
            }

            rowLabel.push(parseInt(k) + "=" +label);//row name of num
            hcrow_looup[parseInt(k)] = inc;
            //hcrow_revlooup[inc] = parseInt(k);//reverse lookup
            hcrow.push(inc);//row num to name
            copyhcrow[inc]=parseInt(k);
            inc++;
        });
        var colLabel = headerrow.slice();//make copy
        colLabel.splice(ididx,1);//remove id - col name of num
        var hccol = [];//col num to name
        //Make numbers 0 to X number of columns in array
        for(var j=0;j<colLabel.length;j++)
        {
            hccol.push(j);
        }
        //white to yellow to red: see http://www.strangeplanet.fr/work/gradient-generator/index.php
        //var colors = ["#FFFF00", "#FFF200", "#FFE500", "#FFD800", "#FFCC00", "#FFBF00", "#FFB200", "#FFA500", "#FF9900", "#FF8C00", "#FF7F00", "#FF7200", "#FF6600", "#FF5900", "#FF4C00", "#FF3F00", "#FF3200", "#FF2600", "#FF1900", "#FF0C00", "#FF0000"];
        var colors = ["#FFFFFF", "#FFFFD6", "#FFFFCC", "#FFFFC1", "#FFFFB7", "#FFFFAD", "#FFFFA3", "#FFFF99", "#FFFF8E", "#FFFF84", "#FFFF7A", "#FFFF70", "#FFFF66", "#FFFF5B", "#FFFF51", "#FFFF47", "#FFFF3D", "#FFFF32", "#FFFF28", "#FFFF1E", "#FFFF14", "#FFFF0A", "#FFFF00", "#FFF400", "#FFE900", "#FFDF00", "#FFD400", "#FFC900", "#FFBF00", "#FFB400", "#FFAA00", "#FF9F00", "#FF9400", "#FF8A00", "#FF7F00", "#FF7400", "#FF6A00", "#FF5F00", "#FF5500", "#FF4A00", "#FF3F00", "#FF3500", "#FF2A00", "#FF1F00", "#FF1500", "#FF0A00", "#FF0000"];
        var maxvalue = 0;


        //Cycle through all rows, skipping the headerrow
        for(var i=1;i<counts.length;i++)
        {
            if(!(parseInt(counts[i][ididx]) in hcrow_looup))
            {
                console.log("Splicing out row " + i + ", because no tag for " + counts[i][ididx]);
                counts.splice(i,1);//remove row because tag does not exist for it
                i--;//backup and try again
                continue;//skip adding this data
            }
            var row = hcrow_looup[parseInt(counts[i][ididx])];//number
            delete copyhcrow[row];//remove so we know what we don't have data for
            for(var j=0;j<counts[i].length; j++)
            {
                //Skip the id column
                if(j!=ididx)
                {
                    var col = j;//number
                    var value = counts[i][j];//number
                    if(value > maxvalue)//save max value
                    {
                        maxvalue = value;
                    }
                    heatdata.push({"row":row,"col":col,"value":value});     
                }
            }
            
        }

        //Remove the extra rows that are not defined in the data
        var removerows = Object.keys(copyhcrow);
        removerows.reverse();//remove in reverse order so we don't look index
        console.log("Removing " + removerows.length);
        $.each(removerows,function(i,v)
        {
            //remove v from rowLabel and hcrow
            console.log(copyhcrow[v] + ":" + v + ":" + hcrow[v] + "," + rowLabel[v]);
            hcrow.splice(v,1);
            rowLabel.splice(v,1);
        });

        //Define the legend
        var domain = [0,maxvalue];//range of numbers
        var legenddomain = [];
        var numLegends = 5;
        //Split the domain into equal steps
        var step = Math.floor(maxvalue/numLegends);
        for(var j=0;j<maxvalue-step;j+=step)
        {
            legenddomain.push(j);
        }
        legenddomain.push(maxvalue);//always end with maxvalue


        //if rule then add more header to the row
        var options = {};
        if(isrule)
        {
            options.rowHeadSize = 500;//25 normal
            options.rowHeadMax = 70;//25 normal
        }

        var hm = new heatMap(addedId,heatdata,rowLabel,colLabel,colors,hcrow,hccol,domain,legenddomain,tag_to_rule,'MONTH',options);
    }

    function plotTagDaily(counts,maps)
    {
        //This is used to plot a line graph of Tags over days
        //counts: [headerrow,tagcount,tagcount,...]
        //counts = [counts[0],counts[1]];//DEBUG
        var headerrow = counts[0].slice();//make copy
        var ididx = headerrow.indexOf('_id');
        headerrow.splice(ididx,1);
        //Turn YYYYMMDD => YYYY-MM-DD to be recognized by Plotly as a date
        for(var ii=0;ii<headerrow.length;ii++)
        {
            var year = headerrow[ii].slice(0,4);
            var month = headerrow[ii].slice(4,6);
            var day = headerrow[ii].slice(6,8);
            headerrow[ii] =  year + "-" + month + "-" + day;
        }
        var title = "Tag Daily Counts";

        //clone array of array
        var data = JSON.parse(JSON.stringify(counts));
        data.shift()//remove header row
        var alltrace = [];


        for(var ii=0;ii<data.length;ii++)
        {
            //remove _id from row
            var rowtag = data[ii][ididx];
            data[ii].splice(ididx,1);//remove id
            var trace1 = {
              x: headerrow,
              y: data[ii],
              name: "("+rowtag+") "+maps[rowtag],
              type: 'scatter'
            };
            alltrace.push(trace1);
        }

        var layout = {
          //title: title,
          xaxis: {
            title: 'Date',
            titlefont: {
              family: 'Courier New, monospace',
              size: 18,
              color: '#777777'
            }
          },
          yaxis: {
            title: 'Counts',
            titlefont: {
              family: 'Courier New, monospace',
              size: 18,
              color: '#777777'
            }
          },
          hovermode: 'closest',
          //autosize: false,
          //width: '100%',
          height: 600,
          // margin: {
          //   l: 20,
          //   r: 20,
          //   b: 100,
          //   t: 100,
          //   pad: 4
          // },
          showlegend:true
          // legend:{
          //       x:0.9,
          //       y:1
          //   }
        };
        var container = 'tagdaily';
        Plotly.newPlot(container, alltrace,layout);

        function updateData(days)
        {
            var graph = document.getElementById(container);
            for(var ii=0;ii<data.length;ii++)
            {
                //remove _id from row
                graph.data[ii].y= movingWindowAvg(data[ii],days);
            }

            Plotly.redraw(graph);
        }

        var slider_range_min_amount = $(".slider-range-min-amount");
        var slider_range_min = $(".slider-range-min");
        slider_range_min.slider({
            range: "min",
            value: 0,
            min: 0,
            max: 30,
            slide: function( event, ui ) {
                var newtext = "";
                if(ui.value == 0)
                {
                    newtext = "Disabled";
                } else {
                    newtext = ui.value +" Days";
                }
                slider_range_min_amount.val( newtext);
                updateData(ui.value);
            }
        });
        slider_range_min_amount.val("Disabled");
        $('#btnshowall').on("click",function() {
            setVisibility(true);
        });

        function setVisibility(visibilty)
        {
            var plotDiv = document.getElementById(container);
            var plotData = plotDiv.data;
            $.each(plotData, function(key, value){
                plotDiv.data[key].visible = visibilty;
            })
            Plotly.redraw(plotDiv);            
        }

        $('#btnhideall').on("click",function() {
            setVisibility('legendonly');
        });

        function resizePlot()
        {
            var plotDiv = document.getElementById(container);
            Plotly.Plots.resize(plotDiv);
        }

        //$('#dailyexpand').on("click",function() {setTimeout(resizePlot,100);});
        $('.show-sidebar').on("click",function() {setTimeout(resizePlot,200);});
        $(window).on("resize",resizePlot);
    }

    function saveDailyData(counts,maps)
    {
        //Replace the header row non-'Date' with actual Tag name
        //clone array of array
        var data = JSON.parse(JSON.stringify(counts));
        var dateidx = data[0].indexOf('Date');
        for(var ii=0;ii<data[0].length;ii++)
        {
            if(ii != dateidx)
            {
                id = data[0][ii];
                var mapid = "";
                if(id in maps)
                {
                    mapid = "("+id+") " + maps[id];
                } else {
                    //no mapping for id
                    mapid = "("+id+") " +"unknown";
                }
                data[0][ii] = mapid;
            }
        }        
        //Change date to better format (skip the header row)
        for(var ii=1;ii<data.length;ii++)
        {
            var year = data[ii][dateidx].slice(0,4);
            var month = data[ii][dateidx].slice(4,6);
            var day = data[ii][dateidx].slice(6,8);
            data[ii][dateidx] =  year + "-" + month + "-" + day;
        }
        var csv = new csvWriter();
        var csvdata = csv.arrayToCSV(data);
        var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
        saveAs(blob, 'tagdaily_counts.csv');
    }

    function saveData(counts,maps,typeid,isrule){

        var headerrow = counts[0];
        var ididx = headerrow.indexOf('_id');

        //clone array of array
        var data = JSON.parse(JSON.stringify(counts));
        data.shift()//remove header row

        //Create header
        var temp = headerrow.slice();//make copy
        temp.splice(ididx,1);//remove id - col name of num
        temp.unshift(typeid + " Name");
        temp.unshift(typeid + " ID");
        var removerow = [];//rows to be removed after editing

        //For rules, add tag name and ID
        if(isrule)
        {
            temp.unshift("Tag Name");
            temp.unshift("Tag ID");
        }

        for(var i=0;i<data.length;i++)
        {
            var id = data[i].splice(ididx,1)[0];//remove id - col name of num
            var mapid = "";
            if(id in maps)
            {
                mapid = maps[id];
            } else {
                //remove row
                removerow.push(i);
                mapid = "unknown";
            }
            data[i].unshift(mapid);
            data[i].unshift(""+id);
            //Add tags if rules
            if(isrule)
            {
                var tagid = ""+dbcounts.DB_rules2tags[id];
                //If tag is undefined for rule, then remove rule
                if(!(parseInt(tagid) in dbcounts.DB_tags))
                {
                    removerow.push(i);
                }
                var tagname = ""+dbcounts.DB_tags[parseInt(tagid)];
                data[i].unshift(tagname);
                data[i].unshift(tagid);
            }
        }
        //Need to remove in reverse order so not to mess up the order
        for(var rowidx = removerow.length-1; rowidx >= 0; rowidx--)
        {
            data.splice(removerow[rowidx],1);
        }
        data.unshift(temp);  //add header row back

        //Add Header row
        var csv = new csvWriter();
        var csvdata = csv.arrayToCSV(data);
        var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
        saveAs(blob, typeid + '_counts.csv');
    }
}
