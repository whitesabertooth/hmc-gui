//Written by DWS
var setupRTCountDB = function(completeFcn)
{
    /*
    Flow of AJAX calls:
    ajaxGetDBInfo(),procesDBError(),processDB()
    -> ajaxGetDBRuleCount(), processDBErrorRuleCount(), processDBRuleCount(), 
    -> ajaxGetDBTagCount(), processDBErrorTagCount(), processDBTagCount()
    -> ajaxGetDBTagDailyCount(), processDBErrorTagDailyCount(), processDBTagDailyCount()
    */


    //Database unique variables
    this.DB_tags;
    this.DB_rules;
    this.DB_rules2tags;
    this.DB_rule2time;
    this.DB_minDate;
    this.DB_maxDate;
    this.DBrunning;
    this.DB_tagcount;
    this.DB_rulecount;
    this.DB_tagdailycount;
    var that = this;
    var configLocation = "/db/config.json";
    var ruleCountLocation = "/db/rulecount.json";
    var tagCountLocation = "/db/tagcount.json";
    var tagDailyCountLocation = "/db/tagdailycount.json";
    var ajaxDB;

    $(document).ready(setup);

    //Successful conpletion of AJAX for DB Info
    function processDB(response) {
        // config mappings
        if(response.success == 'true')
        {
            that.DB_tags = response.data.tags;
            that.DB_rules = response.data.rules;
            that.DB_rules2tags = response.data.rules_tags;
            //Set DB name
            that.DBrunning = $('#queryDBAll').val();
            $('.dbname').html(that.DBrunning);


            //Continue to get rule count
            ajaxGetDBRuleCount(that.DBrunning);

        } else {
            OpenModalBox('Database Selection Error', 'No data returned from about the database selected.');
        }
    }

    //Error when getting DB info
    function processDBError() {
        OpenModalBox('Database Selection Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        endPause(false);
    }

    //Get the Database Information
    function ajaxGetDBInfo(dbname) {
        console.log(dbname);
        ajaxDB = $.ajax({
            url: configLocation,
            type: "post",
            data: JSON.stringify({"db":dbname}),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processDB,
            error: processDBError,
            timeout: 30*1000 //30 seconds
        });
    };




    //Successful conpletion of AJAX for DB Info
    function processDBRuleCount(response) {
        // config mappings
        if(response.success == 'true')
        {
            convertMonth(response.data[0]);

            that.DB_rulecount = response.data;

        } else {
            OpenModalBox('Database History Retrival Error', 'No data returned from request for rule history about the database selected.');
        }

        //Continue to get tag count
        ajaxGetDBTagCount(that.DBrunning);
    }

    //Error when getting DB info
    function processDBErrorRuleCount() {
        OpenModalBox('Database History Retrival Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        endPause(false);
    }

    //Get the Database Information
    function ajaxGetDBRuleCount(dbname) {
        console.log(dbname);
        ajaxDB = $.ajax({
            url: ruleCountLocation,
            type: "post",
            data: JSON.stringify({"db":dbname}),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processDBRuleCount,
            error: processDBErrorRuleCount,
            timeout: 30*1000 //30 seconds
        });
    };


    //Successful conpletion of AJAX for DB Info
    function processDBTagCount(response) {
        // config mappings
        if(response.success == 'true')
        {
            convertMonth(response.data[0]);

            that.DB_tagcount = response.data;

        } else {
            OpenModalBox('Database History Retrival Error', 'No data returned from request for rule history about the database selected.');
        }

        //Continue to get tag count
        ajaxGetDBTagDailyCount(that.DBrunning);
    }

    //Error when getting DB info
    function processDBErrorTagCount() {
        OpenModalBox('Database History Retrival Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        endPause(false);
    }

    //Get the Database Information
    function ajaxGetDBTagCount(dbname) {
        console.log(dbname);
        ajaxDB = $.ajax({
            url: tagCountLocation,
            type: "post",
            data: JSON.stringify({"db":dbname}),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processDBTagCount,
            error: processDBErrorTagCount,
            timeout: 30*1000 //30 seconds
        });
    };




    //Successful conpletion of AJAX for DB Info
    function processDBTagDailyCount(response) {
        // config mappings
        if(response.success == 'true')
        {
            that.DB_tagdailycount = response.data;

        } else {
            OpenModalBox('Database History Retrival Error', 'No data returned from request for rule history about the database selected.');
        }
        endPause(false);
        $('.querybox').show();
        completeFcn();
    }

    //Error when getting DB info
    function processDBErrorTagDailyCount() {
        OpenModalBox('Database History Retrival Error', 'Could not connect to Database or taking too long.  Try again later or contact HMC Tech Group.');
        endPause(false);
    }

    //Get the Database Information
    function ajaxGetDBTagDailyCount(dbname) {
        console.log(dbname);
        ajaxDB = $.ajax({
            url: tagDailyCountLocation,
            type: "post",
            data: JSON.stringify({"db":dbname}),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: processDBTagDailyCount,
            error: processDBErrorTagDailyCount,
            timeout: 30*1000 //30 seconds
        });
    };




    //Called to start getting the database information
    function getdbInfo()
    {
        //ajax query to get:
        startPause();
        ajaxGetDBInfo($('#queryDBAll').val());
    }

    //Freeze the window, put button to cancel
    function startPause()
    {
        $('#pauseAll').html('<span><h4>Getting Database Information</h4> <button type="button" id="endDBquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button></span>');
        $('#endDBquery').on('click', function() {endPause(true);});
        $('#queryDBAll').attr('disabled', 'disabled');
    }

    //Unfreeze window, remove button
    function endPause(isCancel)
    {
        //Check if want to cancel the AJAX too
        if(isCancel)
        {
            ajaxDB.abort();
        }
        $('#pauseAll').html('');
        $('#queryDBAll').removeAttr('disabled');
    }

    function setup()
    {
        $('#queryDBAll').on('change', function(e) {
             if($('#queryDBAll').val() != 'Select...')
             {
                 getdbInfo();
             }
        });
        if($('#queryDBAll').val() != 'Select...')
        {
            setTimeout(getdbInfo,100);
        }
        $('.querybox').hide();

    }
}