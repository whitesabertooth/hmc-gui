function setupCodeManage()
{
    var labelStr = 'LABEL_';
    var imgvidStr = 'INSTAGRAM_';
    var imgStr = 'IMAGE_';
    var youtubeStr = 'YOUTUBE_';
    var videoStr = 'VIDEO_';
    var groupbyStr = "GROUPBY_";
    var imgvidIcon = '<i class="fa fa-instagram"></i>&nbsp;';
    var youtubeIcon = '<i class="fa fa-youtube"></i>&nbsp;';

    //Written by DWS
    function addLabelAnswer() {
        if(document.labelForm.answer_text.value.length > 0)
        {
            var option = document.createElement("option");
            option.text=document.labelForm.answer_text.value;
            document.labelForm.label_data.add(option);
            document.labelForm.answer_text.value = "";
        } else {
            alert("Blank answer is not acceptable");
        }
    };
    function sortLabel(isUp) {
        if(document.labelForm.label_data.selectedIndex >= 0)
        {
            // get all options
            var shippingOptions = $('select[name=label_data] option');
            var firstidx = document.labelForm.label_data.selectedIndex;
            var secondidx;
            if(isUp)
            {
                //swap places with above one
                if(0 <= firstidx - 1)
                {
                    secondidx = firstidx - 1;
                } else {
                    //Do not need to shift
                    return;
                }
            } else {
                //swap places with below one
                if(shippingOptions.length > firstidx + 1)
                {
                    secondidx = firstidx + 1;
                } else {
                    //Do not need to shift
                    return;
                }
            }
            var optiontemp = shippingOptions[firstidx];
            shippingOptions[firstidx] = shippingOptions[secondidx]; 
            shippingOptions[secondidx] = optiontemp; 
            
            
            // update the select with the new option order
            $('select[name=label_data]').append(shippingOptions);
        }
    };
    function removeLabel() {
        if(document.labelForm.label_data.selectedIndex >= 0)
        {
            document.labelForm.label_data.remove(document.labelForm.label_data.selectedIndex);
        }
    };

    function checkLabelAnswer(event) {
        
        if (event.keyCode == 13) {
            event.preventDefault();
            addLabelAnswer();
        }
    };
    function setLabelForm(question,answerset) {
        document.labelForm.question_text.value = question;
        
        for(var i=0; i<answerset.length; i++)
        {
            var option = document.createElement("option");
            option.text=answerset[i];
            document.labelForm.label_data.add(option);
        }
        
    };
    function clearLabelForm() {
        document.labelForm.question_text.value = "";
        document.labelForm.answer_text.value = "";
        
        //Clear select
        var i;
        for(i=document.labelForm.label_data.options.length-1;i>=0;i--)
        {
            document.labelForm.label_data.remove(i);
        }
    };
    var linedata = [];
    linedata.push('<form name="labelForm">');
    linedata.push('    <h4>Coding attributes</h4>');
    linedata.push('    Question:&nbsp;<input id="question_text" type="text" name="question_text" size="40"/><br>');
    linedata.push('    <hr width=100% align=left color=black>');
    linedata.push('    <table width=100%>');
    linedata.push('        <tr style="padding: 50px">');
    linedata.push('            <td style="vertical-align: top" >');
    linedata.push('                <h5><i>Note: May hit <kbd>enter</kbd> to add answer</i></h5><input id="answertext" type="text" name="answer_text" size="40"/>');
    linedata.push('            </td>');
    linedata.push('            <td>');
    linedata.push('                <button id="answerbtn" type="button" class="btn btn-primary btn-lg">Add Answer</button>');                    
    linedata.push('            </td>');
    linedata.push('        </tr>');
    linedata.push('    </table>');
    linedata.push('    Answer List:');
    linedata.push('    <table width=100% align=center>');
    linedata.push('        <tr style="padding: 10px">');
    linedata.push('            <td valign=center align=center style="padding: 10px">');
    linedata.push('                <a id="sortup"><i class="fa fa-arrow-up"></i></a><br/>');
    linedata.push('                <a id="removelbl"><i class="fa fa-times"></i></a><br>');
    linedata.push('                <a id="sortdown"><i class="fa fa-arrow-down"></i></a><br/>');
    linedata.push('            </td>');
    linedata.push('            <td>');
    linedata.push('                <select id="selectanswers" size=5 name="label_data" style="width: 300px">');
    linedata.push('                </select>');
    linedata.push('                <br>(double-click to copy)');
    linedata.push('            </td>');
    linedata.push('        </tr>');
    linedata.push('    </table>');
    linedata.push('    <hr width=100% align=left color=black>');
    linedata.push('    <table width=500>');
    linedata.push('        <tr>');
    linedata.push('            <td width=50% align=center>');
    linedata.push('                <button id="addlabel" type="button" class="btn btn-primary btn-lg">Apply</button>');
    linedata.push('            </td>');
    linedata.push('            <td width=50% align=center>');
    linedata.push('                <button id="cancellabel" type="button" class="btn btn-primary btn-lg">Cancel</button>');
    linedata.push('            </td>');
    linedata.push('        </tr>');
    linedata.push('    </table>');
    linedata.push('    </form>');
    $('#specialBox').html(linedata.join(' '));

    $('#answertext').on('keydown',checkLabelAnswer);
    $('#answerbtn').on('click',addLabelAnswer);
    $('#sortup').on('click',function(){sortLabel(true);});
    $('#removelbl').on('click',removeLabel);
    $('#sortdown').on('click',function(){sortLabel(false);});
    $('#addlabel').on('click',addLabel);
    $('#cancellabel').on('click',toggleOverlay);
    $('#selectanswers').on("dblclick",function() {
        //Copy selected row to text input
        if(document.labelForm.label_data.selectedIndex >= 0)
        {
            document.labelForm.answer_text.value = document.labelForm.label_data[document.labelForm.label_data.selectedIndex].value;
        }
    })

    //Global window.labelData
    window.labelData = [];
    
    //Currently editing label index in window.labelData
    var currentEditLabel = -1;
    
    //Update the list based on window.labelData
    function updateLabelList(){
        //remove all remaining list items
        //Clear select
        var i;
        for(i=document.getElementById('label-list').options.length-1;i>=0;i--)
        {
            document.getElementById('label-list').remove(i);
        }

        var newoptions = [];
        var oneopt;
        var i;
        for(i=0; i<window.labelData.length; i++)
        {
            var str = window.labelData[i]["question"] + "? (";
            for(j=0; j<window.labelData[i]["answerset"].length; j++)
            {
                if(j>0) {str+=", ";}
                str += window.labelData[i]["answerset"][j];
            }
            oneopt = document.createElement("option");
            oneopt.text=str + ")";
            //newoptions.push(oneopt);
            document.getElementById('label-list').add(oneopt);
        }
        //Save
        saveToStorage();
    };
    
    //Submit Add/Edit label defined in label set (might already be editted)
    function addLabel(){
        //pull all options into an array
        var shippingOptions = document.labelForm.label_data.options;
        
        //Convert option array to text array
        var textoptions = [];
        for(var i=0; i<shippingOptions.length; i++)
        {
            textoptions[i] = shippingOptions[i].text;
        }
        
        var question = document.labelForm.question_text.value;
        
        if(currentEditLabel >= 0)
        {
            //replace
            window.labelData[currentEditLabel] = { "question" : question,
                                      "answerset" : textoptions};
        } else {
            //Add new
            window.labelData[window.labelData.length] = { "question" : question,
                                            "answerset" : textoptions};
        }
        
        //call update of select based on window.labelData
        updateLabelList();
        
        toggleOverlay();
        
    };

    function copyLabel(){
        //pull all options into an array
        if(document.getElementById('label-list').selectedIndex >= 0)
        {
            openLabel(document.getElementById('label-list').selectedIndex,true);
        }
                
    };
    
    //Open label add/edit display
    function openLabel(editNumber,iscopy) {
        
        clearLabelForm();
        
        if(editNumber >= 0 && window.labelData.length > editNumber)
        {
            currentEditLabel = editNumber;
            setLabelForm(window.labelData[editNumber]["question"],window.labelData[editNumber]["answerset"]);
        } else {
            currentEditLabel = -1;
        }

        //If copy then select new label
        if(iscopy)
        {
            currentEditLabel = -1;
        }
        
        toggleOverlay();
        
        //Focus after update
        setTimeout ( function () {
            /* show the cursor */
            $('#question_text').focus();
        }, 100);
    };

    //Edit label
    function editLabel() {
        if(document.getElementById('label-list').selectedIndex >= 0)
        {
            openLabel(document.getElementById('label-list').selectedIndex,false);
        }
    }
    $('.edit-label').on('click', editLabel);
    $('#addLabelNew').on('click',function() {openLabel(-1);});
    $('#label-list').on('dblclick',editLabel);
    $('.copy-label').on('click',copyLabel);
    //Remove label
    $('.remove-label').on('click', function(){
        if(document.getElementById('label-list').selectedIndex >= 0)
        {
             var removeidx = document.getElementById('label-list').selectedIndex;

            //remove from array
            window.labelData.splice(removeidx,1);
        }
        
        //Update display to match
        updateLabelList();
    });

    //Save the label setup to persistant location in browswer
    function saveToStorage() {
        localStorage.setItem('window.labelData',JSON.stringify(window.labelData));
    };
    
    function loadFromStorage() {
        if(!localStorage.getItem('window.labelData'))
        {
        } else {
            window.labelData = JSON.parse(localStorage.getItem('window.labelData'));
            updateLabelList();
        }
    };
    

    $(document).ready(function() {
        // Load Datatables and run plugin on tables 
        //TestTable3(true);//true means Label
        
        loadFromStorage();
        
        //DOWNLOAD BUTTON
        $('.label-to-csv').on('click', function(e){
            e.preventDefault();
            
            //Temporary solution - change to save to CSV and load CSV
            //var result_json = JSON.stringify(window.labelData);
            //OpenModalBox('Table to JSON values', result_json);
            var headerdata = ['question'];
            var data = [];
            var maxlength= 0;
            if(window.labelData.length == 0) {return;}//stop if no data
            for(var i=0;i<window.labelData.length;i++)
            {
                //Find max length
                if(window.labelData[i]['answerset'].length > maxlength)
                {
                    maxlength = window.labelData[i]['answerset'].length;
                }
                
            } 
            for(var i=0; i<maxlength; i++)
            {
                headerdata.push('answer' + (i+1));
            }
            for(var i=0;i<window.labelData.length;i++)
            {
                data[i] = [window.labelData[i]['question']];
                var alen = window.labelData[i]['answerset'].length;
                for(var j=0; j<alen; j++)
                {
                    data[i][j+1] = window.labelData[i]['answerset'][j];
                }
                //Pack in empty set to make it max columns
                for(var k=0; k<maxlength - alen; k++)
                {
                    data[i][alen+k+1] = "";//shift by one for question
                }
            }                    
            
            //Add Header row
            var alldata = [headerdata].concat(data);
            var csvdata = $.csv.fromArrays(alldata,{'experimental':true});
            var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
            saveAs(blob, 'labelset.csv');
        });
        
        
        
        //Setup Upload of Label
        var fileUploadLabel = document.getElementById("fileUploadLabel");

        fileUploadLabel.addEventListener('change', function(e) {
            var file = fileUploadLabel.files[0];
            var reader = new FileReader();


            reader.onload = function(e) {
                //DO NOT NEED:
                //fileUploadLabel.disabled = true;
                $('#fileUploadLabel').val('');//reset so they can use the same file again and it will load again
                
                var headerrow = [];
                dataSet = $.csv.toArrays(e.target.result);
                if(dataSet.length == 0 || dataSet[0].length == 0)
                {
                    //No data, so return
                    return;
                }
                var headerdata = dataSet[0];            

                
                dataSet.shift();//remove header row

                dataSet.forEach(function(l){
                    var maxlabel = window.labelData.length;
                    var qq = l[0];
                    l.shift();//remove first element
                    for(var kk=0; kk<l.length; kk++)
                    {
                        //Do not allow blanks
                        if(l[kk] == "")
                        {
                            l.splice(kk);//remove all remaining blanks
                            break;
                        }
                    }
                    window.labelData[maxlabel] = {"question": qq, "answerset":l};
                });
                updateLabelList();
            };

            //console.log("Starting upload");
            reader.readAsText(file);
        });

        
        //FILE LOAD BUTTON
        $('.beauty-table-to-load-csv').on('click', function(e){
            var fileUpload = document.getElementById("fileUpload");
            fileUpload.click();
        });
        
        
        //LABEL LOAD BUTTON
        $('.label-to-load-csv').on('click', function(e){
            var fileUploadLabel = document.getElementById("fileUploadLabel");
            fileUploadLabel.click();
        });
        
        //Initially hide the options
        $('.raw-table-to-csv-all').hide();


        //Setup Upload
        var fileUpload = document.getElementById("fileUpload");
        var dataSet = [];
        var currentfilename = "";
        var modifiedDataSet = [];

        fileUpload.addEventListener('change', function(e) {
            var file = fileUpload.files[0];
            var reader = new FileReader();
            fileUpload.disabled = true;

            //While loading
            reader.onload = function(e) {
                $('#fileUpload').val('');//reset so they can use the same file again and it will load again
                $('#demo').hide();
                //console.log("Finished upload");
                dataSet = $.csv.toArrays(e.target.result);
                firstupdatelist = true;//Loaded new data
                updateDataList(dataSet[0],window.labelData);

                $('.raw-table-to-csv-all').show();

                $('.colorder').show();
                currentfilename = file.name;//save filename
            };
          

            //console.log("Starting upload");
            reader.readAsText(file);
        });

        //Copy data table structure
        var initalltables = $('#split').html();

        $('.colorder').hide();
        $('#show-table').on('click',function() {
            //Only destroy on second display
            //Destroy current table first
            $('#split').html('');
            setTimeout(function() {
                //Wait for DOM to update
                notLoadedDataTable = false;
                window.displayAllColumns = true;
                $('#split').html(initalltables);
                setupDataTable(getDataSet(),currentfilename,[],null,null);
            },50);
        });


        $('#applylabels').on('click',function() {
            //Recreate list
            updateDataList(dataSet[0],window.labelData);
        });
    
        function getDataSet()
        {
            //Create the dataset base one user selection
                //CROP if over 500 entires
                //Reorder them
                //Add labels
                //Add column types
            return getOrderHeader(dataSet.slice(0,500));
        }

        $('.raw-table-to-csv-all').on('click',function() {
            //Download the merged CSV file
            var alldata = getOrderHeader(dataSet);
            var csv = new csvWriter();
            var csvdata = csv.arrayToCSV(alldata);
            startDownload(csvdata,currentfilename);

        });

    });//end READY FUNCTION
    var notLoadedDataTable = true;

    var curlabelset = [];
    var curheaderrow = [];
    var origheader = [];

    var firstupdatelist = true;
    function updateDataList(headerrow,labeldata)
    {
        //Get previous display order - is already displayed for this data
        // if(!firstupdatelist)
        // {
        //     var newidorder = $('#datalist').sortable("toArray");
        //     var displayorder = [];
        //     for(var jj=0; jj<newidorder.length; jj++)
        //     {
        //         var ii = parseInt($('#'+newidorder[jj]).attr('data-item'));
        //         displayorder.push(ii);
        //     }
        // }
        // firstupdatelist = false;


        //Displays the header row and label data
        origheader = headerrow.slice();//make copy
        var newheader = headerrow.slice();//make copy
        curheaderrow = newheader;//save the changes for later
        curlabelset = labeldata.slice();
        var groupdata = [];
        var typedata = [];

        for(var ii=0; ii<newheader.length; ii++)
        {
            var grpIdx = newheader[ii].indexOf(groupbyStr);
            var isgroup = false;
            var handledIdx = false;
            var columntype = 'default';
            if(grpIdx >= 0)
            {
                isgroup = true;
                var startIdx = grpIdx + groupbyStr.length;
                newheader[ii] = newheader[ii].substring(startIdx,newheader[ii].length);
            }

            var labelIdx = newheader[ii].indexOf(labelStr);
            if(labelIdx >= 0)
            {
                columntype = 'code';
                handledIdx = true;
                var startIdx = labelIdx + labelStr.length;
                var labeltemp = newheader[ii].substring(startIdx,newheader[ii].length);

                var splitLabel = labeltemp.split(/[\(,\)]/);
                mylabelData = {}; 
                mylabelData["question"] = splitLabel[0].trim();
                // splitLabel.shift();
                // var newLabel = [];
                // //Filter out extra spaces and blank entries
                // for(var idx = 0; idx<splitLabel.length; idx++)
                // {
                //     var newStr = splitLabel[idx].trim();
                //     if(newStr.length > 0)
                //     {
                //         newLabel.push(newStr);
                //     }
                // }
                // mylabelData["answerset"] = newLabel;
                newheader[ii] = mylabelData["question"] + "?";
            }
            var imgIdx = newheader[ii].indexOf(imgStr);
            if(!handledIdx && imgIdx >=0)
            {
                columntype = 'image';
                handledIdx = true;
                var startIdx = imgIdx + imgStr.length;
                newheader[ii] = newheader[ii].substring(startIdx,newheader[ii].length);
            }
            var imgvidIdx = newheader[ii].indexOf(imgvidStr);
            if(!handledIdx && imgvidIdx >=0)
            {
                //Remove Data field
                columntype = 'instagram';
                handledIdx = true;
                var startIdx = imgvidIdx + imgvidStr.length;
                newheader[ii] = newheader[ii].substring(startIdx,newheader[ii].length);
            }
            var youtubeIdx = newheader[ii].indexOf(youtubeStr);
            if(!handledIdx && youtubeIdx >=0)
            {
                //Remove Data field
                columntype = 'youtube';
                handledIdx = true;
                var startIdx = youtubeIdx + youtubeStr.length;
                newheader[ii] = newheader[ii].substring(startIdx,newheader[ii].length);
            }
            var videoIdx = newheader[ii].indexOf(videoStr);
            if(!handledIdx && videoIdx >=0)
            {
                //Remove Data field
                columntype = 'video';
                handledIdx = true;
                var startIdx = videoIdx + videoStr.length;
                newheader[ii] = newheader[ii].substring(startIdx,newheader[ii].length);
            }
            groupdata.push(isgroup);
            typedata.push(columntype);
        }

        //Add labeldata to header
        for(var ii=0;ii<curlabelset.length; ii++)
        {
            //{ "question" : question,"answerset" : textoptions}
            var curq = curlabelset[ii].question+"?";
            newheader.push(curq)
            groupdata.push(false);
            typedata.push('code');
        }

        var selectoptions = "";
        selectoptions += "<option value='default'>Default</option>";
        selectoptions += "<option value='image'>Image</option>";
        selectoptions += "<option value='instagram'>Instagram</option>";
        selectoptions += "<option value='youtube'>Youtube</option>";
        selectoptions += "<option value='video'>Video</option>";
        var outhead = "";
        var listart = 'class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>';
        for(var jj=0;jj<newheader.length;jj++)
        {
            var ii = jj;
            //If is in the order length then do it by order
            // if(jj < displayorder.length)
            // {
            //     ii = displayorder[jj];
            // }
            var checkbox = '<div class="checkbox-inline"><label><input id="columngroup'+ii+'" type="checkbox">Group<i class="fa fa-square-o"></i></label></div>';
            var removecheckbox = '<div class="checkbox-inline"><label><input id="columnremove'+ii+'" type="checkbox">Remove<i class="fa fa-square-o"></i></label></div>';
            var tempoption = selectoptions;
            //For label give only one option
            if(typedata[ii] == 'code')
            {
                tempoption = "<option value='code'>Code</option>";
            }
            outhead += "<li data-item='"+ii+"' id='newheader"+ii+"' "+listart+newheader[ii]+"&nbsp;&nbsp;&nbsp;"+checkbox+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Type:<select id='columntype"+ii+"'>"+tempoption+"</select>&nbsp;&nbsp;&nbsp;"+removecheckbox+"</li>";

        }
        $('#datalist').html(outhead);

        for(var ii=0;ii<newheader.length;ii++)
        {
            $('#columntype'+ii).val(typedata[ii]);
            $('#columngroup'+ii).attr('checked',groupdata[ii]);
        }
        $('#datalist').sortable({cursor:'move'});
        $("#datalist").disableSelection();
    }


    function getOrderHeader(initData)
    {
        //Returns the modified data and new header
        var newidorder = $('#datalist').sortable("toArray");
        var finalheader = [];
        var neworder = [];
        for(var jj=0; jj<newidorder.length; jj++)
        {
            var ii = parseInt($('#'+newidorder[jj]).attr('data-item'));
            var typedata = $('#columntype'+ii).val() ;
            var groupdata = $('#columngroup'+ii).is(":checked") ;
            var removedata = $('#columnremove'+ii).is(":checked") ;
            //Skip column if removed
            if(!removedata)
            {
                neworder.push(ii);
                var temphead = curheaderrow[ii];
                if(ii >= origheader.length)
                {
                    //is a new label
                    var labelii = ii - origheader.length;
                    temphead = labelStr+curlabelset[labelii].question;
                    temphead+="("+curlabelset[labelii].answerset.join(",")+")";
                }
                else if(typedata == 'code')
                {
                    //get orig head
                    temphead = origheader[ii];
                }
                //For everything else special add initial piece
                else if(typedata == 'image')
                {
                    temphead = imgStr+temphead;
                }
                else if(typedata == 'instagram')
                {
                    temphead = imgvidStr+temphead;
                }
                else if(typedata == 'youtube')
                {
                    temphead = youtubeStr+temphead;
                }
                else if(typedata == 'video')
                {
                    temphead = videoStr+temphead;
                }

                //If added group then append it
                if(groupdata)
                {
                    temphead = groupbyStr + temphead;
                }

                finalheader.push(temphead);
            }
        }

        var finaldata = [];
        finaldata.push(finalheader);
        //Skip the first row because it is the header
        for(var yy=1; yy<initData.length; yy++)
        {
            var temprow = [];
            for(var jj=0; jj<neworder.length; jj++)
            {
                var xx = neworder[jj];
                //If a new column then add blank
                if(xx >= initData[yy].length)
                {
                    temprow.push("");
                } else {
                    temprow.push(initData[yy][xx]);
                }
            }
            finaldata.push(temprow);
        }
        return finaldata;
    }

}