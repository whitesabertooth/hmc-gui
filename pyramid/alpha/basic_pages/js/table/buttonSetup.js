function setupButtonDownloads(oTable,oTableGroup,saveheaderdata,hasGroupBy,filename,header_inputs)
{
    //------------------------------    
    //DOWNLOAD FILTER BUTTON
    //------------------------------    
//     $('.beauty-table-to-csv').show();
    $('.beauty-table-to-csv').on('click.DT', function(e){
        e.preventDefault();

        //Look at filter data
        var data = oTable.rows( { filter: 'applied' } ).data().toArray();

        if(data.length == 0) {
            //No filtered data, so get entire current data
            data = oTable.data().toArray();
        }

        //Add Header row - handle resort columns
        var newheaderdata = [];
        var order = oTable.colReorder.order();
        for(var i=0; i<saveheaderdata.length; i++)
        {
            var newidx = order.indexOf(i);
            newheaderdata[newidx] = saveheaderdata[i];
        }
         
        var alldata = [newheaderdata].concat(data);

        var csv = new csvWriter();
        var csvdata = csv.arrayToCSV(alldata);
        var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
        saveAs(blob, filename);
    });
    
    //------------------------------    
    //DOWNLOAD ALL BUTTON
    //------------------------------    
    $('.beauty-table-to-csv-all').show();

    // Flash plug-in required, but still does not work on Safari
    // $('.beauty-table-to-csv-all').downloadify({
    //     filename: function(){
    //         return filename;
    //     },
    //     data: function(){ 
    //         //Get all data
    //         var data = oTable.data().toArray();

    //         //Add Header row - handle resort columns
    //         var newheaderdata = [];
    //         var order = oTable.colReorder.order();
    //         for(var i=0; i<saveheaderdata.length; i++)
    //         {
    //             var newidx = order.indexOf(i);
    //             newheaderdata[newidx] = saveheaderdata[i];
    //         }
             
    //         var alldata = [newheaderdata].concat(data);
    //         var csv = new csvWriter();
    //         var csvdata = csv.arrayToCSV(alldata);
    //         return csvdata;
    //     },
    //     onComplete: function(){ alert('Your File Has Been Saved!'); },
    //     onCancel: function(){ alert('You have cancelled the saving of this file.'); },
    //     onError: function(){ alert('You must put something in the File Contents or there will be nothing to save!'); },
    //     swf: 'plugins/Downloadify/media/downloadify.swf',
    //     downloadImage: 'plugins/Downloadify/images/download.png',
    //     width: 100,
    //     height: 30,
    //     transparent: true,
    //     append: false
    // });
    $('.beauty-table-to-csv-all').on('click.DT', function(e){
        e.preventDefault();

        //Get all data
        var data = oTable.data().toArray();

        //Add Header row - handle resort columns
        var newheaderdata = [];
        var order = oTable.colReorder.order();
        for(var i=0; i<saveheaderdata.length; i++)
        {
            var newidx = order.indexOf(i);
            newheaderdata[newidx] = saveheaderdata[i];
        }
         
        var alldata = [newheaderdata].concat(data);
        var csv = new csvWriter();
        var csvdata = csv.arrayToCSV(alldata);
        startDownload(csvdata,filename);
    });


    //------------------------------    
    //BROWSWER SAVE BUTTON
    //------------------------------    
    $('.beauty-table-to-save').show();
    $('.beauty-table-to-save').on('click.DT', function(e){
        e.preventDefault();
        e.stopPropagation();
        
        if(typeof window.saveDisplay == 'undefined')
        {
            window.saveDisplay = true;
        }

        //Always get entire current data
        var data = oTable.data().toArray();

        //Add Header row - handle resort columns
        var newheaderdata = [];
        var order = oTable.colReorder.order();
        if(hasGroupBy)
        {
            var ordergrp = oTableGroup.colReorder.order();
        } else {
            var ordergrp = [];
        }
        var orderdata = JSON.stringify(order);
        var ordergrpdata = JSON.stringify(ordergrp);
        for(var i=0; i<saveheaderdata.length; i++)
        {
            var newidx = order.indexOf(i);
            newheaderdata[newidx] = saveheaderdata[i];
        }
         
        var alldata = [newheaderdata].concat(data);
        var jsondata = JSON.stringify(alldata);
        var viscol = [];
        
        //Cycle through all columns and add what is visible
        for(var ii = 0; ii<oTable.columns().length; ++ii)
        {
        //Add everything    if(oTable.column(ii).visible())
            {
                viscol.push(ii);
            }
        } 
        try{
            localStorage.setItem('dataTable',encodeURIComponent(jsondata));
            localStorage.setItem('filename',filename);
            localStorage.setItem('order',orderdata);
            localStorage.setItem('ordergrp',ordergrpdata);
            localStorage.setItem('viscol',JSON.stringify(viscol));
            console.log('Successfully saved dataTable');
        
            if(window.saveDisplay)
            {
                OpenModalBox('Save Results', 'Save Successful');
            }
            
            window.saveDisplay = true;
            window.saveSuccess = true;
        } catch (e) {
            if(window.saveDisplay)
            {
                OpenModalBox('Save Results', "Failed to save: Exceeded Storage Quota!");
            }
            window.saveDisplay = true;
            window.saveSuccess = false;
        }
        
        //Give positive Feed back to user
    });
    
    //------------------------------    
    //CROP BY FILTER DATA - FUTURE CAPABILITY
    //------------------------------    
    //$('.beauty-table-to-crop').show();
    $('.beauty-table-to-crop').on('click.DT', function(e){
        e.preventDefault();

        //Look at filter data
        var filteredData = oTable.rows( { filter: 'applied' } );
        var filteredDataRemove = oTable.rows( { filter: 'removed' } );
        filteredDataRemove.remove();
        
        //Only crop if there is some filter
        if(filteredData.length > 0)
        {
            //clear all filters and set them back to default:
            header_inputs.val('').blur();
            //clear main filter text and set back to default
            $('#datatable-3_filter :input[type=text]').val('').blur();
            
            //Clear all internal table filters
            for(var iCol = 0; iCol < oTable.columns().length; iCol++) {
                oTable.column( iCol ).search = '';
            }
            
            //Redraw
            oTable.draw();
        }
        
    });

}