function setupCoding()
{
  var demoMode = false;
  
  function toggleSave(){
      var specialBox = document.getElementById('overlaytext');
      if(specialBox.style.display == "block"){
          //Slowly make it fade
          $('#overlaytext').fadeOut(10000);//will make hidden after opacity gone
      } else {
          specialBox.style.display = "block";
          specialBox.style.opacity = 1.0;
      }
  }
  var autoSaveTime = 300000;//wait 5 minutes

  function autoSave(){
      //Check if labels have changed
      console.log("Checking autosave...");
      if(window.labelChanged)
      {
          $('#overlaytext').html('Auto Saving...');
          console.log("Auto saving...");
          toggleSave();
          window.saveDisplay = false;
          $('#tablesave').click();
          setTimeout(function() {
            window.labelChanged = false;
            if(window.saveSuccess)
            {
                $('#overlaytext').html('Saved.');
                console.log("Saved.");
            } else {
                $('#overlaytext').html('Failed.');
                console.log("Failed.");
                clearInterval(window.autoSaveInterval);
            }
            toggleSave();
            //clearInterval(window.autoSaveInterval);//Temporarily stop intervals
          },500);
      }
  };
  window.labelChanged = false;
  
  //Global window.labelData
  window.labelData = [];

  $(document).ready(function() {
      // Load Datatables and run plugin on tables 
      TestTable3(true);//true means Label
      
      // Add Drag-n-Drop feature
      //WinMove();
                    
      
      

      //BROWSWER LOAD BUTTON
      $('.beauty-table-to-load').on('click', function(e){
          if(window.notLoadedDataTable)
          {
              
              if(!localStorage.getItem('dataTable'))
              {
                  console.log('Tried to load, but no data');
              } else {
                  var jsondata = decodeURIComponent(localStorage.getItem('dataTable'));
                  var filename = localStorage.getItem('filename');
                  var viscol = JSON.parse(localStorage.getItem('viscol'));
                  var ordergrp = JSON.parse(localStorage.getItem('ordergrp'));
                  var order = JSON.parse(localStorage.getItem('order'));
                  setupDataTable(JSON.parse(jsondata),filename,viscol,order,ordergrp);
                  console.log('Tried to load, with data');
                  
                  //Disable fileUpload to not load anything
                  var fileUpload = document.getElementById("fileUpload");
                  fileUpload.disabled = true;
                  $('.beauty-table-to-load').hide();
                  $('.beauty-table-to-load-csv').hide();
                  $('#demo').hide();
              }
          }
      });

      //DEMO START BUTTON
      $('#demo').on('click',function() {
          if(sideBarVisible())
          {
              $('.show-sidebar').click();//Shrink left side
          }
          $('.beauty-table-to-load').show();//Only for the demo

          //Start the tour now that the data is loaded
          var intro = introJs();
            intro.setOptions({
              steps: [
                { 
                  intro: "This is a tutorial for the Coder using this coding webpage."
                },
                {
                  element: ".show-sidebar",
                  intro: "Click this to show/hide the sidebar."
                },
                {
                  element: ".beauty-table-to-load",
                  intro: "This button loads from the browser cache (a temporary save on this computer's browser only).  This button will only show up if something is already cached on your browser."
                },
                {
                  element: '#mainload',
                  intro: "Load a CSV file sent by the Master Coder.",
                  position: "left"
                }
              ]
            });
            //intro.setOption('showButtons', false);
            //intro.setOption('showBullets', false);
            intro.setOption('doneLabel', 'Load Dummy Data').start().oncomplete(seconddemo);
            console.log("started demo");
            $('#demo').hide();
      });
      function seconddemo() {
          //Start the tour now that the data is loaded
          $('.beauty-table-to-load').hide();
          loadDemo();
          clearInterval(window.autoSaveInterval);//Stop autosaving
          demoMode = true;
          setTimeout(function() {
              var intro = introJs();
                intro.setOptions({
                  steps: [
                    { 
                      element: '.dataTables_scroll',
                      intro: "The data is loaded into a table.",
                      position: 'top'
                    },
                    { 
                      element: '#datatable-3_filter',
                      intro: "You can search all the columns for a string."
                    },
                    { 
                      element: '#datatable-3_info',
                      intro: "This describes how many rows are in the table.  If filtered by a search it will say how many filtered."
                    },
                    { 
                      element: '.editable',
                      intro: "Columns that have \"?\" are codes that need to be entered."
                    },
                    { 
                      element: '.imagevideocolumn',
                      intro: "Columns with a <i class=\"fa fa-instagram\"></i> are Instagram Image or Video columns.  They will show an image or a still video image.  If you click the image it will open another page of that actual image/video on Instagram. In line-edit mode you can view the full sized image or play the video.  If the image does not exist then a bad image will be displayed."
                    },
                    { 
                      element: '.youtubecolumn',
                      intro: "Columns with a <i class=\"fa fa-youtube\"></i> are Youtube Video columns.  They will show a still video image.  If you click the image it will open another page of that actual video on Youtube.  In line-edit mode you can play the video.  If the image does not exist then a bad image will be displayed."
                    },
                    {
                      element: "#alllabel",
                      intro: "This show the overall completion status of coding entry for all columns and rows."
                    },
                    {
                      element: '.dataTables_scrollFoot tfoot .editable',
                      intro: "For each columns you can enter a search filter for this column.  Note: You can use a \"~\" followed by a regular expression to do complicated searches."
                    },
                    {
                      element: '.dataTables_scrollFoot tfoot .tblcount',
                      intro: "For each code entry column this shows the percent complete for how much coding has been down."
                    },
                    { 
                      element: $('#datatable-3 tbody tr').eq(1).find('td').eq(3)[0],
                      intro: "All web addresses in the table will automatically become links that will open a new page with that link."
                    },
                    {
                      element: ".beauty-table-to-csv-all",
                      intro: "Click this button to save the current table to a CSV file (to be sent to the Master Coder or to save progress)."
                    },
                    {
                      element: ".beauty-table-to-save",
                      intro: "This button saves to the browser cache (a temporary save feature that only saves it to the browser on the computer you are using).  Also every 5 minutes your current work will be temporarily saved."
                    },
                    { 
                      element: '.odd .editable',
                      intro: "Click the coding column cell box and see that an input box will be displayed to allow typing of a coding option.  Also a pop-up of all the options that can be selected will be displayed.",
                      position: "right"
                    },
                    { 
                      element: '.odd .editable',
                      intro: "Keyboard Entry: You can use the keyboard and type part or all of a option (note: after entering data, if still multiple options, the top option will be selected).",
                      position: "right"
                    },
                    { 
                      element: '.odd .editable',
                      intro: "Keyboard Entry (cont.): After typing an option, the pop-up options will be filtered.  If nothing is entered all options will be shown.",
                      position: "right"
                    },
                    { 
                      element: '.odd .editable',
                      intro: "Keyboard Entry (cont.): To <b>accept</b> press:<ul><li><kbd>Enter</kbd> to move to the next row</li><li><kbd>Shift</kbd>+<kbd>Enter</kbd> to move to the previous row</li><li><kbd>Tab</kbd> to move to the next code entry column (will wrap)</li><li><kbd>Shift</kbd>+<kbd>Tab</kbd> to move to the previous code entry column (will wrap)</li></ul>",
                      position: "bottom"
                    },
                    { 
                      element: '.odd .editable',
                      intro: "Keyboard Entry (cont.): To <b>cancel</b> the change:<ul><li>Press <kbd>Escape</kbd></li><li>Or click somewhere else on the page</li></ul>",
                      position: "right"
                    },
                    { 
                      element: ".dataTables_scrollHead",
                      intro: "Columns can be resized by grabbing the side of the column header, and reordered by drag-n-dropping the column header."
                    },
                    { 
                      element: $('#datatable-3 tbody tr').eq(3)[0],
                      intro: "To open the line-edit view for a row, double-click in any cell (other than on a link or coding entry column)."
                    }
                  ]
                });
                //intro.setOption('showButtons', false);
                //intro.setOption('showBullets', false);
                intro.setOption('doneLabel', 'Demo Line-Edit').start().oncomplete(demoEntry);
                console.log("started demo 2");
            },200);
      }

      function demoEntry()
      {
          //Do a label entry
          $('#datatable-3 tbody tr').eq(3).find('td').eq(3).dblclick();
          addStyle(".introjs-helperLayer {background-color: transparent !important;} .introjs-overlay {z-index: auto;}");
          setTimeout(function() {
              var intro = introJs();
                intro.setOptions({
                  steps: [
                    { 
                      element: '.devoops-modal',
                      intro: "Shows only one row of the table.",
                      position: "left"
                    },
                    { 
                      element: '#editelement',
                      intro: "Shows the current row being edited."
                    },
                    { 
                      element: ".devoops-modal .devoops-modal-header .box-icons .close-link",
                      intro: "To close the line-edit window, click here."
                    },
                    { 
                      element: "#linetotallabel",
                      intro: "Current percent complete of all the coding columns."
                    },
                    { 
                      element: "#linebuttons",
                      intro: "Click Previous to display the previous row, and Next to display the next row."
                    },
                    { 
                      element: "#minitable",
                      intro: "Current selected row displayed, with the column header on the left column, and the column data on the right column.  If a column is a Youtube or Instagram column, then full image or video can be seen or played.",
                      position: "left"
                    },
                    { 
                      element: ".editdata",
                      intro: "Just like in the full table view, you can click the Code entry column cells to edit their data via keyboard entry or selecting with mouse."
                    },
                    { 
                      element: ".editdata",
                      intro: "Just like in the full table view, you can click the Code entry column cells to edit their data via keyboard entry or selecting with mouse.  If accepting the change of the last code column using the keyboard, then the line-edit will immediately proceed to the next row."
                    },
                    { 
                      element: "#kbline",
                      intro: "Using the keyboard you can shift to the next or previous row."
                    }
                  ]
                });
                //intro.setOption('showButtons', false);
                //intro.setOption('showBullets', false);
                //intro.start();
                intro.setOption('doneLabel', 'End of Tour').start().oncomplete(demoEnd);
                console.log("started demo 3");
            },600);
      }

      function demoEnd()
      {
          $(".devoops-modal .devoops-modal-header .box-icons .close-link").click();
          $('.show-sidebar').click();//Show left side
      }



      //BROWSER DEMO LOAD BUTTON
      function loadDemo() {
          if(window.notLoadedDataTable)
          {
              var jsondata = [
                  ['LABEL_Relevance(1. Yes,2. No)','LABEL_Location(1. US,2. Non-US,3. EU)','Idpost','bodypost','actorname','actorlinkshref','INSTAGRAM_Instagram Post','LABEL_Category:: Part 1(1. Performs,2. Skips,3. Adds)','YOUTUBE_video_link'],
                  ['','','tw12345678','I smoke a lot when I sleep.','supersmoker','http://www.healtheducation.org','','',''],
                  ['','','tw12345679','E-cigs are da\' bomb @ www.healtheducation.org','ecigbody','http://www.healtheducation.org','','',''],
                  ['','','O_qe9L0gd44','enjoy my vid (video has been removed)','weirdsmoker2','','','','O_qe9L0gd44'],
                  ['','','kmK2rdGMo3I','hookah bar','weirdsmoker','','','','kmK2rdGMo3I'],
                  ['','','ig374893330','Cool pic (with different resolution images)','igmonster2','','https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/s150x150/e15/11334685_1442417569395586_1303567342_n.jpg,https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/e15/11334685_1442417569395586_1303567342_n.jpg','',''],
                  ['','','ig374893331','Cool vid','igmonster2','','https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/s150x150/e15/11247745_1092461160767932_747207549_n.jpg,https://scontent.cdninstagram.com/hphotos-xaf1/t50.2886-16/11389164_1606670312951702_162098708_n.mp4','',''],
                  ['','','ig3741113331','Cool pic (that has been removed)','igmonster3','','https://scontent.cdninstagram.com/hphotos-xfa1/t51.2885-15/e15/11330522_641846484916250_208402853_n.jpg','','']
              ];
              var filename = "demo.csv";
              setupDataTable(jsondata,filename,[],null,null);

              //Disable fileUpload to not load anything
              var fileUpload = document.getElementById("fileUpload");
              fileUpload.disabled = true;
              $('.beauty-table-to-load').hide();
              $('.beauty-table-to-load-csv').hide();
          }
      }
      
      
      //BROWSWER LOAD BUTTON
      $('.beauty-table-to-load-csv').on('click', function(e){
          if(window.notLoadedDataTable)
          {
              var fileUpload = document.getElementById("fileUpload");
              fileUpload.click();
          }
      });
      
      
      //Start autoSave - defined in another file
      window.autoSaveInterval = setInterval(autoSave,autoSaveTime);//start autoSave at 5 minute intervals
      
      //Initially hide the options
      $('.beauty-table-to-save').hide();
      $('.beauty-table-to-csv').hide();
      $('.beauty-table-to-csv-all').hide();
      $('.beauty-table-to-crop').hide();
      $('.labeltitle').hide();

      //Hide temp load if nothing to load
      if(!localStorage.getItem('dataTable'))
      {
         $('.beauty-table-to-load').hide();
      }

  });//end READY FUNCTION
  window.notLoadedDataTable = true;
  window.displayAllColumns = true;


  //
  // Function for table, located in element with id = datatable-3
  //
  function TestTable3(isLabel){
      //Default isLabel to false
      if(typeof isLabel == 'undefined')
      {
          var isLabel = false;
      }
      

      //Setup Upload
      var fileUpload = document.getElementById("fileUpload");

      fileUpload.addEventListener('change', function(e) {
          var file = fileUpload.files[0];
          var reader = new FileReader();
          fileUpload.disabled = true;

          //While loading
          //Create progress bar id=thebar,textbar
          var newprogress = '<div class="progress progress-striped active"><div class="progress-bar progress-bar-danger" id="thebar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span id="textbar">0% Complete</span></div></div>';
          var tempprogress = '<button type="button" id="endquery" class="btn btn-danger btn-app-sm"><i class="fa fa-spinner fa-spin"></i></button>';
          $('#loadprogress').html(tempprogress);
          reader.onload = function(e) {
              $('.beauty-table-to-load-csv').hide();
              $('.beauty-table-to-load').hide();
              $('#demo').hide();
              //console.log("Finished upload");
              var dataSet = $.csv.toArrays(e.target.result);
              $('#loadprogress').html("");//delete progress bar
              
              setupDataTable(dataSet,file.name,[],null,null);
          };
          
          reader.onprogress = function(e) {
  //             if(e.lengthComputable)
  //             {
  //                 var currentpercent = Math.round((e.loaded / e.total) * 100);
  //                 $('#thebar').css('width', currentpercent+'%').attr('aria-valuenow', currentpercent);
  //                 $('#textbar').html(currentpercent + "% Complete");
  //             }
              console.log("progress load called")
          };

          //console.log("Starting upload");
          reader.readAsText(file);
      });

  }

}