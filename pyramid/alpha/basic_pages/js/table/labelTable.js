var handleLabelCounts = function()
{
	var oTable;
	var oTableGroup;
	var tblcount;
	var tblcountGroup;
	var hasGroupBy = false;
	var labelColumns;
	var groupbyColumns;
	var skiplabelColumns;

	this.setupTable = function(inTable,inTableGroup)
	{
		oTable = inTable;
		oTableGroup = inTableGroup;
	};

	this.getData = function(hasgroup,labelC,labelGroup,skiplabelC)
	{
	    //Setup label count data
	    var tablecountData = '0% Complete';
	    $('.labelCount').html(tablecountData);
	    tblcount = $('#datatable-3 > tfoot .tblcount');
	    tblcountGroup = $('#grouptable > tfoot .tblcount');

	    window.labelChanged = false;
	    $('.labeltitle').show();
	    hasGroupBy = hasgroup;
	    labelColumns = labelC;
	    groupbyColumns = labelGroup;
	    skiplabelColumns = skiplabelC;
	};

    this.updateLabelCount = function()
    {
        //Count each label column if it is not blank
        var filledCell = 0;
        var runningFill = 0;
        var runningTotal = 0;
        var order = oTable.colReorder.order();//Handle for reorder
        if(hasGroupBy)
        {
            var orderGroup = oTableGroup.colReorder.order();//Handle for reorder
        }
        for(var i=0; i<labelColumns.length; i++)
        {
            if($.inArray(labelColumns[i],skiplabelColumns) >= 0)
            {
                //Do not count
            } else {
                filledCell = 0;
                var newidx = order.indexOf(labelColumns[i]);//Handle for reorder
                filledCell = oTable.column(newidx).data().reduce( function(a,b) {
                    if(b !== "")
                    {
                        return a+1;
                    } else {
                        return a;
                    }
                }, 0);
                var curtotal = oTable.column(newidx).data().length;
                if(hasGroupBy)
                {
                    var itogroup = groupbyColumns.indexOf(labelColumns[i]);
                    if(itogroup > -1)
                    {
                        //var newidxgroup = orderGroup.indexOf(itogroup);//Handle for reorder
                        curtotal = oTableGroup.column(itogroup).data().length;
                    }
                }
                var computedPercent = Math.round(100*10*filledCell/curtotal)/10;
                runningFill += filledCell;
                runningTotal += curtotal;
                if(hasGroupBy && itogroup > -1)
                {
                    tblcountGroup.eq(itogroup).html(computedPercent.toString() + '% Complete');
                } else {
                    tblcount.eq(i).html(computedPercent.toString() + '% Complete');
                }
            }
        }
        //Compute overall
        var computedPercent = Math.round(100*10*runningFill/runningTotal)/10;
        $('.labelCount').html(computedPercent.toString() + '% Complete');
        window.labelChanged = true;
    };

};