var tableConstruct = function() 
{
    var isLabel = true;//default to true right now
    this.setupHeader = function(tmpheaderdata,iorder,tblid,addtoggle,labelColumns,headerdata)
    {
        var colheader = "";
        var colfooter = "";
        var toggleHeader = "";
        var isLabelFont = "";
        var isLabelClass = "";
        var tablecountHeader = "";
        var tablecountData = "";
        
        //----------------------------
        //Setup Labels and Header row
        //----------------------------
        var togglegroups = {};
        if(tmpheaderdata.length > 0) {
            for(var i = 0; i<tmpheaderdata.length; i++) {
                isLabelFont = "";
                isLabelClass = " nonlabelcolumn";
                if($.inArray(iorder[i],labelColumns) >= 0 && isLabel) {
                    //Label header
                    isLabelFont = "style='font-weight:bold'";
                    isLabelClass = " labelcolumn";
                    tablecountData += '<td class="tblcount" align="center">0% Complete</th>';
                } else {
                    tablecountData += "<td>&nbsp;</td>";
                }

                var val = "search_" + tmpheaderdata[i];
                var name = "Search " + tmpheaderdata[i];
                colheader += '<th><label><input type="text" idx=' + i + ' name="' + val + '" value="' + name + '" class="search_init' + isLabelClass + '" /></label></th>';
                colfooter += '<th>' + headerdata[iorder[i]] + "</th>\n";

                if(i == 0) { toggleHeader += '&nbsp;';}
                //Save off columns that have digits at end and put as group
                if(tmpheaderdata[i].match(/\d\d$/))
                {
                    var mainhead = tmpheaderdata[i].substring(0,tmpheaderdata[i].length-2);
                    var digithead = parseInt(tmpheaderdata[i].substring(tmpheaderdata[i].length-2,tmpheaderdata[i].length));
                    //ends in 2 digits, so remember and add later
                    if(!(mainhead in togglegroups))
                    {
                        togglegroups[mainhead] = {};    
                    }
                    togglegroups[mainhead][digithead] = i;
                } else {
                    toggleHeader += '<li><a class="toggle-vis" ' + isLabelFont + ' data-column="' + i + '">&nbsp;' + tmpheaderdata[i] + '&nbsp;</a></li>';
                }
                
            }
        }
      
        for(var mainelm in togglegroups)
        {
            toggleHeader += '&nbsp;<li>'+mainelm+'[';
            for(var digitelm in togglegroups[mainelm])
            {
                var i = togglegroups[mainelm][digitelm];
                var isLabelFont = "";
                //Check if label
                if($.inArray(iorder[i],labelColumns) >= 0 && isLabel) {
                    isLabelFont = "style='font-weight:bold'";
                }
                toggleHeader += '<a class="toggle-vis" ' + isLabelFont + ' data-column="' + i + '">&nbsp;' + digitelm + '&nbsp;</a>';
            }
            toggleHeader += ']</li>&nbsp;';
        }
      
        //DO NOT NEED - Takes too long to process
        //toggleHeader += '<li><a class="toggle-allvis">SHOW-ALL</a></li>';
        toggleHeader += '<li><a class="toggle-allhide">HIDE-ALL</a></li>';

        if(addtoggle)
        {
            var toggleStart = '<form><input id="search-text" placeholder="Filter columns..."></form><ul class="list-inline" id="list">';
            var toggleEnd = '</ul>';

            $('#toggleMe').html("");
            $('#toggleMe').append('Toggle column by clicking: ' + toggleStart + toggleHeader + toggleEnd);
        }
        $('#'+tblid+' > thead').html("");
        $('#'+tblid+' > tbody').html("");
        $('#'+tblid+' > tfoot').html("");
        $('#'+tblid+' > tfoot').append('<tr>' + colheader + '</tr>');//Needs to be footer because the ColResize messes up if it is the header
        $('#'+tblid+' > tfoot').append('<tr>' + tablecountData + '</tr>');
        $('#'+tblid+' > thead').append('<tr>' + colfooter + '</tr>');
    }
}