//Written by DWS
//------------------------------
// Create the Datatable    
//------------------------------   
var initialsetupDone = false; 
//Things that have to happen only one time
function initialSetup()
{
    if(!initialsetupDone)
    {
        initialsetupDone = true;
        
        
        Array.prototype.move = function(from, to) {
            this.splice(to, 0, this.splice(from, 1)[0]);
        };
        
        //-----------------------------
        //Search method for table entry
        //-----------------------------
        $.expr[':'].textEquals = function (a, i, m) {
            return $(a).text().match("^" + m[3] + "$");
        };
        
        
    }

    //Cause a pop-up to occur if navigating to another page
    $(window).on("beforeunload.table", function (e) {
        if(window.labelChanged)
        {
            var confirmationMessage = 'Navigating to another page will loose all unsaved data!';
            
            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        }
    });
    $(document).on("ajaxPageChange.tableload", function (e) {
        //TODO: Get this to work
        // if(window.labelChanged)
        // {
        //     var confirmationMessage = 'Navigating to another page will loose all unsaved data!';
            
        //     (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        //     return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        // }
        $(document).off("ajaxPageChange.tableload");
        $(window).off("beforeunload.table");
        resetTable();
    });
}

function resetTable() {
    $( window ).off('keydown.GROUP');
    $( window ).off('resize.DT');
    $('.beauty-table-to-csv').off('click.DT');
    $('.beauty-table-to-csv-all').off('click.DT');
    $('.beauty-table-to-save').off('click.DT');
    $('.beauty-table-to-crop').off('click.DT');
}
function setupDataTable(dataSet,filename,viscol,orderIn,orderGrpIn) {

    initialSetup();
    resetTable();
    var display = new tableDisplay();
    var tabelConst = new tableConstruct();
    var labelTable = new handleLabelCounts();
    
    //------------------------------    
    //Add table keys
    //------------------------------    
    $('.beauty-table-mod').each(function(){
        // Run keyboard navigation in table
        $(this).beautyTablesModified();
    });

    window.notLoadedDataTable = false;//not allow to load again
    var asInitVals = [];
    if(typeof window.displayAllColumns == 'undefined')
    {
        window.displayAllColumns = false;
    }
    
    if(dataSet.length == 0)
    {
        //No data, so return
        return;
    }

    //----------------------------
    //Add labels to header data
    //----------------------------
    var labelColumn_to_LabelData = {};
    var labelColumns = [];
    var imageColumns = [];
    var imagevideoColumns = [];
    var youtubeColumns = [];
    var videoColumns = [];
    var groupbyColumns = [];
    var groupbyColumnsOrder = [];
    var labelStr = 'LABEL_';
    var imgvidStr = 'INSTAGRAM_';
    var imgStr = 'IMAGE_';
    var youtubeStr = 'YOUTUBE_';
    var videoStr = 'VIDEO_';
    var groupbyStr = "GROUPBY_";
    var imgvidIcon = '<i class="fa fa-instagram"></i>&nbsp;';
    var youtubeIcon = '<i class="fa fa-youtube"></i>&nbsp;';
    
    var headerdata = dataSet[0];  //reference to dataSet[0]
    var saveheaderdata = dataSet[0].slice();   //Makes identical copy 
    
    //Always add labels in data
    var lblidx = 0;
    var labelData = [];
    var tmpheaderdata = [];
    var hasGroupBy = false;
    var grouplabelColumns = [];
    var nonlabelgroup = [];
    var grouplabelColumns = [];
    var skiplabelColumns = [];
    var groupimageColumns = [];
    var groupimagevideoColumns = [];
    var groupyoutubeColumns = [];
    var groupvideoColumns = [];
    var groupbySpecialColumns = [];
    var groupbyNormalColumns = [];
    var allgroup = [];

    //----------------------------
    //Identify the labels and other unique fields based on header
    //----------------------------
    for(var jj=0; jj<dataSet[0].length; jj++)
    {
        var handledIdx = false;
        var addedGroupby = false;
        var addedGroupbyLabel = false;
        var grpIdx = dataSet[0][jj].indexOf(groupbyStr);
        var addedLabel = false;
        if(grpIdx >=0)
        {
            //A groupby label
            var startIdx = grpIdx + groupbyStr.length;
            //Remove the groupby label
            dataSet[0][jj] = dataSet[0][jj].substring(startIdx,dataSet[0][jj].length);
            groupbyColumns.push(jj);
            allgroup.push(groupbyColumns.length-1);
            groupbyColumnsOrder.push(jj);
            hasGroupBy = true;
            addedGroupby = true;
        }
        var labelIdx = dataSet[0][jj].indexOf(labelStr);
        if(labelIdx >=0)
        {
            //Label column
            //pull out answers and question
            var startIdx = labelIdx + labelStr.length;
            var labeltemp = dataSet[0][jj].substring(startIdx,dataSet[0][jj].length);
            var splitLabel = labeltemp.split(/[\(,\)]/);
            labelData[lblidx] = {}; 
            labelData[lblidx]["question"] = splitLabel[0].trim();
            splitLabel.shift();
            var newLabel = [];
            //Filter out extra spaces and blank entries
            for(var idx = 0; idx<splitLabel.length; idx++)
            {
                var newStr = splitLabel[idx].trim();
                if(newStr.length > 0)
                {
                    newLabel.push(newStr);
                }
            }
            //If no labels, then do not count in label count
            if(newLabel.length === 0)
            {
                skiplabelColumns.push(jj);
            }
            labelData[lblidx]["answerset"] = newLabel;
            dataSet[0][jj] = labelData[lblidx]["question"] + "?";
            tmpheaderdata.push(dataSet[0][jj]);
            labelColumns.push(jj);
            if(addedGroupby)
            {
                grouplabelColumns.push(groupbyColumns.length-1);
                addedGroupbyLabel = true;
            }
            labelColumn_to_LabelData[jj] = labelData[lblidx]["answerset"];
            lblidx++;
            handledIdx = true;
            addedLabel = true;
        }
        var imgIdx = dataSet[0][jj].indexOf(imgStr);
        if(!handledIdx && imgIdx >=0)
        {
            //Remove Data field
            var startIdx = imgIdx + imgStr.length;
            dataSet[0][jj] = dataSet[0][jj].substring(startIdx,dataSet[0][jj].length);
            tmpheaderdata.push(dataSet[0][jj]);

            //Image column (used for instagram)
            imageColumns.push(jj);
            handledIdx = true;
            if(addedGroupby)
            {
                groupimageColumns.push(groupbyColumns.length-1);
            }
        }
        var imgvidIdx = dataSet[0][jj].indexOf(imgvidStr);
        if(!handledIdx && imgvidIdx >=0)
        {
            //Remove Data field
            var startIdx = imgvidIdx + imgvidStr.length;
            dataSet[0][jj] = dataSet[0][jj].substring(startIdx,dataSet[0][jj].length);
            tmpheaderdata.push(dataSet[0][jj]);
            dataSet[0][jj] = imgvidIcon + dataSet[0][jj];

            //Image or Video column (used for instagram)
            imagevideoColumns.push(jj);
            handledIdx = true;
            if(addedGroupby)
            {
                groupimagevideoColumns.push(groupbyColumns.length-1);
            }
        }
        var youtubeIdx = dataSet[0][jj].indexOf(youtubeStr);
        if(!handledIdx && youtubeIdx >=0)
        {
            //Remove Data field
            var startIdx = youtubeIdx + youtubeStr.length;
            dataSet[0][jj] = dataSet[0][jj].substring(startIdx,dataSet[0][jj].length);
            tmpheaderdata.push(dataSet[0][jj]);
            dataSet[0][jj] = youtubeIcon + dataSet[0][jj];

            //Youtube view in page
            youtubeColumns.push(jj);
            handledIdx = true;
            if(addedGroupby)
            {
                groupyoutubeColumns.push(groupbyColumns.length-1);
            }
        }
        var videoIdx = dataSet[0][jj].indexOf(videoStr);
        if(!handledIdx && videoIdx >=0)
        {
            //Remove Data field
            var startIdx = videoIdx + videoStr.length;
            dataSet[0][jj] = dataSet[0][jj].substring(startIdx,dataSet[0][jj].length);
            tmpheaderdata.push(dataSet[0][jj]);

            //Video column (used for instagram)
            videoColumns.push(jj);
            handledIdx = true;
            if(addedGroupby)
            {
                groupvideoColumns.push(groupbyColumns.length-1);
            }
        }
        if(addedLabel && addedGroupby)
        {
            //Added this label to groupby and it is special
            //So remove from groupbyColumnsOrder
            groupbyColumnsOrder.pop();//should be end since just added
        }
        //Mark not group
        if(addedGroupby && !addedGroupbyLabel)
        {
            nonlabelgroup.push(groupbyColumns.length-1);
        }
        //Mark special for  group
        if(addedGroupby && handledIdx)
        {
            groupbySpecialColumns.push(groupbyColumns.length-1);
        }
        //Mark not special (plus labels) for group
        if(addedGroupby && (!handledIdx || addedGroupbyLabel) )
        {
            groupbyNormalColumns.push(groupbyColumns.length-1);
        }
        //Add if not added above
        if(!handledIdx)
        {
            tmpheaderdata.push(dataSet[0][jj]);
        }
    }
    
    

    //----------------------------------------------
    //Create iorder with every index in tmpheaderdata
    var iorder = [];
    for(var idx=0;idx<tmpheaderdata.length;idx++)
    {
        iorder.push(idx);
    }
    tabelConst.setupHeader(tmpheaderdata,iorder,"datatable-3",true,labelColumns,headerdata);
    dataSet.shift();//remove header row - that row is not needed now
    //----------------------------------------------

    /**********************************************
    **  Determine Group By information
    **********************************************/
    var groupbyrows = [];
    var groupbyNewOrder = [];
    var groupbyrowsOrigRow = [];//matches groupbyrows mapping to oTable row
    if(hasGroupBy)
    {
        //Computer one time the non order groupby columns
        var groupbyColumnsNonOrder = [];
        for(var idx=0;idx<groupbyColumns.length;idx++)
        {
            var col = groupbyColumns[idx];
            if(groupbyColumnsOrder.indexOf(col) == -1)
            {
                //Not in order
                groupbyColumnsNonOrder.push(col);
            }
        }

        //find unique groups
        var rowunique = {};
        for(var row=0;row<dataSet.length;row++)
        {
            var stru = "";
            var datau = [];

            for(var idx=0;idx<groupbyColumns.length;idx++)
            {
                var col = groupbyColumns[idx];
                var data = dataSet[row][col];
                datau.push(data);
                //if an order column
                if(groupbyColumnsOrder.indexOf(col) > -1)
                {
                    stru += "##[GROUP_BY]##"+data;
                }
            }

            if(!(stru in rowunique))
            {
                //first time seeing unique row
                rowunique[stru] = datau;
                groupbyrowsOrigRow.push(row);
            }
        }

        //Get all the values
        groupbyrows = Object.keys(rowunique).map(function(key){
            return rowunique[key];
        });

        //Add header
        var uheader = [];
        for(var idx=0;idx<groupbyColumns.length;idx++)
        {
            var col = groupbyColumns[idx];
            uheader.push(tmpheaderdata[col]);
        }
        tabelConst.setupHeader(uheader,groupbyColumns,"grouptable",false,labelColumns,headerdata);
        $('#grouptable').show();
    }

    labelTable.getData(hasGroupBy,labelColumns,groupbyColumns,skiplabelColumns);//Must be performed before Table creation, but after table setup


    
    var header_inputs = $("#grouptable tfoot input,#datatable-3 tfoot input");//must be before dataTable created to get all inputs
    var header_inputsSingle = $("#datatable-3 tfoot input");//must be before dataTable created to get all inputs
    var header_inputsGroup = $("#grouptable tfoot input");//must be before dataTable created to get all inputs

    var totalColumns = headerdata.length;
    var allNonViselem = [];
    var allelem = [];
    var allVisElem = [];
    //Add first two elements as a default to display
    if(headerdata.length > 0)
    {
        allVisElem.push(0);
    }
    if(headerdata.length > 1)
    {
        allVisElem.push(1);
    }
    
    for(var i=0; i<totalColumns;i++) {
        allelem.push(i);//Make everything
    }
    //--------------------
    //Allow custom visible
    //--------------------
    if(viscol.length > 0)
    {
        allVisElem = viscol;
    }

    var allColumnsExceptLabels = [];
    var allColumnsExceptImgVid = [];
    var nongroupbyColumns = [];
    allVisElem = allVisElem.concat(labelColumns);//Add label columns too
    if(window.displayAllColumns != undefined && window.displayAllColumns)
    {
        allVisElem = allelem;
    }
    //If doing a group, remove the groupby
    if(hasGroupBy)
    {
        for(var grp=0;grp<groupbyColumns.length;grp++)
        {
            var elm = allVisElem.indexOf(groupbyColumns[grp]);
            if(elm > -1)
            {
                allVisElem.splice(elm,1);
            }
        }
    }
    for(var i=0; i<totalColumns;i++) {
        //remove all visible columns
        if($.inArray(i,allVisElem) == -1) {
            allNonViselem.push(i);
        }
        
        //Remove all label columns
        if($.inArray(i,labelColumns) == -1) {
            allColumnsExceptLabels.push(i);
        }
        //Remove all img vid columns
        if($.inArray(i,imageColumns) == -1
           && $.inArray(i,videoColumns) == -1
           && $.inArray(i,youtubeColumns) == -1
           && $.inArray(i,imagevideoColumns) == -1) {
            allColumnsExceptImgVid.push(i);
        }
        if(hasGroupBy)
        {
            if($.inArray(i,groupbyColumns) == -1)
            {
                nongroupbyColumns.push(i);
            }
        }
    }

    var sortorder = groupbyColumnsOrder.map(function(group,i) {
        return [ group, 'asc' ];
    });

    var groupsortorder = [];
    for(var ii = 0; ii < groupbyColumns.length; ii++)
    {
        if (groupbyColumnsOrder.indexOf(groupbyColumns[ii]) > -1)
        {
            groupsortorder.push( [ ii, 'asc' ]);
        }
    }


    //Set initial AutoFill
    var autoFillConfig = {};
    var currentCell = null;
    InitAutoFillConfig(allColumnsExceptLabels,labelColumns);
    var reordered = false;
    var reorderedGroup = false;

    //Must be after the allelem defined
    var orderSingleLastFirst = allelem.slice();//copy
    var orderGroupLastFirst = allgroup.slice();//copy
    if(orderGrpIn)
    {
        orderGroupListFirst = orderGrpIn;
    }
    //Move last to first - used for adding a new column button
    //TODO:orderSingleLastFirst.unshift(orderSingleLastFirst.pop());
    //TODO:orderGroupLastFirst.unshift(orderGroupLastFirst.pop());

    //Used to create Small images and videos
    function renderCell(rownum,idx,data)
    {
        if (data != null && data.substring && data.length > 0)
        {
            if($.inArray(idx,youtubeColumns) > -1) 
            {
                data = display.createYouTubeLinkSmall(data,display.checkImageError(idx,rownum));
            }
            else if($.inArray(idx,imagevideoColumns) > -1) 
            {
                data = display.createImgVidLinkSmall(data,display.checkImageError(idx,rownum));
            }
            else if($.inArray(idx,imageColumns) > -1) 
            {
                data = display.createImgVidLinkSmall(data,display.checkImageError(idx,rownum));
            }
            else if($.inArray(idx,videoColumns) > -1) 
            {
                data = display.createImgVidLinkSmall(data,display.checkImageError(idx,rownum));
            }
            else if (data != null && data.substring)//make sure string
            {
                data = linkifyStr(data);
            }
        }
        //Only add for non label columns
        // if(labelColumns.indexOf(idx) == -1 && (data == null || data.length == 0))
        // {
        //     data = "&nbsp;";//Don't like it empty because the row gets shrunk
        // }
        return data;
    }

    //Used to create Large images and videos
    function drawcellCallback(rownum,idx,data)
    {
        if (data != null && data.substring && data.length > 0)
        {
            if($.inArray(idx,youtubeColumns) > -1) 
            {
                data = display.createYouTubeLink(data,display.checkImageError(idx,rownum));
            }
            else if($.inArray(idx,imagevideoColumns) > -1) 
            {
                data = display.createImgVidLink(data,display.checkImageError(idx,rownum));
            }
            else if($.inArray(idx,imageColumns) > -1) 
            {
                data = display.createImgLink(data,display.checkImageError(idx,rownum));
            }
            else if($.inArray(idx,videoColumns) > -1) 
            {
                data = display.createVidLink(data,display.checkImageError(idx,rownum));
            }
            else if (data != null && data.substring)//make sure string
            {
                data = linkifyStr(data);
            }
        }

        return data;
    }


    //------------------
    //Group Setup Table
    //------------------    
    if(hasGroupBy)
    {
        var oTableGroup = $('#grouptable').DataTable( {
            "dom": "ZR<'box-content small-content'<'col-sm-6'f><'col-sm-6 text-right'i><'clearfix'>>rt",//resiZe,Reorder,Scroller
            "columnDefs": [
                 {"className" : "editable default", "targets" : grouplabelColumns},
                 {"orderable" : false, "targets" : groupbySpecialColumns},
                 {"width" : "100px", "targets" : groupbyNormalColumns},
                 {"width" : display.smallSizeWidth, "targets" : groupimageColumns},
                 {"width" : display.smallSizeWidth, "targets" : groupimagevideoColumns},
                 {"width" : display.smallSizeWidth, "targets" : groupvideoColumns},
                 {"width" : display.smallSizeWidth, "targets" : groupyoutubeColumns},
                 {"className" : "imagecolumn", "targets" : groupimageColumns},
                 {"className" : "imagevideocolumn", "targets" : groupimagevideoColumns},
                 {"className" : "videocolumn", "targets" : groupvideoColumns},
                 {"className" : "youtubecolumn", "targets" : groupyoutubeColumns},
                 {
                    "render":function (data,type,row,meta){
                        //Linkify all data
                        //if (data != null && data.substring && data.length > 0)
                        {
                            var idx = meta.col;
                            var rownum = meta.row;
                            //Handle reorder
                            if(reorderedGroup)
                            {
                                var order = oTableGroup.colReorder.order();
                                idx = order[idx];//just look up original column
                            } else {
                                //DO NOT NEED TO HANDLE INITIAL: Order is stored in dataSet
//                               idx = orderGroupLastFirst[idx];
                            }
                            //Map idx back to original column
                            idx = groupbyColumns[idx];

                            data = renderCell(rownum,idx,data);
                            //data = drawcellCallback(rownum,idx,data);//Currently doesn't play fullscreen :(
                        }
                        return data;
                    },
                    "targets" : allgroup
                 }
             ],
            "order": groupsortorder,
            "ordering":true,
            "data": groupbyrows,
            "autoWidth":false,
            "paging" : false,//Needs to be true for Scroller
            "language": {
                "search": "Group By Search&nbsp;&nbsp;",
                "lengthMenu": '_MENU_'
            },
            "scrollY" : "500px", 
            "scrollX" : "100%", 
            "scrollCollapse": false,//TEST-Need this set false for scrollbar resize issue to work
            "processing": true,
            "drawCallback": function ( settings ) {
                //Setup for split
                setTimeout(function() {
                    display.updateSplit('grouptable');
                },10);
            },
            "deferRender": false,//NEEDS to be true for Scroller
            "oScroller": {
              "displayBuffer": 100
            },
            "colResize":{
                "tableWidthFixed": false,
                "resizeCallback": function(column) {
                                //Scrollbar gets chopped off, so need to redraw it.
                                //NOT NEEDED NOW: display.updateScrollBar('grouptable');
                            }
            },
            "colReorder": {
                "reorderCallback": function () {
                    //Update AutoFill
                    var order = this.fnOrder();
                    var newlabelColumns = [];
                    var allelemTemp = allgroup.slice();//make clone
                    for(var i=0; i<grouplabelColumns.length; i++) {
                        var newidx = order.indexOf(grouplabelColumns[i]);
                        newlabelColumns.push(newidx);
                        var removeidx = allelemTemp.indexOf(newidx);
                        allelemTemp.splice(removeidx,1);//remove index
                    }
                    //Destroy AutoFill:
                    $('#grouptable').trigger('destroy.dt.DTAF');//Destroy current set
                    
                    //Re-init
                    updateAutoFillConfig(allelemTemp,newlabelColumns,true);
                    autoFillGroup = new $.fn.dataTable.AutoFill(this.s.dt, autoFillConfig[true]);     
                    //Setup for split
                    display.updateSplit('grouptable');
                    //Scrollbar gets chopped off, so need to redraw it.
                    //NOT NEEDED NOW: display.updateScrollBar('grouptable');
                },
               // "order": orderGroupLastFirst //note: ORDER IS SAVED IN THE dataSet, this is for moving the button to the first column
            },
            "destroy":true,
            select: {
                style: 'single',
                className: 'primary' //this is used in the newer Datatable versions, but not the current one using
                    //defaults to "selected" class
            }
        });
    }

    //------------------
    //Setup Table
    //------------------
    var oTable = $('#datatable-3').DataTable( {
        "dom": "ZR<'box-content small-content'<'col-sm-6'f><'col-sm-6 text-right'i><'clearfix'>>rt",//resiZe,Reorder,Scroller
        "columnDefs": [
             {"visible":true, "targets":allVisElem},
             {"visible":false, "targets":allNonViselem},//only show first 2 columns
             {"className" : "editable default", "targets" : labelColumns},
             {"orderable" : false, "targets" : labelColumns},//Doesn't work good, so Temp remove
             {"width" : "100px", "targets" : allColumnsExceptImgVid},
             {"width" : display.smallSizeWidth, "targets" : imageColumns},
             {"width" : display.smallSizeWidth, "targets" : imagevideoColumns},
             {"width" : display.smallSizeWidth, "targets" : videoColumns},
             {"width" : display.smallSizeWidth, "targets" : youtubeColumns},
             {"className" : "imagecolumn", "targets" : imageColumns},
             {"className" : "imagevideocolumn", "targets" : imagevideoColumns},
             {"className" : "videocolumn", "targets" : videoColumns},
             {"className" : "youtubecolumn", "targets" : youtubeColumns},
             {
                "render":function (data,type,row,meta){
                    //Linkify all data
                    if (data != null && data.substring && data.length > 0)
                    {
                        var idx = meta.col;
                        var rownum = meta.row;
                        var order = orderIn;
                        //Handle reorder
                        if(reordered)
                        {
                            order = oTable.colReorder.order();
                            idx = order[idx];//just look up original column
                        } else {
                            //DO NOT NEED TO HANDLE INITIAL: Order is stored in dataSet
                           //idx = orderSingleLastFirst[idx];
                        }
                        data = renderCell(rownum,idx,data);
                    }
                    return data;
                },
                "targets" : allelem
             }
         ],
        //"order": sortorder,
        "ordering":false,//NO MORE ORDERING/SORTING
        "data": dataSet,
        "autoWidth":false,
        "paging" : false,//Needs to be true for Scroller
        "language": {
            "search": "Global Search&nbsp;&nbsp;",
            "lengthMenu": '_MENU_'
        },
        "scrollY" : "500px", 
        "scrollX" : "100%", 
        "scrollCollapse": false,//TEST-Need this set false for scrollbar resize issue to work
        "processing": true,
        "drawCallback": function ( settings ) {
            //Setup for split
            setTimeout(function() {
                display.updateSplit('datatable-3');
            },10);
        },
        "deferRender": false,//NEEDS to be true for Scroller
        "oScroller": {
          "displayBuffer": 100
        },
        "colResize":{
            "tableWidthFixed": false,
            "resizeCallback": function(column) {
                            //Scrollbar gets chopped off, so need to redraw it.
                            //NOT NEEDED NOW: display.updateScrollBar('datatable-3');
                        }
        },
        "colReorder": {
            "reorderCallback": function () {
                //Update AutoFill
                var order = this.fnOrder();
                var newlabelColumns = [];
                var allelemTemp = allelem.slice();//make clone
                for(var i=0; i<labelColumns.length; i++) {
                    var newidx = order.indexOf(labelColumns[i]);
                    newlabelColumns.push(newidx);
                    var removeidx = allelemTemp.indexOf(newidx);
                    allelemTemp.splice(removeidx,1);//remove index
                }
                //Destroy AutoFill:
                $('#datatable-3').trigger('destroy.dt.DTAF');//Destroy current set
                
                //Re-init
                updateAutoFillConfig(allelemTemp,newlabelColumns,false);
                autoFill = new $.fn.dataTable.AutoFill(this.s.dt, autoFillConfig[false]);     
                //Setup for split
                display.updateSplit('datatable-3');

                //Scrollbar gets chopped off, so need to redraw it.
                //NOT NEEDED NOW: display.updateScrollBar('datatable-3');
            },
            //"order": orderSingleLastFirst //ORDER IS SAVED IN THE dataSet
        },
        "destroy":true
    });

    //Set this after initial creation of tables
    reordered = true;//the colReorder object has been created
    reorderedGroup = true;//the colReorder object has been created    

    //Update counts based on loaded data
    labelTable.setupTable(oTable,oTableGroup);
    labelTable.updateLabelCount();
    
    //------------------
    //Used for resize update of table - to delay it if in the middle of editing
    //------------------
    var editableOnPrev = false;
    var editableOn = function(val) {
        if(val != editableOnPrev && editableOnPrev && redrawDelay) {
            resizeTable();
            redrawDelay = false;
        }
        editableOnPrev = val;
    };
    var redrawDelay = false;
    
    //--------------------------
    //Used for editable in table
    //--------------------------
    function submitAutoComplete()
    {
        if ($('.typeahead input').size() > 0 && $(".ui-autocomplete li:textEquals('" + $('.typeahead input').val() + "')").size() == 0 && $('.typeahead input').val().length > 0) {
            if($(".ui-autocomplete li:visible:first").text() !== "")
            {
                $('.typeahead input').val($(".ui-autocomplete li:visible:first").text());
            } else {
                //Only blank out if there are answers to choose from - therewise allow freeform
                if($.inArray($('.typeahead input').val(),tempInputData) < 0) {
                    $('.typeahead input').val('');
                }
            }
        }
    }
    var tempInputData = [];
    //MIGHT HAVE ISSUE HERE WITH RE-CALLING TABLE CREATION
    $.editable.addInputType('autocomplete', {
        element : $.editable.types.text.element,
        plugin : function(settings, original) {
            //Don't show the autocomplete if no options
            if(tempInputData.length > 0)
            {
                var $inputs = $('input', this).autocomplete({
                    minLength:0,
                    source: tempInputData,
                    change: function (event, ui) {
                        //if the value of the textbox does not match a suggestion, clear its value
                        if ($(".ui-autocomplete li:textEquals('" + $(this).val() + "')").size() === 0) {
                            $(this).val('');
                        }
                    },
                    delay: 0,
                    appendTo: $('#selectionoption'),
                    position: {of: $(this)}
                }).on('keydown', function (e) {
                    var keyCode = e.keyCode || e.which;
                    //if TAB or RETURN is pressed and the text in the textbox does not match a suggestion, set the value of the textbox to the text of the first suggestion
                    if(keyCode == 9 || keyCode == 13) {
                        submitAutoComplete();
                    }
                }).focus(function(){
                    $(this).autocomplete("search","");//show everything on focus
                }).on('autocompleteselect', function(event,ui){
                        //If someone selects an option, then accept it/trigger submit
                        event.stopPropagation();//Only this function handles this event (some other event was causing it to stay focused)
                        $('.typeahead input').val(ui.item.value);
                        $('.typeahead').trigger('submit');
                        setTimeout(function() {
                            //Move right+wrap to the next element
                            var target = currentCell;
                            var tr = $(target).closest("tr");
                            var col = $(target).closest("td");
                            var findstr = "td.editable";
                            do {
                              tr = tr.next();
                              var nextCell = tr.children(findstr).first();
                              if(col.nextAll(findstr).size() > 0)
                              {
                                  nextCell = col.nextAll(findstr).first();
                              }
                              nextCell.click().focus();
                            } while(nextCell.size() == 0 && tr.size() > 0);
                        },0);
                });
            }
        }
    });


    //-------------------------------
    //Setup resize of window to table
    //-------------------------------   
    var resizeTable = function() {
        //toggle visibility of first column back and forth to cause auto width
        var bVis = oTable.column( 0).visible();
        oTable.column( 0).visible(bVis ? false : true );
        oTable.column( 0).visible( bVis ? true : false ,true);
        oTable.columns.adjust().draw();
        if(hasGroupBy)
        {
            var bVis = oTableGroup.column( 0).visible();
            oTableGroup.column( 0).visible(bVis ? false : true );
            oTableGroup.column( 0).visible( bVis ? true : false ,true);
            oTableGroup.columns.adjust().draw();
        }
    };
    //This listener is removed in the init function on page change
    $( window ).on('resize.DT',function() {
        if(editableOnPrev) {
            redrawDelay = true;
        } else {
            //Redraw now
            resizeTable();
        }
        //Also change the table height
        display.updateMaxBox();
    });



    $('#datatable-3 tbody td.editable').editable( function(value,settings) {
        return(value);
    },
    {
        "placeholder" : "",
        "callback": function( sValue, y ) {
            /* Update the table from the new data set */
            oTable.cell(this).data(sValue);//FOR INPUT (don't let redraw)
            
            editableOn(false);
            labelTable.updateLabelCount();
        },
        "height": "14px",
        "tooltip" : "Click to edit...",
        "onedit" : function() {
            editableOn(true);
            var idxdata = oTable.cell(this).index().column;
            var order = oTable.colReorder.order();
            var newidx = order[idxdata];
            tempInputData = labelColumn_to_LabelData[newidx];
            currentCell = this;
        },
        "onerror":function () {
            editableOn(false);
        },
        "onreset" : function () {
            editableOn(false);
        },
        "type" : "autocomplete",
        "cssclass":"typeahead"
    } );

    $('#grouptable tbody td.editable').editable( function(value,settings) {
        return(value);
    },
    {
        "placeholder" : "",
        "callback": function( sValue, y ) {
            /* Update the table from the new data set */
            oTableGroup.cell(this).data(sValue);//FOR INPUT (don't let redraw)
            var row = oTableGroup.cell(this).index().row;
            var idx = oTableGroup.cell(this).index().column;
            //Map column/row back to original column/row
            var orderGroup = oTableGroup.colReorder.order();
            var order = oTable.colReorder.order();
            //Map idx back to original column
            var newidx = orderGroup[idx];
            newidx = groupbyColumns[newidx];
            newidx = order.indexOf(newidx);
            row = groupbyrowsOrigRow[row];
            
            //Save data on original table so it gets saved
            oTable.cell(row,newidx).data(sValue);
            oTableGroup.cell(this).data(sValue);

            editableOn(false);
            labelTable.updateLabelCount();
        },
        "height": "14px",
        "tooltip" : "Click to edit...",
        "onedit" : function() {
            editableOn(true);
            var idxdata = oTableGroup.cell(this).index().column;
            var order = oTableGroup.colReorder.order();
            var newidx = order[idxdata];

            //Map idx back to original column
            newidx = groupbyColumns[newidx];

            tempInputData = labelColumn_to_LabelData[newidx];
            currentCell = this;
        },
        "onerror":function () {
            editableOn(false);
        },
        "onreset" : function () {
            editableOn(false);
        },
        "type" : "autocomplete",
        "cssclass":"typeahead"
    } );


    //****************************
    //Setup AutoFill configuration
    //****************************
    function updateAutoFillConfig(allelemTemp,newlabelColumns,isgroup)
    {
            
        autoFillConfig[isgroup]["columnDefs"] =[
                {"enable":false, "targets":allelemTemp},//disable all columns execpt label columns
                {"increment":false, "targets":newlabelColumns}//no not allow auto-increment
            ];
    }
    
    function InitAutoFillConfig(allelemTemp,newlabelColumns)
    {
            
        var autoFillConfigBoth0 = {
            "columnDefs":[],
            "onselectstart": function(e) {
                submitAutoComplete();
                $('.typeahead').trigger('submit');
            },   
            "complete": function(e) {
                labelTable.updateLabelCount();
            }        
        };
        var autoFillConfigBoth1 = {
            "columnDefs":[],
            "onselectstart": function(e) {
                submitAutoComplete();
                $('.typeahead').trigger('submit');
            },   
            "complete": function(e) {
                labelTable.updateLabelCount();
            }        
        };
        autoFillConfig = {false:autoFillConfigBoth0,true:autoFillConfigBoth1};
        updateAutoFillConfig(nonlabelgroup,grouplabelColumns,true);
        updateAutoFillConfig(allelemTemp,newlabelColumns,false);
    }
    var autoFill;
    var autoFillGroup;
    if(hasGroupBy)
    {
        autoFillGroup = new $.fn.dataTable.AutoFill(oTableGroup, autoFillConfig[true]);
    }
    autoFill = new $.fn.dataTable.AutoFill(oTable, autoFillConfig[false]);


    console.log("Recreated datatable");

    if(hasGroupBy)
    {    
        var totalgrouprows = oTableGroup.rows().indexes().length;
    }

    //Used by Group shift and SingeEdit Shift
    var selectFirst = function()
    {
        var tr = $('#minitable > tbody tr:first');
        var colidx = 0;//always the first TD (sind the other column is TH) column (first column is 0)
        do {
          var nextCell = tr.find('td:eq('+colidx+')');
          nextCell.click().focus();// + findstr
          tr = tr.next();
        } while(!nextCell.hasClass("editable") && tr.size() > 0);
    }

    //------------------------------    
    // Setup Open Single Edit window
    //------------------------------  
    var currentEditRow = null;
    $('#datatable-3 > tbody').on( 'dblclick', 'tr', function () {
        //stop the editing of any field:
        submitAutoComplete();
        $('.typeahead').trigger('submit');
        //Remove monitor of keys when Modal Window closes
        $('body').on('click.EDIT', 'a.close-link', function(e){
            $(window).off('keydown.EDIT');
            $('body').off('click.EDIT');
            currentEditRow = null;
        });
        //Add navigation with shift+-> and shift+<-
        $(window).on('keydown.EDIT', function(event) {//
            if (event.shiftKey === true){
                switch(event.keyCode) {
                    case 37: // left arrow  
                        if(prevClick())
                        {
                            //Only select if changed
                            selectFirst();
                        }
                        break;
                    case 39: // right arrow
                        if(nextClick())
                        {
                            //Only select if changed
                            selectFirst();
                        }
                        break;
                }
            }
        });


        //found current row
        currentEditRow = oTable.row(this);
        var rowdata = currentEditRow.data();
        var totaltablerows = oTable.rows().data().length;
        var visible_row_total = oTable.rows({search:'applied'}).indexes().length

        //Add Header row - handle resort columns
        var newheaderdata = [];
        var labelMappedColumnData = [];
        var order = oTable.colReorder.order();
        var isLabel = [];
        for(var i=0; i<headerdata.length; i++)
        {
            var newidx = order.indexOf(i);
            newheaderdata[newidx] = headerdata[i];
            labelMappedColumnData[newidx] = labelColumn_to_LabelData[i];
            if($.inArray(i,labelColumns) > -1) {
                isLabel[newidx] = ' editable default';
            } else {
                isLabel[newidx] = '';
            }
        }


        //rowdata and newheaderdata match the order of the columns
        //Create a simple table of two columns::header & data

        //Display element number of number  All Labels % Complete
        var headstuff = "<span><h4 style='display: inline;' id='editelement'></h4><br><span id='linetotallabel'><h5 style='display: inline;'>All Labels&nbsp;</h5><h5 style='display:inline;' class='labelCount'></h5></span></span><br>";
        var tableall = "<table style='table-layout:fixed;' id='minitable' class='table table-striped table-bordered'>";// table-heading
        var tabledata = "";
        var maptdtorowdata
        for(var i=0; i<rowdata.length; i++)
        {
            //Only show visible columns
            var newi = order[i];
            if(allVisElem.indexOf(newi)>-1)
            {
                //Handle reorder
                var data = drawcellCallback(currentEditRow.index()[0],newi,rowdata[i]);
                var cc = "info";
                //for label columns use different class
                if($.inArray(newi,labelColumns) > -1) {
                    cc = "editable default";
                }
                tabledata += "<tr><th class='"+cc+"' width='35%'>" + newheaderdata[i] + "</th><td width='65%' class='editdata" + isLabel[i] + "' column='" + i + "'>" + data + "</td></tr>";
            }
        }

        var tablehead = "";
        //var tablehead = "<thead><th>Field Name</th><th>Field Value</th></thead>";
        var tablebody = "<tbody>" + tabledata + "</tbody>";

        tableall += tablehead;
        tableall += tablebody;
        tableall += "</table>";

        var buttondata = "<span id='linebuttons'><button id='editPrev' class='primary'>Previous</button>&nbsp;<button id='editNext' class='primary'>Next</button></span>&nbsp;&nbsp;<span id='kbline'>(Use <kbd>Shift</kbd>+<i class='fa fa-arrow-circle-right'></i> = Next; <kbd>Shift</kbd>+<i class='fa fa-arrow-circle-left'></i> = Previous)</span>";
        var bufferspace = "<br><br>";
        var groupname = "";
        if(hasGroupBy)
        {
            //Add selected group row information
            var currow = $(oTableGroup.row({selected:true}).node()).index();//in the order shown
            //See if something is selected
            if(currow > -1)
            {
                groupname = " in Group " + (currow+1) + " of " + totalgrouprows;
            }
        }
        OpenModalBox("Edit Row"+groupname,headstuff + buttondata + bufferspace + tableall);

        labelTable.updateLabelCount();//Will also update the Single Element display

        function updateElementNum() 
        {
            var visible_row_num = $(currentEditRow.node()).index()+1;
            //currentEditRow.index()+1
            //totaltablerows
            $('#editelement').html("Editing element " + (visible_row_num) + " of " + visible_row_total);
        }

        updateElementNum();
        display.addImageErrorCheckLarge();

        function changeRow(newrow)
        {
            //Make sure nothing is being edited
            submitAutoComplete();
            $('.typeahead').trigger('submit');

            currentEditRow = newrow;

            //Scroll to cntr position
            var trPos = $(currentEditRow.node()).position();
            var trCtr = ($(currentEditRow.node()).height()) / 2;
            var dataTblctr = ($('#datatable-3_wrapper .dataTables_scrollBody').height()) / 2;
            var curscroll = $('#datatable-3_wrapper .dataTables_scrollBody').scrollTop();
            var finalPos = trPos.top - dataTblctr + trCtr + curscroll;
            //console.log("Moving to: " + trPos.top + ", final = " + finalPos);
            $('#datatable-3_wrapper .dataTables_scrollBody').animate({scrollTop:finalPos},150);//Animate scrolling
            
            rowdata = currentEditRow.data();
            updateElementNum();
            
            $('#minitable tbody td.editdata').each( function (index)
            {
                var td = $(this).closest('td');
                column = parseInt(td.attr('column'));
                var order = oTable.colReorder.order();
                var idx = order[column];
                var data = drawcellCallback(currentEditRow.index()[0],idx,rowdata[column]);
                $(this).html(data);
            });

            display.addImageErrorCheckLarge();
        }
        function prevClick() {
            //Check for first option
            //This should handle filtering
            var prevNode = $(currentEditRow.node()).prev();
            if(prevNode.size() == 1)
            {
                changeRow(oTable.row(prevNode));
                return true;
            }
            return false;
        };
        
        function nextClick() {
            //Check for last option
            //This should handle filtering
            var nextNode = $(currentEditRow.node()).next();
            if(nextNode.size() == 1)
            {
                changeRow(oTable.row(nextNode));
                return true;
            }
            return false;
        };

        $('#editPrev').on('click', function() {prevClick();});
        $('#editNext').on('click', function() {nextClick();});
        
        var column = -1;
        $('#minitable').beautyTablesModified();
        $('#minitable tbody td.editable').editable( function(value,settings) {
            return(value);
        },
        {
            "placeholder" : "",
            "height": "20px",
            "tooltip" : "Click to edit...",
            "callback": function( sValue, y ) {
                /* Update the table from the new data set */
                var newdata = currentEditRow.data();
                newdata[column] = sValue;
                currentEditRow.data(newdata);

                labelTable.updateLabelCount();
            },
            "onedit" : function() {
                var td = $(this).closest('td');
                column = parseInt(td.attr('column'));
                tempInputData = labelMappedColumnData[column];
                currentCell = this;
            },
            "type" : "autocomplete",
            "cssclass":"typeahead"
        } );
    } );


    
    //------------------------------    
    //Add filter hook-up for updating table
    //------------------------------    
    header_inputsSingle.on('keyup', function(e) { header_keyup(e,oTable,this);});
    header_inputsGroup.on('keyup', function(e) { header_keyup(e,oTableGroup,this);});
    function header_keyup(e,table,that)
    {
        /* Filter on the column (the index) of this element */
        var order = table.colReorder.order();
        var col = order.indexOf(parseInt(that.getAttribute('idx')));
        if(that.value.length > 0 && that.value.charAt(0) == '~') {
            if(e.which == 13) {
                e.preventDefault();
                var inputstring = that.value.substring(1);
                console.log('Doing regex = ' + inputstring);
                var order = table.colReorder.order();
                table.columns(col).search( inputstring, true, false, false);
                table.draw();
            }
        } else {
            table.columns(col).search( that.value , false, true, true);
            table.draw();
            e.preventDefault();
        }
    }
    //Add CSS change on focus
    header_inputs.on('focus', function(){
        if ( this.className.indexOf("search_init") >= 0 ){
            $(this).removeClass("search_init");
            this.value = "";
        }
    })
    //Add CSS change on defocus
    .on('blur', function (i) {
        if ( this.value == "" ){
            $(this).addClass("search_init");
            this.value = asInitVals[header_inputs.index(this)];
        }
    });
    //Initialize values for de-focus
    header_inputs.each( function (i) {
        asInitVals[i] = this.value;
    });
    

    //update one time    
    display.updateMaxBox();
    display.addImageErrorCheckSmall();//Used for the initial table
    display.addImageErrorCheckLarge();//Used for the initial table

    //Perform other setup functions    
    setupOtherTable(oTable);
    setupButtonDownloads(oTable,oTableGroup,saveheaderdata,hasGroupBy,filename,header_inputs);
    if(hasGroupBy)
    {
        setupGroupTable(oTable,oTableGroup,groupbyColumnsOrder,submitAutoComplete,groupbyColumns,selectFirst);
    }
    return oTable;


    //FUTURE Capability
    // function addextraRows()
    // {
    //     //Add extra rows for groupby
    //     if(hasGroupBy)
    //     {
    //         var api = oTable;
    //         var rows = api.rows(  ).nodes();
    //         var last=null;
    //         var rowsize = api.row(0).data().length;
    //         var groupbyColumnsClassnames = groupbyColumns.map(function(elm,i) {
    //             var classnames = api.cell(0,elm).nodes()[0].className;
    //             return classnames;
    //         });
 
    //         var datagroup = api.columns(groupbyColumns).data();
    //         var grouprows = [];
    //         var grouphead = -1;
    //         for(var loopnum=0;loopnum<datagroup[0].length; loopnum++)
    //         {
    //             //Grab one row out of all the columns
    //             var group = [];
    //             for(var subloop=0; subloop < datagroup.length; subloop++)
    //             {
    //                 group.push(datagroup[subloop][loopnum]);
    //             }
    //             if ( is_same(last,group) ) {
    //                 //hide the rows are that the same group
    //                 // grouprows.push($(rows).eq(loopnum)[0]);
    //                 // $(rows).eq(loopnum).hide();
    //             } else {
    //                 // if(grouphead >= 0)
    //                 // {
    //                 //     api.row(grouphead).child(grouprows);
    //                 //     api.row(grouphead).child.hide();
    //                 // }
    //                 // grouphead = loopnum;

    //                 var trstr = '<tr class="groupby">';
    //                 var count = group.length-1;
    //                 var offsetcount = 0;
    //                 while(count > 0)
    //                 {
    //                     var data = renderCell(loopnum,groupbyColumns[offsetcount],group[offsetcount]);
    //                     trstr += '<td class="'+groupbyColumnsClassnames[offsetcount]+'">'+data+'</td>';
    //                     count--;
    //                     offsetcount++;
    //                 }
    //                 var data = renderCell(loopnum,groupbyColumns[offsetcount],group[offsetcount]);
    //                 trstr += '<td class="'+groupbyColumnsClassnames[offsetcount]+'" colspan="'+(rowsize-offsetcount)+'">'+data+'</td>';
    //                 $(rows).eq( loopnum ).before(
    //                     trstr
    //                 );
 
    //                 last = group;
    //             }
    //         }
    //         api.draw();
    //     }
    // }

    //addextraRows();-NOT WORKING GOOD RIGHT NOW

}

