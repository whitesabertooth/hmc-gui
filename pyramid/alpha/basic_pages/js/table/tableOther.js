function setupOtherTable(oTable)
{

	var totalColumns = oTable.columns().eq(0).length;
    //------------------------------    
    //Re-establish filters
    //------------------------------  
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Search');
    });


	
    //------------------------------    
    //we want this function to fire whenever the user types in the search-box
    //------------------------------    
    $("#search-text").keyup(function () {
      
        //first we create a variable for the value from the search-box
        var searchTerm = $("#search-text").val();
    
        //then a variable for the list-items (to keep things clean)
        var listItem = $('#list').children('li');
        
        //extends the default :contains functionality to be case insensitive
        //if you want case sensitive search, just remove this next chunk
        $.extend($.expr[':'], {
          'containsi': function(elem, i, match, array)
          {
            return (elem.textContent || elem.innerText || '').toLowerCase()
            .indexOf((match[3] || "").toLowerCase()) >= 0;
          }
        });//end of case insensitive chunk
    
    
        //this part is optional
        //here we are replacing the spaces with another :contains
        //what this does is to make the search less exact by searching all words and not full strings
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
        
        
        //here is the meat. We are searching the list based on the search terms
        $("#list li").not(":containsi('" + searchSplit + "')").each(function(e)   {
    
              //add a "hidden" class that will remove the item from the list
              $(this).addClass('hidden');
    
        });
        
        //this does the opposite -- brings items back into view
        $("#list li:containsi('" + searchSplit + "')").each(function(e) {
    
              //remove the hidden class (reintroduce the item to the list)
              $(this).removeClass('hidden');
    
        });
    });


    //------------------------------------------    
    //Setup button clicks for Column visability
    //------------------------------------------    
    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
        var idxdata = parseInt($(this).attr('data-column'));
        var order = oTable.colReorder.order();
        var newidx = order.indexOf(idxdata);
        // // Toggle the visibility
        var nothingvisible = false;
        if(oTable.columns(":visible").eq(0).length == 0)
        {
        	nothingvisible = true;
        }
        var bVis = oTable.column(newidx).visible();
        oTable.column(newidx).visible( bVis ? false : true ,true);

        //Only redraw columns if nothing is visible
        if(nothingvisible)
        {
        	oTable.columns.adjust().draw();
        }
        
    } );
    //Show All
    $('a.toggle-allvis').on('click',function(e) {
        //show everything
        e.preventDefault();

        var bVis;
        for(var ii = 0; ii<totalColumns; ++ii)
        {
            //Only make visible those that are not visible
            bVis = oTable.column(ii).visible();
            if(!bVis)
            {
                oTable.column(ii).visible(true);
            }
        }
        oTable.columns.adjust().draw();

    });
    //Hide all
    $('a.toggle-allhide').on('click',function(e) {
        //hide everything
        e.preventDefault();

        for(var ii = 0; ii<totalColumns; ++ii)
        {
            bVis = oTable.column(ii).visible();
            if(bVis)
            {
                oTable.column(ii).visible(false);
            }
        }
        oTable.columns.adjust().draw();

    });
    

}