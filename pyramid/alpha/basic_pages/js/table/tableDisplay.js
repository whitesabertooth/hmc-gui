var tableDisplay = function()
{
    var videoExtentions = ["mp4","ogg","mov","avi"]
    var instagramThumbAddress = "s150x150";
    var CONST_NO_IMAGE_SMALL = "img/noimg_small.png";
    var CONST_NO_IMAGE_LARGE = "img/noimg_large.png";
    var smallSizeWidth = "150px";
    this.smallSizeWidth = smallSizeWidth;
    var smallSizeHeight = "150px";
    this.smallSizeHeight = smallSizeHeight;
    var largeSizeWidth = "250px";
    var largeSizeHeight = "250px";
    var errorSmallImage = [];
    var errorLargeImage = [];
    var that = this;
    var oTable = null;
    var oTableGroup = null;

    function getTable()
    {
    	if(oTable == null)
    	{
    		oTable = $('#datatable-3').DataTable();
    	}
    	return oTable;
    }

    function getTableGroup()
    {
        if(oTableGroup == null)
        {
            oTableGroup = $('#grouptable').DataTable();
        }
        return oTableGroup;
    }
        //Left element is small - right element is large
    var splitElement = function(input)
    {
        var ss = input.split(/,/);
        //Handle if not two pieces of data, then make input both
        if(ss.length != 2)
        {
            ss = [input,input];
        }
        return ss;
    };

    this.instagramThumbnail = function(input)
    {
        //https://scontent.cdninstagram.com/hphotos-xfa1/t51.2885-15/s150x150/e15/11330522_651846484916250_208402853_n.jpg
        //The s150x150 makes it a thumbnail pic
        var section = input.split(/\//);
        section.splice(section.length-2, 0, instagramThumbAddress);
        return section.join("/");
    };

    //Checks if the row and col has been set as a No Image already (so we don't call again)
    this.checkImageError = function(col,row)
    {
        for(var i=0;i<errorSmallImage.length;i++)
        {
            if(errorSmallImage[i].column == col && errorSmallImage[i].row == row)
            {
                return true;
            }
        }
        return false;
    };

    //Checks Large if the row and col has been set as a No Image already (so we don't call again)
    this.checkLargeImageError = function(col,row)
    {
        for(var i=0;i<errorLargeImage.length;i++)
        {
            if(errorLargeImage[i].column == col && errorLargeImage[i].row == row)
            {
                return true;
            }
        }
        return false;
    };

    //IMAGE ERRORS
    this.addImageErrorCheckSmall = function()
    {
        $('.imgcheckSmall').on("error", function ()
        {
            //Save what cell had no image so we do not try to reload again
            var nearcell = $(this).closest('td');
            var cellLocation = getTable().cell(nearcell).index();//format of .column and .row
            if(cellLocation == undefined)
            {
                //Try group table
                cellLocation = getTableGroup().cell(nearcell).index();//format of .column and .row
            }
            errorSmallImage.push(cellLocation);

            $(this).attr("src",CONST_NO_IMAGE_SMALL);
        });
    };

    this.addImageErrorCheckLarge = function()
    {
        $('.imgcheckLarge').on("error", function ()
        {
            //Save what cell had no image so we do not try to reload again
            var nearcell = $(this).closest('td');
            var cellLocation = getTable().cell(nearcell).index();//format of .column and .row
            if(cellLocation == undefined)
            {
                //Try group table
                cellLocation = getTableGroup().cell(nearcell).index();//format of .column and .row
            }
            errorLargeImage.push(cellLocation);

            $(this).attr("src",CONST_NO_IMAGE_LARGE);
        });
    };

    //SMALL
    //Allows the input of one element, and if one element then it will try to get the thumbnail
    // else it looks for a comma in the field and splits it by thumbnail,image/video
    this.createImgVidLinkSmall = function(input,noimg)
    {
        var sinput = splitElement(input);
        if(noimg)
        {
            sinput[0] = CONST_NO_IMAGE_SMALL;
        } else {
            if(sinput[0] == input)
            {
                sinput[0] = that.instagramThumbnail(input);
            }
        }
        return '<a target="_blank" href="' + sinput[1] + '"><img class="imgcheckSmall" style="max-width: ' + smallSizeWidth + '; max-height:' + smallSizeHeight + '; height: ' + smallSizeHeight + '; width: auto;" src="' + sinput[0] + '"></a>';
    };

    this.createYouTubeLinkSmall = function(input,noimg)
    {
        var sinput = [];
        if(noimg)
        {
            sinput[0] = CONST_NO_IMAGE_SMALL;
        } else {
            sinput[0] = 'http://img.youtube.com/vi/' + input + '/default.jpg';
        }
        sinput[1] = 'https://www.youtube.com/watch?v=' + input;
        return '<a target="_blank" href="' + sinput[1] + '"><img class="imgcheckSmall" style="max-width: ' + smallSizeWidth + '; max-height:' + smallSizeHeight + '; height: ' + smallSizeHeight + '; width: auto;" src="' + sinput[0] + '"></a>';
    };

    
    //LARGE
    this.createImgVidLink = function(input,noimg)
    {
        sinput = splitElement(input);
        var extention = sinput[1].substring(sinput[1].length - 3);//get last 3 elements for the extention

        //If error already, then just display bad image
        if(noimg)
        {
            return that.createImgLink(input,noimg);
        }
        //Check if second piece is a video
        if($.inArray(extention,videoExtentions) > -1) 
        {
            return that.createVidLink(input,noimg);
        } else {
            return that.createImgLink(input,noimg);
        }
    };

    this.createImgLink = function(input,noimg)
    {
        sinput = splitElement(input);
        var href = sinput[1];
        var imgsrc = sinput[1];
        if(noimg)
        {
            imgsrc = CONST_NO_IMAGE_LARGE;
        }
        return '<a target="_blank" href="' + sinput[1] + '"><img class="imgcheckLarge" style="max-width: ' + largeSizeWidth + '; max-height:' + largeSizeHeight + '; height: ' + largeSizeHeight + '; width: auto;" src="' + sinput[1] + '"></a>';
    };
    
    this.createVidLink = function(input)
    {
        //Does not check bad image because will not be called if bad image
        sinput = splitElement(input);
        var extention = sinput[1].substring(sinput[1].length - 3);//get last 3 elements for the extention
        var videodata = '<video id="my-video" class="video-js imgcheckLarge" controls preload="auto" width="' + largeSizeWidth + '" height="' + largeSizeHeight + '" data-setup="{}">';
        videodata += '<source src="' + sinput[1] + '" type="video/' + extention + '">';
        videodata += '<p class="vjs-no-js">';
        videodata += '  To view this video please enable JavaScript, and consider upgrading to a web browser that';
        videodata += '<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>';
        videodata += '</p>';
        videodata += '</video>';
        return videodata;
    };


    this.createYouTubeLink = function(input,noimg)
    {
        //If error already, then just display bad image
        if(noimg)
        {
            input = ',https://www.youtube.com/watch?v=' + input;
            return that.createImgLink(input,noimg);
        }
        var youtubeStart = '<iframe width="' + largeSizeWidth + '" height="' + largeSizeHeight + '" src="https://www.youtube.com/embed/';
        var youtubeEnd = '" frameborder="0" allowfullscreen class="imgcheckLarge"></iframe>';
        return youtubeStart + input + youtubeEnd;
    };


    this.updateSplit = function(table) {
        //Setup for split
        var boxsize = $('#'+table+'_wrapper div.box-content').css("height");//is string size
        $('#'+table+'_wrapper .dataTables_scroll').css("height","calc(100% - "+boxsize+")");

        $('#'+table+'_wrapper').css("height","100%");
        $('#'+table+'_wrapper .dataTables_wrapper').css("height","100%");
        var oversize = parseInt($('#'+table+'_wrapper .dataTables_scrollHead').css("height").replace(/px/,""));
        oversize += parseInt($('#'+table+'_wrapper .dataTables_scrollFoot').css("height").replace(/px/,""));

        $('#'+table+'_wrapper .dataTables_scrollBody').css("height","calc(100% - "+oversize+"px)");
        var tableheight = parseInt($('#'+table+'_wrapper .dataTables_scrollBody table').css("height").replace(/px/,""));
        tableheight += 22;//make room for scrollbar
        $('#'+table+'_wrapper .dataTables_scrollBody').css("max-height",tableheight+"px");
        $('#'+table+'_wrapper .dataTables_scrollBody').css("overflow-y","auto");
        $('#'+table+'_wrapper .dataTables_scrollBody').css("overflow-x","scroll");
    };

    this.updateScrollBar = function(table)
    {
        //This function is used to fix the issue of the scrollbar partially vanishing
        $('#'+table+'_wrapper .dataTables_scrollBody').css("overflow","hidden");
        setTimeout(function() {
            $('#'+table+'_wrapper .dataTables_scrollBody').css("overflow","auto");
        },0);//need to delay so a drawing will happen before removing.  Don't know why
    };
	

	this.updateMaxBox = function()
	{
	    //Decide how big to make the table to match screen size:
    	var wheight = window.innerHeight-20 + "px";//60 is from the top bar
	    $('#tablebox').css("height",wheight);
	}

    //------------------------------    
    //Setup fullscreen capability for table
    //------------------------------    
    var isfullscreen = false;
    function makefullscreen()
    {
        if(!isfullscreen)
        {
            $('#navbar').hide();
            $('#sidebar-left').hide();
            $('.fullscreenboxhide').hide();
            $('#main').css("margin-top","0px");
            $('#main').css("margin-bottom","0px");
            isfullscreen = true;
            $('div#main').addClass('sidebar-show');
            setTimeout(MessagesMenuWidth, 250);
            $('#ajax-content').css("margin-top","0px");
        } else {
            $('#navbar').show();
            $('#sidebar-left').show();
            $('.fullscreenboxhide').show();
            $('#main').css("margin-top","20px");
            $('#main').css("margin-bottom","20px");
            isfullscreen = false;
            $('div#main').removeClass('sidebar-show');
            setTimeout(MessagesMenuWidth, 250);
            $('#ajax-content').css("margin-top","35px");
        }
    }

    $('.largecode').on('click',makefullscreen);

};