function setupGroupTable(oTable,oTableGroup,groupbyColumnsOrder,submitAutoComplete,groupbyColumns,selectFirst)
{
	//This is only called if a GroupBy Table was created

    //Add Splitter if group
    var guttersize = 14;
    Split(['#groupdiv', '#singlediv'], {
        sizes: [30, 70],
        direction: 'vertical',
        minSize: 200,
        gutterSize: guttersize
    });
    var arrowsize = 20;//number of arrows to draw
    var addedup = "<span id='gutterup' style='margin-left: 10%;  vertical-align: top; cursor: pointer;'>";
    for(var ii=0;ii<arrowsize;ii++)
    {
        addedup+= "<i class='fa fa-angle-double-up'></i>";
    }
    addedup+= "</span>";
    var addeddown = "<span id='gutterdown' style='position: absolute; right: 0; margin-right: 10%;  vertical-align: top; cursor: pointer;'>";
    for(var ii=0;ii<arrowsize;ii++)
    {
        addeddown+="<i class='fa fa-angle-double-down'></i>";
    }
    addeddown+="</span>";

    $('.gutter').html(addedup + addeddown);
    $('#gutterup').on('click',function(e) {
        e.preventDefault();
        $('#singlediv').css('height','calc(100% -  '+guttersize+'px)');
        $('#groupdiv').css('height','0%');
    });
    $('#gutterdown').on('click',function(e) {
        e.preventDefault();
        $('#groupdiv').css('height','calc(100% -  '+(guttersize+5)+'px)');
        $('#singlediv').css('height','0%');
    });
	

    //Add clickable rows for Group Table
    //$('#grouptable > tbody').on( 'click', 'tr', function () {
    oTableGroup.on("deselect",function ( e, dt, type, indexes ) {
        if ( type !== 'row' ) {
            return;
        }
        var row = oTableGroup.row(indexes);//,{selected: false}
        selectGroup(row,true);
    });
    oTableGroup.on("select",function ( e, dt, type, indexes ) {
        if ( type !== 'row' ) {
            return;
        }
        //var data = table.rows( indexes ).data().pluck( 'id' );
        var row = oTableGroup.row(indexes);//,{selected: false}
        selectGroup(row,false);
    });

    function selectGroup(row,unselect)
    {
        //stop the editing of any field:
        submitAutoComplete();
        $('.typeahead').trigger('submit');

        $('.myselected').removeClass('primary myselected');
        //filter single table by the selected row in group table
        var rowdata = row.data();
        var orderGroup = oTableGroup.colReorder.order();
        var order = oTable.colReorder.order();
        for(var i=0;i<rowdata.length;i++)
        {
            var origcol = orderGroup.indexOf(i);
            var singlecol = groupbyColumns[origcol];
            var curdata = rowdata[i];
            if(groupbyColumnsOrder.indexOf(singlecol) > -1)
            {
                //It is an order column, so filter
                var inputstring = "^"+escapeRegExp(curdata)+"$";//regex exact string
                if(unselect)
                {
                    inputstring = "";
                }

                oTable.columns(singlecol).search( inputstring, true, false, false);
            }
        }
        //Draw the table with filters
        oTable.draw();

        //Add class after draw
        if(!unselect)
        {
            $('.selected').addClass('primary myselected');
        }
    }

    //Add navigation with ctrl+^ and ctrl+\/
    $(window).on('keydown.GROUP', function(event) 
    {
        if (event.ctrlKey === true){
            switch(event.keyCode) {
                case 38: // up arrow  
                    //Select previous group if exists
                    var currow = oTableGroup.row({selected:true}).node();
                    var newrow = $(currow).prev();
                    if(newrow.size()>0)
                    {
                    	oTableGroup.row(currow).deselect();
                    	oTableGroup.row(newrow).select();
                    	myscrollTo(newrow);
                    	checkSingleEdit();
                    }
                    break;
                case 40: // down arrow
                    //Select next group if exists
                    var currow = oTableGroup.row({selected:true}).node();
                    var newrow = $(currow).next();
                    if(newrow.size()>0)
                    {
                    	oTableGroup.row(currow).deselect();
                    	oTableGroup.row(newrow).select();
                    	myscrollTo(newrow);
                    	checkSingleEdit();
                    }
                    break;
            }
        }
    });

	function checkSingleEdit()
	{
        var modalclose = $('body').find('a.close-link').eq(0);
    	if(modalclose.is(":visible") )
    	{
    		modalclose.click();//Close modal if open
        	//Reopen modal on first entry
        	setTimeout(function() {
	            $('#datatable-3 tbody tr').eq(0).find('td').eq(0).dblclick();
	            //Select first entry
	            setTimeout(function() {
		            selectFirst();
	            },100)
        	},500);
        }

	}

	function myscrollTo(curnode)
	{
        //Scroll to cntr position
        var trPos = $(curnode).position();
        var trCtr = ($(curnode).height()) / 2;
        var dataTblctr = ($('#grouptable_wrapper .dataTables_scrollBody').height()) / 2;
        var curscroll = $('#grouptable_wrapper .dataTables_scrollBody').scrollTop();
        var finalPos = trPos.top - dataTblctr + trCtr + curscroll;
        //console.log("Moving to: " + trPos.top + ", final = " + finalPos);
        $('#grouptable_wrapper .dataTables_scrollBody').animate({scrollTop:finalPos},150);//Animate scrolling

	}


}