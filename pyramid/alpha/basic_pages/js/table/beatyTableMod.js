//
//  [MODIFIED]: Beauty tables plugin (navigation in tables with inputs in cell)
//  Created by DevOOPS.
//
(function( $ ){
    $.fn.beautyTablesModified = function() {
        var table = this;
        var string_fill = false;
        this.on('keydown', function(event) {
            var target = event.target;
            var tr = $(target).closest("tr");
            var input = $(target).closest("input");
            //var input = $(target).closest("select");
            var col = $(target).closest("td");
            var findstr = "td.editable";
            if (target.tagName.toUpperCase() == 'INPUT'){
                if (event.shiftKey === true){
                    switch(event.keyCode) {
//                         case 37: // left arrow  $('#datatable-3 tbody td.editable')
//                             input.trigger("submit");
//                             col.prev().click().focus();//.children(findstr)
//                             break;
//                         case 39: // right arrow
//                             input.trigger("submit");
//                             col.next().click().focus();//.children(findstr)
//                             break;
//                         case 40: // down arrow
//                             event.preventDefault();
//                             if (string_fill==false){
//                                 input.trigger("submit");
//                                 tr.next().find('td:eq('+col.index()+')').click().focus();// + findstr
//                             }
//                             break;
                        case 9: //Shift+Tab - shift previous column, or row if beginning of columns
                            event.preventDefault();
                            if (string_fill==false){
                                input.trigger("submit");
                                do {
                                  tr = tr.prev();
                                  var nextCell = tr.children(findstr).last();
                                  if(col.prevAll(findstr).size() > 0)
                                  {
                                      nextCell = col.prevAll(findstr).first();
                                  }
                                  nextCell.click();//.focus();
                                } while(nextCell.size() == 0 && tr.size() > 0);
                            }
                            break;
                        case 13://Shift Enter - move straight up
                            event.preventDefault();
                            if (string_fill==false){
                                input.trigger("submit");
                                do {
                                  tr = tr.prev();
                                  var nextCell = tr.find('td,th').eq(col.index());//will find based on td and tr in a row
                                  nextCell.click();//.focus();// + findstr
                                } while(!nextCell.hasClass("editable") && tr.size() > 0);
                            }
                            break;
                    }
                }
                
                 if (event.ctrlKey === true){
                     switch(event.keyCode) {
                            //DO NOT NEED - hard to find first or last row element                     
//                          case 37: // left arrow
//                              tr.find('td:eq(2)').click().focus();//.find(findstr)
//                              break;
//                          case 39: // right arrow
//                              tr.find('td:last-child').click().focus();//.find(findstr)
//                              break;
//                      case 40: // down arrow
//                          if (string_fill==false){
//                             input.trigger("submit");
//                              table.find('tr:last-child td:eq('+col.index()+')').click().focus();// + findstr
//                          }
//                          break;
//                      case 38: // up arrow
//                          if (string_fill==false){
//                             input.trigger("submit");
//                              table.find('tr:eq(2) td:eq('+col.index()+')').click().focus();// + findstr
//                          }
//                          break;
                     }
                 }
                //Enter - shift down
                if (event.shiftKey === false && event.keyCode == 13) {
                    event.preventDefault();
                    if (string_fill==false){
                        input.trigger("submit");
                        do {
                          tr = tr.next();
                          var nextCell = tr.find('td,th').eq(col.index());//will find based on td and tr in a row
                          nextCell.click();//.focus();// + findstr
                        } while(!nextCell.hasClass("editable") && tr.size() > 0);
                      
                    }
                }
                //Tab - shift next column, or row if end of columns
                if (event.shiftKey === false && event.keyCode == 9) {
                    event.preventDefault();
                    if (string_fill==false){
                        input.trigger("submit");
                        
                        do {
                          tr = tr.next();
                          var nextCell = tr.children(findstr).first();
                          if(col.nextAll(findstr).size() > 0)
                          {
                              nextCell = col.nextAll(findstr).first();
                          }
                          nextCell.click();//.focus();
                        } while(nextCell.size() == 0 && tr.size() > 0);
                    }
                }
                    //DO NOT NEED - since not all col/rows are editable then this doesn't make sense
//                 if (string_fill==false){
//                     if (event.keyCode == 34) {
//                         event.preventDefault();
//                         table.find('tr:last-child td:last-child').click().focus();}//.find(findstr)
//                     if (event.keyCode == 33) {
//                         event.preventDefault();
//                         table.find('tr:eq(1) td:eq(1)').click().focus();}//.find(findstr)
//                 }
            }
        });
    };
})( jQuery );