//Written by DWS
function setupRegionData()
{
    var dateClass = new MonthStartEnd({makesame:false});
    var ALL_STATES = ['AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY'];
    var ALL_REGION = ALL_STATE.slice();
    ALL_REGION.push('non-USA');
    ALL_REGION.push('Unknown');
    var regioncontainer = 'tagdaily';
    var regioncounts;
    var regionmaps;

	//var dbcounts = new setupRegionCountDB(gotDB);
    //Temporary by-pass DB and hardcode data
    var dbcounts = {
        'DB_regioncount' : {
            //Region x Tag x Month x Count
            'AL':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'AK':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'AZ':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'AR':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'CA':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'CO':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'CT':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'DE':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'FL':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'GA':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'HI':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'ID':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'IL':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'IN':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'IA':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'KS':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'KY':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'LA':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'ME':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'MD':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'MA':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'MI':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'MN':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'MS':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'MO':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'MT':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'NE':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'NV':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'NH':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'NJ':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'NM':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'NY':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'NC':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'ND':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'OH':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'OK':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'OR':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'PA':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'RI':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'SC':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'SD':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'TN':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'TX':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'UT':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'VT':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'VA':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'WA':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'WV':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'WI':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'WY':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'Unknown':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}
            ,'non-USA':{'1':{'2016-08':10,'2016-05':14,'2016-01':5},'3':{'2016-08':3,'2016-02':4}}

            
        },
        DB_tags : {'1':'Cig','3':'Smoke'}
    };
    var alltags = [];
    var fulltimerange = {};
    $(document).ready(gotDB);

    function gotDB()
    {
        //Got the information from the database
        //Allow filtering by tags for Map
        // 
        alltags = Object.keys(dbcounts.DB_tags);
        fulltimerange = findfulltimerange(dbcounts.DB_regioncount);

        //screenHold("Processing Counts","Generating the Map of the Counts.  Please wait...","",
        // function() {
            setupTagSelection();
            plotTagMonthly(dbcounts.DB_regioncount,dbcounts.DB_tags);
            plotMap(dbcounts.DB_regioncount);
        //});

        
        $('.all-tag-region-count-to-json').off('click.all');
        $('.all-tag-region-count-to-json').on('click.all', function() {
            //Save the raw JSON to a file for download
            var data = JSON.stringify(dbcounts.DB_regioncount);
            var blob = new Blob([data], {type: "text/csv;charset=utf-8"});
            saveAs(blob, 'all_tag_region_time_counts.json');
        });
    } 

    //setup tag selection
    function setupTagSelection()
    {
        var secondstr = '<select class="s2_with_tag filtercolumn" multiple>';
        $.each ( dbcounts.DB_tags,function(k,v)
        {
            secondstr += "<option value='"+k+"'>("+k+") " + v + "</option>";
        });
        secondstr += "</select>";
        $('#tagfilter').html(secondstr);

        //Turn on Select2 capability
        $('.s2_with_tag').select2({placeholder: "Select Tags"});

        //Look for changes in column filters

    }

    function getDates()
    {
        var outdata = {start:'',end:''};
        if(dateClass.getEnd() != '')
        {
            var edate = dateClass.getEndDate();
            outdata.end = makeEndOfDay(edate);
        }
        if(dateClass.getStart() != '')
        {
            var sdate = dateClass.getStartDate();
            outdata.start = makeStartOfDay(sdate);
        }

        return outdata;
    }

    function plotMap(counts)
    {
        var codearray = ALL_STATES;
        var countarray = [];
        var worldarray = [];
        var textarray = [];
        var curzmax = 0;

        function computeArrays(tags,timerange)
        {
            //Reset
            countarray = [];
            worldarray = [];
            curzmax = 0;
            var supertotal = 0;
            var allcodearray = ALL_REGION.slice();//copy
            var lenusa = ALL_STATES.length;
            
            //Region -> Tag -> Month -> Count
            for (var codeidx = 0;  codeidx < allcodearray.length; codeidx++) 
            {
                var code = allcodearray[codeidx];
                var totalcount = 0;
                if(code in counts)
                {
                    var regiondata = counts[code];
                    if(tags.length == 0)
                    {
                        //do all tags if empty tag list
                        tags = alltags;
                    }
                    for (var tagidx = 0; tagidx < tags.length; tagidx++) 
                    {
                        if(tags[tagidx] in regiondata)
                        {
                            var regiontagdata = regiondata[tags[tagidx]];
                            var monthkey = Object.keys(regiontagdata);
                            for (var monthidx = 0; monthidx < monthkey.length; monthidx++) 
                            {
                                var curmonth = monthkey[monthidx];
                                var tmpdate = inversefixDateOffsetDate(new Date(curmonth));
                                var datecheck = true;
                                
                                //Check if month is in range
                                if(timerange.start != "")
                                {
                                    if(dates.compare(timerange.start,tmpdate) > 0)
                                    {
                                        datecheck = false;
                                    }
                                }
                                if(timerange.end != "")
                                {
                                    if(dates.compare(timerange.end,tmpdate) < 0)
                                    {
                                        datecheck = false;
                                    }
                                }

                                if(true == datecheck)
                                {
                                    totalcount += regiontagdata[curmonth];
                                }
                            }
                        }
                    }
                }
                if(codeidx < lenusa)
                {
                    //USA only
                    countarray.push(totalcount);
                    supertotal += totalcount;
                    curzmax = Math.max(curzmax,totalcount);//keep track of max
                } else {
                    //Unknown and non-USA
                    if(codeidx == lenusa)
                    {
                        worldarray.push(supertotal);
                    }
                    worldarray.push(totalcount);
                }
            }
            
        }
        computeArrays([],{start:'',end:''});

        var mydata = [{
            type: 'choropleth',
            locationmode: 'USA-states',
            locations: codearray,
            z: countarray,
            //text: textarray,
            zmin: 0,
            zmax: curzmax,
            colorscale: [
                [0, 'rgb(242,240,247)'], [0.2, 'rgb(218,218,235)'],
                [0.4, 'rgb(188,189,220)'], [0.6, 'rgb(158,154,200)'],
                [0.8, 'rgb(117,107,177)'], [1, 'rgb(84,39,143)']
                ],
            colorbar: {
                title: 'Counts',
                thickness: 0.2
            },
            marker: {
                line:{
                    color: 'rgb(255,255,255)',
                    width: 2
                }
            }
        }];


        var layout = {
            title: 'Database Counts per Across Time',
            geo:{
                scope: 'usa',//requires access to the web to show this figure
                showlakes: true,
                lakecolor: 'rgb(255,255,255)'
            }
        };

        var container = 'mapplot';
        Plotly.newPlot(container, mydata, layout, {showLink: true});  


        //world plot
        var mydata2 = [{
            //x : [1,2,3],
            values : worldarray,
            labels : ["USA","non-USA","Unknown"],
            type : 'pie'
        }];
        
        var layout2 = {
            title: "World Location Percent"        
        };
        var container2 = "worldplot";
        Plotly.newPlot(container2,mydata2,layout2);    


        function updateData(tags,timerange)
        {
            computeArrays(tags,timerange);
            var graph = document.getElementById(container);
            graph.data[0].z= countarray;
            graph.data[0].zmax = curzmax;

            Plotly.redraw(graph);
            
            //world plot
            var worldgraph = document.getElementById(container2);
            worldgraph.data[0].values = worldarray;
            Plotly.redraw(worldgraph);
        }      

        function chgfcn() {
            //Get array of elements selected
            var vals = $( '.filtercolumn').find(":selected").map(function(){ return this.value }).get();

            updateData(vals,getDates());
            //screenHold("Processing Row Changes","Redrawing row, please wait...","",function() {showRows(vals);});
        }

        $('.filtercolumn').on('change',chgfcn);
        
        $('.daterange').on('change',chgfcn);
        $('.daterange').MonthPicker('option', 'OnAfterChooseMonth', chgfcn ); 

        function savePlottedData()
        {
            var data = [];
            var hdr = ['Region','Count'];
            data.push(hdr);
            var allcodearray = codearray.slice();//copy
            var lenusa = codearray.length;
            allcodearray.push("non-USA");
            allcodearray.push("Unknown");
            for(var idx=0;idx<allcodearray.length;idx++)
            {
                var linedata = [];
                linedata.push(allcodearray[idx]);
                if(idx < lenusa)
                {
                    linedata.push(countarray[idx]);
                } else {
                    //Add one to skip USA
                    linedata.push(worldarray[idx-lenusa+1]);
                }
                data.push(linedata);
            }
            var csv = new csvWriter();
            var csvdata = csv.arrayToCSV(data);
            var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
            saveAs(blob, 'tag_region_filter_counts.csv');
        }
        
        
        $('.label-to-csv').show();
        $('.region-count-to-csv').off('click.all');
        $('.region-count-to-csv').on('click.all', function() {
            //Save single region data
            savePlottedData();
        });
    }


//REGIONAL Monthly Plots
    function getRegionData(region)
    {
        //regioncounts: {region:{tag:{month:count}}}
        var alltrace = [];
        var allregion = Object.keys(regioncounts);
        $('.regionid').html(region)
        if(region === 'All')
        {
            region = allregion;
        } else if(region == 'USA') {
            region = ALL_STATES;                
        }
        //otherwise it is a specific state, or unknown, or non-US
        //  which all exist in counts
        for(var tagidx=0; tagidx<alltags.length; tagidx++)
        {
            var tag = alltags[tagidx];
            var tagarray = [];

            var totalcount = {};
            for(var idx=0; idx<region.length; idx++)
            {
                var regiondata = regioncounts[region[idx]];
                if(tag in regiondata)
                {
                    //Add up each data by date
                    $.each(regiondata[tag],function(k,v) {
                        if(k in totalcount)
                        {
                            totalcount[k] += v;
                        } else {
                            totalcount[k] = v;
                        }
                    });
                }
                
            }
            var alldates = Object.keys(totalcount);
            for(var dateidx=0; dateidx<alldates.length; dateidx++)
            {
                tagarray.push(totalcount[alldates[dateidx]]);
            }
            
                
            var trace1 = {
              x: alldates,
              y: tagarray,
              name: "("+tag+") "+regionmaps[tag],
              type: 'scatter'
            };
            alltrace.push(trace1);
        }
        return alltrace;
    }

    function updateRegionData(region)
    {
        var graph = document.getElementById(regioncontainer);
        graph.data = getRegionData(region);

        Plotly.redraw(graph);
    }

    function saveTagData(regiondata)
    {
        var timerange = fulltimerange; 
        var data = [];
        var hdr = ['Tag'];
        var monthrange = createMonthArray(timerange);
        for(var tidx = 0; tidx<regiondata.length; tidx++)
        {
            var tagdata = regiondata[tidx];
            var linedata = [];
            linedata.push(tagdata.name);
            var monthidx = 0;
            for(var ridx = 0; ridx<monthrange.length; ridx++)
            {
                var month = monthrange[ridx];
                if(tidx==0)
                {
                    //only add the header on first time
                    hdr.push(month);//TODO: reformat to Aug-2016  from 2016-08
                }
                while(monthidx < tagdata.x.length)
                {
                    if(month == tagdata.x[monthidx])
                    {
                        //found a month
                        linedata.push(tagdata.y[monthidx]);
                        monthidx++;
                        break;
                    }
                    else if(month > tagdata.x[monthidx])
                    {
                        break;
                    }
                    else
                    {
                        linedata.push(0);//fill with 0 if no data
                        monthidx++;
                    }
                }
                //tagdata.x //alldates
                //tagdata.y //countarray
                //tagdata.name //(#) TAG
            }     
            //Add header if first time through
            if(tidx==0)
            {
                data.push(hdr);
            }       
            data.push(linedata);
        }
        
        var csv = new csvWriter();
        var csvdata = csv.arrayToCSV(data);
        var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
        saveAs(blob, 'tag_time_filter_counts.csv');            
    }


    function plotTagMonthly(counts,maps)
    {
        //counts: {region:{tag:{month:count}}}
        regioncounts = counts;//save for outside functions above.
        regionmaps = maps;//save for outside functions above.
        
        //Make arrays of tag counts over months
        //Flatten the dictionary data:
        // find all dates (filter by region selected)
        // find all tags (filter by region selected)
        //This is used to plot a line graph of Tags over days
        //Turn YYYY-MM => YYYY-MM-01 to be recognized by Plotly as a date
        var title = "Tag Monthly Counts";
        var alltrace = getRegionData('All');//all regions

        $('.tag-region-count-to-csv').off('click.all');
        $('.tag-region-count-to-csv').on('click.all', function() {
            //Save single region data
            saveTagData(alltrace);
        });
        

        var layout = {
          //title: title,
          xaxis: {
            title: 'Date',
            titlefont: {
              family: 'Courier New, monospace',
              size: 18,
              color: '#777777'
            }
          },
          yaxis: {
            title: 'Counts',
            titlefont: {
              family: 'Courier New, monospace',
              size: 18,
              color: '#777777'
            }
          },
          hovermode: 'closest',
          //autosize: false,
          //width: '100%',
          height: 600,
          // margin: {
          //   l: 20,
          //   r: 20,
          //   b: 100,
          //   t: 100,
          //   pad: 4
          // },
          showlegend:true
          // legend:{
          //       x:0.9,
          //       y:1
          //   }
        };
        Plotly.newPlot(regioncontainer, alltrace,layout);


//         var slider_range_min_amount = $(".slider-range-min-amount");
//         var slider_range_min = $(".slider-range-min");
//         slider_range_min.slider({
//             range: "min",
//             value: 0,
//             min: 0,
//             max: 30,
//             slide: function( event, ui ) {
//                 var newtext = "";
//                 if(ui.value == 0)
//                 {
//                     newtext = "Disabled";
//                 } else {
//                     newtext = ui.value +" Days";
//                 }
//                 slider_range_min_amount.val( newtext);
//                 updateData(ui.value);
//             }
//         });
//         slider_range_min_amount.val("Disabled");
        $('#btnshowall').on("click",function() {
            setVisibility(true);
        });

        function setVisibility(visibilty)
        {
            var plotDiv = document.getElementById(regioncontainer);
            var plotData = plotDiv.data;
            $.each(plotData, function(key, value){
                plotDiv.data[key].visible = visibilty;
            })
            Plotly.redraw(plotDiv);            
        }

        $('#btnhideall').on("click",function() {
            setVisibility('legendonly');
        });

        function resizePlot()
        {
            var plotDiv = document.getElementById(regioncontainer);
            Plotly.Plots.resize(plotDiv);
        }

        //$('#dailyexpand').on("click",function() {setTimeout(resizePlot,100);});
        $('.show-sidebar').on("click",function() {setTimeout(resizePlot,200);});
        $(window).on("resize",resizePlot);
    }

    function createMonthArray(timerange)
    {
        //returns an array with 2016-04 ... of all months in timerange
        //timerange = {start,stop} as blank or date at beginning of day / end of day
        // if blank then start/end is start/end of data
        var findstart = false;
        var findend = false;
        if(timerange.start === "")
        {
            timerange.start = fulltimerange.start;
        }
        if(timerange.end === "")
        {
            timerange.end = fulltimerange.end;
        }


        var tmpdate = new Date(timerange.start);//copy time
        
        //make array
        var outarray = [];
        while( date.compare(tmpdate,timerange.end) < 0 )
        {
            var mm = tmpdate.getMonth()+1;
            var yy = tmpdate.getFullYear();
            var newdate = yy+"-"+pad(mm.toString(),2);
            outarray.push(newdate);
            
            //Increment
            tmpdate.setMonth(tmpdate.getMonth()+1);
                
        }
        return outarray;
    }

    function findfulltimerange(counts)
    {
        //returns timerange = {start,end} for entire range
        //Region -> Tag -> Month -> Count
        var codearray = ALL_REGION;
        var timerange = {
            start:new Date(), 
            end:new Date('1900-01-01') //oldest date possible
        };
        for (var codeidx = 0;  codeidx < codearray.length; codeidx++) 
        {
            var code = codearray[codeidx];
            var totalcount = 0;
            if(code in counts)
            {
                var regiondata = counts[code];
                //use all tags to determine start / end
                for (var tagidx = 0; tagidx < alltags.length; tagidx++) 
                {
                    if(alltags[tagidx] in regiondata)
                    {
                        var regiontagdata = regiondata[alltags[tagidx]];
                        var monthkey = Object.keys(regiontagdata);
                        for (var monthidx = 0; monthidx < monthkey.length; monthidx++) 
                        {
                            var curmonth = monthkey[monthidx];
                            var tmpdate = inversefixDateOffsetDate(new Date(curmonth));


                            if(dates.compare(timerange.start,tmpdate) > 0)
                            {
                                timerange.start = tmpdate;
                            }
                            if(dates.compare(timerange.end,tmpdate) < 0)
                            {
                                timerange.end = tmpdate;
                            }
                        }
                    }
                }
            }
        }//end for code
        return timerange;
    }


    function createArray(counts,tags,timerange,maps)
    {
        //Reset
        var codearray = ALL_STATES;
        var countarray = [];
        var hdr = ["Region"];
        var alltimes = createMonthArray(counts,timerange);
        hdr.pushAll(timerange);

        //Region -> Tag -> Month -> Count
        for (var codeidx = 0;  codeidx < codearray.length; codeidx++) 
        {
            var code = codearray[codeidx];
            var totalcount = 0;
            if(code in counts)
            {
                var regiondata = counts[code];
                if(tags.length == 0)
                {
                    //do all tags if empty tag list
                    tags = alltags;
                }
                for (var tagidx = 0; tagidx < tags.length; tagidx++) 
                {
                    if(tags[tagidx] in regiondata)
                    {
                        var regiontagdata = regiondata[tags[tagidx]];
                        var monthkey = Object.keys(regiontagdata);
                        for (var monthidx = 0; monthidx < monthkey.length; monthidx++) 
                        {
                            var curmonth = monthkey[monthidx];
                            var tmpdate = inversefixDateOffsetDate(new Date(curmonth));
                            var datecheck = true;
                            
                            //Check if month is in range
                            if(timerange.start != "")
                            {
                                if(dates.compare(timerange.start,tmpdate) > 0)
                                {
                                    datecheck = false;
                                }
                            }
                            if(timerange.end != "")
                            {
                                if(dates.compare(timerange.end,tmpdate) < 0)
                                {
                                    datecheck = false;
                                }
                            }

                            if(true == datecheck)
                            {
                                totalcount += regiontagdata[curmonth];
                            }
                        }
                    }
                }
            }
            countarray.push(totalcount);
            curzmax = Math.max(curzmax,totalcount);//keep track of max
        }
    }


    function saveDailyData(counts,maps)
    {
        //Replace the header row non-'Date' with actual Tag name
        //clone array of array
        var data = JSON.parse(JSON.stringify(counts));
        var dateidx = data[0].indexOf('Date');
        for(var ii=0;ii<data[0].length;ii++)
        {
            if(ii != dateidx)
            {
                id = data[0][ii];
                var mapid = "";
                if(id in maps)
                {
                    mapid = "("+id+") " + maps[id];
                } else {
                    //no mapping for id
                    mapid = "("+id+") " +"unknown";
                }
                data[0][ii] = mapid;
            }
        }        
        //Change date to better format (skip the header row)
        for(var ii=1;ii<data.length;ii++)
        {
            var year = data[ii][dateidx].slice(0,4);
            var month = data[ii][dateidx].slice(4,6);
            var day = data[ii][dateidx].slice(6,8);
            data[ii][dateidx] =  year + "-" + month + "-" + day;
        }
        var csv = new csvWriter();
        var csvdata = csv.arrayToCSV(data);
        var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
        saveAs(blob, 'tagdaily_counts.csv');
    }

    function saveData(counts,maps,typeid,isrule){

        var headerrow = counts[0];

        //clone array of array
        var data = JSON.parse(JSON.stringify(counts));
        data.shift()//remove header row

        //Create header
        var temp = headerrow.slice();//make copy
        temp.splice(ididx,1);//remove id - col name of num
        temp.unshift(typeid + " Name");
        temp.unshift(typeid + " ID");
        var removerow = [];//rows to be removed after editing

        //For rules, add tag name and ID
        if(isrule)
        {
            temp.unshift("Tag Name");
            temp.unshift("Tag ID");
        }

        for(var i=0;i<data.length;i++)
        {
            var id = data[i].splice(ididx,1)[0];//remove id - col name of num
            var mapid = "";
            if(id in maps)
            {
                mapid = maps[id];
            } else {
                //remove row
                removerow.push(i);
                mapid = "unknown";
            }
            data[i].unshift(mapid);
            data[i].unshift(""+id);
            //Add tags if rules
            if(isrule)
            {
                var tagid = ""+dbcounts.DB_rules2tags[id];
                //If tag is undefined for rule, then remove rule
                if(!(parseInt(tagid) in dbcounts.DB_tags))
                {
                    removerow.push(i);
                }
                var tagname = ""+dbcounts.DB_tags[parseInt(tagid)];
                data[i].unshift(tagname);
                data[i].unshift(tagid);
            }
        }
        //Need to remove in reverse order so not to mess up the order
        for(var rowidx = removerow.length-1; rowidx >= 0; rowidx--)
        {
            data.splice(removerow[rowidx],1);
        }
        data.unshift(temp);  //add header row back

        //Add Header row
        var csv = new csvWriter();
        var csvdata = csv.arrayToCSV(data);
        var blob = new Blob([csvdata], {type: "text/csv;charset=utf-8"});
        saveAs(blob, typeid + '_region_counts.csv');
    }
}
