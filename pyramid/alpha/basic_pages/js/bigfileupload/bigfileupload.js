//Written by DWS
var BigFileUpload = function (options) {
    'use strict';

    function getUrl()
    {
        //Figure out what the location is for AJAX call
        var pathArray = location.href.split( '/' );
        var protocol = pathArray[0];
        var host = pathArray[2];
        return protocol + '//' + host;
    }
    if(options == undefined)
    {
        options = {};
    }

    // Change this to the location of your server-side upload handler:
    var url = getUrl() + '/db/savefile.json';
    var fileupload;
    var progress;
    //var submitbtn;
    var fileids = [];
    var delaysubmit = options.delaysubmit || false;
    this.filetypes = options.filetypes || /(\.|\/)(csv)$/i;//allow to be overwritten
    var fileuploadtxt = options.fileupload || '#fileupload';
    var progresstxt = options.progress || '#progress';
    var filesadded = false;


    //Can be used externally to do something on completion
    this.completefcn = undefined; //function(fileid,filelink) - called per file
    this.failfcn = undefined; //function()
    this.cancelAfterOneChunk = options.cancelAfterOneChunk || false;
    var that = this;

    $(document).ready(setup);

    var filedata;
    function setup()
    {
        //Define variables
        fileupload = $(fileuploadtxt);
        progress = $(progresstxt+' .progress-bar');
        //submitbtn = $('#filesubmit');

        var filedata = fileupload.fileupload({
            maxChunkSize: 10000000, // 10 MB
            acceptFileTypes: that.filetypes,
            url: url,
            dataType: 'json',
            done: completefcn,
            progressall: progressfcn,
            fail: failfcn,
            formData: {localid: getRandomInt(0,1000)},//give some unique id to track the upload
            add: function (e, data) {
                filedata = data;//save to call submit later
                if(delaysubmit == false)
                {
                    //Submit now
                    data.submit();
                }
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled')
            .on('fileuploadchunkdone', function (e, data) {
                if(that.cancelAfterOneChunk)
                {
                    filedata.abort();//cancel the upload
                }
            });

    }

    this.getFiles = function()
    {
        if(filedata != undefined)
        {
            return filedata.files;
        }
    }

    this.submit = function()
    {
        if(filedata != undefined && delaysubmit == true)
        {
            filedata.submit();
        }
    }
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
    function progressfcn (e, data) {
        var progressnum = parseInt(data.loaded / data.total * 100, 10);
        progress.css(
            'width',
            progressnum + '%'
        );
    }

    function addfcn(e, data) {
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            node.appendTo(data.context);
            filesadded = true;
        });

    }

    function completefcn(e,data) {
        $.each(data.result.files, function (index, file) {
            var filelink = '/db/getfile?fileid='+file.fileid;
            var fileid = file.fileid;
            if(that.completefcn)
            {
                that.completefcn(fileid,filelink,data.files[index]);
            } else {
                fileids.push(fileid);
                $('<a/>').text(file.name).attr('href',filelink).appendTo('#files');
                $('<br/>').appendTo('#files');
            }
        });
        progressfcn('e',{loaded:0,total:1});//Set progress bar back to 0
    }

    function failfcn(e,data) {
        console.log("Error on upload");
        if(that.failfcn)
        {
            that.failfcn();
        }
    }
};