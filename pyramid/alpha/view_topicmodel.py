'''
Written by DWS.
'''
from pyramid.view import view_config
import json
from shared.topic_model import topic_model
from shared.savefile import SaveFile

@view_config(route_name='topicmodel', renderer="json")
def topicmodelIn(request):
	#myjson = {
	#    textid: Text FileId
	#    textcolumn: The column name of the text
	#  }
	#
	#out = {
	#    saveid : .json file ID
	#    result : False/True
	#  }
    myjson = request.json_body
    outjson = SaveFile()
    incsv = SaveFile(fileid=myjson['textid'])
    text_file_handle = incsv.getFilePath()
    json_save_handle = outjson.getFilePath()
    text_key = myjson['textcolumn']
    out = {}
    out['saveid'] = outjson.getFileID()
    out['result'] = topic_model(text_file_handle,json_save_handle,text_key)
    return out
