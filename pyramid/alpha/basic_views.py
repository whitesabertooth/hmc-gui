from pyramid.view import view_config
from pyramid.response import Response
'''
Written by DWS.
'''

@view_config(route_name='basic_pages_index', renderer='basic_pages/index.pt')
def index_view(request):
	newdomain = "ihrp-hmc.ihrp.uic.edu"
	if request.domain.startswith("ihrp-hmc") and request.domain != newdomain:
		#status 301 = Moved Perminently
		return Response(status_int=301, location=request.current_route_url(_host=newdomain)) 

	isdebug=False
	if 'isdebug' in request.registry.settings:
		if request.registry.settings['isdebug'] == 'true':
			isdebug = True

	return dict(logged_in=request.unauthenticated_userid,
		editor=request.has_permission('edit').boolval,isdebug=isdebug)


@view_config(route_name='basic_pages_watch_demo', renderer='basic_pages/watch_coding_entry_demo.pt')
def demo_view(request):
	return {}

@view_config(route_name="basic_pages_coder", renderer='basic_pages/master_coding_instructions.pt')
def coder_view(request):
	return {}

@view_config(route_name='submit_error', renderer="json")
def submit_error(request):
	myjson = request.json_body
	#{inputtext: $('#emailtext').val(), email: $('#emailinput').val(), log: alllog, page: pagedata}
	replytoemail = myjson['email'] #reply-to
	alllog = "\n".join(myjson['log'])
	entirepage = myjson['page']
	message = myjson['inputtext']
	from datetime import datetime
	nowdate = datetime.now()
	datestr = str(nowdate)
	allbody = "Someone reported an error or feedback on the HMC website.  Below is their message and reply-to email.  Attached is the error log and webpage content."
	allbody += "\n\nMessage: " + message 
	allbody += "\nReply-to: " + replytoemail
	allbody += "\n\n-HMC Website Automated Feedback"
	files = [
		('body.html',entirepage),
		('alllog.txt',alllog)
	]
	finalstatus = send_email('hmcdeveloper','slemery528','dsabin2@uic.edu','Website Error @ '+datestr,allbody,files, replytoemail)
	return {'status':finalstatus}



from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
import smtplib
def send_email(user, pwd, recipient, subject, body, files, REPLY_TO_ADDRESS):

    gmail_user = user
    gmail_pwd = pwd
    FROM = user
    TO = recipient if type(recipient) is list else [recipient]
    SUBJECT = subject
    TEXT = body

    msg = MIMEMultipart()
    msg['from'] = FROM
    msg['to'] = COMMASPACE.join(TO)
    msg['date'] = formatdate(localtime=True)
    msg['subject'] = SUBJECT
    msg.add_header('reply-to', REPLY_TO_ADDRESS)
    msg.attach(MIMEText(TEXT))

    #Attache logs as files
    for fname,fdata in files or []:
        attachment = MIMEText(fdata.encode('utf-8'))
        attachment.add_header('Content-Disposition', 'attachment', filename=fname)           
        msg.attach(attachment)

    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, msg.as_string())
        server.close()
        return True
    except:
        return False