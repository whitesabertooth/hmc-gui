'''
Written by DWS.
'''
from pyramid.view import view_config
from etl.general.mongo_database import MongoDatabase
import numbers
import json
import csv

@view_config(route_name='home_mongo_cmd', renderer="json")
def mongo_cmd(request):
    # Perform Mongo transation

    myparams = request.params
    if "db" in myparams:
        myjson = myparams
    else:
        #load input as json
        myjson = request.json_body

    issuccess = False
    outdata = []

    if "cmd" in myjson and isinstance(myjson["cmd"],numbers.Number):
        # Get Collections
        if myjson["cmd"] == 1:
            dbname = myjson['db']

            db = MongoDatabaseExtend(dbname)
            outdata = db.collection_names()
            issuccess = True
        # Get count of a collection
        elif myjson["cmd"] == 2 and "args" in myjson:
            dbname = myjson['db']
            coll = myjson["args"][0]

            db = MongoDatabase(dbname)
            outdata = db[coll].count()
            issuccess = True
        # Get all collections and their counts
        elif myjson["cmd"] == 3:
            dbname = myjson['db']

            db = MongoDatabaseExtend(dbname)
            collnames = db.collection_names()
            outdata.append(["Collection","Count"])
            for coll in collnames:
                outdata.append([coll,db[coll].count()])
            issuccess = True

    result = {"success":issuccess, "data":outdata};
    return result

'''Class to extend the MongoDatabase functionality, to get the collection names of the database
'''
class MongoDatabaseExtend(MongoDatabase):
    def __init__(self, db_name):
        super(MongoDatabaseExtend,self).__init__(db_name)

    def collection_names(self):
        return self._db.collection_names()
