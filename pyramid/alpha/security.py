'''
Written by DWS.
'''
USERS = {'sabin-david':'sabin',
         'binns-steven':'binns'}
GROUPS = {'sabin-david': ['group:editors'],
          'binns-steven': ['group:editors']}

def groupfinder(userid, request):
    if userid in USERS:
        return GROUPS.get(userid, [])