'''
Written by DWS.
'''
from pyramid.view import view_config
from etl.general.mongo_database import MongoDatabase
from etl.general.rulemanager import RuleManager
from pymongo import MongoClient
import json
import csv
import sys
import StringIO
import copy

@view_config(route_name='home_config', renderer="json")
def config(request):
    #Defer all output so nothing prints to console to crash the Windows Service
    f = StringIO.StringIO()
    sys.stderr = f
    sys.stdout = f
    # Perform Mongo transation

    myparams = request.params
    if "db" in myparams:
        myjson = myparams
    else:
        #load input as json
        myjson = request.json_body

    if 'debug' in myjson:
        debug = myjson['debug']
    else:
        debug = False


    dbname = myjson['db']
    db = MongoDatabase(dbname)
    
    # For each of the collections
    coll = db["config"]
    
    
    outData = {}
    issuccess = 'false'
    
    # Query to find all records of a particular rule
    actualData = coll.find(no_cursor_timeout=True)
    issuccess = 'true'
    
    for line in actualData:
        # Skip if id is set and this not the id we are looking for
        if "id" in myjson:
            if myjson["id"] != line["_id"]:
                continue
        outData[line["_id"]] = line["data"]
        if debug:
            print "ID=" + line["_id"]

           
    result = {"success":issuccess, "data":outData};
    return result


@view_config(route_name='home_rulehistory', renderer="json")
def rulehistory(request):
    # Perform Mongo transation

    myparams = request.params
    if "db" in myparams:
        myjson = myparams
    else:
        #load input as json
        myjson = request.json_body

    if 'debug' in myjson:
        debug = myjson['debug']
    else:
        debug = False


    dbname = myjson['db']
    db = MongoDatabase(dbname)
    

    # For each of the collections
    coll = db["ruleactive"]
    ruleactiveLookup = {}
    # Query to find all records of a particular rule
    actualDataActive = coll.find(no_cursor_timeout=True)
    for line in actualDataActive:
        ruleactiveLookup[line['_id']] = line['active']

    # For each of the collections
    coll = db["rulehistory"]
    
    
    outData = []
    issuccess = 'false'
    
    # Query to find all records of a particular rule
    actualData = coll.find(no_cursor_timeout=True)
    issuccess = 'true'
    headerdata = []
    for line in actualData:
        if len(headerdata) == 0:
            headerdata = sorted(line.keys())
            headerdata.insert(0,"Active")
            outData.append(headerdata)
        rowdata = []
        for col in headerdata:
            if col == "Active":
                if line['_id'] not in ruleactiveLookup:
                    rowdata.append(False) #default to false if not exist in lookup
                else:
                    rowdata.append(ruleactiveLookup[line['_id']])
            else:
                if col not in line:
                    rowdata.append(0)
                else:
                    rowdata.append(line[col])
        outData.append(rowdata)

           
    result = {"success":issuccess, "data":outData};
    return result


@view_config(route_name='home_rulecount', renderer="json")
def rulecount(request):
    # Perform Mongo transation

    myparams = request.params
    if "db" in myparams:
        myjson = myparams
    else:
        #load input as json
        myjson = request.json_body

    if 'debug' in myjson:
        debug = myjson['debug']
    else:
        debug = False


    dbname = myjson['db']
    db = MongoDatabase(dbname)
    
    # For each of the collections
    coll = db["rulecounts"]
    
    
    outData = []
    issuccess = 'false'
    
    # Query to find all records of a particular rule
    actualData = coll.find(no_cursor_timeout=True)
    issuccess = 'true'
    headerdata = []
    for line in actualData:
        if len(headerdata) == 0:
            headerdata = sorted(line.keys())
            outData.append(headerdata)
        rowdata = []
        for col in headerdata:
            if col not in line:
                rowdata.append(0)
            else:
                rowdata.append(line[col])
        outData.append(rowdata)

           
    result = {"success":issuccess, "data":outData};
    return result

@view_config(route_name='home_tagcount', renderer="json")
def tagcount(request):
    # Perform Mongo transation

    myparams = request.params
    if "db" in myparams:
        myjson = myparams
    else:
        #load input as json
        myjson = request.json_body

    if 'debug' in myjson:
        debug = myjson['debug']
    else:
        debug = False


    dbname = myjson['db']
    db = MongoDatabase(dbname)
    
    # For each of the collections
    coll = db["tagcounts"]
    
    
    outData = []
    issuccess = 'false'
    
    # Query to find all records of a particular rule
    actualData = coll.find(no_cursor_timeout=True)
    issuccess = 'true'
    headerdata = []
    for line in actualData:
        if len(headerdata) == 0:
            headerdata = sorted(line.keys())
            outData.append(headerdata)
        rowdata = []
        for col in headerdata:
            if col not in line:
                rowdata.append(0)
            else:
                rowdata.append(line[col])
        outData.append(rowdata)

           
    result = {"success":issuccess, "data":outData};
    return result


@view_config(route_name='home_tagdailycount', renderer="json")
def tagdailycount(request):
    # Perform Mongo transation

    myparams = request.params
    if "db" in myparams:
        myjson = myparams
    else:
        #load input as json
        myjson = request.json_body

    if 'debug' in myjson:
        debug = myjson['debug']
    else:
        debug = False


    dbname = myjson['db']
    db = MongoDatabase(dbname)
    
    # For each of the collections
    coll = db["tagcountsdaily"]
    
    
    outData = []
    issuccess = 'false'
    
    # Query to find all records of a particular rule
    actualData = coll.find(no_cursor_timeout=True)
    issuccess = 'true'
    headerdata = set()
    savedata = []
    tagset = {}
    #Figure out entire date range (i.e. headerdata)
    idx = 0
    for line in actualData:
        savedata.append(line)
        tagset[line['_id']] = idx
        headerdata = headerdata.union(set(line.keys()))
        idx += 1

    headerdata = sorted(list(headerdata))
    outData.append(headerdata)

    daterows = copy.deepcopy(headerdata)
    daterows.remove('_id')

    ordertagset = sorted(tagset.keys())
    downloaddata = []
    downloadheader = copy.deepcopy(ordertagset)
    downloadheader.insert(0,'Date')
    downloaddata.append(downloadheader)
    for col in daterows:
        rowdata = []
        rowdata.append(col)
        for tag in ordertagset:
            idx = tagset[tag]
            line = savedata[idx]

            if col not in line:
                rowdata.append(0)
            else:
                rowdata.append(line[col])
        downloaddata.append(rowdata)



    for line in savedata:
        rowdata = []
        for col in headerdata:
            if col not in line:
                rowdata.append(0)
            else:
                rowdata.append(line[col])
        outData.append(rowdata)

           
    result = {"success":issuccess, "data":{'data':outData,"download":downloaddata}};
    return result

@view_config(
    route_name='home_addruletag', 
    permission='edit',
    renderer="json"
    )
def addruletag(request):
    # Perform Mongo transation
    myjson = request.json_body

    dbname = myjson['db']

    r = RuleManager(dbname)
    new_rules = [{'value': myjson['rule'], 'tag': myjson['tag']}]
    r.update_rules_from_list(new_rules)
    db = MongoDatabase(dbname)
    #db.refresh()
    newruleid = db.rules[myjson['rule']]
    return {'status':'success','newid':newruleid}

if __name__ == '__main__':
    class mydict(dict):
        pass
    request = mydict()
    request.json_body = {"db":"twitter","debug":True}
    
    result = config(request)
    
    #print 'data: ' + json.dumps(result['data'])