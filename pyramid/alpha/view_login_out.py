'''
Written by DWS.
'''
from pyramid.view import (
    view_config,
    forbidden_view_config,
    )

from pyramid.security import (
    remember,
    forget,
    )
from pyramid.httpexceptions import HTTPFound
#from pyramid_google_login import redirect_to_signin
from .security import USERS

@view_config(route_name='login',
             renderer='basic_pages/login.pt')
@forbidden_view_config(renderer='basic_pages/login.pt')
def login(request):
    login_url = request.resource_url(request.context, 'login')
    referrer = request.url
    if referrer == login_url:
        referrer = '/' # never use the login form itself as came_from
    came_from = request.params.get('came_from', referrer)
    message = ''
    login = ''
    password = ''
    if 'form.submitted' in request.params:
        login = request.params['login']
        password = request.params['password']
        if USERS.get(login) == password:
            headers = remember(request, login)
            return HTTPFound(location = came_from,
                             headers = headers)
        message = 'Failed login'
 
    return dict(
        message = message,
        url = request.application_url + '/login',
        came_from = came_from,
        login = login,
        password = password,
        )
 
@view_config(route_name='logout')
def logout(request):
    headers = forget(request)
    return HTTPFound(location = request.resource_url(request.context),
                     headers = headers)
#@forbidden_view_config()
#def unauthenticated(context, request):
#    return redirect_to_signin(request, url=request.path_qs)