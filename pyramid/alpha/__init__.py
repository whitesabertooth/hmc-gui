from pyramid.config import Configurator
from webassets import Bundle

from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
#from pyramid_google_login.events import UserLoggedIn

from .security import groupfinder
from pyramid.security import (
    Allow,
    Everyone,
    )
'''
Written by DWS.
'''

#Used for authentication setup
class Root(object):
    __name__ = None
    __parent__ = None
    __acl__ = [ (Allow, Everyone, 'view'),
                (Allow, 'group:editors', 'edit') ]

    def __init__(self, request):
        self.request = request

def createBundle(config,bname, bundle):
    config.add_webasset(bname, bundle)
    assets_env = config.get_webassets_env()
    assets_env[bname].urls() #Create the packages


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    #Setup Authentication
    authn_policy = AuthTktAuthenticationPolicy(
        settings['auth.secret'],
        callback=groupfinder,
    )
    authz_policy = ACLAuthorizationPolicy()


    config = Configurator(settings=settings,
        authentication_policy=authn_policy,
        authorization_policy=authz_policy,
        root_factory=Root)

    #config = Configurator(settings=settings)

    config.include('pyramid_chameleon')
    #config.include('pyramid_google_login')


    #def subscriber(event):
    #    #event.userid        
    #    pass

    #config.add_subscriber(subscriber, UserLoggedIn)

    nofilter = None #Combines but no compression
    if settings['webassets.nofilter'] == 'True':
        print "Using Debug Javascript"
        jsminfilter = nofilter #Used for debug, it only combines, it does not compress
    else:
        print "Using Production Javascript"
        jsminfilter = 'jsmin'#Production, it combines and compresses
    alljs = ['bigfileupload','code_manage','estimate','query','table',
             'rule','common','dbmanage','dataset','classifier','visualize','devoops','count','topic','rehydrate','region']
    for js in alljs:
        jst = Bundle('../../basic_pages/js/'+js+'/*.js', #This path is relative to the output directory of alpha/js
            filters=jsminfilter,
            output=js+'.js')
        createBundle(config,js,jst)

    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view('img', 'substatic/img', cache_max_age=3600)
    config.add_static_view('plugins', 'substatic/plugins', cache_max_age=3600)
    config.add_static_view('fonts', 'substatic/fonts', cache_max_age=3600)
    config.add_static_view('css', 'basic_pages/css', cache_max_age=3600)
    config.add_static_view('input', 'basic_pages/input') # This is temporary for Topic Model example
    config.add_route('home_db', '/db/db.json')
    config.add_route('home_config', '/db/config.json')
    config.add_route('home_rulehistory', '/db/rulehistory.json')
    config.add_route('home_rulecount', '/db/rulecount.json')
    config.add_route('home_tagcount', '/db/tagcount.json')
    config.add_route('home_tagdailycount', '/db/tagdailycount.json')
    config.add_route('home_addruletag', '/db/addruletag.json')
    config.add_route('home_dataset', '/db/dataset.json')
    config.add_route('checkJobs', '/db/getEstimates.json')
    config.add_route('submitJob', '/db/submitEstimate.json')
    config.add_route('submitAccept', '/db/submitAccept.json')
    config.add_route('downloadJob', '/db/downloadEstimate.json')
    config.add_route('savefile', '/db/savefile.json')
    config.add_route('getfile', '/db/getfile')
    config.add_route('submitLongJob', '/db/submitLongJob.json')
    config.add_route('cancelLongJob', '/db/cancelLongJob.json')
    config.add_route('getLongJob', '/db/getLongJob.json')
    config.add_route('home_mongo_cmd', '/db/mongo.json')
    config.add_route('classify', '/db/classifier.json')
    config.add_route('getheader', '/db/getHeader.json')
    config.add_route('getclassresult', '/db/getClassResult.json')
    config.add_route('predictor', '/db/predictor.json')
    config.add_route('quickpredict', '/db/quickpredict.json')
    config.add_route('basic_pages_index', '/')
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')
    config.add_route('submit_error', '/db/submit_error.json')
    config.add_static_view('ajax', 'basic_pages/ajax', cache_max_age=3600)
    config.add_route('basic_pages_watch_demo', '/watch_coding_entry_demo.html')
    config.add_route('basic_pages_coder', '/master_coding_instructions.html')
    config.add_route('topicmodel','/db/topicmodel.json')
    config.scan()
    return config.make_wsgi_app()
