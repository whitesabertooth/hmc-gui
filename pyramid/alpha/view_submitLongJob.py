'''
Written by DWS.
'''
from pyramid.view import view_config
from shared.longjob import SampleDownloadJob
from shared.longjob import TopicModelJob
from shared.longjob import DatasetSampleDownloadJob
from shared.longjob import ReHydrateJob
from shared.machine_class import PredictClassJob
import numbers
import os
import sys

DEFAULT_UNAME = 'dsabin2'

@view_config(route_name='submitLongJob', request_method='POST', accept="application/json", renderer='json')
def submitLongJob(request):
	myjson = request.json_body
	# Set the username
	if request.unauthenticated_userid:
		uname = request.unauthenticated_userid
	else:
		#Default anonymous username
		uname = DEFAULT_UNAME
	myjson['username'] = uname

	result = False

	#Check for development.ini
	isdebug=False
	if 'isdebug' in request.registry.settings:
		if request.registry.settings['isdebug'] == 'true':
			isdebug = True

	if 'job' in myjson:
		jobtype = myjson['job']

		#Verify jobtype is a number
		if isinstance(jobtype,numbers.Number):
			if jobtype == 1:
				#Sample Job
				#print "Processing job 1"
				newjob = SampleDownloadJob(True,development=isdebug)#True argument means print
				result = newjob.SubmitSample(myjson)

			elif jobtype == 2:
				#Topic Model
				newjob = TopicModelJob(True)
				result = newjob.SubmitTopic(myjson)
			elif jobtype == 3:
				#Dataset sample
				newjob = DatasetSampleDownloadJob(True,development=isdebug)
				result = newjob.SubmitSample(myjson)
			elif jobtype == 4:
				#Predictor Classification
				newjob = PredictClassJob(True,development=isdebug)
				result = newjob.SubmitPred(myjson)
			elif jobtype == 5:
				#ReHydrate
				newjob = ReHydrateJob(True)
				result = newjob.SubmitJob(myjson)

	response = {'result':result}
	sys.stdout.flush()
	return response

@view_config(route_name='getLongJob', request_method='POST', accept="application/json", renderer='json')
def getLongJob(request):
	myjson = request.json_body
	# Set the username
	if request.unauthenticated_userid:
		uname = request.unauthenticated_userid
	else:
		#Default anonymous username
		uname = DEFAULT_UNAME

	myjson['username'] = uname
	result = False

	#Check for development.ini
	isdebug=False
	if 'isdebug' in request.registry.settings:
		if request.registry.settings['isdebug'] == 'true':
			isdebug = True

	if 'job' in myjson:
		jobtype = myjson['job']

		#Verify jobtype is a number
		if isinstance(jobtype,numbers.Number):
			if jobtype == 1:
				#Sample Job
				newjob = SampleDownloadJob(True,development=isdebug)#True argument means print
				result = newjob.GetSamples(myjson)

			elif jobtype == 2:
				#Topic Model Job
				newjob = TopicModelJob(True)#True argument means print
				result = newjob.GetTopics(myjson)
			if jobtype == 3:
				#Dataset Sample Job
				newjob = DatasetSampleDownloadJob(True,development=isdebug)#True argument means print
				result = newjob.GetSamples(myjson)
			elif jobtype == 4:
				#Predictor Classification
				newjob = PredictClassJob(True,development=isdebug)
				result = newjob.GetPred(myjson)
			elif jobtype == 5:
				#ReHydrate
				newjob = ReHydrateJob(True)
				result = newjob.GetJobs(myjson)

	response = {'result':result}
	return response


@view_config(route_name='cancelLongJob', request_method='POST', accept="application/json", renderer='json')
def cancelLongJob(request):
	myjson = request.json_body
	# Set the username
	if request.unauthenticated_userid:
		uname = request.unauthenticated_userid
	else:
		#Default anonymous username
		uname = DEFAULT_UNAME

	myjson['username'] = uname
	result = False

	#Check for development.ini
	isdebug=False
	if 'isdebug' in request.registry.settings:
		if request.registry.settings['isdebug'] == 'true':
			isdebug = True

	if 'job' in myjson:
		jobtype = myjson['job']

		#Verify jobtype is a number
		#TODO: Add check if user owns the job
		if isinstance(jobtype,numbers.Number) and 'jobid' in myjson and isinstance(myjson['jobid'],numbers.Number):
			if jobtype == 1:
				#Sample Job
				newjob = SampleDownloadJob(True,development=isdebug)#True argument means print
				result = newjob.Cancel(myjson['jobid'])
			elif jobtype == 2:
				#Topic Model Job
				newjob = TopicModelJob(True)#True argument means print
				result = newjob.GetTopics(myjson['jobid'])
			if jobtype == 3:
				#Dataset Sample Job
				newjob = DatasetSampleDownloadJob(True,development=isdebug)#True argument means print
				result = newjob.Cancel(myjson['jobid'])
			elif jobtype == 4:
				#Predictor Classification
				newjob = PredictClassJob(True,development=isdebug)
				result = newjob.Cancel(myjson['jobid'])
			elif jobtype == 5:
				#ReHydrate
				newjob = ReHydrateJob(True)
				result = newjob.Cancel(myjson['jobid'])

	response = {'result':result}
	return response
