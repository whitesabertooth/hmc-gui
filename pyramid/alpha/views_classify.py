# -*- coding: utf-8 -*-
'''
Written by DWS.
'''
from pyramid.view import view_config
from shared.machine_class import create_classifier
from shared.machine_class import predictor
from shared.machine_class import predictSingle
from shared.machine_class import get_config_results_zip
from shared.savefile import SaveFile
import os
import pandas as pd
import json

@view_config(route_name='classify', renderer="json")
def classifyJob(request):
    '''Receive a request to classify a file already uploaded.
    Return a results and new file created id with pkl package and configuration and results and description (+date).
    Input:
        1. A file (*.xlsx) ID
        2. column identification of: text body, label
        //3. Header line included: yes/no
    Input Configuration:
        1. Text processing (optional)
        2. Feature extraction (require)
        3. Feature weighting (optional)
        4. Feature selection (optional)
        5. Classifier Model (required)
        6. Description
        7. Instructions on what it applies too
        8. classifier name
        9. Cross-validation Style (optional) [Default = 5-fold]
    Input Test Configuration:
        1. Specifc % test / % training - Selected using uniform random
        2. Statificated sampling for training: Specific % test, and single (or multi) statification variable (e.g. column)
    Output pkl package:
        1. *.pkl file
        2. *.pkl_npy# files
    Output Results:
        1. Cross-validation
            a. Average CV score
            b. CV Standard Dev
        2. Classification Report text


    Input format:
        {
            'config': {
                'file': {
                    'fileid'
                    'columntext'
                    'columnlabel'
                    'prevfileid'
                    },
                'classifier': {
                    # http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html
                    'CountVectorizer': {  Convert a collection of text documents to a matrix of token counts.
                        'analyzer' = Whether the feature should be made of word or character n-grams. Option 'char_wb' creates character n-grams only from text inside word boundaries.
                            'word' or 'char' or 'char_wb'
                        'ngram_range':[min_n,max_n] or null = The lower and upper boundary of the range of n-values for different n-grams to be extracted. All values of n such that min_n <= n <= max_n will be used.
                        'stop_words' = If 'english', a built-in stop word list for English is used.  If a list, that list is assumed to contain stop words, all of which will be removed from the resulting tokens.  If None, no stop words will be used.
                            'english' or array or null; default is null
                        'lowercase':true or false = Convert all characters to lowercase before tokenizing.
                        'token_pattern':regexp string or null = Regular expression denoting what constitutes a “token”.
                        'max_df': float between 0.0 and 1.0, or int; default 1.0 = When building the vocabulary ignore terms that have a document frequency strictly higher than the given threshold. If float, the parameter represents a proportion of documents, integer absolute counts.
                        'min_df': float between 0.0 and 1.0, or int; default 1 = When building the vocabulary ignore terms that have a document frequency strictly lower than the given threshold. If float, the parameter represents a proportion of documents, integer absolute counts.
                        'max_features: int or null; default null = If not None, build a vocabulary that only consider the top max_features ordered by term frequency across the corpus.
                    },
                    # http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfTransformer.html
                    'TfidfTransformer': { Transform a count matrix to a normalized term-frequency (tf) or term-frequency times inverse document-frequency (tf-idf) representation.
                        'norm': 'l1' or 'l2' or null = Norm used to normalize term vectors. None for no normalization.
                        'use_idf': true or false; default true = Enable inverse-document-frequency reweighting.
                        'smooth_idf': true or false; default true = Smooth idf weights by adding one to document frequencies, as if an extra document was seen containing every term in the collection exactly once. Prevents zero divisions.
                        'sublinear_tf': true or false; default false = Apply sublinear tf scaling, i.e. replace tf with 1 + log(tf).
                    },
                    # http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.SGDClassifier.html
                    'SGDClassifier': { Linear classifiers (SVM, logistic regression, a.o.) with stochastic gradient descent (SGD) training.
                        'loss': str, ‘hinge’, ‘log’, ‘modified_huber’, ‘squared_hinge’, ‘perceptron’, or a regression loss: ‘squared_loss’, ‘huber’, ‘epsilon_insensitive’, or ‘squared_epsilon_insensitive’; default hinge = The loss function to be used.
                        'penalty': str, ‘none’, ‘l2’, ‘l1’, or ‘elasticnet’; default l2 = The penalty (aka regularization term) to be used.
                        'alpha': float; default 0.0001 = Constant that multiplies the regularization term.
                        'l1_ratio': float; default 0.15 = The Elastic Net mixing parameter, with 0 <= l1_ratio <= 1. l1_ratio=0 corresponds to L2 penalty, l1_ratio=1 to L1.
                        'fit_intercept': bool; default true = Whether the intercept should be estimated or not. If False, the data is assumed to be already centered.
                        'n_iter': int; default 5 = The number of passes over the training data (aka epochs). The number of iterations is set to 1 if using partial_fit.
                        'shuffle': bool; default true = Whether or not the training data should be shuffled after each epoch.
                        'random_state': int or null; default null = The seed of the pseudo random number generator to use when shuffling the data.
                        ??'epsilon': float; = Epsilon in the epsilon-insensitive loss functions; only if loss is ‘huber’, ‘epsilon_insensitive’, or ‘squared_epsilon_insensitive’.
                        #'learning_rate' :
                        'eta0': double; default 0.0 = The initial learning rate for the ‘constant’ or ‘invscaling’ schedules.
                        'power_t': double; default 0.5 = The exponent for inverse scaling learning rate.
                        'class_weight': "balanced" or null; default null = Weights associated with classes. If not given, all classes are supposed to have weight one.
                        'average': bool or int or null; default null = When set to True, computes the averaged SGD weights and stores the result in the coef_ attribute. If set to an int greater than 1, averaging will begin once the total number of samples seen reaches average.
                    }
                    'CrossValidation': {
                        'cv': int; default 3 = Determines the cross-validation splitting strategy.  Integer, to specify the number of folds.
                        'scoring': accuracy, average_percision, f1, [micro,macro,weighted,samples], log_loss, precision..., recall..., roc_auc,  default F1
                        'skiplarge': true false, default true; if > 100,000 lines then will skip CV because takes too long
                    }
                }
                'info': {
                    'description'
                    'name'
                    'instructions'
                }
                'test': {
                    'type': 'specific' or 'stratified'
                    'testpercent'
                    'stratification'
                }
            }
        }

    Output/Result format:
        {
            'result': {
                'cv': { //Normal Data
                    'avg'
                    'std'
                    'all'
                },
                'cm': { //Normal Data
                    'labels'
                    'matrix'
                },
                'learn': { //Learning Curve data
                    'train_sizes'
                    'train_scores'
                    'valid_scores'
                }
                'report' //Normal Data
                'outfileid' //Normal Data
                'config'
            }
            'status'
            'msg' : if status = 'error'
        }
    Save everything (including input) in .zip:
        1. Input file
            *.xlsx
        2. Config file (.json of all information, including column identification and test configuration, and input filename)
            config.json
        3. Test result file (.json of CVs and Report)
            result.json
        4. pkl files
            *.pkl
            *.pkl_npy*
        5. Prediction vs Labeled Test Result .csv file
    '''
    myjson = request.json_body

    return create_classifier(myjson)

@view_config(route_name='quickpredict', renderer="json")
def quickpredict(request):
    '''
    Quickly predict without going through Jenkins
        input:{
                'fileid': new file to predict
                'columntext' : title of the text column
                'columnlabel' : new title of label column
                'prevfileid' : last classifier .zip
            }
    '''
    myjson = request.json_body
    outdata = predictor(myjson)
    print json.dumps(outdata,indent=2)
    return outdata


@view_config(route_name='predictor', renderer="json")
def predictorJob(request):
    '''Receive a request to predict from a classifier already created.
    Return a status and new file created id.

    input = {
        'config': {
            'file': {
                'fileid': new file to predict
                'columntext' : title of the text column
                'columnlabel' : new title of label column
                'prevfileid' : last classifier .zip
            },
            'info': {
                'type': str, file,text (if file then processes a file, if text then processes just a few texts)
            },
            'text': []  array of text to classify
        }
    }

    output = {
        'result': {
            'config'
            'outfileid'
            'classtext': []  array of the classification (this matches the config.text order)
        },
        'status': success
        'msg': is status is error
    }
    '''
    myjson = request.json_body
    outdata = predictSingle(myjson)
    print json.dumps(outdata,indent=2)
    return outdata




@view_config(route_name='getheader', renderer='json')
def getHeader(request):
    '''Given a fileid, check if .csv or .xlsx, load into Pandas and then get the header information and return.
    Input:
        {
            'fileid'
        }
    Output:
        {
            'status'
            'msg' : if status = 'error'
            'header': ['',...]
        }
    '''
    myjson = request.json_body


    myfile = SaveFile(fileid=myjson['fileid'])
    fileloc = myfile.getFilePath()
    origname = myfile.getOriginalName()
    ex = os.path.splitext(origname)
    if ex[1] == '.xlsx':
        df = pd.read_excel(fileloc,encoding='utf-8')
    elif ex[1] == '.csv':
        df = pd.read_csv(fileloc,encoding='utf-8')
    else:
        return {'status':'error','msg':"File is not a known type for figuring out header."}

    return {'status':'success','header':list(df)}

@view_config(route_name='getclassresult', renderer='json')
def getClassResult(request):
    '''Get config and results from a previous classified .zip file.
    Input:
        {
            'fileid'
        }
    Output:
        {
            'status'
            'msg' : if status = 'error'
            'result': {
                'config': 
                'result': 
            }
        }
    '''
    myjson = request.json_body


    myfile = SaveFile(fileid=myjson['fileid'])
    fileloc = myfile.getFilePath()
    result = get_config_results_zip(fileloc)
    if result['config'] == None or result['result'] == None:
        return {'status':'error','msg':'Zip file does not have a config.json or result.json.'}
    result['result']['outfileid'] = myjson['fileid'] #Overwrite to make sure it matches

    return {'status':'success','result':result}


if __name__ == '__main__':
    pass