'''
Written by DWS.
'''
from pyramid.view import view_config
from shared.savefile import SaveFile
import os

@view_config(route_name='savefile', request_method='POST', xhr=True, accept="application/json", renderer='json')
def savefile(request):
    results = []
    for name, fieldStorage in request.POST.items():
        # This will always be first
    	if name == 'localid':
            uniqueid = fieldStorage
            continue
        if isinstance(fieldStorage,unicode):
            continue


        result = {}
        result['name'] = os.path.basename(fieldStorage.filename)
        result['type'] = fieldStorage.type

        # Need to send filename because it is being chunked
        # (as in calling this routine mutliple times for one file upload and 
        # all the information needs to be appended together)
        myfile = SaveFile(filetype=fieldStorage.type,origname=fieldStorage.filename,uniqueid=uniqueid)
        result['fileid'] = myfile.getFileID()

        myfile.writeToFile(fieldStorage.file)

        results.append(result)

    return {"files":results}

@view_config(route_name='getfile')
def loadfile(request):

    # Either load the myparams or the json post
    myparams = request.params
    if "fileid" in myparams:
        myjson = myparams
    else:
        #load input as json
        myjson = request.json_body

    if "fileid" not in myjson:
        raise NotFound

    fileid = myjson['fileid']
    #Only convert from String to int if it is int format, else leave as string for perminent file
    if fileid.isdigit():
        fileid = int(fileid)

    try:
        myfile = SaveFile(fileid=fileid)
        request.response.content_type = myfile.getContentType() #"application/force-download"
        filename = myfile.getOriginalName()
        filesize = myfile.getFileSize()
        request.response.content_disposition = 'attachment; filename="' + filename + '"'
        request.response.body_file = myfile.openFileHandle()
        request.response.content_length = filesize # must be after body_file set (for some reason)
    except IOError:
        # If file does not exist
        raise NotFound
    return request.response