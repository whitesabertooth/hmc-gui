from pyramid.view import view_config
from shared.sampleJob import sampleFirstJob
from shared.find_rule_counts import FindRuleCounts
'''
Written by DWS.
'''


@view_config(route_name='home_db', renderer="json")
def mongodb(request):

    #load input as json
    myjson = request.json_body
    # Request data 
    #request = {
    #   "query":tempquery,
    #   "collection":collectionRanges[collectionGroup]['range'][collectionInc],
    #   "increment":increment,
    #   "step":step,
    #   "sample":needSampleSize,      is a number needed, or a percent needed
    #   "sampletype":sampleType,      "percent" or "number"
    #   "samplemethod":sampleMethod,  "first" or "random"
    #   "fileid":window.fileid,
    #   "totalsize":0,
    #   "savedata":saveQuery,         True = save to csv, False = do not save
    #   "createSample":!delaySample,  True = return a sample, if performquery = True then perform just on query
    #   "performquery":true           True = perform query
    #   "data":false                  True if getting data, false if just count
    #};
    return sampleFirstJob(myjson)


@view_config(route_name='home_dataset', renderer="json")
def dataset(request):

    #load input as json
    myjsonAll = request.json_body
    # Request data = {allQuery:{'datestart','dateend','query'},db,sampletype}
    ff = FindRuleCounts(myjsonAll["db"])
    return ff.findAll(groups=[myjsonAll["sampletype"] ],allQuery=myjsonAll['allquery'],isreturn=True)

