from datetime import datetime
from datetime import timedelta
from datetime import date
import re
import json
import pprint
import os
import pandas as pd
import csv
import unicodedata
import random
from savefile import SaveFile
from etl.general.mongo_database import MongoDatabase
import calendar
import sys
import twitter_lib
'''
Written by DWS.
'''

def sampleFirstJob(myjson):
    # This is also given a coll in myjson, and no dates given as separate variables, it is all internal to query
    # Perform Mongo transation
    #Allow for an already open db
    if 'opendb' in myjson:
        db = myjson['opendb']
    else:
        db = MongoDatabase(myjson['db'])
    yearcol = int(myjson['collection'][:4])
    monthcol = int(myjson['collection'][4:])

    #Convert times to python times
    dateField = 'postedTime'
    queryjson = myjson['query']
    
    #Skip over convert date if already in correct format    
    if 'notconvertdate' not in myjson and dateField in queryjson:
        if '$gte' in queryjson[dateField].keys():
            sdate = convertJavascriptDate(queryjson[dateField]['$gte'])
            sdate = datetime(sdate.year,sdate.month,sdate.day,0,0,0)
            if sdate.year == yearcol and sdate.month == monthcol and sdate.day == 1:
                del queryjson[dateField]['$gte']
                #Ignore start of day
            else:
                queryjson[dateField]['$gte'] = sdate
        if '$lte' in queryjson[dateField].keys():
            edate = convertJavascriptDate(queryjson[dateField]['$lte'])
            edate = datetime(edate.year,edate.month,edate.day,0,0,0)
            edate += timedelta(days=1) #add an extra day to be the beginning of the next day
            testdate = datetime(yearcol,monthcol,1,0,0,0)
            testdate += timedelta(days=32) #Add a month by adding more than 31 days
            if edate.year == testdate.year and edate.month == testdate.month and edate.day == 1:
                del queryjson[dateField]['$lte']
                #Ignore end of day
            else:
                queryjson[dateField]['$lte'] = edate
        if len(queryjson[dateField].keys()) == 0:
            del queryjson[dateField]


    coll = db[myjson['collection']]
    if myjson['createSample']:
        #Get sample data first style for the web
        return saveOrReturnQuery(db,coll,True,queryjson,True,myjson['sample'],False,None)
    else:
        #Just get the count
        if(len(queryjson.keys()) > 0):
            totalcount = coll.count(queryjson)
            #totalcount = coll.find(queryjson,no_cursor_timeout=True).skip(myjson['increment']*myjson['step']).limit(myjson['increment']).count(with_limit_and_skip=True)
        else:
            #print "Empty query"
            totalcount = coll.count()
            #totalcount = coll.find(no_cursor_timeout=True).skip(myjson['increment']*myjson['step']).limit(myjson['increment']).count(with_limit_and_skip=True)
        return {"issuccess":"success", "totalcount" : totalcount}

def downloadOrSampleJob(myjson):
    # Expects a getSample = True or False to determine to get sample or not
    # Perform Mongo transation
    db = MongoDatabase(myjson['db']) #TODO: Improve Sampling to not open a database connection every collection

    #Setup sample sub query to find count
    submyjson = {}
    submyjson['db'] = myjson['db']
    submyjson['createSample'] = False
    submyjson['increment'] = 100000000  #Currently not used
    submyjson['sample'] = 0
    submyjson['samplemethod'] = 'first'
    submyjson['sampletype'] = 'number'
    submyjson['notconvertdate'] = True #This just needs to be set, doesn't matter if true or false, if it exists in the dictionary it will be true
    submyjson['opendb'] = db #Give the open Mongo connection
    submyjson['step'] = 0 #Currently not used

    # myjson = {
    #   "query":tempquery,
    #   "sample":needSampleSize,      is a number needed, or a percent needed
    #   "sampletype":sampleType,      "percent" or "number"
    #   "samplemethod":sampleMethod,  "first" or "random"
    #   "fileid":window.fileid,
    #   "savedata":saveQuery,         True = save to csv, False = do not save
    #   "createSample":!delaySample,  True = return a sample, if performquery = True then perform just on query
    #   "performquery":true           True = perform query
    #   "data":false                  True if getting data, false if just count
    #};

    #Convert times to python times
    dateField = 'postedTime'
    sdate = convertJavascriptDate(myjson['startdate'])
    sdate = datetime(sdate.year,sdate.month,sdate.day,0,0,0)
    edate = convertJavascriptDate(myjson['enddate'])
    edate = datetime(edate.year,edate.month,edate.day,0,0,0)
    edate += timedelta(days=1) #add an extra day to be the beginning of the next day

    # Download the data by collection:
    # Determine the collections needed to be looked at based on startdate and enddate
    totalcount = 0
    issuccess = 'success'
    errorstr = ""

    if myjson['getSample'] == 'true':
        #Make variables for sample only if doing sample because myjson will not have them
        sampletype = myjson['sampletype']
        samplemethod = myjson['samplemethod']
        samplesize = myjson['sample']
        #Get sample only by first counting everything, then go through and download only the sample
        totalpasses = 2
    else: #Not a sample, just download
        totalpasses = 1

    numpasses = 0
    prevCount = 0
    finalsamplesize = 0
    appendfile = None
    lasttime = False 
    nextZipNumber = 0
    #Loop through the number of passes needed; also end if error
    while numpasses < totalpasses and issuccess == 'success':
        numpasses+=1 #Go to next pass
        print "Currently on Pass #" + str(numpasses)
        curdate = datetime(sdate.year,sdate.month,1,0,0,0) # Always set to first day of the month

        #set Sample set after getting totalcount and only once at beginning of 2nd pass
        if numpasses == 2 and myjson['getSample'] == 'true':
            # Calculate percent if needed
            if sampletype == 'percent':
                samplesize = int(samplesize/100.0 * totalcount) #round down

            # Cap the sampling at the total size
            if totalcount < samplesize:
                samplesize = totalcount

            if samplemethod == 'first':
                outputselection = xrange(samplesize) #creates 0-(n-1) array
            elif samplemethod == 'random':
                #RANDOM SELECTION
                random.seed() #use time
                outputselection = random.sample(xrange(totalcount),samplesize)
            else:
                raise IOError #cause error if not method given

        while curdate < edate: # We don't want <= because the equal case will only happen when we have rolled over a month on the end date, and therefore we don't want to query that month
            #Predicte if this is the last loop:
            if addMonths(curdate,1) >= edate:
                #end of loop
                lasttime = True

            curquery = myjson['query']
            if curdate.year == sdate.year and curdate.month == sdate.month and sdate.day > 1:
                if dateField not in curquery:
                    curquery[dateField] = {}
                curquery[dateField]['$gte'] = sdate
            if curdate.year == edate.year and curdate.month == edate.month and edate.day > 1:# Since I rolled the end date by 1 day, then the end of the month will always be the first day of the next month
                if dateField not in curquery:
                    curquery[dateField] = {}
                curquery[dateField]['$lte'] = edate

            #Download the month data
            collname = curdate.strftime("%Y%m") #Handles putting 0 infront of <10 months
            print "Processing " + collname
            sys.stdout.flush() #Flush to screen
            if myjson['getSample'] == 'true':
                if numpasses == 1:
                    # First pass just collect the total
                    submyjson['collection'] = collname
                    submyjson['query'] = curquery
                    outdata = sampleFirstJob(submyjson)
                elif numpasses == 2:
                    # Second pass query for data but only save the sample
                    coll = db[collname]
                    outdata = saveOrReturnQuery(db,coll,True,curquery,False,0,True,myjson['samplefileid'],outputselection=outputselection,prevCount=prevCount,nextZipNumber=nextZipNumber,appendfile=appendfile,lasttime=lasttime)
                    nextZipNumber = outdata["nextZipNumber"] #Save to pass to next iteration
                    appendfile = outdata["appendfile"] #Save to pass to next iteration
                    prevCount += outdata['totalcount'] #Add to prevCount
                    finalsamplesize += outdata['samplesize'] #Add to sample count
            else:
                #Just Download - This only has one pass
                coll = db[collname]
                outdata = saveOrReturnQuery(db,coll,True,curquery,False,0,True,myjson['downloadfileid'],nextZipNumber=nextZipNumber,appendfile=appendfile,lasttime=lasttime)
                nextZipNumber = outdata["nextZipNumber"] #Save to pass to next iteration
                appendfile = outdata["appendfile"] #Save to pass to next iteration

            if outdata['issuccess'] != 'success':
                issuccess = 'fail'
                errorstr = outdata['error']
                print errorstr
                break

            if numpasses == 1:
                # Only compute total on first pass
                totalcount = totalcount + outdata['totalcount']

            #Increment the month to next collection
            curdate = addMonths(curdate,1)
        #-END WHILE-
    #-END WHILE-



    return {'issuccess':issuccess, 'totalcount':totalcount,'samplecount':finalsamplesize, "error": errorstr}

def convertJavascriptDate(s):
    '''Converts standard Javascript date string to Python datetime
    '''
    return datetime(*map(int, re.split('[^\d]', s)[:-1]))

def addMonths(source,months):
    '''Adds one month to the input source datetime, and returns a new datetime
    '''
    month = source.month -1 + months
    year = int(source.year + month/12)
    month = month % 12 + 1
    day = min(source.day,calendar.monthrange(year,month)[1])
    return datetime(year,month,day,source.hour,source.minute,source.second,source.microsecond)

def saveOrReturnQuery(db,coll,setupHeader,query,createFirstSample,sampleNumber,savefile,fileid,outputselection=None,prevCount=0,nextZipNumber=0,appendfile=None,lasttime=False,bygroup=None):
    '''Download the data from the Mongo database, and either save it to a file, or return a set number of them.
    If outputselection given, then it will filter the data returned.
    If this function is called multiple times and savefile is true then: make sure to give back nextZipNumber and appendfile
    '''
    # Query to find all records of a particular rule
    # No incrementing
    max_rules = 20
    max_tags = 20
    entitiesusrmentions_max = 20
    MAX_MINI_FILE_SIZE = 10000000 #10 MB
    MAX_FILE_SIZE = 300000000000 # About 300 gigs
    MAX_RETURN_SIZE = 3000000000 # About 30 gigs
    listok = ['postedTime', 'entitieshtagstext', 'entitiesusrmentions', 'matchingrulestag', 'matchingrulesvalue', 'is', 'n', 'sn']

    #Pull mongo data of Tags and Rules
    tw_mongo2fields = db.mongo_to_fields
    tw_rules = db.rules
    tw_invertedRules = db.i_rules
    tw_tags = db.tags
    tw_invertedTags = db.i_tags

    #Perform query
    if(len(query.keys()) > 0):
        actualData = coll.find(query,no_cursor_timeout=True)
    else:
        actualData = coll.find(no_cursor_timeout=True)

    #Init sample data for loop
    samplesize = 0 #The actual sample size gathered this query
    checkCount = prevCount-1 #Start one less because one will be added initially; used for Sample check

    #Init data counting for loop        
    finalData = []
    numCount = 0 #Counts only the data sampled
    numCountTotal = 0 # Counts how much data total, whether sampled or not
    issuccess = 'success'
    errorstr = ""
    outData = []
    zipappendfile = None #Always re-open zip file for saving
    needFileSetup = True

    headerlistorder = twitter_lib.formatHeader(entitiesusrmentions_max,entitiesusrmentions_max,max_rules,max_tags,db)

    #Save sample header if needed
    if createFirstSample:
        outData.append(headerlistorder)

    for aline in actualData:
        numCountTotal += 1
        #Handle sample filtering
        if outputselection != None:
            if bygroup != None:
                #The structure of outputselection looks like this:
                #outputselection[ruleNum] = {'samplecount':samplecount,'totalcount':totalcount,'outputselection':ruleoutputselection,'finalsamplesize':0,'outputRuleTotal':0,'prevCount':0}
                acceptLine = False
                if bygroup in aline and aline[bygroup] != None:
                    for ruleNum in aline[bygroup]:
                        if ruleNum in outputselection:
                            outputselection[ruleNum]['prevCount'] += 1
                            outputselection[ruleNum]['outputRuleTotal'] += 1
                            if outputselection[ruleNum]['prevCount'] in outputselection[ruleNum]['outputselection']:
                                outputselection[ruleNum]['finalsamplesize'] += 1
                                acceptLine = True
                if acceptLine:
                    samplesize += 1
                else:
                    continue #Skip to next data line
            else:
                #Normal sampling
                checkCount += 1
                if checkCount in outputselection:
                    samplesize += 1
                else:
                    continue #Skip to next data line

        #Check for max number of returns (don't allow run away scripts)
        if numCount > MAX_RETURN_SIZE:
            errorstr = "Exceeded Max Return Size = " + str(MAX_RETURN_SIZE)
            issuccess = 'fail'
            break
        numCount += 1

        # Translate field names
        formatedtweet = twitter_lib.formatTweet(aline,db)
        
        #headerlistorder = formatedtweet.keys().sort() #Should always be the same
        modifiedrow = []
        # Modify for unicode issues
        lowerkeys = {s.lower():s for s in formatedtweet.keys()}

        for v in headerlistorder:
            if v.lower() in lowerkeys:
                s = formatedtweet[lowerkeys[v.lower()]]
            else:
                s = ""

            if type(s) == unicode:
                modifiedrow.append(s.encode("ascii",'ignore'))
            else:
                modifiedrow.append(str(s))

        #Save web-return sample if needed
        if createFirstSample and numCount <= sampleNumber:
            outData.append(modifiedrow)
            if numCount+1 > sampleNumber and not savefile:
                # Reached max sample grabbing, and not saving, so break and return
                break
        
        if savefile:
            firsttime = False
            # If haven't opened the file, then open it - should be one time
            if appendfile == None:
                appendfile = SaveFile()
                csvfile = appendfile.openFileHandle('ab')
                writer = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
                firsttime = True #Mark it to save header
                needFileSetup = False #Don't get header, already did it

            if needFileSetup:
                needFileSetup = False
                csvfile = appendfile.getFileHandle() #Just get the header
                writer = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)

            #Always re-open the zip file
            if zipappendfile == None:
                zipappendfile = SaveFile(fileid=fileid)
                zipappendfile.openFileHandle('a',iszip=True) #Always append a zipfile (because file should already exist)

            # Verify file size is less than max (don't allow run-away script)
            if appendfile.getFileSize() > MAX_MINI_FILE_SIZE: 
                #Close file and add to Zip, then open new file
                appendfile.closeFileHandle()

                newname = determineInsideZipFileName(zipappendfile.getOriginalName(),nextZipNumber)

                zipappendfile.addToZip(appendfile.getFilename(),rename=newname)
                nextZipNumber += 1 #Increment for next zip file

                #Do not need file now
                appendfile.deleteAllFiles()

                appendfile = SaveFile()
                csvfile = appendfile.openFileHandle('ab')
                writer = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
                firsttime = True #Resave the header row

                #Check zip file size to see if gone over limit
                if zipappendfile.getFileSize() > MAX_FILE_SIZE:
                    issuccess = 'fail'
                    errorstr = "Filesize is larger than " + str(MAX_FILE_SIZE) + " bytes"
                    break
            
            if firsttime: #First time running the query on the temp file: so save the header
                writer.writerow(headerlistorder)
                   

            writer.writerow(modifiedrow)

    #-END OF LOOP-

    # Make sure to close the file
    if savefile:
        if lasttime:
            #Since last time, close out file and save to zip and delete it
            closeLastFile(appendfile,nextZipNumber,fileid,zipappendfile)

        elif zipappendfile != None:
            #After added new file if last time, then close zip
            zipappendfile.closeFileHandle()

    totalcount = numCountTotal
    #print(outData)
    #Make sure to pass back nextZipNumber and appendfile, since they will be given back to this function on the next call
    return {"issuccess":issuccess, "totalcount" : totalcount, "data":outData, "error": errorstr, "samplesize":samplesize, "nextZipNumber":nextZipNumber, "appendfile":appendfile, 'outputselection':outputselection}

def closeLastFile(appendfile,nextZipNumber,fileid,zipappendfile=None):
    #Check that nothing was written
    if appendfile == None:
        return

    if zipappendfile == None:
        zipappendfile = SaveFile(fileid=fileid)
        zipappendfile.openFileHandle('a',iszip=True) #Always append a zipfile (because file should already exist)
        
    appendfile.closeFileHandle()
    newname = determineInsideZipFileName(zipappendfile.getOriginalName(),nextZipNumber)

    zipappendfile.addToZip(appendfile.getFilename(),rename=newname)
    nextZipNumber += 1

    #Do not need file now
    appendfile.deleteAllFiles()

    #After added new file if last time, then close zip
    zipappendfile.closeFileHandle()

def determineInsideZipFileName(initfilename,nextZipNumber):
    '''Used to have a common function to determine the filename of a .csv inside a zip file
    Set to format: <filename>_<zipnum>.csv
    '''
    namesplit = os.path.splitext(initfilename)

    #Replace characters that should not be in filenames with -
    namesplitre = namesplit[0].replace("\"","-").replace(":","-")
    return namesplitre+"_"+str(nextZipNumber)+".csv"
