import os
import json
from etl.general.mongo_database import MongoDatabase
'''
Written by DWS.
'''


def saveActiveRules(filename,dbname):
	'''Save file format in JSON for loading old active
	[ {"value":rule}, ... ]
	'''
	db = MongoDatabase(dbname,dev=True)

	# For each of the collections
	coll = db["ruleactive"]
	ruleactivelist = []
	# Query to find all records of a particular rule
	actualDataActive = coll.find(no_cursor_timeout=True)
	for line in actualDataActive:
		if line['active']:
			ruleNum = line['_id']
			rule = db.i_rules[str(ruleNum)]
			ruleactivelist.append({"value":rule})

	with open(filename,'w') as f:
		json.dump(ruleactivelist,f,indent=2)

	print("Saved previous active rules to: " + filename)


def updateActiveRules(filename,dbname,savefirst=True):
	'''Format of JSON file is:
	[ {"value":rule,"tag":tag}, ... ]
	'''

	collname = "ruleactive"
	rulefield = "_id"
	activefield = "active"


	issuccess = "fail"
	errorstr = ""
	if not os.path.isfile(filename):
		issuccess="fail"
		errorstr= "File does not exist"
	else:
		if savefirst:
			#Split the name and add _prev
			ex = os.path.splitext(filename)
			savename = ex[0] + '_prev' + ex[1]
			if not os.path.isfile(savename):
				#if not already saved, then save
				saveActiveRules(savename,dbname)
				
		with open(filename,'rb') as afile:
			data = json.load(afile)

		db = MongoDatabase(dbname,dev=True)

		#Make copy
		rules = []
		for rule in db.rules:
			rules.append(rule)

		coll = db[collname]
		# Query to find all rules that are active
		activeData = coll.find({activefield:True},projection=[],no_cursor_timeout=True)
		activeList = []
		for active in activeData:
			activeList.append(db.i_rules[str(active[rulefield])])

		# Query all rules that are in-active
		inactiveData = coll.find({activefield:False},projection=[],no_cursor_timeout=True)
		inactiveList = []
		for inactive in inactiveData:
			inactiveList.append(db.i_rules[str(inactive[rulefield])])
			rules.remove(db.i_rules[str(inactive[rulefield])])
		#Do not handle the case that a rule could be in the DB but have been removed

		makeActive = []
		makeInActive = []
		for ruletag in data:
			rule = ruletag["value"]
			if rule in activeList:
				#find the rule that changed from in-active => active
				activeList.remove(rule)
				rules.remove(rule)
			else:
				#If not in active then it must be in inactive
				#find the rule that changed from active => in-active
				makeActive.append(rule)
		
		#Whatever is still in activeList needs to be turned inactive				
		for rule in rules:
			ruleNum = db.rules[rule]
			coll.update( {rulefield:ruleNum}, {activefield:False} ,upsert=True)

		#Whatever is in makeActive needs to be turned active
		for rule in makeActive:
			ruleNum = db.rules[rule]
			coll.update( {rulefield:ruleNum}, {activefield:True} ,upsert=True)

		issuccess = "true"

	print issuccess
	print errorstr
	return {"success":issuccess,"error":errorstr}

