# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 16:24:58 2015

@author: htran31,dsabin2
"""
from savefile import SaveFile
from datetime import datetime
import json
import os
import copy
import zipfile
from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.cross_validation import train_test_split
from sklearn.learning_curve import learning_curve
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.metrics import f1_score
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import confusion_matrix
from sklearn.externals import joblib
import pandas as pd
import numpy as np
import StringIO
from longjob import LongJob

templocation = 'C:/Temp/'
CONST_RESULT_START = 'result_'
CONST_CONFIG_START = 'config_'

def load_class_from_zip(filename):
    foundClassifier = False
    filehandle = open(filename, 'rb')
    zfile = zipfile.ZipFile(filehandle)
    for j in zfile.namelist():
        ext = os.path.splitext(j)
        if ext[1] == '.pkl':
            #zinfo = zfile.getinfo(j)
            #size = zinfo.file_size
            timestr = datetime.now().strftime('%y-%m-%d_%H%M%S%f')
            outfileloc = templocation + 'prev_class_' + timestr
            outfile = zfile.extract(j,outfileloc)
            foundClassifier = True
            break

    if foundClassifier:
        text_classifier = joblib.load(outfile)
        os.remove(outfile) #Delete temp file
        os.rmdir(outfileloc) #Delete temp directory
        return text_classifier
    else:
        return None

def get_config_results_zip(filename):
    resultdata = None
    configdata = None

    filehandle = open(filename, 'rb')
    zfile = zipfile.ZipFile(filehandle)
    for j in zfile.namelist():
        ext = os.path.splitext(j)
        if ext[1] == '.json' and ext[0].startswith(CONST_RESULT_START):
            resultdata = json.load(StringIO.StringIO(zfile.read(j))) #don't forget this line!
        elif ext[1] == '.json' and ext[0].startswith(CONST_CONFIG_START):
            configdata = json.load(StringIO.StringIO(zfile.read(j))) #don't forget this line!
    return {'config':configdata,'result':resultdata}

def create_classifier(myjson):
    ''' Simple of example of training a classifier using default setting
        Expected input: 
            A dataframe containing text body and label
            A classifier name
            A path to save the classifier
            (name of the text and label key)
        Expected output: 
            Cross-validation and testing output
            A series of PKL object saved in the designated path
    '''
    myfile = SaveFile(fileid=myjson['config']['file']['fileid'])
    label_data_source = myfile.getFilePath()
    origname = myfile.getOriginalName()
    myjson['config']['file']['name']=origname

    classifier_name = myjson['config']['info']['name']
    timestr = datetime.now().strftime('%y-%m-%d_%H%M%S%f')
    classifier_save_location = templocation + classifier_name + '_'+timestr+'.pkl'

    key_for_text = myjson['config']['file']['columntext']
    key_for_label = myjson['config']['file']['columnlabel']

    ex = os.path.splitext(origname)
    if ex[1] == '.xlsx':
        label_df = pd.read_excel(label_data_source, encoding='utf-8')
    elif ex[1] == '.csv':
        label_df = pd.read_csv(label_data_source, encoding='utf-8')
    else:
        return {'status':'error','msg':'File is not a known format for reading (i.e. xlsx or csv).'}

    islearn_curve = False
    if myjson['config']['test']['testpercent'] > 1:
        islearn_curve = True
        test_size = np.linspace(0.1, 0.9, myjson['config']['test']['testpercent']).astype(float)
    else:
        test_size = [myjson['config']['test']['testpercent']]

    '''  Rename columns to a simple format that Numpy is ok with. '''
    key_for_textOLD = key_for_text
    key_for_labelOLD = key_for_label
    key_for_text = 'text'
    key_for_label = 'label'
    label_df.rename(columns={key_for_textOLD: key_for_text, key_for_labelOLD: key_for_label}, inplace=True)

    ''' Dropping duplicated text (not necessary if data was processed properly in a previous step) '''
    label_df.drop_duplicates(key_for_text, 0,inplace=True)

    ''' Remove blanks label lines, and replace blank text so Numpy can handle it. '''
    label_df = label_df[label_df[key_for_label].notnull()]
    label_df[key_for_text].fillna("",inplace=True)

    corpus = (label_df[key_for_text])
    y = (label_df[key_for_label])

    ''' Get unique label names (after changing blank) '''
    y_label = list(set(y.tolist()))
    y_label.sort()


    #Convert a string to int or float depending (handles 1.0 being a float not int)
    def num(s):
        try:
            return int(s)
        except ValueError:
            return float(s)

    sgd_n_iter = round(10**6/len(y))#used to be y_train

    ''' Setting up Pipeline '''
    CountVectorizerConfig = copy.deepcopy(myjson['config']['classifier']['CountVectorizer'])
    if CountVectorizerConfig['ngram_range'] == None:
        del CountVectorizerConfig['ngram_range']
    if CountVectorizerConfig['token_pattern'] == None:
        del CountVectorizerConfig['token_pattern']
    CountVectorizerConfig['max_df'] = num(CountVectorizerConfig['max_df'])

    ''' Check for loading previous classifier '''
    SGDClassifierConfig = copy.deepcopy(myjson['config']['classifier']['SGDClassifier'])
    SGDClassifierConfig['n_iter'] = sgd_n_iter

    if myjson['config']['file']['prevfileid'] != None:
        #Load previous classifier from zip file
        prevclassfile = SaveFile(fileid=myjson['config']['file']['prevfileid'])
        prev_classifier_zipfile = prevclassfile.getFilePath()
        pipeline = load_class_from_zip(prev_classifier_zipfile)
    else:
        #Create new classifier
        pipeline = Pipeline([
            ('vect', CountVectorizer()),
            ('tfidf', TfidfTransformer()),
            ('clf', SGDClassifier()),
        ])

    #Change the configuration of the pipeline
    pipeline.steps[0][1].set_params(**CountVectorizerConfig)
    pipeline.steps[1][1].set_params(**myjson['config']['classifier']['TfidfTransformer'])
    pipeline.steps[2][1].set_params(**SGDClassifierConfig)



    LARGE_NUM_LINES = 100000
    ''' Fit model '''
    #if islearn_curve:
    #Convert back to Python float from Numpy
    train_sizes = []
    train_scores = []
    test_scores = []
    valid_scores = []
    for cur_size in test_size:
        ''' Setting up train - test '''
        text_train, text_test, y_train, y_test = train_test_split(corpus, y, test_size=cur_size, random_state=44)
        #Update n_iters based on training size
        sgd_n_iter = round(10**6/len(y_train))
        pipeline.steps[2][1].set_params(n_iter=sgd_n_iter)
        try:
            text_clf = pipeline.fit(text_train, y_train)
        except ValueError as a:
            return {'status':'error','msg':"Classification Error:"+str(a)+"  Suggest examining the data to make sure file is correctly structured, or choose different configuration options."}

        ''' Cross Validation Accuracy '''
        if myjson['config']['classifier']['CrossValidation']['skiplarge'] and len(y) > LARGE_NUM_LINES:
            warn = 'Skipped cross validation because line was too large and skip selected.'
            cv = {'avg':0.0,'std':0.0,'all':warn}
        else:
            try:
                score = cross_val_score(text_clf, corpus, y, cv = myjson['config']['classifier']['CrossValidation']['cv'], scoring = myjson['config']['classifier']['CrossValidation']['scoring'])
            except ValueError as a:
                return {'status':'error','msg':"Cross Validation Error:"+str(a)+"  Suggest trying a different scoring type."}            
            cv = {'avg':score.mean(),'std':score.std(), 'all':str(score)}


        if islearn_curve:
            ''' Prediction on Training Data '''
            y_pred = text_clf.predict(text_train)
            trainscores = f1_score(y_train, y_pred,average='weighted',pos_label=None)

            y_pred = text_clf.predict(text_test)
            testscores = f1_score(y_test, y_pred,average='weighted',pos_label=None)

            train_sizes.append(cur_size)
            train_scores.append(trainscores.mean())
            valid_scores.append(cv['avg'])
            test_scores.append(testscores.mean())


        if not islearn_curve:
            ''' Prediction on Test '''
            y_pred = text_clf.predict(text_test)

            ''' Precision and Recall ''' 
            testing_report = classification_report(y_test, y_pred)

            #Make CSV with text_test,y_test,y_pred
            csvTestData = templocation+'test_true_vs_pred_'+timestr+'.csv'
            test_data = pd.DataFrame()
            test_data['Text'] = text_test
            test_data['True'] = y_test
            test_data['Predicted'] = y_pred
            test_data.to_csv(csvTestData, encoding='utf-8', index=False)

            testing_matrix = confusion_matrix(y_test, y_pred, labels=y_label)
            #testing_matrix = testing_matrix.astype('float') / testing_matrix.sum(axis=1)[:, np.newaxis] #Normalize IGNORE
            testing_matrix = testing_matrix.tolist()
            new_label = [str(x) for x in y_label] #Make labels string for plotting and saving
            #X = Predicted label
            #Y = True Label

            ''' Save Model '''
            outfiles = joblib.dump(text_clf, classifier_save_location,compress=3)

            zipfile = SaveFile(origname=classifier_name + '_'+timestr+'.zip')
            zipfileid = zipfile.getFileID()

            outdata = {'cv':cv,'report':testing_report,'outfileid':zipfileid}
            outdata['cm'] = {'labels':new_label,'matrix':testing_matrix}

            #Zip the following files together
            configfile = templocation + CONST_CONFIG_START + timestr + '.json'
            with open(configfile,'w') as f:
                json.dump(myjson['config'],f)
            resultfile = templocation + CONST_RESULT_START + timestr + '.json'
            with open(resultfile,'w') as f:
                json.dump(outdata,f)

            zipfile.openFileHandle('w',iszip=True)
            zipfile.addToZip(configfile,rename=os.path.basename(configfile))
            zipfile.addToZip(resultfile,rename=os.path.basename(resultfile))
            zipfile.addToZip(label_data_source,rename=origname)
            zipfile.addToZip(csvTestData,rename=os.path.basename(csvTestData))
            for outfile in outfiles:
                zipfile.addToZip(outfile,rename=os.path.basename(outfile))
                os.remove(outfile)

            zipfile.closeFileHandle()

            #Delete temp files:
            os.remove(csvTestData)
            os.remove(configfile)
            os.remove(resultfile)

    if islearn_curve:
        outdata = {'learn':{'train_sizes':train_sizes,'train_scores':train_scores,'valid_scores':valid_scores,'test_scores':test_scores}}
    #Give back configuration
    outdata['config'] = myjson['config']

    return {'status':'success','result':outdata}


def predictSingle(myjson):
    prevclassfile = SaveFile(fileid=myjson['config']['file']['prevfileid'])
    prev_classifier_zipfile = prevclassfile.getFilePath()
    pipeline = load_class_from_zip(prev_classifier_zipfile)
    if pipeline == None:
        return {'status':'error','msg':'Classifier file ID is not a zip containing a .pkl file.'}

    example_text = myjson['config']['text']
    resultclass = []
    for d in example_text:
        resultclass.append(pipeline.predict([d])[0])
    outdata = {'config':myjson['config'],'outfileid':None,'classtext':resultclass}
    return {'status':'success','result':outdata}


def predictor(myjson):
    ''' Pipeline for applying a pre-trained classifier.
        input:{
                'fileid': new file to predict
                'columntext' : title of the text column
                'columnlabel' : new title of label column
                'prevfileid' : last classifier .zip
            }
    '''

    ''' 1. Load classifier 
    Classifier = joblib.load('Stored path to classifier pkl file')
    '''
    prevclassfile = SaveFile(fileid=myjson['prevfileid'])
    prev_classifier_zipfile = prevclassfile.getFilePath()
    pipeline = load_class_from_zip(prev_classifier_zipfile)
    if pipeline == None:
        return {'status':'error','msg':'Classifier file ID is not a zip containing a .pkl file.'}

    ''' 2. Load data into a dictionary or pandas dataframe from a source
        Here the source is a csv file, to be replaced with a source given by MongoDB or Pyramid
    '''
    myfile = SaveFile(fileid=myjson['fileid'])
    label_data_source = myfile.getFilePath()
    origname = myfile.getOriginalName()

    ex = os.path.splitext(origname)
    if ex[1] == '.xlsx':
        label_df = pd.read_excel(label_data_source, encoding='utf-8', low_memory=False)
    elif ex[1] == '.csv':
        label_df = pd.read_csv(label_data_source, encoding='utf-8', low_memory=False)
    else:
        return {'status':'error','msg':'File is not a known format for reading (i.e. xlsx or csv).'}


    key_for_text = myjson['columntext']
    key_for_label = myjson['columnlabel']
    key_for_textOLD = key_for_text
    key_for_labelOLD = key_for_label
    key_for_text = 'text'
    key_for_label = 'label'
    label_df.rename(columns={key_for_textOLD: key_for_text}, inplace=True)


    ''' 3. Create a new field storing the classification result '''
    label_df[key_for_label] = pipeline.predict(label_df[key_for_text])

    ''' 4. Save result to same location '''
    label_df.rename(columns={key_for_text: key_for_textOLD, key_for_label: key_for_labelOLD}, inplace=True)

    if ex[1] == '.xlsx':
        label_df.to_excel(label_data_source, encoding='utf-8', index=False)
    elif ex[1] == '.csv':
        label_df.to_csv(label_data_source, encoding='utf-8', index=False)

    return {'status':'success'}
    

class PredictClassJob(LongJob):
    def __init__(self,printif=False,development=False):
        if development:
            jobtext = " Development"
        else:
            jobtext = ""        
        super(PredictClassJob,self).__init__("Predict Class"+jobtext)
        self.baseexpected = [
            {'name': 'username', 'type': 'string', 'min':1, 'max':50},
            {'name': 'columnlabel', 'type': 'string', 'min':1, 'max': 300},
            {'name': 'columntext', 'type': 'string', 'min':1, 'max': 300},
            {'name': 'db', 'type': 'string', 'min':1, 'max': 300},
            {'name': 'fileid', 'type': 'number','min': 10000000, 'max':99999999},
            {'name': 'prevfileid', 'type': 'number','min': 10000000, 'max':99999999}
        ]

        self.printif = printif

    '''This is called by Pyramid POST through HTTP
    '''
    def SubmitPred(self,myjson):
        '''
            input:{
                'fileid': new file to predict
                'columntext' : title of the text column
                'columnlabel' : new title of label column
                'prevfileid' : last classifier .zip
            }
        '''
        outdata = False
        super(PredictClassJob,self).printIF(json.dumps(myjson))
        redata = super(PredictClassJob,self).analyzeInputs(self.baseexpected,myjson)
        if redata['result']:
            super(PredictClassJob,self).printIF("Successful SubmitPred analysis")

            #Successful analysis, so submit
            outdata = super(PredictClassJob,self).submitJob(redata['data'])

        return outdata

    '''This is indirectly called by Jenkins
    Whatever is returned as a Dictionary is put into the Environment variables to be retrieved by Get method.
    '''
    def RunSubmitPred(self,myjson):
        outdata = {'status':'fail'}
        # If performing a run, then these extra ones are expected
        redata = super(PredictClassJob,self).analyzeInputs(self.baseexpected,myjson)
        if redata['result']:
            super(PredictClassJob,self).printIF("RunSubmitPred analysis good")
            #Perform topic modelling on Jenkins
            outdata = predictor(redata['data'])

        return outdata

    '''This is called by Pyramid POST through HTTP
    '''
    def GetPred(self,myjson):
        #Allow myjson to have a start and max to define what to show
        defstart = 0
        if 'start' in myjson and isinstance(myjson['start'],numbers.Number):
            defstart = myjson['start']
        defmax = 30
        if 'max' in myjson and isinstance(myjson['max'],numbers.Number):
            defmax = myjson['max']
        envMap = None #["totalcount", "samplecount"] #UPDATE TO WHAT ENV TO CAPTURE

        return super(PredictClassJob,self).getJobs(defstart,defmax,myjson['username'],envMap)

    '''This is directly called by Jenkins
    '''
    def JenkinsRunSubmitPred(self):
        import json,os

        myjson = {}
        myjson['username'] = os.getenv('username', "")
        myjson['columnlabel'] = os.getenv('columnlabel', "")
        myjson['columntext'] = os.getenv('columntext', "")
        myjson['db'] = os.getenv('db', "")
        fileid = os.getenv('fileid', "0")
        if not fileid.isdigit():
            fileid = "0"
        myjson['fileid'] = int(fileid)
        prevfileid = os.getenv('prevfileid', "0")
        if not prevfileid.isdigit():
            prevfileid = "0"
        myjson['prevfileid'] = int(prevfileid)

        return self.RunSubmitPred(myjson)


if __name__ == "__main__":
    myjson = json.loads('{"config":{"info": {"instructions": "", "description": "", "name": "d"}, "test": {"testpercent": 12, "type": "specific", "stratification": []}, "classifier": {"CountVectorizer": {"lowercase": true, "stop_words": null, "token_pattern": null, "analyzer": "word", "ngram_range": null, "max_df": "1.0", "min_df": 1, "max_features": null}, "SGDClassifier": {"loss": "hinge", "shuffle": true, "fit_intercept": true, "l1_ratio": 0.15, "average": null, "penalty": "l2", "power_t": 0.5, "random_state": null, "eta0": 0, "alpha": 0.0001, "class_weight": null}, "CrossValidation": {"scoring": "f1", "cv": 5, "skiplarge": "true"}, "TfidfTransformer": {"use_idf": true, "smooth_idf": true, "sublinear_tf": false, "norm": null}}, "file": {"columntext": "description", "columnlabel": "LABEL_E-DeviceRelevance(1. Yes, 2. No)", "name": "Ecig Raw Label.xlsx", "fileid": 15345645}}}')
    labelfile = 'C:\Users\dsabin2\Documents\Ecig Raw Label.xlsx'
    myfile = SaveFile(origname='Ecig Raw Label.xlsx')
    with open(labelfile,'rb') as f:
        myfile.writeToFile(f)
    
    myjson['config']['file']['fileid'] = myfile.getFileID()
    outdata = create_classifier(myjson)
    print(json.dumps(outdata,indent=2))