from savefile import createUniqueID
import datetime
import unittest
import copy
'''
Written by DWS.
'''

class idTest(unittest.TestCase):
	def test_multiple_unique(self):
		''' Test if run multiple times it will still be unique. '''
		aa = 0
		bb = 1
		idx = 0
		totalloop = 50
		while aa != bb and idx < totalloop:
			aa = createUniqueID(True)
			bb = createUniqueID(True)
			idx += 1
		self.assertEqual(idx,totalloop)

	def test_month(self):
		cur = datetime.datetime(2016,12,1,1,1,1,100000)
		curid = createUniqueID(True,indate=cur)
		dupsec = copy.copy(cur)
		dupsec += datetime.timedelta(seconds=1)
		secid = createUniqueID(True,indate=dupsec)
		dupmin = copy.copy(cur)
		dupmin += datetime.timedelta(minutes=1)
		minid = createUniqueID(True,indate=dupmin)
		duphr = copy.copy(cur)
		duphr += datetime.timedelta(hours=1)
		hrid = createUniqueID(True,indate=duphr)
		dupday = copy.copy(cur)
		dupday += datetime.timedelta(days=1)
		dayid = createUniqueID(True,indate=dupday)
		dupmonth = copy.copy(cur)
		dupmonth += datetime.timedelta(days=32)
		monthid = createUniqueID(True,indate=dupmonth)

		self.assertNotEqual(curid,secid)
		self.assertNotEqual(curid,minid)
		self.assertNotEqual(curid,hrid)
		self.assertNotEqual(curid,dayid)
		self.assertNotEqual(curid,monthid)

if __name__ == '__main__':
	unittest.main(verbosity=2)