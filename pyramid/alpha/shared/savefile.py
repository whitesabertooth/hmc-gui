import random
import os
import shutil
import numbers
import zipfile
import zlib
import datetime
import socket
import time
'''
Written by DWS.
'''

#Set the Temp and Perminent Directories based on the computers hostname
CURRENT_HOSTNAME = socket.gethostname()
SERVER_HOSTNAME = "S12ZAPAP02165"
if SERVER_HOSTNAME==CURRENT_HOSTNAME:
    TMPDIR = 'e:/data/webdata/tempdata/'
    PERMDIR = 'e:/data/webdata/permdata/'
else:
    TMPDIR = 'c:/temp/tempdata/'
    PERMDIR = 'c:/perm/permdata/'

class SaveFile:
    ##############################
    # Private Functions
    def __init__(self,fileid=None,filetype='application/force-download',origname='1',uniqueid=None,istemp=None):
        # This is assumed to be run from alpha directory of pyramid
        self.tempdirname = TMPDIR
        self.permdirname = PERMDIR

        if istemp == None and fileid != None:
            #Determine istemp based on fileid:
            if self.goodFileID(fileid,True):
                istemp = True
            elif self.goodFileID(fileid,False):
                istemp = False
            else:
                raise IOError
        elif istemp == None:
            #Default to Temp
            istemp = True

        #Toggle dependant of whether the file is perminent or temporary
        if istemp:
            self.istemp = True
            self.dirname = self.tempdirname
            if not os.path.isdir(self.tempdirname):
                os.makedirs(self.tempdirname)
        else:
            self.istemp = False
            self.dirname = self.permdirname
            if not os.path.isdir(self.permdirname):
                os.makedirs(self.permdirname)

        self.typeextention = '.type'
        self.original_filename_extention = '.orig'
        self.unique_filename = '.id'

        self.filehandletype = ''
        self.filehandle = None
        self.fileopen = False

        nouniqueid = False #Check for no unique id

        # Make sure it is a correct style fileid, else through error
        if fileid != None and not self.goodFileID(fileid,istemp):
            raise IOError
        if fileid == None:
            #Check for no unique Id
            if uniqueid == None:
                uniqueid = 0
                nouniqueid = True

            # If no unique id, then do new file
            # Or if origname and uniqueid does not exist by cycling through all files (if directory exists)
            if nouniqueid or not self.findUniqueUpload(uniqueid,origname):
                # Didn't find a continuation, so start new
                self.fileid = createUniqueID(istemp)
                self.setDataByFileID()
                self.appending = False
                if not os.path.exists(self.dirname): #make directory if not exists
                    os.makedirs(self.dirname)

                self.touchFileName()

                self.saveFileType(filetype)
                self.saveOriginalName(origname)
                self.saveUniqueID(uniqueid)

        elif os.path.exists(self.dirname):#make sure directory exits
            # If is a legitimate number then do not add header line
            self.fileid = fileid
            self.setDataByFileID()
            self.appending = True

            self.loadFileType()
            self.loadUniqueID()
            self.loadOriginalName()


    def goodFileID(self,fileid,istemp):
        if istemp:
            return (isinstance(fileid,numbers.Number) and fileid>=10000000 and fileid<=99999999)
        else:
            return (isinstance(fileid,str) and len(fileid) == 36)

    def touchFileName(self):
        # Delete if exists
        if os.path.isfile(self.filename):
            os.unlink(self.filename)
        open(self.filename,'w+').close() # touch the file

    def saveFileType(self,filetype):
        with open( self.filename + self.typeextention, 'w') as f:
            f.write(filetype)
        self.contenttype = filetype

    def saveUniqueID(self,uniqueid):
        with open( self.filename + self.unique_filename, 'w') as f:
            f.write(str(uniqueid))
        self.uniqueid = uniqueid

    def saveOriginalName(self,origname):
        with open( self.filename + self.original_filename_extention, 'w') as f:
            f.write(origname)
        self.origname = origname


    def loadUniqueID(self):
        # Get file type
        with open( self.filename + self.unique_filename, 'r') as f:
            self.uniqueid = int(f.read())

    def loadOriginalName(self):
        # Get file type
        with open( self.filename + self.original_filename_extention, 'r') as f:
            self.origname = f.read()

    def setDataByFileID(self):
        self.filename = self.dirname + str(self.fileid)


    def loadFileType(self):
        # Get file type
        with open( self.filename + self.typeextention, 'r') as f:
            self.contenttype = f.read()

    # Method to go over individual files in the directory
    # Finds the fileid that matches the uniqueid and original file name
    def findUniqueUpload(self,testuniqueid,testorigname):
        # Make sure directory exists first
        if not os.path.exists(self.dirname):
            return False
        fileList = os.listdir(self.dirname)
        # Iterate over every file in the source directory
        for name in fileList:
            testname = name[-len(self.unique_filename):]
            if testname == self.unique_filename:
                filename = self.dirname + name
                with open(filename,'r') as f:
                    tempuniqueid = f.read()
                if tempuniqueid == testuniqueid:
                    # Found uniqueid match, now look for original filename
                    tempfilename = self.dirname + name[:-len(self.unique_filename)] + self.original_filename_extention
                    if os.path.exists(tempfilename):
                        with open(tempfilename,'r') as f:
                            temporigname = f.read()
                        if temporigname == testorigname:
                            self.uniqueid = tempuniqueid
                            if self.istemp:
                                self.fileid = int(name[:-len(self.unique_filename)]) # Remove .orig
                            else:
                                self.fileid = name[:-len(self.unique_filename)]
                            self.setDataByFileID()
                            self.origname = temporigname
                            self.loadFileType()
                            self.appending = True
                            return True
        return False

    ##############################
    # Public Functions

    def deleteAllFiles(self):
        '''Delete all files that are used by this savefile
        '''
        #Close file if open
        self.closeFileHandle()

        files = [self.filename + self.typeextention, 
                 self.filename + self.unique_filename,
                 self.filename + self.original_filename_extention,
                 self.filename]
        for afile in files:
            if os.path.isfile(afile):
                os.unlink(afile)

    def getFileSize(self):
        return os.path.getsize(self.filename)

    def getFilePath(self):
        return os.path.abspath(self.filename)

    def getFilename(self):
        return self.filename

    def openFileHandle(self,rw='rb',iszip=False):
        if self.fileopen:
            # Trying to open with a different type
            # So close and reopen (as in continue to below)
            self.closeFileHandle()

        self.iszip = iszip

        if iszip:
            #Open a zip file and set the compression
            self.filehandle = zipfile.ZipFile(self.filename, mode=rw)
            self.compression = zipfile.ZIP_DEFLATED
        else:
            #Open a regular file
            self.filehandle = open(self.filename,rw)

        self.filehandletype = rw
        self.fileopen = True
        return self.filehandle

    def getFileHandle(self):
        if self.fileopen:
            return self.filehandle
        else:
            return None

    def closeFileHandle(self):
        if self.fileopen:
            self.filehandle.close()
            self.fileopen = False


    def writeToFile(self,fileobj):
        # Always append (allows continuation)
        fh = self.openFileHandle('ab')
        shutil.copyfileobj(fileobj,fh)
        self.closeFileHandle()

    def getFileLink(self):
        return self.linkpath

    def isAppending(self):
        return self.appending

    def getFileID(self):
        return self.fileid

    def close(self):
        self.closeFileHandle()

    def getContentType(self):
        return self.contenttype

    def getOriginalName(self):
        return self.origname

    def addToZip(self,filename,rename=None):
        if self.iszip and self.fileopen:
            if rename == None:
                rename = filename

            self.filehandle.write(filename, arcname=rename, compress_type=self.compression)

def createUniqueID(istemp,indate=None):
    '''
    Create unique ID based on time.
    Temp files only.  Perminent files should use uuid.uuid1()
    Modifications for unique number based on time to 8 places
    years       months      days        microseconds
    Always 0    Mod(#,2)    Minus 1     #/ 1000
                                        floor(#/128)

    Digit  mm  dd  hh  nn  ss  uu
    Bits    1   5   5   6   6   3
    Shift  25  20  15   9   3   0

    '''
    if istemp:
        #Temporary ID so needs to be unique for 30 days
        time.sleep(0.15) #Wait minimum time slice to make the number unique (which is 1/1000 * 128, then add margin)

        if indate == None:
            datenow = datetime.datetime.now()
        else:
            datenow = indate
        mm = datenow.month % 2  #Mod 2
        dd = datenow.day - 1
        hh = datenow.hour
        nn = datenow.minute
        ss = datenow.second
        uu = int(datenow.microsecond / 1000 / 128)

        final = 10000000 #Initial offset to make it 10000000-99999999
        final += mm << 25
        final += dd << 20
        final += hh << 15
        final += nn << 9
        final += ss << 3
        final += uu
    else:
        #Perminent ID, so needs to be unique forever
        import uuid
        final = str(uuid.uuid1()) #create a time and machine unique ID that is very long

    return final

def modification_date(filename):
    ''' Returns the modification date of the filename as datetime obj '''
    t = os.path.getmtime(filename)
    return datetime.datetime.fromtimestamp(t)

def deleteold(daysold=30):
    ''' Delete all file in TMPDIR that are older than 30 days '''
    if not os.path.isdir(TMPDIR):
        return
    datenow = datetime.datetime.now()

    print("Examining files in directory "+TMPDIR+ " to delete files that are > " + str(daysold)+" days old")
    fileList = os.listdir(TMPDIR)
    # Iterate over every file in the source directory
    for name in fileList:
        filename = TMPDIR + '/' +name

        filedate = modification_date(filename)
        tdelta = datenow - filedate

        if tdelta.days > daysold:
            os.remove(filename)
            print("Deleting file "+name+ " ("+str(tdelta.days)+" > " + str(daysold)+" days old)")
