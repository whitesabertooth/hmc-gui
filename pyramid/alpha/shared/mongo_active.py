import pprint
from etl.general.mongo_database import MongoDatabase
'''
Written by DWS.
'''

def getActive():
    db = MongoDatabase('twitter')
    all_ops = db['admin']['$cmd.sys.inprog'].find_one('inprog')['inprog']
    active_ops = [op for op in all_ops if op['active']]
    return active_ops

if __name__ == '__main__':
    active_ops = getActive()
    print '%d/%d active operations' % (len(active_ops), len(all_ops))
    pprint.pprint(active_ops)