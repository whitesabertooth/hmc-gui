from etl.general.mongo_database import MongoDatabase
from datetime import datetime,timedelta
from sampleJob import saveOrReturnQuery
from sampleJob import closeLastFile
import json
import sys
import re
import random

'''
Written by DWS.
'''

class FindRuleCounts:
    def __init__(self,dname):
        self.db = MongoDatabase(dname)

    def find(self,mrvalue,ruleNum,sdate,edate,initquery,getSample=False,outputselection=None,samplefileid=None):

        outdata = {}
        #print mrvalue + " = " + str(ruleNum)

        dateField = 'postedTime'

        totalcount = 0
        issuccess = 'success'
        errorstr = ""
        finalsamplesize = 0

        if getSample:
            #For sampling initialization
            totalcount = 0
            nextZipNumber = 0
            appendfile = None

            #Cycle through collections
            for collname,curquery in initquery.iteritems():
                print "Processing " + collname
                sys.stdout.flush() #Flush to screen

                coll = self.db[collname]

                #Save off the sample
                outdata = saveOrReturnQuery(self.db,coll,True,curquery,False,0,True,samplefileid,outputselection=outputselection,nextZipNumber=nextZipNumber,appendfile=appendfile,bygroup=mrvalue)
                nextZipNumber = outdata["nextZipNumber"] #Save to pass to next iteration
                appendfile = outdata["appendfile"] #Save to pass to next iteration
                outputselection = outdata['outputselection'] #pass back outputselection

                #Keep track of totals
                totalcount += outdata['totalcount'] #Add to total count
                finalsamplesize += outdata['samplesize'] #Add to sample count

            #Close the zip file
            closeLastFile(appendfile,nextZipNumber,samplefileid)

            #FOR DEBUG
            #print(json.dumps(outputselection,indent=4, sort_keys=True))
            print('Total count = %d\nTotal sample = %d'%(totalcount,finalsamplesize))
            #for ruleNum,output in outputselection.getitems():
            #    #Handle the saving the sample download
            #    outdata.append({'id':twname, 'total':outputRuleTotal,'sample':finalsamplesize})
            outdata = outputselection


        else:
            #For not sampling, just getting dataset stratification
            if len(initquery.keys()) > 0:
                #Append
                tmpinitquery = {'$and':[initquery,{mrvalue:ruleNum}]}
            else:
                #Nothing set, so just add
                tmpinitquery = {mrvalue:ruleNum}

            curdate = datetime(sdate.year,sdate.month,1,0,0,0) # Always set to first day of the month


            while curdate < edate: # We don't want <= because the equal case will only happen when we have rolled over a month on the end date, and therefore we don't want to query that month
                curquery = tmpinitquery
                if curdate.year == sdate.year and curdate.month == sdate.month and sdate.day > 1:
                    if dateField not in curquery:
                        curquery[dateField] = {}
                    curquery[dateField]['$gte'] = sdate
                if curdate.year == edate.year and curdate.month == edate.month and edate.day > 1:# Since I rolled the end date by 1 day, then the end of the month will always be the first day of the next month
                    if dateField not in curquery:
                        curquery[dateField] = {}
                    curquery[dateField]['$lte'] = edate

                #Download the month data
                collname = curdate.strftime("%Y%m") #Handles putting 0 infront of <10 months
                print "Processing " + collname
                sys.stdout.flush() #Flush to screen

                #Increment the month to next collection (for next loop) - MUST BE DONE BEFORE CHECK FOR LAST TIME
                curdate = self.addMonth(curdate)

                coll = self.db[collname]
                numfound = coll.count(curquery)
                print("Query="+str(curquery)+", count="+str(numfound))
                #DEBUG: Improvement by hinting what index to use
                #hintidx = mrvalue + '_1_pt_1'
                #numfound = coll.find(curquery).hint(hintidx).count()
                totalcount += numfound
                outdata[collname]=numfound


            #-END WHILE-


        return {'data':outdata,'totalcount':totalcount,'samplesize':finalsamplesize}

    def addMonth(self,newdate):
        curdate = datetime(newdate.year,newdate.month,1,0,0,0)
        curdate += timedelta(days=32)
        curdate = datetime(curdate.year,curdate.month,1,0,0,0)
        return curdate

    def findAll(self,groups=["rule","tag"],allQuery=[None],isreturn=False,saveid=None,allsample={}):
        '''Find all rule or tag groups
        If isreturn is set, then it will perform allQuery queries and return the counts in JSON.
        If saveid is set, then it will perform a sampling of the allQuery queries, with allsample lookup for how many of each.
        allsample is a lookup for rulename=>number
        '''
        #Initial settings, only changed if allQuery has values
        initquery={}
        sdate = datetime(2011,12,1,0,0,0)
        edate = datetime.now()
        dateField = 'postedTime'

        #Handle taking the sample again
        if saveid != None:
            #determine groups
            groups = [allsample['type']]

            #Initialize for the download of sample
            random.seed() #use time

        #Convert times to python times
        outdata = []
        difftotal = 0
        diffsample = 0
        finalsamplesize = 0
        for group in groups:

            if group == "rule":
                mrvalue = "matchingrulesvalue"
                tablename = "rulecounts"
                if saveid == None:
                    outdata.append(["Tag","Rule","Total"])#Add header row
                tw_lookup = self.db.rules
                tw_relookup = self.db.i_rules
                tw_retaglookup = self.db.i_tags
        


            if group == "tag":
                mrvalue = "matchingrulestag"
                tablename = "tagcounts"
                if saveid == None:
                    outdata.append(["Tag","Total"])#Add header row
                tw_lookup = self.db.tags
                tw_relookup = self.db.i_tags

            if saveid != None:
                #Handle getting actual data from DB based on stratification counts
                predsamplecount = 0
                predtotalcount = 0
                #Do not perform a loop on rules/tags.
                #Instead query everything and then count the rules/tags seen as the data is downloaded.
                #This handles the de-duplication, and makes it go faster since every data is only seen once
                outputselection = {}
                for ruleNum in sorted(tw_lookup.values()):
                    #Calculate all the sampling, and make it easily look-upable by ruleNum
                    samplecount = 0
                    totalcount = 0
                    try:
                        twname = tw_relookup[ruleNum]
                    except KeyError:
                        twname = tw_relookup[str(ruleNum)]
                    try:
                        samplecount = allsample['allsample'][twname]
                        totalcount = allsample['alltotal'][twname]
                        predsamplecount += samplecount
                        predtotalcount += totalcount
                    except KeyError:
                        pass #Skip because nothing defined for this ruleNum

                    #Generate the random sampling for this rule/tag
                    ruleoutputselection = random.sample(xrange(totalcount),samplecount)
                    outputselection[ruleNum] = {'samplecount':samplecount,'totalcount':totalcount,'outputselection':ruleoutputselection,'finalsamplesize':0,'outputRuleTotal':0,'prevCount':0}

                print('Predicted sample size = %d\nPredicted total size = %d' % (predsamplecount, predtotalcount))

                ruleNum = None #Dummy value, not used

                #Create a dictionary monthQuery that for each collection month it has the correct start and end date (if not the full month).
                #  Do this for each dataset query, appending to make a super query by collection.
                monthQuery = {}
        
                for myjson in allQuery:
                    #Handle if allQuery is set, otherwise the defaults are above
                    if myjson == None:
                        raise RuntimeError('Cannot perform sampling without a datasets')

                    initquery = myjson['query']
                    sdate = convertDate(myjson['datestart'])
                    sdate = datetime(sdate.year,sdate.month,sdate.day,0,0,0)
                    edate = convertDate(myjson['dateend'])
                    edate = datetime(edate.year,edate.month,edate.day,0,0,0)
                    edate += timedelta(days=1) #add an extra day to be the beginning of the next day

                    curdate = datetime(sdate.year,sdate.month,1,0,0,0) # Always set to first day of the month
                    while curdate < edate: # We don't want <= because the equal case will only happen when we have rolled over a month on the end date, and therefore we don't want to query that month
                        curquery = initquery
                        if curdate.year == sdate.year and curdate.month == sdate.month and sdate.day > 1:
                            if dateField not in curquery:
                                curquery[dateField] = {}
                            curquery[dateField]['$gte'] = sdate
                        if curdate.year == edate.year and curdate.month == edate.month and edate.day > 1:# Since I rolled the end date by 1 day, then the end of the month will always be the first day of the next month
                            if dateField not in curquery:
                                curquery[dateField] = {}
                            curquery[dateField]['$lte'] = edate

                        #Download the month data
                        collname = curdate.strftime("%Y%m") #Handles putting 0 infront of <10 months

                        if collname in monthQuery:
                            #merge queries
                            curquery = {'$or':[curquery,monthQuery[collname]]}

                        monthQuery[collname] = curquery

                        #Increment the month to next collection (for next loop) - MUST BE DONE BEFORE CHECK FOR LAST TIME
                        curdate = self.addMonth(curdate)

                ruleData = self.find(mrvalue,ruleNum,sdate,edate,monthQuery,getSample=True,outputselection=outputselection,samplefileid=saveid)
                finalsamplesize = ruleData['samplesize'] #Add to sample count
                outputRuleTotal = ruleData['totalcount']
                outdata = ruleData['data']

                #Compute how much de-duplication removed
                diffsample = predsamplecount - finalsamplesize
                difftotal = predtotalcount - outputRuleTotal
                print('Number of duplicates removed from sample set = %d\nNumber of duplicates removed from total = %d' % (diffsample, difftotal))

            else:
                #Handle getting initial counts of the stratification
                #Cycle through all rules or tags
                for ruleNum in sorted(tw_lookup.values()):
                    outputRuleData = []
                    outputRuleTotal = 0
                    #Cycle through all datasets
                    for myjson in allQuery:
                        #Handle if allQuery is set, otherwise the defaults are above
                        if myjson != None:
                            initquery = myjson['query']
                            sdate = convertDate(myjson['datestart'])
                            sdate = datetime(sdate.year,sdate.month,sdate.day,0,0,0)
                            edate = convertDate(myjson['dateend'])
                            edate = datetime(edate.year,edate.month,edate.day,0,0,0)
                            edate += timedelta(days=1) #add an extra day to be the beginning of the next day

                        ruleData = self.find(mrvalue,ruleNum,sdate,edate,initquery)
                        outputRuleData = ruleData['data'] #Should only be called once for isreturn = False
                        outputRuleTotal += ruleData['totalcount']
                    if not isreturn:
                        self.db[tablename].update({'_id':ruleNum},outputRuleData,upsert=True)
                    else:
                        try:
                            twname = tw_relookup[ruleNum]
                        except KeyError:
                            twname = tw_relookup[str(ruleNum)]
                        if group == "rule":
                            try:
                                tagnum = self.db.rules_tags[ruleNum]
                            except KeyError:
                                tagnum = self.db.rules_tags[str(ruleNum)]

                            try:
                                tagname = self.db.i_tags[tagnum]
                            except KeyError:
                                try:
                                    tagname = self.db.i_tags[str(tagnum)]
                                except:
                                    tagname = "unknown"


                            outdata.append([tagname,twname, outputRuleTotal])
                        else:
                            outdata.append([twname, outputRuleTotal])
            ##--END FOR RULE--##
        ##--END FOR GROUP--##
        if isreturn and saveid == None:
            #Sort outdata by total, largest on top
            if len(outdata) > 0:
                subdatalast = len(outdata[0])-1
                #key=itemgetter(2)
                outdata.sort(key=lambda x: x[subdatalast], reverse=True)

        return {"issuccess":"success", "data":outdata, 'samplesize':finalsamplesize, 'totalcount':outputRuleTotal, 'diffsample':diffsample,'difftotal':difftotal}


def convertJavascriptDate(s):
    '''Converts standard Javascript date string to Python datetime
    '''
    return datetime(*map(int, re.split('[^\d]', s)[:-1]))

def convertDate(s):
    '''Converts standard Javascript date string to Python datetime
    '''
    return datetime.strptime(s, '%m/%d/%Y')

if __name__ == '__main__':
    ff = FindRuleCounts('twitter')
    ff.findAll()