#Need to get libraries working:
#  twokenize
#  pyLDAvis - follow Hy's procedure
from .twokenize import twokenize


def topic_model(text_file_handle,json_save_handle,text_key):
	import unicodecsv #more robust importing of csv files with unicode
	import nltk #mainly used for the stopwords, to be trimmed down to import stopwords only
	from gensim import corpora, models #main modeling pieces
	import pyLDAvis.gensim #for transforming gensim models into visual data 
	from pyLDAvis import _display  #for saving the prepared data as JSON 

	''' Parameters to be received '''
	number_of_topic = 50

	''' Future parameters '''
	custom_stoplist=['rt']
	min_token_len = 2

	''' Reading in raw text documents'''
	doc=[]
	with open(text_file_handle,'r') as csvfile:
	    csvreader = unicodecsv.DictReader(csvfile, delimiter=',', quotechar='"')
	    for row in csvreader:
	        #Importing text only 
	        doc.append(row[text_key])
	    csvfile.close()

	''' Tokenizing '''
	filter_sent = []
	for t in doc:
	    sent = twokenize.tokenizeRawTweetText(t)
	    #Simple text processing: to lower case, remove stopwords
	    filter_sent.append([w.lower() for w in sent if not w.lower() in 
	            nltk.corpus.stopwords.words('english') and not len(w) < min_token_len
	            and not w.lower() in custom_stoplist]
	            )

	''' Modeling '''          
	dictionary = corpora.Dictionary(filter_sent)
	corpus = [dictionary.doc2bow(text) for text in filter_sent]
	tfidf = models.TfidfModel(corpus)
	corpus_tfidf = tfidf[corpus]
	model = models.ldamodel.LdaModel(corpus_tfidf, id2word=dictionary, num_topics=number_of_topic, alpha='auto', eval_every=5)

	''' Create the visual data '''
	visual_data = pyLDAvis.gensim.prepare(model, corpus_tfidf, dictionary, n_jobs=1)
	''' Save visual data as a JSON file '''
	_display.save_json(visual_data, json_save_handle)
	return True
