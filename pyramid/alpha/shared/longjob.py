import urllib
import urllib2
import ast
import json
import requests
import numbers
import re
from savefile import SaveFile
import copy
from sampleJob import downloadOrSampleJob
from find_rule_counts import FindRuleCounts
'''
Written by DWS.
'''

'''This handles a submit or check of a job in Jenkins
'''
class LongJob(object):
    def __init__(self,jobname):
        self.jenkinsweb = "http://david:david@localhost:8080/"
        self.jobname = jobname
        self.printif = False

    '''Cancel a Job by ID
    '''
    def Cancel(self,jobid):
        newjobname = urllib.quote(self.jobname) #Should make spaces %20
        webpageget = self.jenkinsweb + "job/" + newjobname + '/' + str(jobid) + '/stop'
        result = urllib2.urlopen(webpageget,data="").getcode()
        if result == 200:
            return True
        else:
            return False


    '''Gets the jobs with success, duration, timestamp,
    '''
    def getJobs(self,startidx=0,maxidx=30,username=None,envMap=None,latestStatus=None):
        newjobname = urllib.quote(self.jobname) #Should make spaces %20
        webpageget = self.jenkinsweb + "job/" + newjobname + '/api/python?tree=builds[number]'
        jobdata = ast.literal_eval(urllib.urlopen(webpageget).read())


        startFound = 0
        countFound = 0
        removeidx = []
        # Look at builds array, subelement url, and add all data from URL
        for i in xrange(0,len(jobdata['builds'])):
            #If reached the max, then stop and add to remove list
            if countFound >= maxidx:
                removeidx.append(i)
                continue
            jobid = jobdata['builds'][i]['number']
            webpageget = self.jenkinsweb + "job/" + newjobname + '/' + str(jobid) + '/api/python?tree=result,timestamp,duration,actions[parameters[name,value]],building,estimatedDuration'
            # Retrieve actions->[parameters->[{name,value}]]  (array of name/value pair), convert to dictionary 'parameters'        
            # Retrieve duration (number)
            # Retrieve estimatedDuration (number)
            # Retrieve timestamp (Number)
            # Retrieve result (SUCCESS,FAILURE,UNSTABLE)
            # Retrieve building (true/false)
            onejobdata = ast.literal_eval(urllib.urlopen(webpageget).read())
            newparams = {}
            for act in onejobdata['actions']:
                if 'parameters' in act:
                    for param in act['parameters']:
                        if "name" in param:
                            newparams[param['name']] = param['value']

            if username != None and newparams['username'] != username:
                removeidx.append(i)
            else:
                startFound += 1
                if startFound <= startidx:
                    #Skip adding the data, and remove after the fact
                    removeidx.append(i)
                    continue 
                countFound += 1

            onejobdata.pop('actions',None)
            onejobdata['parameters'] = newparams
            jobdata['builds'][i]['data'] = onejobdata
            #To get Environment variables: returns {envMap : {param:value}} http://ihrp-hmc/jenkins/job/Get%20Sample/50/injectedEnvVars/api/python
            #{envMap : {totalcount, samplecount}}
            if envMap != None:
                webpageget = self.jenkinsweb + "job/" + newjobname + '/' + str(jobid) + '/injectedEnvVars/api/python'
                onejobdata = ast.literal_eval(urllib.urlopen(webpageget).read())
                if 'envMap' in onejobdata:
                    jobdata['builds'][i]['env'] = {}
                    for env in envMap:
                        if env in onejobdata['envMap']:
                            jobdata['builds'][i]['env'][env] = onejobdata['envMap'][env]

            # this is a string of what to look for in the Console output to get the current status, if building
            if latestStatus != None and jobdata['builds'][i]['data']['building']:
                webpageget = self.jenkinsweb + "job/" + newjobname + '/' + str(jobid) + '/consoleText'
                prog = re.compile(latestStatus)
                for line in urllib.urlopen(webpageget):
                    if prog.match(line):
                        jobdata['builds'][i]['status'] = line




        #iterate backwards to not delete so I have to shift
        for i in xrange(len(removeidx)-1,-1,-1):
            del jobdata['builds'][removeidx[i]]

        return jobdata

    '''Submits the job with alldata as parameters.
    Returns true if status code is 200 or 201, otherwise false
    '''
    def submitJob(self,alldata):
        newjobname = urllib.quote(self.jobname) #Should make spaces %20
        webpagesubmit = self.jenkinsweb + "job/" + newjobname + '/buildWithParameters'
        response = requests.post(url=webpagesubmit, data=alldata)
        success = (response.status_code == 200 or response.status_code == 201)
        if not success:
            print("Error with submitJob: "+response.text)
        return success

    def printIF(self,data):
        if self.printif:
            print data

    '''Analyzes the input data and makes sure it is in the format and constrainst set.
    Returns dictionary with 'result' = True if it is correct.  Also a 'data' field with only the data specified
    '''
    def analyzeInputs(self,expected,inputs):
        #expected = [{'name', 'type','min','max','choice'}], min max for string is length
        #inputs = {}
        result = True
        dataout = {}
        if not isinstance(inputs,dict):
            result = False
            self.printIF("inputs is not dictionary")
        if not isinstance(expected,list):
            result = False
            self.printIF("expected is not list")

        for exp in expected:
            if exp['name'] not in inputs:
                result = False
                self.printIF(exp['name'] + " is not in inputs")
            else:
                if exp['type'] == 'number':
                    if not isinstance(inputs[exp['name']],numbers.Number):
                        result = False
                        self.printIF(exp['name'] + " is not a number")
                    else:
                        if 'min' in exp:
                            if inputs[exp['name']] < exp['min']:
                                result = False
                                self.printIF(exp['name'] + " is not >= " + str(exp['min']))
                        if 'max' in exp:
                            if inputs[exp['name']] > exp['max']:
                                result = False
                                self.printIF(exp['name'] + " is not <= " + str(exp['max']))
                        if 'choice' in exp:
                            if inputs[exp['name']] not in exp['choice']:
                                result = False
                                self.printIF(exp['name'] + " is not in list of " + str(exp['choice']))
                elif exp['type'] == 'string':
                    if  not isinstance(inputs[exp['name']],basestring):#In python 3.x it needs to be str
                        result = False
                        self.printIF(exp['name'] + " is not a string")
                    else:
                        if 'min' in exp:
                            if len(inputs[exp['name']]) < exp['min']:
                                result = False
                                self.printIF(exp['name'] + " length is not >= " + str(exp['min']))
                        if 'max' in exp:
                            if len(inputs[exp['name']]) > exp['max']:
                                result = False
                                self.printIF(exp['name'] + " length is not <= " + str(exp['max']))
                        if 'choice' in exp:
                            if inputs[exp['name']] not in exp['choice']:
                                result = False
                                self.printIF(exp['name'] + " is not in list of " + str(exp['choice']))
                elif exp['type'] == 'dict':
                    if not isinstance(inputs[exp['name']],dict):
                        result = False
                        self.printIF(exp['name'] + " is not a dictionary")
                elif exp['type'] == 'array':
                    if not isinstance(inputs[exp['name']],list):
                        result = False
                        self.printIF(exp['name'] + " is not an array")
                    else:
                        if 'min' in exp:
                            if len(inputs[exp['name']]) < exp['min']:
                                result = False
                                self.printIF(exp['name'] + " length is not >= " + str(exp['min']))
                        if 'max' in exp:
                            if len(inputs[exp['name']]) > exp['max']:
                                result = False
                                self.printIF(exp['name'] + " length is not <= " + str(exp['max']))


                if result:
                    dataout[exp['name']] = inputs[exp['name']]

        return {'result':result,'data':dataout}

class SampleDownloadJob(LongJob):
    def __init__(self,printif=False,development=False):
        if development:
            jobtext = " Development"
        else:
            jobtext = ""
        super(SampleDownloadJob,self).__init__("Get Sample"+jobtext)
        self.baseexpected = [
            {'name': 'username', 'type': 'string', 'min':1, 'max':50},
            {'name': 'filename', 'type': 'string', 'min':1, 'max': 100},
            {'name': 'startdate', 'type': 'string', 'min':1, 'max': 100},
            {'name': 'enddate', 'type': 'string', 'min':1, 'max': 100},
            {'name': 'getSample', 'type': 'string', 'choice' : ['true','false']},
            {'name': 'db', 'type': 'string', 'choice' : ['twitter','instagram']}
        ]

        self.expected = copy.deepcopy(self.baseexpected)
        self.expected.append(
            {'name': 'query', 'type': 'string', 'min': 1, 'max': 500})

        self.sampleexpected = copy.deepcopy(self.expected)
        sample = {'name': 'sample', 'type': 'number', 'min': 1, 'max': 100000}
        sampletype = {'name': 'sampletype', 'type': 'string', 'choice': ['percent','number']}
        samplemethod = {'name': 'samplemethod', 'type': 'string', 'choice': ['random', 'first']}
        self.sampleexpected.append(sample)
        self.sampleexpected.append(sampletype)
        self.sampleexpected.append(samplemethod)

        self.jenkinsexpected = copy.deepcopy(self.baseexpected)
        querydict = {'name': 'query', 'type': 'dict'}
        downloadfileid = {'name': 'downloadfileid', 'type': 'number','min': 10000000, 'max':99999999}
        self.jenkinsexpected.append(querydict)
        self.jenkinsexpected.append(downloadfileid)

        self.jenkinssampleexpected = copy.deepcopy(self.jenkinsexpected)
        self.jenkinssampleexpected.append(sample)
        self.jenkinssampleexpected.append(sampletype)
        self.jenkinssampleexpected.append(samplemethod)
        self.jenkinssampleexpected.append({'name': 'samplefileid', 'type': 'number','min': 10000000, 'max':99999999})

        self.printif = printif

    def SubmitSample(self,myjson):
        outdata = False
        super(SampleDownloadJob,self).printIF(json.dumps(myjson))
        redata = super(SampleDownloadJob,self).analyzeInputs(self.expected,myjson)
        if redata['result']:
            super(SampleDownloadJob,self).printIF("Successful submitSample analysis")
            # Create file location - so it is recorded as an input to the job
            finalfilename = redata['data']['filename']
            downloadfile = finalfilename
            if redata['data']['getSample'] == 'true':
                # If sample then validate the sample also
                redata = super(SampleDownloadJob,self).analyzeInputs(self.sampleexpected,myjson)
                if redata['result']:
                    samplefile = SaveFile(origname=finalfilename)
                    samplefileid = samplefile.getFileID()
                    redata['data']['samplefileid'] = samplefileid
                    downloadfile = 'raw_' + finalfilename #since this isn't final file, then 
                else:
                    # Not setup right for sample so return
                    return outdata

            tempfile = SaveFile(origname=downloadfile)
            tempfileid = tempfile.getFileID()
            redata['data']['downloadfileid'] = tempfileid

            #Successful analysis, so submit
            outdata = super(SampleDownloadJob,self).submitJob(redata['data'])

        return outdata

    def RunSubmitSample(self,myjson):
        outdata = {'issuccess':'fail'}
        # If performing a run, then these extra ones are expected
        redata = super(SampleDownloadJob,self).analyzeInputs(self.jenkinsexpected,myjson)
        if redata['result']:
            super(SampleDownloadJob,self).printIF("RunSubmitSample analysis good")
            if redata['data']['getSample'] == 'true':
                # If sample then validate the sample also
                redata = super(SampleDownloadJob,self).analyzeInputs(self.jenkinssampleexpected,myjson)
                if not redata['result']:
                    return outdata
                super(SampleDownloadJob,self).printIF("Sample analysis good")
            outdata = downloadOrSampleJob(redata['data'])

        return outdata

    def GetSamples(self,myjson):
        #Allow myjson to have a start and max to define what to show
        defstart = 0
        if 'start' in myjson and isinstance(myjson['start'],numbers.Number):
            defstart = myjson['start']
        defmax = 30
        if 'max' in myjson and isinstance(myjson['max'],numbers.Number):
            defmax = myjson['max']
        envMap = ["totalcount", "samplecount", "error"]

        return super(SampleDownloadJob,self).getJobs(defstart,defmax,myjson['username'],envMap,"Processing:.*")

    def JenkinsRunSubmitSample(self):
        import json,os

        myjson = {}
        myjson['username'] = os.getenv('username', "")
        myjson['db'] = os.getenv('db', "")
        sample = os.getenv('sample', "0")
        if not sample.isdigit():
            sample = "0"
        myjson['sample'] = int(sample) #convert to int
        myjson['sampletype'] = os.getenv('sampletype', "")
        myjson['samplemethod'] = os.getenv('samplemethod', "")
        myjson['filename'] = os.getenv('filename', "")
        try:
            myjson['query'] = json.loads(os.getenv('query', "{}"))
        except AttributeError:
            myjson['query'] = {}
        myjson['startdate'] = os.getenv('startdate', "")
        myjson['enddate'] = os.getenv('enddate', "")
        myjson['getSample'] = os.getenv('getSample', "")
        samplefileid = os.getenv('samplefileid', "0")
        if not samplefileid.isdigit():
            samplefileid = "0"
        myjson['samplefileid'] = int(samplefileid)
        downloadfileid = os.getenv('downloadfileid', "0")
        if not downloadfileid.isdigit():
            downloadfileid = "0"
        myjson['downloadfileid'] = int(downloadfileid)
        return self.RunSubmitSample(myjson)


class TopicModelJob(LongJob):
    def __init__(self,printif=False):
        super(TopicModelJob,self).__init__("Topic Modeling")
        self.baseexpected = [
            {'name': 'username', 'type': 'string', 'min':1, 'max':50},
            {'name': 'description', 'type': 'string', 'min':0, 'max': 300},
            {'name': 'inputfileid', 'type': 'number','min': 10000000, 'max':99999999}
        ]
        modelfileid = {'name': 'modelfileid', 'type': 'number','min': 10000000, 'max':99999999}
        jsonfileid = {'name': 'jsonfileid', 'type': 'number','min': 10000000, 'max':99999999}
        self.modelfilecheck = [
            modelfileid
        ]

        self.jenkinsexpected = copy.deepcopy(self.baseexpected)
        self.jenkinsexpected.append(jsonfileid)
        self.jenkinsexpected.append(modelfileid)

        self.updateexpected = [
            jsonfileid,
            {'name': 'description', 'type': 'string', 'min':0, 'max': 300},
            {'name': 'topictext', 'type': 'array', 'min':50, 'max': 50},
        ]

        self.printif = printif

    '''This is called by Pyramid POST through HTTP
    '''
    def SubmitTopic(self,myjson):
        outdata = False
        super(TopicModelJob,self).printIF(json.dumps(myjson))
        redata = super(TopicModelJob,self).analyzeInputs(self.baseexpected,myjson)
        if redata['result']:
            super(TopicModelJob,self).printIF("Successful SubmitTopic analysis")
            # Create file location - so it is recorded as an input to the job
            redatamodel = super(TopicModelJob,self).analyzeInputs(self.modelfilecheck,myjson)
            if redatamodel['result']:
                redata['data']['modelfileid'] = redatamodel['data']['modelfileid']
            else:
                tempfile = SaveFile(origname='model.lda')
                tempfileid = tempfile.getFileID()
                redata['data']['modelfileid'] = tempfileid

            tempfile = SaveFile(origname='output.json')
            tempfileid = tempfile.getFileID()
            redata['data']['jsonfileid'] = tempfileid

            #Successful analysis, so submit
            outdata = super(TopicModelJob,self).submitJob(redata['data'])

        return outdata

    '''This is indirectly called by Jenkins
    Whatever is returned as a Dictionary is put into the Environment variables to be retrieved by Get method.
    '''
    def RunSubmitTopic(self,myjson):
        outdata = {'issuccess':'fail'}
        # If performing a run, then these extra ones are expected
        redata = super(TopicModelJob,self).analyzeInputs(self.baseexpected,myjson)
        if redata['result']:
            super(TopicModelJob,self).printIF("RunSubmitTopic analysis good")
            #Perform topic modelling on Jenkins
            #outdata = downloadOrSampleJob(redata['data'])

        return outdata

    def UpdateDescription(self,myjson):
        outdata = {'issuccess':'fail'}
        # If performing a run, then these extra ones are expected
        redata = super(TopicModelJob,self).analyzeInputs(self.updateexpected,myjson)
        if redata['result']:
            super(TopicModelJob,self).printIF("UpdateDescription analysis good")
            #Perform topic modelling on Jenkins
            #outdata = downloadOrSampleJob(redata['data'])

        return outdata


    '''This is called by Pyramid POST through HTTP
    '''
    def GetTopics(self,myjson):
        #Allow myjson to have a start and max to define what to show
        defstart = 0
        if 'start' in myjson and isinstance(myjson['start'],numbers.Number):
            defstart = myjson['start']
        defmax = 30
        if 'max' in myjson and isinstance(myjson['max'],numbers.Number):
            defmax = myjson['max']
        envMap = None #["totalcount", "samplecount"] #UPDATE TO WHAT ENV TO CAPTURE

        return super(TopicModelJob,self).getJobs(defstart,defmax,myjson['username'],envMap)

    '''This is directly called by Jenkins
    '''
    def JenkinsRunSubmitTopic(self):
        import json,os

        myjson = {}
        myjson['username'] = os.getenv('username', "")
        myjson['description'] = os.getenv('description', "")
        modelfileid = os.getenv('modelfileid', "0")
        if not modelfileid.isdigit():
            modelfileid = "0"
        myjson['modelfileid'] = int(modelfileid)
        jsonfileid = os.getenv('jsonfileid', "0")
        if not jsonfileid.isdigit():
            jsonfileid = "0"
        myjson['jsonfileid'] = int(jsonfileid)
        inputfileid = os.getenv('inputfileid', "0")
        if not inputfileid.isdigit():
            inputfileid = "0"
        myjson['inputfileid'] = int(inputfileid)
        return self.RunSubmitTopic(myjson)

class DatasetSampleDownloadJob(LongJob):
    def __init__(self,printif=False,development=False):
        if development:
            jobtext = " Development"
        else:
            jobtext = ""
        super(DatasetSampleDownloadJob,self).__init__("Get Dataset Sample"+jobtext)
        db = {'name': 'db', 'type': 'string', 'choice' : ['twitter','instagram']}
        uname = {'name': 'username', 'type': 'string', 'min':1, 'max':50}
        predtotoal = {'name': 'predtotal', 'type': 'number', 'min':0}
        predsample = {'name': 'predsample', 'type': 'number', 'min':0}
        self.expected = [
            uname,
            {'name': 'filename', 'type': 'string', 'min':1, 'max': 100},
            {'name': 'allquery', 'type': 'string', 'min':1, 'max': 1000},
            {'name': 'allsample', 'type': 'string', 'min':1, 'max': 10000},
            db,
            predsample,
            predtotoal
        ]

        self.jenkinsexpected = [
            uname,
            {'name': 'samplefileid', 'type': 'number','min': 10000000, 'max':99999999},
            {'name': 'allquery', 'type': 'list'},
            {'name': 'allsample', 'type': 'dict'},
            db,
            predsample,
            predtotoal
        ]

        self.printif = printif

    def SubmitSample(self,myjson):
        outdata = False
        super(DatasetSampleDownloadJob,self).printIF(json.dumps(myjson))
        redata = super(DatasetSampleDownloadJob,self).analyzeInputs(self.expected,myjson)
        if redata['result']:
            super(DatasetSampleDownloadJob,self).printIF("Successful submitSample for Dataset analysis")
            # Create file location - so it is recorded as an input to the job
            finalfilename = redata['data']['filename']

            tempfile = SaveFile(origname=finalfilename)
            tempfileid = tempfile.getFileID()
            redata['data']['samplefileid'] = tempfileid

            #Successful analysis, so submit
            outdata = super(DatasetSampleDownloadJob,self).submitJob(redata['data'])

        return outdata

    def RunSubmitSample(self,myjson):
        outdata = {'issuccess':'fail'}
        # If performing a run, then these extra ones are expected
        redata = super(DatasetSampleDownloadJob,self).analyzeInputs(self.jenkinsexpected,myjson)
        if redata['result']:
            super(DatasetSampleDownloadJob,self).printIF("RunSubmitSample for Dataset analysis good")

            finda = FindRuleCounts(redata['data']['db'])
            outdata = finda.findAll(groups=[],allsample=redata['data']["allsample"],allQuery=redata['data']['allquery'],isreturn=False,saveid=redata['data']['samplefileid'])

        return outdata

    def GetSamples(self,myjson):
        #Allow myjson to have a start and max to define what to show
        defstart = 0
        if 'start' in myjson and isinstance(myjson['start'],numbers.Number):
            defstart = myjson['start']
        defmax = 30
        if 'max' in myjson and isinstance(myjson['max'],numbers.Number):
            defmax = myjson['max']

        #Extra env variables output by findAll
        #{"issuccess":"success", "data":outdata, 'samplesize':finalsamplesize, 'totalcount':outputRuleTotal, 'diffsample':diffsample,'difftotal':difftotal}
        envMap = ["totalcount", "samplesize", "difftotal", "diffsample"] #Pull these from the environment variables created by python

        return super(DatasetSampleDownloadJob,self).getJobs(defstart,defmax,myjson['username'],envMap,"Processing:.*")

    def JenkinsRunSubmitSample(self):
        import json,os

        myjson = {}
        myjson['username'] = os.getenv('username', "")

        myjson['filename'] = os.getenv('filename', "")

        myjson['db'] = os.getenv('db', "")

        try:
            myjson['allquery'] = json.loads(os.getenv('allquery', "{}"))
        except AttributeError:
            myjson['allquery'] = []

        try:
            myjson['allsample'] = json.loads(os.getenv('allsample', "{}"))
        except AttributeError:
            myjson['allsample'] = {}

        samplefileid = os.getenv('samplefileid', "0")
        if not samplefileid.isdigit():
            samplefileid = "0"
        myjson['samplefileid'] = int(samplefileid)

        predsample = os.getenv('predsample', "0")
        if not predsample.isdigit():
            predsample = "0"
        myjson['predsample'] = int(predsample)

        predtotal = os.getenv('predtotal', "0")
        if not predtotal.isdigit():
            predtotal = "0"
        myjson['predtotal'] = int(predtotal)

        return self.RunSubmitSample(myjson)


class AcceptAndWaitGNIPJob(LongJob):
    def __init__(self,printif=False):
        super(AcceptAndWaitGNIPJob,self).__init__("Accept GNIP Twitter Estimate for Download")
        self.expected = [
            {'name': 'GNIP Job ID', 'type': 'string', 'min':1, 'max': 100},
            {'name': 'run_download', 'type': 'string', 'choice':['yes','no']},
            {'name': 'Month', 'type': 'string', 'choice':['01','02','03','04','05','06','07','08','09','10','11','12']},
            {'name': 'Year', 'type': 'string', 'choice':['2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020']},
            {'name': 'Project Name', 'type': 'string', 'min':1, 'max': 100},
            {'name': 'jobname', 'type': 'string', 'min':1, 'max': 300},
            {'name': 'update_active_rules', 'type': 'string', 'choice':['yes','no']}
        ]

        self.printif = printif

    def verifyInput(self,myjson):
        redata = super(AcceptAndWaitGNIPJob,self).analyzeInputs(self.expected,myjson)
        return redata['result']


    def SubmitJob(self,myjson):
        outdata = False
        super(AcceptAndWaitGNIPJob,self).printIF(json.dumps(myjson))
        redata = super(AcceptAndWaitGNIPJob,self).analyzeInputs(self.expected,myjson)
        if redata['result']:
            super(AcceptAndWaitGNIPJob,self).printIF("Successful submitSample for Dataset analysis")
            #Successful analysis, so submit
            outdata = super(AcceptAndWaitGNIPJob,self).submitJob(redata['data'])

        return outdata

    def GetJobs(self,myjson):
        #Allow myjson to have a start and max to define what to show
        defstart = 0
        if 'start' in myjson and isinstance(myjson['start'],numbers.Number):
            defstart = myjson['start']
        defmax = 30
        if 'max' in myjson and isinstance(myjson['max'],numbers.Number):
            defmax = myjson['max']

        #Extra env variables output by findAll

        return super(AcceptAndWaitGNIPJob,self).getJobs(defstart,defmax)

class ReHydrateJob(LongJob):
    def __init__(self,printif=False):
        super(ReHydrateJob,self).__init__("ReHydrate Twitter")
        self.expected = [
            {'name': 'inputpath', 'type': 'string', 'min':1, 'max': 300},
            {'name': 'outputpath', 'type': 'string', 'min':1, 'max': 300},
        ]

        self.printif = printif

    def verifyInput(self,myjson):
        redata = super(ReHydrateJob,self).analyzeInputs(self.expected,myjson)
        return redata['result']


    def SubmitJob(self,myjson):
        outdata = False
        super(ReHydrateJob,self).printIF(json.dumps(myjson))
        redata = super(ReHydrateJob,self).analyzeInputs(self.expected,myjson)
        if redata['result']:
            super(ReHydrateJob,self).printIF("Successful submitHydrate")
            #Successful analysis, so submit
            outdata = super(ReHydrateJob,self).submitJob(redata['data'])

        return outdata

    def GetJobs(self,myjson):
        #Allow myjson to have a start and max to define what to show
        defstart = 0
        if 'start' in myjson and isinstance(myjson['start'],numbers.Number):
            defstart = myjson['start']
        defmax = 30
        if 'max' in myjson and isinstance(myjson['max'],numbers.Number):
            defmax = myjson['max']

        #Extra env variables output by findAll

        return super(ReHydrateJob,self).getJobs(defstart,defmax)


if __name__ == '__main__':
    finalfilename = "blah.zip"

    tempfile = SaveFile(origname=finalfilename)
    tempfileid = tempfile.getFileID()

    samples = DatasetSampleDownloadJob(True)
    myjson = {'allquery':
        [
            {'query': {'mrv':478},
             'datestart': '1/1/2015',
             'dateend': '1/8/2015'},
            {'query': {'mrv':478},
             'datestart': '01/9/2015',
             'dateend': '1/21/2015'},
        ],
        'allsample':
        {
            'type':'tag',
            'allsample':
            {
                'antismoking':231,
                'cdc tips':25
            },
            'alltotal':
            {
                'antismoking':474,
                'cdc tips':51
            }
        },
        'samplefileid':tempfileid,
        'username':'dsabin2',
        'db':'twitter',
        'predtotal':700,
        'predsample':50
    }
    outdata = samples.RunSubmitSample(myjson)
    print "Save file id=" + str(tempfileid)
    print(json.dumps(outdata))
    # To test analyzeInputs() function:
    # gooddata = {'username': 'dsabin2', 'sample': 1, 'totalsize':10, 'sampletype': 'percent', 'samplemethod': 'random'}
    # baddata = [{'username': 'dsabin2', 'sample': 0, 'totalsize':10, 'sampletype': 'percent', 'samplemethod': 'random'},
    #  {'username': '', 'sample': 1, 'totalsize':10, 'sampletype': 'percent', 'samplemethod': 'random'},
    #  {'username': 'dsabin2', 'sample': 1, 'totalsize':10, 'sampletype': 'percen', 'samplemethod': 'random'},
    #  {'username': '012345678901234567890123456789012345678901234567890', 'sample': 1, 'totalsize':10, 'sampletype': 'percent', 'samplemethod': 'random'},
    #  {'username': 'dsabin2', 'sample': 1, 'totalsize':900000000, 'sampletype': 'percent', 'samplemethod': 'random'},
    #  {'username': 1, 'sample': 1, 'totalsize':10, 'sampletype': 'percent', 'samplemethod': 'random'},
    #  {'username': 'dsabin2', 'sample': '1', 'totalsize':10, 'sampletype': 'percent', 'samplemethod': 'random'}
    # ]
    # samples.SubmitSample(gooddata)
    # for data in baddata:
    #     samples.SubmitSample(data)
    #samples.GetSamples()

