from datetime import datetime
'''
Written by DWS.
'''
def utcFromTweetId(twID):
    '''Simple conversion from Tweet ID to Date object, then to common date-string.
    '''
    utctime = ((twID >> 22) + 1288834974657)/1000.0
    dateObj = datetime.utcfromtimestamp(utctime)
    datestr = dateObj.strftime("%m/%d/%Y %H:%M:%S")
    return datestr
