
from etl.twitter.format_output_tweets import format_tweet, format_header

def formatTweet(unformatted_tweet,db):
    ''' Takes an unformated tweet (dict) and a MongoDatabase object and returns a formated Tweet. '''
    return format_tweet(unformatted_tweet,db.i_tags,db.i_rules)

def formatHeader(maxhtag,maxuser,maxrule,maxtag,db):
    return format_header(db.fields.values(),max_htags=maxhtag,max_user_mentions=maxuser,max_matching_rules=maxrule,max_matching_tags=maxtag)