#!/usr/bin/env python
'''
Written by RN.  Edited by DS.
'''
import urllib2
import base64
import json
import sys
import string
import os.path
import cStringIO
import gzip
import datetime
import time
import re
import shutil
from longjob import AcceptAndWaitGNIPJob

UN = 'binns-steven@norc.org'
PWD = 'HMCollabNORC'
INITIAL_GNIP_WEB = 'https://gnip-api.gnip.com/historical/powertrack/accounts/UIC/publishers/twitter'

#Wraps the necessary GET or PUT request in a wrapper using urllib2
class RequestWithMethod(urllib2.Request):
    def __init__(self, url, method, headers={}):
        self._method = method
        urllib2.Request.__init__(self, url, headers)

    def get_method(self):
        if self._method:
            return self._method
        else:
            return urllib2.Request.get_method(self) 

#Convert from Zulu String to datetime
def convertDate(s):
    return datetime.datetime(*map(int, re.split('[^\d]', s)[:-1]))

def createDateString(d):
    return "".join(["%d"%d.year,"%02d"%d.month,"%02d"%d.day,"%02d"%d.hour,"%02d"%d.minute])

def getJobPath():
    webSubmitDir = 'webTempRules/'
    rulesdir = "E:/Data/PRODUCTION/rules/"
    return [rulesdir,webSubmitDir]

def submitWebJob(myjson):
    '''Submit a Job from Webpage
    '''

    #rulesdir = "H:/code/rules/"
    jobpath = getJobPath()
    jobpathjoin = ''.join(jobpath)


    #Write to JSON in rulesdir
    if not os.path.exists(jobpathjoin):
        os.makedirs(jobpathjoin)
    dateStr = createDateString(datetime.datetime.now())+'_'
    rulesFileName = jobpath[1]+dateStr+myjson['filename']
    filePath = jobpath[0]+rulesFileName

    with open(filePath, 'wb') as outfile:
        outfile.write(myjson['file'])

    sdate = convertDate(myjson['start'])
    startstr = createDateString(sdate)

    edate = convertDate(myjson['end'])
    endstr = createDateString(edate)

    #postNewJob("201408010000","201409080000","new_twitter_legacy_rules.json")
    return postNewJob(startstr,endstr,rulesFileName,True)

def getWebJobFile(jobname):
    jobpath = getJobPath()

    #Remove UIC_ at beginning, and _date at end
    #Example: UIC_webTempRules/201601111713_twitter_master_rules.json_20160111171326
    startremove = "UIC_" #Used only for the length
    endremove = "_20160111171326" #This is an example to just get the length
    if len(jobname) > ( len(startremove) + len(endremove) ):
        jobnamepath = jobname[len(startremove):-len(endremove)]

        if jobnamepath.startswith(jobpath[1]):
            #Is a web job
            return jobpath[0] + jobnamepath

    return None #Didn't find jobpathname

#============================================================================
#postNewJob("201409080000","201409150000")
#postNewJob("201408010000","201409080000","new_twitter_legacy_rules.json")
#YYYYMMDDHHMM in UTC
def postNewJob(fromDate, toDate, rulesFileName="twitter_master_rules.json",ifprint=True):
    
    rulesdir = getJobPath()[0] + "/"
    #rulesdir = "h:/code/rules/"


    url = INITIAL_GNIP_WEB+'/jobs.json'
    publisher = "Twitter"
    streamType = "track_v2"
    dataFormat = "activity_streams"
    d = datetime.datetime.now()
    jobTitle = "".join(["UIC_",rulesFileName,"_","%d"%d.year,"%02d"%d.month,"%02d"%d.day,"%02d"%d.hour,"%02d"%d.minute,"%02d"%d.second])
    rulesFile = open(rulesdir+rulesFileName, "r")
    rules = rulesFile.read()

    jobString = '{"publisher":"' + publisher + '","streamType":"' + streamType + '","dataFormat":"' + dataFormat + '","fromDate":"' + fromDate + '","toDate":"' + toDate + '","title":"' + jobTitle + '","rules":' + rules + '}'

    base64string = base64.encodestring('%s:%s' % (UN, PWD)).replace('\n', '')
    req = urllib2.Request(url=url, data=jobString)
    req.add_header('Content-type', 'application/json')
    req.add_header("Authorization", "Basic %s" % base64string)

    try:
        response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        errormsg = e.read()
        printIF(ifprint,errormsg)
        sys.stdout.flush()
        entry = json.loads(errormsg)
        errormsgdetail = entry["statusMessage"]+"; Detail:\n"+json.dumps(entry['rules']["detail"],indent=4)
        return {"error":errormsgdetail,"status":"error","jobURL":""}
    else:
        the_page = response.read()
        printIF(ifprint,the_page)
        entry = json.loads(the_page)
        return {"error":"","status":"success","jobURL":entry['jobURL']}

#createAcceptDownload("201501060000","201501160000", folder="January-2015-Master/")
#createAcceptDownload("201402020000","201404070000", folder="2014_cdctips_0202_0406/", rulesFileName="h:/data/code/rules/cdctips2014.json")
def createAcceptDownload(fromDate, toDate, folder="", rulesFileName="e:/data/PRODUCTION/rules/twitter_master_rules.json"):
    joburl = postNewJob(fromDate, toDate, rulesFileName)['jobURL']
    print joburl
    while getStatus(joburl) != 'quoted':
        time.sleep(300)
    postAccept(joburl)
    while getStatus(joburl) != 'delivered':
        time.sleep(900)
    jobinfo = checkJob(joburl)
    dataURL = jobinfo['results']['dataURL']
    getData(dataURL, folder=folder)

def accept(jobid,jobname,myjson):
    #Backup current master rules
    #Then copy web to: twitter_master_rules.json
    checkname = 'run_upload'
    startDownload = False
    if checkname in myjson and myjson[checkname]:
        startDownload = True
        acceptJob = AcceptAndWaitGNIPJob(True)
        if not acceptJob.verifyInput(myjson):
            return {'issuccess':'fail', 'error':'Inputs are not correct.'}

    if len(jobname) > 0:
        jobpath = getWebJobFile(jobname)
        #Check that file exists
        if os.path.exists(jobpath):
            rulesLoc=getJobPath()[0] + "/acceptedDownload/"
            basepath = os.path.basename(jobpath)
            rulesplit = os.path.splitext(basepath)
            d = datetime.datetime.now()
            submitfile = "".join([ rulesLoc,rulesplit[0],"_","%d"%d.year,"%02d"%d.month,"%02d"%d.day,"%02d"%d.hour,"%02d"%d.minute,"%02d"%d.second,rulesplit[1] ])
            #shutil.copyfile(rulesFileName,backupfile)
            shutil.copyfile(jobpath,submitfile)
            print "Saved " + jobpath + " to file " + submitfile
        else:
            print "File " + jobpath + " does not exist, so not copying it to acceptDownload directory."
        sys.stdout.flush()
    joburl = INITIAL_GNIP_WEB + '/jobs/' + jobid + '.json'
    print joburl
    outdata = {'issuccess':'success','error':'Unknown'}
    outdata = postAccept(joburl)
    if outdata['issuccess'] == 'success':
        #Start the Jenkins Job
        if startDownload:
            aoutdata = acceptJob.SubmitJob(myjson)
            if not aoutdata:
                outdata = {'issuccess':'fail','error':'Error submitting to Jenkins.'}

    return outdata


def acceptAndWait(jobid):
    joburl = INITIAL_GNIP_WEB+'/jobs/' + jobid + '.json'
    print joburl
    while getStatus(joburl) != 'delivered':
        print "Still not delivered (waiting 15 minutes)..."
        sys.stdout.flush()
        time.sleep(900) #wait 15 minutes
    
def getStatus(joburl):
    base64string = base64.encodestring('%s:%s' % (UN, PWD)).replace('\n', '')

    req = RequestWithMethod(joburl, 'GET')
    req.add_header('Content-type', 'application/json')
    req.add_header("Authorization", "Basic %s" % base64string)

    try:
        #Send the request
        response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        #Print any error found
        print e.read()
    else:
        #If request succeeded, read the page
        the_page = response.read()
        entry = json.loads(the_page)
        return entry['status']
    
def checkJob(joburl,ifprint=True):
    base64string = base64.encodestring('%s:%s' % (UN, PWD)).replace('\n', '')

    req = RequestWithMethod(joburl, 'GET')
    req.add_header('Content-type', 'application/json')
    req.add_header("Authorization", "Basic %s" % base64string)

    try:
        #Send the request
        response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        #Print any error found
        printIF(ifprint,e.read())
    else:
        #If request succeeded, read the page
        the_page = response.read()
        entry = json.loads(the_page)
        return entry

def checkJob2(joburl,ifprint=True):

    base64string = base64.encodestring('%s:%s' % (UN, PWD)).replace('\n', '')

    req = RequestWithMethod(joburl, 'GET')
    req.add_header('Content-type', 'application/json')
    req.add_header("Authorization", "Basic %s" % base64string)

    try:
        #Send the request
        response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        #Print any error found
        printIF(ifprint,e.read())
    else:
        #If request succeeded, read the page
        the_page = response.read()
        entry = json.loads(the_page)
        status = entry['status']
        #Display the title and the dates included
        printIF(ifprint,"job title: "+entry['title']+"\tfrom "+entry['fromDate']+" to "+entry['toDate'])
        if status == 'estimating' or status == 'running':
            printIF(ifprint,status+": "+str(entry['percentComplete'])+"% complete\n")
        else:
            jobURL = entry['jobURL']
            if status == 'quoted':
                #Display the quote, if waiting for approval
                printIF(ifprint,str(entry['quote']))
                printIF(ifprint,"jobURL: "+jobURL+"\n")
            elif status == 'delivered':
                #Display the dataURL if delivered
                info = getJobInfo(jobURL)
                printIF(ifprint,"delivered:\t"+info['results']['dataURL']+"\n")
            else:
                printIF(ifprint,"job status: "+status)

#Uses the job URL to check the status of all jobs listed and print relevant information
def checkJobs(includeDelivered=True,printall=False,ifprint=True):
    url = INITIAL_GNIP_WEB+'/jobs.json'

    base64string = base64.encodestring('%s:%s' % (UN, PWD)).replace('\n', '')

    req = RequestWithMethod(url, 'GET')
    req.add_header('Content-type', 'application/json')
    req.add_header("Authorization", "Basic %s" % base64string)

    try:
        #Send the request
        response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        #Print any error found
        printIF(ifprint,e.read())
    else:
        #If request succeeded, read the page
        the_page = response.read()
        if printall:
            print the_page
        #Load it into a JSON interpreter
        mydict = json.loads(the_page)
        jobs = mydict['jobs']

        returnData = []#Used for the web
        #For each entry in the job list
        for entry in jobs:
            #Find the status
            status = entry['status']
            #Exclude jobs that have been rejected
            if status != 'rejected':
                #Display the title and the dates included
                jobURL = entry['jobURL']
                jobJson = {'jobid':entry['uuid'],'jobURL':jobURL,'status':entry['status'],'percentComplete':entry['percentComplete'],'quote':{},'title':entry['title'],"from":entry['fromDate'],"to":entry['toDate'],"result":{}}#Used for the web
                if includeDelivered or status !="delivered":
                    printIF(ifprint,"job title: "+entry['title']+"\tfrom "+entry['fromDate']+" to "+entry['toDate'])
                    printIF(ifprint,"jobURL: "+jobURL)
                if status == 'estimating' or status == 'running':
                    printIF(ifprint,status+": "+str(entry['percentComplete'])+"% complete")
                    returnData.append(jobJson)#Used for the web
                else:
                    info = getJobInfo(jobURL,False)
                    if info:
                        if status == 'quoted':
                            #Display the quote, if waiting for approval
                            jobJson['quote'] = info['quote']#Used for the web
                            printIF(ifprint,str(info['quote'])+"\n")
                        elif status == 'delivered':
                            #Display the dataURL if delivered
                            if includeDelivered:
                                printIF(ifprint,"delivered:\t"+info['results']['dataURL']+"\n")
                                printIF(ifprint,str(info['quote'])+"\n")
                                printIF(ifprint,str(info['results'])+"\n")
                                jobJson['quote'] = info['quote']#Used for the web
                                jobJson['result'] = info['results']#Used for the web
                        else:
                            printIF(ifprint,"job status: "+status)
                    returnData.append(jobJson)#Used for the web
        #Used for the web
        return {"data":returnData}

#Give JobID
def postAccept(joburl):
    if "https" not in joburl:
        joburl = INITIAL_GNIP_WEB+"/jobs/"+joburl
    if ".json" not in joburl:
        joburl = joburl + ".json"
    return postReply(joburl, 'accept')


def postReject(jobURL):
    postReply(jobURL, 'reject')


#Post the appropriate reply for the specified job ('accept' or 'reject' only) and print the response
def postReply(jobURL, reply,ifprint=True):

    choice = reply

    payload = '{"status":"' + choice + '"}'

    base64string = base64.encodestring('%s:%s' % (UN, PWD)).replace('\n', '')
    req = urllib2.Request(url=jobURL, data=payload)
    req.add_header('Content-type', 'application/json')
    req.add_header("Authorization", "Basic %s" % base64string)
    req.get_method = lambda: 'PUT'

    try:
        response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        errormsg = e.read()
        printIF(ifprint,errormsg)
        entry = json.loads(errormsg)
        return {"error":entry["reason"],"issuccess":"fail","jobURL":""}
    else:
        the_page = response.read()
        printIF(ifprint,the_page)
        entry = json.loads(the_page)
        return {"error":"","issuccess":"success","jobURL":entry['jobURL']}


#Return the dictionary of information for the given job
def getJobInfo(jobURL,ifprint=True):
    base64string = base64.encodestring('%s:%s' % (UN, PWD)).replace('\n', '')

    req = RequestWithMethod(jobURL, 'GET')
    req.add_header('Content-type', 'application/json')
    req.add_header("Authorization", "Basic %s" % base64string)

    try:
        response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        printIF(ifprint,e.read())
    else:        
        the_page = response.read()
        mydict = json.loads(the_page)
        return mydict


#Downloads all datafiles for the specified job (by dataURL)
#Places in current folder, with possible subfolder specified, including a /
#getData("https://historical.gnip.com:443/accounts/UIC/publishers/twitter/historical/track/jobs/e185hz9xdb/results.json","September-2014-Master/")
def getData(dataURL, folder=""):
    base64string = base64.encodestring('%s:%s' % (UN, PWD)).replace('\n', '')

    req = RequestWithMethod(dataURL, 'GET')
    req.add_header('Content-type', 'application/json')
    req.add_header("Authorization", "Basic %s" % base64string)

    #Use the dataURL to find the list of download links
    try:
        response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        print e.read()
    else:        
        the_page = response.read()
        mydict = json.loads(the_page)
        fileList = mydict['urlList']

        out_folder = "e:/data/rawdata/gnip/twitterhistoricalpowertrack/"+folder+"/"
        if not os.path.isdir(out_folder):
		    os.makedirs(out_folder)
        #Iterate through list of download links.
        for item in fileList:
            splitItem = string.split(item, "_")
            mydatetime0 = splitItem[3]
            mydatetime1 = string.split(mydatetime0, "/")
            filename = out_folder + "twitter_" + "_".join(mydatetime1)+".json"
            #Determine the output file name, based on specified folder,
            #as well as job id and start date and time
            print "Processing ",filename

            #Only if the file does not yet exist (in case needing to restart)
            if not os.path.isfile(filename):
                #Download the data
                try:
                    response = urllib2.urlopen(item)
                except urllib2.HTTPError as e:
                    print e.read()
                else:
                    #Open the output file
                    myfile = open(filename, "w")
                    #Check if encoded
                    content = str(response.info().get('Content-Type'))
                    if "gzip" in content or "gz" in content:
                        #If gzipped, get the data
                        buf = cStringIO.StringIO( response.read())
                        #Process it
                        f = gzip.GzipFile(fileobj=buf)
                        #Read the decoded data
                        data = f.read()
                        print "Now unzipped: "+str(len(data))
                        #Write it to the output file
                        myfile.write(data)
                    else:
                        #If not encoded (doesn't happen), just write it
                        myfile.write(response.read())
                myfile.close()

#createEstimateCounts("201410010000","201410080000","h:/data/code/rules/lcc_low1.txt","h:/data/rawdata/gnip/twitterhistoricalpowertrack/lcc_counts.txt")
def createEstimateCounts(fromDate, toDate, textRules, outfilename):
    outfile = open(outfilename, "a")
    filerules = open(textRules, "r")
    myjobs = []
    
    for rule in filerules:
        if rule.strip() != "":
            myresponse = postNewJobOneRule(fromDate, toDate, rule.strip())
            if "error" in myresponse or myresponse["status"]=="error":
                print rule+":\t"+myresponse
                break
            else:
                myjobs.append(myresponse["jobURL"])
    for joburl in myjobs:
        while getStatus(joburl) != 'quoted':
            time.sleep(30)
        jobinfo = checkJob(joburl)
        name = jobinfo['title'].split("-")
        outfile.write(name[0]+"\t"+str(jobinfo['quote']['estimatedActivityCount']))
    
def downloadJob(jobid,folder):
    #myjson = {'data'} is the jobid
    joburl = INITIAL_GNIP_WEB+"/jobs/"+jobid+".json"
    jobinfo= checkJob(joburl)
    print json.dumps(jobinfo)
    if jobinfo['status'] == 'delivered':
        print "Processing the delivery"
        dataURL = jobinfo['results']['dataURL']
        getData(dataURL, folder=folder)

def checkJobWeb(myjson):
    #myjson = {'data'} is the jobid
    joburl = INITIAL_GNIP_WEB+"/jobs/"+myjson['data']+".json"
    jobinfo= checkJob2(joburl)
    quote = None
    if jobinfo['status'] == 'quoted':
        quote = jobinfo['quote']['estimatedActivityCount']
    if jobinfo['status'] == 'delivered':
        quote = jobinfo['quote']
        #results = jobinfo['results']
    #jobJson = {'jobid':myjson['job'],'status':entry['status'],'percentComplete':entry['percentComplete'],'quote':quote,"from":entry['fromDate'],"to":entry['toDate'],"result":results}#Used for the web
    return {'status':'success','data':{
        'state':jobinfo['status'],'jobid':jobinfo['uuid'],'totalcount':quote,'percent':jobinfo['percentComplete']
        },
        'msg':""}
    #return jobJson

#web request for single rule
def postNewJobOneRuleWeb(myjson):
    sdate = convertDate(myjson['start'])
    fromDate = createDateString(sdate)

    edate = convertDate(myjson['end'])
    toDate = createDateString(edate)
    
    ruleText = myjson['search']
    #theinput = {'start':myjson['start'],'end':myjson['end'],'search':myjson['search']}
    if ruleText.strip() != "":
        myresponse = postNewJobOneRule(fromDate, toDate, ruleText.strip())
        if "error" in myresponse or myresponse["status"]=="error":
            print ruleText+":\t"+myresponse
            return {'status':'fail','msg':ruleText+":\t"+myresponse}
        else:
            jobid = myresponse["uuid"]
    #while getStatus(joburl) != 'quoted':
    #    time.sleep(5)
    #jobinfo = checkJob(joburl)
    #name = jobinfo['title'].split("-")
    return {'status':'success','jobid':jobid}
    
#postNewJobOneRule("201410010000", "201410080000", "blunt lang:en -(trauma James perfectly speaking spoken weapon statement declaration attack assault)")
def postNewJobOneRule(fromDate, toDate, ruleText):
    url = INITIAL_GNIP_WEB+'/jobs.json'
    publisher = "Twitter"
    streamType = "track_v2"
    dataFormat = "activity_streams"
    d = datetime.datetime.now()
    jobTitle = "-".join(["%d"%d.year,"%d"%d.month,"%d"%d.day,"%d"%d.hour,"%d"%d.minute,"%d"%d.second,"%d"%d.microsecond])
    #TODO: Might need to do a double quote escaping
    rules = "[{\"value\":\""+ruleText+"\"}]"
    print rules
    jobString = '{"publisher":"' + publisher + '","streamType":"' + streamType + '","dataFormat":"' + dataFormat + '","fromDate":"' + fromDate + '","toDate":"' + toDate + '","title":"' + jobTitle + '","rules":' + rules + '}'

    base64string = base64.encodestring('%s:%s' % (UN, PWD)).replace('\n', '')
    req = urllib2.Request(url=url, data=jobString)
    req.add_header('Content-type', 'application/json')
    req.add_header("Authorization", "Basic %s" % base64string)

    try:
        response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        print e.read()
    else:
        the_page = response.read()
        print the_page
        entry = json.loads(the_page)
        return entry

def printIF(aif,mytext):
    if aif:
        print mytext