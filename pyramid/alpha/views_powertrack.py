'''
Written by DWS.
'''
from pyramid.view import view_config
import json
from shared.get_powertrack import submitWebJob
from shared.get_powertrack import checkJobs
from shared.get_powertrack import getWebJobFile
from shared.get_powertrack import accept
from datetime import datetime
import re
import sys
import os
import StringIO

@view_config(route_name='submitJob', renderer="json")
def submitJob(request):
    myjson = request.json_body
    return submitWebJob(myjson)

@view_config(route_name='downloadJob')
def downloadJob(request):
    myjson = request.params

    filepath = getWebJobFile(myjson["job"])
    #myjson = {filename, data, startdate, enddate, debug}
    rulesdir = "H:/Data/code/rules/"
    #rulesdir = "H:/code/rules/"

    if 'debug' in myjson:
        if myjson['debug'] == 'remote':
            #for remote the drive mapping is different, H: is \\ihrp-hmc\Data
            rulesdir = "H:/code/rules/"

    if filepath == None or not os.path.isfile(filepath):
        #If file not found, then raise error
        raise IOError

    request.response.content_type = 'application/force-download'
    filename = os.path.basename(filepath)
    filesize = os.path.getsize(filepath)
    request.response.content_disposition = 'attachment; filename="' + filename + '"'
    request.response.body_file = open(filepath,'rb')
    request.response.content_length = filesize # must be after body_file set (for some reason)
    return request.response





@view_config(route_name='checkJobs', renderer="json")
def webcheckJobs(request):
    return checkJobs(includeDelivered=True,printall=False,ifprint=False)


@view_config(
    route_name='submitAccept', 
    permission='edit',
    renderer="json"
    )
def submitAccept(request):

    myparams = request.params
    if "jobname" in myparams:
        myjson = myparams
    else:
        #load input as json
        myjson = request.json_body
    return accept(myjson['GNIP Job ID'],myjson['jobname'],myjson)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: views_powertrack.py submit|check"
    elif sys.argv[1] == 'submit':
        data = [
                {"value":"\"truth ad\" OR \"truth ads\" OR \"truth commercial\" OR \"truth commercials\" OR \"truth campaign\" OR \"truth PSA\" OR \"truth PSAs\"","tag":"Legacy Campaign"},
                {"value":"\"social smoking\"","tag":"Legacy Campaign"},
                {"value":"socialsmoking","tag":"Legacy Campaign"},
                {"value":"itsatrap","tag":"Legacy Campaign"},
                {"value":"\"it's a trap\"","tag":"Legacy Campaign"},
                {"value":"\"its a trap\"","tag":"Legacy Campaign"},
                {"value":"pukingunicorn","tag":"Legacy Campaign"},
                {"value":"puking unicorn","tag":"Legacy Campaign"},
                {"value":"vomitingunicorn","tag":"Legacy Campaign"},
                {"value":"vomiting unicorn","tag":"Legacy Campaign"},
                {"value":"TheRealRyanHiga (ad OR commercial OR PSA)","tag":"Legacy Campaign"},
                {"value":"TheGabbieShow (ad OR commercial OR PSA)","tag":"Legacy Campaign"},
                {"value":"christiand (ad OR commercial OR PSA)","tag":"Legacy Campaign"},
                {"value":"purpdrank (ad OR commercial OR PSA)","tag":"Legacy Campaign"},
                {"value":"rclbeauty101 (ad OR commercial OR PSA)","tag":"Legacy Campaign"}
            ]
        sdate = '2014-07-01T05:00:00.000Z'
        edate = '2014-07-11T05:00:00.000Z'
        class mydict(dict):
            pass
        request = mydict()
        request.json_body = {"filename":"testfile.json","data":data,"startdate":sdate,"enddate":edate, "debug":"remote"}
        
        result = submitJob(request)
    elif sys.argv[1] == 'check':
        print "Checking jobs"
        result = webcheckJobs('')
        print json.dumps(result)