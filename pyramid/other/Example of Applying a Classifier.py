# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 15:17:21 2015

@author: htran31
"""

from sklearn.externals import joblib #For importing classifier built with sci-kit
import pandas as pd 

''' Loading a pre-trained classifier '''
TobEcigClassifier = joblib.load('H:\Data\Projects\ANALYSES\Twitter_and_SmokingPrev\Data Preparation\TobEcig Relevant Classifier\TobEcig Relevant Classifier (sklearn joblib).pkl')
''' 
    This is a simple text classifier built for predicting whether a tweet body is relevant to Tobacco or E-cig or not
    Expected input: a list of text, a numpy array of text, or a panda series of text
    Expected output: an array of classified result (True or False)
'''

''' Simple example of applying classifier with some toy data '''
example_text = [{'id':1,'Text':'I like to smoke a cig'},
                     {'id':2,'Text':'People who smoke weed are gross'}]

''' Try classifier on a list of text '''
#Example with single text
print TobEcigClassifier.predict('I like to smoke a cig') #Wrong usage: print a list of False
print TobEcigClassifier.predict(['I like to smoke a cig'])  #Right usage: print True
#Example with list of text
example_text_list = ['I like to smoke a cig','People who smoke weed are gross']
print TobEcigClassifier.predict(example_text_list) #Print: [True, False]
#Example with list of dictionary containing text
result_1 = []
for d in example_text:
    d['classified'] = TobEcigClassifier.predict([d['Text']])[0]
    result_1.append(d)
print result_1

''' Try classifier on a pandas dataframe '''
#Create a dataframe source
example_df = pd.DataFrame(example_text)
#Give the text column of the dataframe to the classfier, store result in another column
example_df['classified'] = TobEcigClassifier.predict(example_df['Text'])
print example_df

''' Example Pipeline for applying a pre-trained classifier '''

''' 1. Load data into a dictionary or pandas dataframe from a source
    Here the source is a csv file, to be replaced with a source given by MongoDB or Pyramid
'''
data_source_handle = r'H:\Data\Projects\ANALYSES\Twitter_and_SmokingPrev\Data\Cessation Drug.csv'
key_of_text_variable = 'Text'
name_of_classified_variable = 'PRED_TobaccoEcigRevlevance'
data_save_handle = r'C:\Temp\result.csv'

data_to_classify = pd.read_csv(data_source_handle, encoding='utf-8', low_memory=False)

''' 2. Load classifier 
Classifier = joblib.load('Stored path to classifier pkl file')
'''
text_classifier = joblib.load('H:\Data\Projects\ANALYSES\Twitter_and_SmokingPrev\Data Preparation\TobEcig Relevant Classifier\TobEcig Relevant Classifier (sklearn joblib).pkl')

''' 3. Create a new field storing the classification result '''
data_to_classify[name_of_classified_variable] = text_classifier.predict(data_to_classify[key_of_text_variable])

''' 4. Save result to somewhere '''
data_to_classify.to_csv(data_save_handle, encoding='utf-8', index=False)