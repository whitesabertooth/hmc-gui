########################################
# Setup gui and db children
# Setup reverse proxy to Pyramid port
from twisted.internet import reactor
from twisted.web import static, server, proxy

print "Starting Web Server on http://localhost:8000/gui/"
print " and pointing to the Pyramid port 8001 at http://localhost:8000/db/"
print " This is how the server is setup."
print "starting...",
root = static.File(".")
root.putChild("gui", static.File(".\\static"))
root.putChild("db", proxy.ReverseProxyResource('localhost', 8001, ''))
reactor.listenTCP(8000, server.Site(root))
print "done"
reactor.run()

