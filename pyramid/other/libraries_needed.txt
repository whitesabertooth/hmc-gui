All libraries for win32 came from:
http://www.lfd.uci.edu/~gohlke/pythonlibs/
Files already at H:\DATA\TECHTEAM\davidsabin\pylibs\
Also download latest pyLDavis at https://github.com/bmabey/pyLDAvis

Install Microsoft Visual C++ for Python 2.7.

Topic Model:
1. pip install numpy-1.10.4+mkl-cp27-none-win_amd64.whl
2. pip install scipy-0.17.0-cp27-none-win_amd64.whl 
3. pip install numexpr-2.6.1-cp27-cp27m-win_amd64.whl
4. pip install nltk
5. pip install gensim
6. pip install matplotlib-1.5.2-cp27-cp27m-win_amd64.whl
6. pip install "scikit-bio==0.4.2"
7. pip install pyLDAvis-master.zip
8. python -c 'import nltk; nltk.download()'  and click download button on GUI (this takes a while)

Classifier:
0. NEED numpy and scipy from above
1. pip install scikit_learn-0.17-cp27-none-win_amd64.whl
2. pip install pandas

