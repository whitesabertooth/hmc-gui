# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 16:24:58 2015

@author: htran31
"""

from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.cross_validation import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.cross_validation import cross_val_score
from sklearn.externals import joblib
import pandas as pd

''' Simple of example of training a classifier using default setting
    Expected input: 
        A dataframe containing text body and label
        A classifier name
        A path to save the classifier
        (name of the text and label key)
    Expected output: 
        Cross-validation and testing output
        A series of PKL object saved in the designated path
'''

label_data_source = r'J:\Projects\ANALYSES\Legacy\phase2\Data\combined label.xlsx'
classifier_name = 'Example Classifier'
classifier_save_location = r'C:\Temp' + '\\' + classifier_name + '.pkl'

key_for_text = 'bodypost'
key_for_label = 'LABEL_Relevant(Yes, No)'

label_df = pd.read_excel(label_data_source, encoding='utf-8')

test_size = 0.2
k_fold = 5

''' Dropping duplicated text (not necessary if data was processed properly in a previous step) '''
label_df = label_df.drop_duplicates(key_for_text, 0)

''' Setting up train - test '''
corpus = (label_df[key_for_text])
y = (label_df[key_for_label])
text_train, text_test, y_train, y_test = train_test_split(corpus, y, test_size=test_size, random_state=44)

sgd_n_iter = round(10**6/len(y_train))
''' Setting up Pipeline '''
pipeline = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', SGDClassifier(n_iter = sgd_n_iter)),
])

''' Fit model '''
text_clf = pipeline.fit(text_train, y_train)

''' Cross Validation Accuracy '''
score = cross_val_score(text_clf, corpus, y, cv = k_fold)
print "CV scores:"
print score 
print "Average CV score: %.4f - CV Standard Dev: %.4f" % (score.mean(), score.std())

''' Prediction on Test '''
y_pred = text_clf.predict(text_test)
''' Precision and Recall '''
testing_report = classification_report(y_test, y_pred)
print testing_report

''' Save Model '''
joblib.dump(text_clf, classifier_save_location)
