
from pymongo import MongoClient
from datetime import datetime
import re
import json
import pprint
import os
import csv
import unicodedata
import random



def addBlanks(aline,tw_mongo2fields,modifiedObj,listok):
    # Add blank elements if not exist
    for (key, val) in tw_mongo2fields.iteritems():
        if key in aline:
            # Account for unknown array fields should be strings
            if isinstance(aline[key],list) and val not in listok:
                modifiedObj[val] = str(aline[key])                        
            else:
                modifiedObj[val] = aline[key]
        else:
            modifiedObj[val] = None
    return modifiedObj

def convertData(modifiedObj,max_tags,entitiesusrmentions_max,tw_invertedTags,tw_invertedRules,max_rules):
    # Add extra fields for Posted Time
    if 'postedTime' in modifiedObj:
        date = modifiedObj['postedTime']
        modifiedObj['Year'] = date.strftime("%Y")
        modifiedObj['Month'] = date.strftime("%b")
        modifiedObj['Day'] = date.strftime("%d")
        modifiedObj['Time'] = date.strftime("%H:%M:%S")
        modifiedObj['postedTime'] = str(modifiedObj['postedTime'])# Convert datetime field to string
    # If actorutcOffset is None, then initialize field with a '.'
    if modifiedObj['actorutcOffset'] is None:
        modifiedObj['actorutcOffset'] = '.'
    # Unroll all the arrays
    index = 1
    if 'entitieshtagstext' in modifiedObj:
        #Fix for when it is not an array
        if isinstance(modifiedObj['entitieshtagstext'],list):
            for i in modifiedObj['entitieshtagstext']:
                modifiedObj['entitieshtagstext' + str(index).zfill(2)] = i
                index += 1
        else:
            modifiedObj['entitieshtagstext' + str(index).zfill(2)] = modifiedObj['entitieshtagstext']
        # Add extra columns to max
        for i in range(index,max_tags+1):
            modifiedObj['entitieshtagstext' + str(i).zfill(2)] = None
        del(modifiedObj['entitieshtagstext'])
        index = 1
    if 'entitiesusrmentions' in modifiedObj:
        for i in modifiedObj['entitiesusrmentions']:
            modifiedObj['entitiesusrmentionsidstr' + str(index).zfill(2)] = i['is']
            modifiedObj['entitiesusrmentionsname' + str(index).zfill(2)] = i['n']
            modifiedObj['entitiesusrmentionssname' + str(index).zfill(2)] = i['sn']
            index += 1
        # Add extra columns to max
        for i in range(index,entitiesusrmentions_max+1):
            modifiedObj['entitiesusrmentionsidstr' + str(i).zfill(2)] = None
            modifiedObj['entitiesusrmentionsname' + str(i).zfill(2)] = None
            modifiedObj['entitiesusrmentionssname' + str(i).zfill(2)] = None
        del(modifiedObj['entitiesusrmentions'])
        index = 1
    # Translated and unroll matchingrulestag array
    if 'matchingrulestag' in modifiedObj:
        for i in modifiedObj['matchingrulestag']:
            tag = tw_invertedTags[int(i)]
            modifiedObj['matchingrulestag' + str(index).zfill(2)] = tag
            index += 1
        # Add extra columns to max
        for i in range(index,max_tags+1):
            modifiedObj['matchingrulestag' + str(i).zfill(2)] = None
        del(modifiedObj['matchingrulestag'])
        index = 1
    # Unroll matchingrulesvalue array and create a new field to hold all the translated matchingrulesvalues
    if 'matchingrulesvalue' in modifiedObj:
        translatedRules = []
        for i in modifiedObj['matchingrulesvalue']:
            modifiedObj['matchingrulesvalue' + str(index).zfill(2)] = i
            index += 1
            try:
                rule = tw_invertedRules[int(i)]
            except KeyError:
                continue
            # Add extra columns to max
            translatedRules.append(rule)
        modifiedObj['matchingrulesvalues'] = ';'.join(translatedRules)
        for i in range(index,max_rules+1):
            modifiedObj['matchingrulesvalue' + str(i).zfill(2)] = None
        del(modifiedObj['matchingrulesvalue'])
        index = 1
    return modifiedObj

def createSortOrder(modifiedObj):
    listorder = modifiedObj.keys()
    listorder.sort()
    return listorder


def sortList(listorder,modifiedObj):
    # Sort the list as convert to tabular format
    listdata = []
    for val in listorder:
        listdata.append(modifiedObj[val])
    return listdata

def saveToFile(filename,MAX_FILE_SIZE,finalData,startidx):
    # Verify file size is less than max (don't allow run-away script)
    issuccess = 'false'
    if os.path.getsize(filename) < MAX_FILE_SIZE:
        with open(filename, 'ab') as csvfile:
            writer = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
            for row in finalData[startidx:]:
                modifiedrow = []
                for s in row:
                    if type(s) == unicode:
                        modifiedrow.append(s.encode("ascii",'ignore'))
                    else:
                        modifiedrow.append(s)
                writer.writerow(modifiedrow)
        issuccess = 'true'
    return issuccess

# from pyramid.view import view_config
# @view_config(route_name='home', renderer='templates/mytemplate.pt')
# def my_view(request):
#     return {'project': 'alpha'}

# @view_config(route_name='home_db', renderer="json")
def mongodb(request):
    # Perform Mongo transation

    host='10.177.54.27'
    port=27017
    username='HMCDev'
    password='KaThC0k3'
    authDB='admin';
    mongoClient = MongoClient(host, port)
    mongoClient.twitter.authenticate(username, password, source=authDB)
    db = mongoClient['playground']
    max_rules = 20
    max_tags = 10
    entitiesusrmentions_max = 20
    fileid = '0'
    MAX_FILE_SIZE = 3000000000 # About 3 gigs
    MAX_SAMPLE_SIZE = 100000
    MIN_SAMPLE_SIZE = 1


    #load input as json
    myjson = request.json_body
    # Request data 
    #request = {
    #   "query":tempquery,
    #   "collection":collectionRanges[collectionGroup]['range'][collectionInc],
    #   "increment":increment,
    #   "step":step,
    #   "sample":needSampleSize,      is a number needed, or a percent needed
    #   "sampletype":sampleType,      "percent" or "number"
    #   "samplemethod":sampleMethod,  "first" or "random"
    #   "fileid":window.fileid,
    #   "totalsize":0,
    #   "savedata":saveQuery,         True = save to csv, False = do not save
    #   "createSample":!delaySample,  True = return a sample, if performquery = True then perform just on query
    #   "performquery":true           True = perform query
    #   "data":false                  True if getting data, false if just count
    #};

    #Request sample at end
    #request = {
    #   "query":{},
    #   "collection":'n/a',
    #   "increment":0,
    #   "step":0,
    #   "sample":sampleSize,
    #   "sampletype":sampleType,
    #   "samplemethod":sampleMethod,
    #   "fileid":window.fileid,
    #   "totalsize":totalCount,       Total size of csv file
    #   "savedata":false,           
    #   "createSample":delaySample,
    #   "performquery":false
    #   "data":false                  True if getting data, false if just count
    #};

    if 'debug' in myjson.keys():
        debug = myjson['debug']
    else:
        debug = False

    mongo2fields_file = 'tw_mongoToFields.json'
    rulesMap_file = 'tw_rules.json'
    tagsMap_file = 'tw_tags.json'
    dirname = 'static/tempdata/'
    linkpath = '/static/tempdata/'
    if not debug:
        mongo2fields_file = 'alpha/' + mongo2fields_file
        rulesMap_file = 'alpha/' + rulesMap_file
        tagsMap_file = 'alpha/' + tagsMap_file
        dirname = 'alpha/' + dirname
    tw_mongo2fields = json.load(open(mongo2fields_file,'r'))
    tw_rules = json.load(open(rulesMap_file,'r'))
    tw_invertedRules = {v: k for k, v in tw_rules.items()}
    tw_tags = json.load(open(tagsMap_file,'r'))
    tw_invertedTags = {v: k for k, v in tw_tags.items()}
    
    #Add hints to improve performance
    hintstr = []

    #Convert times to python times
    dateField = 'pt'
    queryjson = myjson['query'];
    if dateField in queryjson.keys():
        if '$gte' in queryjson[dateField].keys():
            s=queryjson[dateField]['$gte']
            d=datetime(*map(int, re.split('[^\d]', s)[:-1]))#Convert from Zulu String to datetime
            queryjson[dateField]['$gte'] = d
        if '$lte' in queryjson[dateField].keys():
            s=queryjson[dateField]['$lte']
            d=datetime(*map(int, re.split('[^\d]', s)[:-1]))#Convert from Zulu String to datetime
            queryjson[dateField]['$lte'] = d


    listok = ['postedTime', 'entitieshtagstext', 'entitiesusrmentions', 'matchingrulestag', 'matchingrulesvalue', 'is', 'n', 'sn']
    totalcount = 0
    countdata = 0
    # For each of the collections
    coll = db[myjson['collection']]
    finalData = []
    outData = []
    setupHeader = False
    filename = ''
    issuccess = 'false'
    
    # This is the final Query, so do sample
    if not myjson['performquery'] and myjson['createSample'] and myjson['sample'] > 0 and myjson['totalsize'] > 0 and 'fileid' in myjson and myjson['fileid'].isdigit() and len(myjson['fileid']) == 8:
        # If is a legitimate number then do not add header line
        fileid = myjson['fileid']
        filename = dirname + fileid + '.csv'
        linkpath = linkpath + fileid + '.csv'
        mysample = myjson['sample']
        mytotal = myjson['totalsize']

        # Calculate percent if needed
        if myjson['sampletype'] == 'percent':
            mysample = int(mysample/100.0 * mytotal) #round down

        # Cap the sampling at the total size
        if mytotal < mysample:
            mysample = mytotal

        if os.path.isfile(filename):
            # Verify file size is less than max (don't allow run-away script)
            if os.path.getsize(filename) < MAX_FILE_SIZE:
                with open(filename, 'rb') as csvfile:
                    myreader = csv.reader(csvfile)
                    countdata = mysample
                    if myjson['samplemethod'] == 'first':
                        outputselection = xrange(mysample) #creates 0-(n-1) array
                    elif myjson['samplemethod'] == 'random':
                        #RANDOM SELECTION
                        random.seed() #use time
                        outputselection = random.sample(xrange(mytotal),mysample)
                    else:
                        outputselection = []
                    idx = 0
                    notHeaderRow = False
                    for row in myreader:
                        if notHeaderRow:
                            if idx in outputselection:
                                outData.append(row)
                            idx += 1
                        else:
                            # Header row
                            notHeaderRow = True
                            outData.append(row)
                issuccess = 'true'



    # Query to find all records of a particular rule
    if myjson['data'] and myjson['performquery']:
        # Get actual data
        # if(queryjson and hintstr):
        #     actualData = coll.find(queryjson,no_cursor_timeout=True).hint(hintstr).skip(myjson['increment']*myjson['step']).limit(myjson['increment'])
        # elif(not queryjson and hintstr):
        #     actualData = coll.find(no_cursor_timeout=True).hint(hintstr).skip(myjson['increment']*myjson['step']).limit(myjson['increment'])
        # elif(queryjson and not hintstr):
        #     actualData = coll.find(queryjson,no_cursor_timeout=True).skip(myjson['increment']*myjson['step']).limit(myjson['increment'])
        # elif(not queryjson and not hintstr):
        #     actualData = coll.find(no_cursor_timeout=True).skip(myjson['increment']*myjson['step']).limit(myjson['increment'])
        #countdata = actualData.count()
        # incrementing
        if(queryjson):
            actualData = coll.find(queryjson,no_cursor_timeout=True).skip(myjson['increment']*myjson['step']).limit(myjson['increment'])
        else:
            actualData = coll.find(no_cursor_timeout=True).skip(myjson['increment']*myjson['step']).limit(myjson['increment'])
        
        if debug:
            print "processed"
        numCount = 0
        printFirst = True
        for aline in actualData:
            numCount = numCount+1
            #Convert from Mongo to Field names
            modifiedObj = {}
            # Translate field names
            
            modifiedObj = addBlanks(aline,tw_mongo2fields,modifiedObj,listok)
            
            
        
            modifiedObj = convertData(modifiedObj,max_tags,entitiesusrmentions_max,tw_invertedTags,tw_invertedRules,max_rules)
            
            if not setupHeader:
                setupHeader = True
                listorder = createSortOrder(modifiedObj)
                # Add header row to tabular data
                # Sort the list as convert to tabular format
                if debug:
                    print listorder
                finalData.append(listorder)
                #Save sample if needed
                if myjson['samplemethod'] == 'first' and myjson['sampletype'] == 'number' and myjson['createSample'] and numCount <= myjson['sample']:
                    outData.append(listorder)
                
            listdata = sortList(listorder,modifiedObj)
                
            if debug and printFirst:
                pprint.pprint(modifiedObj)
                pprint.pprint(listdata)
                printFirst = False
            finalData.append(listdata)

            #Save sample if needed
            if myjson['samplemethod'] == 'first' and myjson['sampletype'] == 'number' and myjson['createSample'] and numCount <= myjson['sample']:
                outData.append(listdata)
            
        countdata = numCount
        if myjson['savedata']:
            if 'fileid' in myjson.keys():
                
                # make sure all characters are digits and that it is a length of 8
                if not myjson['fileid'].isdigit() or len(myjson['fileid']) != 8:
                    fileid = str(random.randint(10000000,99999999))
                    filename = dirname + fileid + '.csv'
                    linkpath = linkpath + fileid + '.csv'
                    startidx = 0
                    # Delete if exists
                    if os.path.isfile(filename):
                        os.unlink(filename)
                    if not os.path.exists(dirname): #make directory if not exists
                        os.makedirs(dirname)
                    open(filename,'w+').close() # touch the file
                else:
                    # If is a legitimate number then do not add header line
                    fileid = myjson['fileid']
                    filename = dirname + fileid + '.csv'
                    linkpath = linkpath + fileid + '.csv'
                    startidx = 1
                    if debug:
                        print "Appending"
                
                if os.path.isfile(filename):
                    issuccess = saveToFile(filename,MAX_FILE_SIZE,finalData,startidx)
        else:
            issuccess = 'true'


    elif myjson['performquery']:
        # Get count of data
        #Handle if hint is there or not and if query is there or not
        if(queryjson and hintstr):
            countdata = coll.find(queryjson,no_cursor_timeout=True).hint(hintstr).skip(myjson['increment']*myjson['step']).limit(myjson['increment']).count(with_limit_and_skip=False)
        elif(not queryjson and hintstr):
            countdata = coll.find(no_cursor_timeout=True).hint(hintstr).skip(myjson['increment']*myjson['step']).limit(myjson['increment']).count(with_limit_and_skip=False)
        elif(queryjson and not hintstr):
            countdata = coll.find(queryjson,no_cursor_timeout=True).skip(myjson['increment']*myjson['step']).limit(myjson['increment']).count(with_limit_and_skip=False)
        elif(not queryjson and not hintstr):
            countdata = coll.find(no_cursor_timeout=True).skip(myjson['increment']*myjson['step']).limit(myjson['increment']).count(with_limit_and_skip=False)
        issuccess = 'true'
    totalcount = totalcount + countdata
        
    result = {"success":issuccess, "totalcount" : totalcount, "data":outData, "location":linkpath, "fileid":fileid};
    return result

def shescape(s):
    return s.replace('\\','\\\\').replace("(","\\(").replace(")","\\)").replace(" ","\\ ").replace('"','\\"')

if __name__ == '__main__':
    class mydict(dict):
        pass
    request = mydict()
    tempquery = {
     'mrt':{'$in': [7]},
     'pt':{'$gte': '2014-07-01T05:00:00.000Z',
           '$lte': '2014-07-17T04:59:59.999Z'}      
    }
     
    request.json_body = {
      "query":tempquery,
      "collection":'201407',
      "increment":100000,
      "step":0,
      "sample":False,      #is a number needed, or a percent needed
      "sampletype":"number",      #"percent" or "number"
      "samplemethod":"first",  #"first" or "random"
      "fileid":"0",
      "totalsize":0,
      "savedata":True,         #True = save to csv, False = do not save
      "createSample":False,  #True = return a sample, if performquery = True then perform just on query
      "performquery":True,           #True = perform query
      "data":True,                  #True if getting data, false if just count
      "debug":True
    };

    
    result = mongodb(request)
    print 'file location: ' + result['location']
    print 'total count: ' + str(result['totalcount'])
    print 'fileid: ' + result['fileid']
