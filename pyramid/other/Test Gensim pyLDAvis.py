# -*- coding: utf-8 -*-
"""
@author: htran31
"""

import sys #used for importing twokenize, which is saved in the I drive
sys.path.append('I:\group\YKim\Tran\Legacy Campaign\Python Resources')
import twokenize #from https://github.com/myleott/ark-twokenize-py

import unicodecsv #more robust importing of csv files with unicode
import nltk #mainly used for the stopwords, to be trimmed down to import stopwords only
from gensim import corpora, models #main modeling pieces
import pyLDAvis.gensim #for transforming gensim models into visual data 
from pyLDAvis import _display  #for saving the prepared data as JSON 

''' Parameters to be received '''
text_file_handle = 'H:\Data\ANALYSES\Twitter_and_SmokingPrev\Data\Anti Smoking.csv'
json_save_handle = r'C:\Users\htran31\Desktop\vis\lda.json'
model_save_handle = r'C:\Users\htran31\Desktop\vis\model.lda'
dict_save_handle = r'C:\Users\htran31\Desktop\vis\dictionary.dict'
corpus_save_handle = r'C:\Users\htran31\Desktop\vis\corpus.mm'
number_of_topic = 50

text_key = 'Text'

''' Future parameters '''
custom_stoplist=['rt']
min_token_len = 2

''' Reading in raw text documents'''
doc=[]
with open(text_file_handle,'r') as csvfile:
    csvreader = unicodecsv.DictReader(csvfile, delimiter=',', quotechar='"')
    for row in csvreader:
        #Importing text only 
        doc.append(row[text_key])
    csvfile.close()

''' Tokenizing '''
filter_sent = []
for t in doc:
    sent = twokenize.tokenizeRawTweetText(t)
    #Simple text processing: to lower case, remove stopwords
    filter_sent.append([w.lower() for w in sent if not w.lower() in 
            nltk.corpus.stopwords.words('english') and not len(w) < min_token_len
            and not w.lower() in custom_stoplist]
            )

''' Modeling '''          
dictionary = corpora.Dictionary(filter_sent)
corpus = [dictionary.doc2bow(text) for text in filter_sent]
tfidf = models.TfidfModel(corpus)
corpus_tfidf = tfidf[corpus]
model = models.ldamodel.LdaModel(corpus_tfidf, id2word=dictionary, num_topics=number_of_topic, alpha='auto', eval_every=5)

''' Create the visual data '''
visual_data = pyLDAvis.gensim.prepare(model, corpus_tfidf, dictionary, n_jobs=1)
''' Save visual data as a JSON file '''
_display.save_json(visual_data, json_save_handle)
''' Save the model from topic modeling '''
model.save(model_save_handle)
dictionary.save(dict_save_handle)
corpora.MmCorpus.serialize(corpus_save_handle, corpus)

''' Launching web display '''
_display.show(visual_data)




