__author__ = 'swang207'

from pymongo import MongoClient
import urllib, urllib2
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

client = MongoClient('cu580-2.ihrp.uic.edu',27017)
db = client['gnip_instagram']


collection_name = "instagram_for_labeling_may_2015"

print "start extracting from database: ", collection_name

# cur = db['instagram_for_labeling'].find({},{'object':1,'published':1,'matching_rules':1},timeout=False) # query
cur = db[collection_name].find({},{'object':1,'published':1,'matching_rules':1},timeout=False) # query
# cur = db['instagram_for_labeling'].find({},{'object':1,'published':1,'matching_rules':1},timeout=False).limit(10) # query testing

http_error_cnt = 0
url_error_cnt = 0
len_error_cnt = 0
not_jpeg_error_cnt = 0
success_cnt = 0
unknown_error_cnt = 0

all_cnt = 0
msg_lag = 1000

image_path = "image_2"
data_path = "data_2"
index_path = "index_2"

index_list = []

data_file = open(data_path+'/data.txt','w')



for e in cur:
    all_cnt+=1
    if all_cnt % msg_lag ==0:
        print all_cnt

    try: #for handering the error like KeywordError "e[object]"
        if e['object']['object-type'] == 'http://activitystrea.ms/schema/1.0/image':
            if 'content' in e['object']:
                # print e['object']

                if len(e['object']['link']):
                    # print e['object']['link'][2]['href']
                    # save the imgage
                    # urllib.urlretrieve(e['object']['link'][2]['href'],e['object']['id']+".jpg")
                    url = e['object']['link'][2]['href']
                    try:
                        response = urllib2.urlopen(urllib2.Request(url))
                        '''
                        Note The except HTTPError must come first, otherwise except URLError will also catch an HTTPError.
                        '''
                    except urllib2.HTTPError as e: # server exist, but the request is not satisfied, e.g., the file does not exist
                        # print 'the server couldn\'t fulfill the reqeust'
                        # print 'error code: ', e.code
                        http_error_cnt +=1
                    except urllib2.URLError as e: # the server is not existed, or down
                        # print 'We fialed to reach a server.'
                        # print 'Reason: ', e.reason
                        url_error_cnt +=1
                    else:
                        maintype_num = len(response.headers['Content-Type'].split(';'))
                        maintype= response.headers['Content-Type'].split(';')[0].lower()
                        # print "full header: "+ response.headers['Content-Type']
                        # print maintype
                        # print maintype_num
                        if maintype != "image/jpeg":
                            not_jpeg_error_cnt+=1
                        else:
                            if maintype_num!=1:
                                len_error_cnt+=1
                            else:
                                # handle unknown error
                                try:
                                    one_ele_str = ''

                                    #index
                                    id = e['object']['id']
                                    index_list.append(id)
                                    one_ele_str = one_ele_str + str(len(index_list)) + '\n'

                                    # save the image
                                    urllib.urlretrieve(e['object']['link'][2]['href'], image_path + "/"+ str(len(index_list))+ ".jpg")

                                    # save the text/content
                                    text = e['object']['content']
                                    one_ele_str = one_ele_str + text + '\n'

                                    #save the category
                                    cate = e['object']['category']
                                    if isinstance(cate, list): # can be a list or a string
                                        cate_str_list = [one_term['term'] for one_term in cate]
                                    else:
                                        cate_str_list = cate

                                    one_ele_str = one_ele_str + ' '.join(cate_str_list) + '\n'

                                    # save the matching rules
                                    rule = e['matching_rules']['matching_rule']
                                    if isinstance(rule, list):# can be a list or a string
                                        one_ele_str = one_ele_str + ' '.join(rule) + '\n'
                                    else:
                                        one_ele_str = one_ele_str + rule + '\n'


                                    #published time
                                    published_time = e['published']
                                    one_ele_str = one_ele_str + published_time + '\n'

                                    data_file.write(one_ele_str)
                                    data_file.flush() # cache bug fixed
                                    success_cnt +=1
                                except:
                                    unknown_error_cnt +=1
                                    pass
    except:
        unknown_error_cnt +=1 # should be an error around 29000
        pass

data_file.flush() # cache bug fixed
data_file.close()

with open(index_path+'/index.txt','w') as index_file:
    print "size: ", len(index_list)
    for index in index_list:
        index_file.write(index+"\n")


print "http_error_cnt: " + str(http_error_cnt)
print "url_error_cnt: " +  str(url_error_cnt)
print "len_error_cnt: " + str(len_error_cnt)
print "not_jpeg_error_cnt " + str(not_jpeg_error_cnt)
print "success_cnt: " +  str(success_cnt)
print "unknown_error_cnt: " + str(unknown_error_cnt)

