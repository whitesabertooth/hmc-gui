# -*- coding: utf-8 -*-
"""
Extracting Ngrams from csv file
Author: Hy Tran
"""
import sys
sys.path.append('I:\group\YKim\Tran\Legacy Campaign\Python Resources')
import twokenize

from nltk.corpus import stopwords 
import unicodecsv
from collections import Counter

''' Baisc parameters '''
raw_doc_handle = 'I:\group\YKim\Tran\Legacy Campaign\Phase 1 Cleaned 1-15-15 dedup.csv'
text_field = 'body'
min_gram = 1
max_gram = 4
output_file_folder = 'I:\group\YKim\Tran\Legacy Campaign'

''' Read in raw csv file '''
filter_sent = []
with open(raw_doc_handle,'r') as csvfile:
    csvreader = unicodecsv.DictReader(csvfile, delimiter=',', quotechar='"')
    for row in csvreader:
        #Importing text only (column titled 'body') for ngrams
        sent = twokenize.tokenizeRawTweetText(row[text_field])
        #Simple text processing: to lower case, remove stopwords
        filter_sent.append([w.lower() for w in sent if not w in 
            stopwords.words('english') and not len(w) < 2])
    csvfile.close()


def zipgram(wordlist,n=2):
    return zip(*[wordlist[i:] for i in range(n)])

''' Collect ngrams frequencies '''
ngrams = {}
for n in xrange(min_gram, max_gram + 1):
    gram_counter=Counter()
    for s in filter_sent:
        for g in zipgram(s,n):
            gram_counter[g]+=1
    ngrams[n] = gram_counter

''' Write into csv '''
for n in xrange(min_gram, max_gram + 1):
    filename = output_file_folder + '\\ngrams' +str(n) + '.csv' 
    with open(filename, 'wb') as f:  # Just use 'w' mode in 3.x
       writer=unicodecsv.writer(f, delimiter=",", lineterminator="\r\n") 
       writer.writerows(ngrams[n].most_common())
       